#!/bin/sh
dir=$1
if [ ! -d $dir ]; then
    echo 'error: output must be a directory'
    exit -1
fi

nohup ~/util/boost.sh 20 2>&1 >/dev/null &
cd $dir

rm -fv orig_*.png

for f in `ls -1 *.png *.jpg`; do
    echo "converting $f"
    newfn=`echo -n $f | sed -e "s:\.png:\.dds:" -e "s:\.jpg:\.dds:"`
    convert -flip -define dds:compression=dxt1 -define dds:cluster-fit=true -define dds:mipmaps=false $f $newfn
done

sed -i -e "s:\.png:\.dds:g" -e "s:\.jpg:\.dds:g" *.dae

rm -fv *.png *.jpg

echo "pack finished"
