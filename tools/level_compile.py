#!/usr/bin/env python
## post-compile and post-process level files
import sys, os
from PIL import Image, ImageChops, ImageEnhance, ImageOps

output = sys.argv[1]
output_dir = os.path.dirname(output)
print __file__
print 'using output_dir:', output_dir

opt_add_ao = False
opt_texture_downsample = 0 # 2
opt_normalmap_downsample = 0 # 4
opt_process_normalmap = True

def add_ao(texmap, ao):
    dir = output_dir
    im1 = Image.open(os.path.join(dir, ao)).convert('RGB')
    im2 = Image.open(os.path.join(dir, texmap)).convert('RGB')

    # backup original texmap
    im2.save(os.path.join(dir, 'orig_' + texmap))

    #enhancer = ImageEnhance.Brightness(im1)
    #im1 = enhancer.enhance(1.1)

    # calculates color levels
    def level(color, black, mid, white):
        if color < black:
            return 0
        elif color < mid:
            width = float(mid - black)
            stepSize = width / 128.0
            return (color - black) / stepSize
        elif color < white:
            width = (white - mid)
            stepSize = width / 128.0
            return 128 + (color - mid) / stepSize
        return 255
    

    #im1.save('d:/temp/1.bmp')
    im1 = im1.convert('L')
    pix = im1.load()
    sx, sy = im1.size
    for y in xrange(sy):
        for x in xrange(sx):
            p = pix[x, y]
            #pix[x, y] = max(100, p)
            n = 40 # minimal black value/level
            pix[x, y] = level(p, n, 127-n, 255)

    im1 = im1.convert('RGB')
    #im1.save('d:/temp/2.bmp')

    out = ImageChops.multiply(im1, im2)
    out.save(os.path.join(dir, texmap))

def process_normalmap(fn):
    # must be before texture downsampling
    f = os.path.join(output_dir, fn)
    im = Image.open(f)
    if im.mode == 'RGBA':
        im2 = Image.new('RGBA', size=im.size, color=(127, 127, 255, 255))
        im = Image.alpha_composite(im2, im)
        im = im.convert('RGB')
        im.save(f)

def texture_downsample(fn, k):
    f = os.path.join(output_dir, fn)
    im = Image.open(f)
    im.save(os.path.join(output_dir, 'orig_scale_' +  fn))
    im = im.resize((im.size[0]/k, im.size[1]/k), Image.ANTIALIAS)
    im.save(f)


def main():
    #os.chdir(output_dir)
    for fn in os.listdir(output_dir):
        texmap = ao = normalmap = None
        if fn.endswith('texmap.png') and not fn.startswith('orig_'):
            texmap = fn
        else:
            continue

        aofn = fn.replace('texmap', 'ao')
        if os.path.isfile(os.path.join(output_dir, aofn)):
            ao = aofn

        nmfn = fn.replace('texmap', 'normalmap')
        if os.path.isfile(os.path.join(output_dir, nmfn)):
            normalmap = nmfn

        print 'processing: ', fn

        if opt_add_ao:
            add_ao(texmap, ao)

        if opt_process_normalmap:
            process_normalmap(normalmap)
        
        if opt_texture_downsample:
            texture_downsample(texmap, opt_texture_downsample)

        if opt_normalmap_downsample:
            texture_downsample(normalmap, opt_normalmap_downsample)

    print "done"

main()
