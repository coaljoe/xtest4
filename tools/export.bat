set blender="C:\Program Files\Blender Foundation\Blender\blender.exe"
set python="c:\Python27\python.exe"
set tmp="d:\temp"

%blender% %1 -b -P script_level_baker.py -- "%2"
%python% level_compile.py "%2"

@echo errlev = %ERRORLEVEL%

@pause
