#!/usr/bin/env python2
import pstats, sys
#print sys.argv[1]
p = pstats.Stats(sys.argv[1])

#p.strip_dirs().sort_stats(-1).print_stats()
p.sort_stats('time', 'name').print_stats()

#py dump_profile.py | sed -n '1,50p'
