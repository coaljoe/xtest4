#!/bin/sh
file=$2
if [ -d $file ]; then
    echo 'error: output must be a file, not directory'
    exit -1
fi

nohup ~/util/boost.sh 30 2>&1 >/dev/null &
#ulimit -Sv 1500000 # limit memory XXX not working

nice -n 19 blender $1 -b -P script_level_baker.py -- "$file" && 
nice -n 19 python2 level_compile.py "$file"

notify-send "export finished"
