#!/usr/bin/env python
# usage: blender dirtmap.blend -b -P script_level_baker.py -- "/tmp/ex.dae"
from __future__ import absolute_import, division
import bpy, bmesh, os, sys, tempfile
from mathutils import Vector
C = bpy.context; D = bpy.data

# get output .dae file path
try:
    idx = sys.argv.index('--')
    output = sys.argv[idx+1]
    #print(output)
except (IndexError, ValueError):
    output = '/tmp/ex.dae'
print(__file__)
print('output:', output)

opt_draft = True
def draft(v1,v2):
    return v1 if not opt_draft else v2
def quit(code=0):
    print("quit...")
    #bpy.ops.wm.quit_blender()
    sys.exit(code)
def save_tmp_file(fn):
    print("saving tmp file " + fn)
    bpy.ops.wm.save_as_mainfile(filepath=os.path.join(tempfile.gettempdir(), fn), copy=True, compress=False)
def in_rect(x, y, rx, ry, rw, rh):
    return True if x >= rx and x <= rx + rw and y >= ry and y <= ry + rh else False
def scale_image(img, sx, sy):
    #orig_path = str(img.filepath)
    path = "/tmp/_scale.png"
    try: os.remove(path)
    except: pass
    img.filepath = path
    img.save()
    os.system("convert -scale %sx%s %s %s" % (int(sx), int(sy), path, path)) # or extent
    img.reload()
    #img.filepath = orig_path

## options
opt_add_ao = False
opt_texture_size = 1024
opt_normal_map_size = 256
opt_split = True
opt_remove_downfacing_faces = True

# map setting
map_sx = 80 # 250
map_sy = 80 # 200
map_cell_size = 20

# ao light settings
ls = bpy.data.worlds['World.001'].light_settings
ls.distance = 2.0
ls.falloff_strength = 16.0
ls.sample_method = draft('ADAPTIVE_QMC', 'CONSTANT_JITTERED') # CONSTANT_QMC, CONSTANT_JITTERED
ls.samples = draft(8.0, 2.0)

# tune textures for baking
for tex in [x for x in D.textures if x.type == 'IMAGE']:
    tex.filter_type = 'BOX' #EWA
    tex.filter_size = 0.133
for mat in D.materials:
    for ts in [ts for ts in mat.texture_slots if ts]:
        if ts.use_map_normal:
            ts.normal_factor /= 40

# blender settings
bpy.context.user_preferences.edit.use_global_undo = False
bpy.context.user_preferences.system.memory_cache_limit = 64

def split_object(obj):
    # options
    split_step = map_cell_size
    max_size = max(map_sx + map_cell_size, map_sy + map_cell_size) # fixme
    #max_size = map_cell_size*2 # will not work

    bm = bmesh.new()
    bm.from_mesh(obj.data)

    edges = []

     # object data
    pos = obj.matrix_world.translation

    # vertical (y)
    for i in range(-max_size, max_size, split_step):
        #i = -0.5
        #i = -pos.x + 1
        ret = bmesh.ops.bisect_plane(bm, geom=bm.verts[:]+bm.edges[:]+bm.faces[:], plane_co=(-pos.x + i,0,0), plane_no=(-1,0,0))
        bmesh.ops.split_edges(bm, edges=[e for e in ret['geom_cut'] if isinstance(e, bmesh.types.BMEdge)])

    # horizontal (x)
    for i in range(-max_size, max_size, split_step):
        ret = bmesh.ops.bisect_plane(bm, geom=bm.verts[:]+bm.edges[:]+bm.faces[:], plane_co=(0,-pos.y + i,0), plane_no=(0,1,0))
        bmesh.ops.split_edges(bm, edges=[e for e in ret['geom_cut'] if isinstance(e, bmesh.types.BMEdge)])
                
    bm.to_mesh(ob.data)
    
    bpy.ops.mesh.separate(type='LOOSE')
    #bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')
    bm.free()

def bbox_in_rect(ob, rect):
    """returns true if 2d bbox is in the rect"""
    bb = ob.bound_box
    scale = ob.scale
    loc = ob.location
    minx = bb[0][0] * scale.x + loc.x; maxx = bb[4][0] * scale.x + loc.x
    miny = bb[0][1] * scale.y + loc.y; maxy = bb[2][1] * scale.y + loc.y
    return (in_rect(minx, miny, *rect) or in_rect(minx, maxy, *rect) or \
            in_rect(maxx, miny, *rect) or in_rect(maxx, maxy, *rect))

def select_by_rect(obj, rect):
    ret = False
    _rect = [rect[0], rect[1], rect[2], rect[3]]
    
    fcs = [(obj.matrix_world * Vector(f.center)) for f in obj.data.polygons]
    for c in fcs:
        if in_rect(c.x, c.y, *_rect):
            obj.select = True
            ret = True
            break

    return ret

def select_downfacing_faces(k=-0.85):
    # use in edit mode
    obj = C.active_object
    bm = bmesh.from_edit_mesh(obj.data)

    for face in bm.faces:
        if face.normal.z < k:
            face.select = True

    bmesh.update_edit_mesh(obj.data)

def cleanup_mesh():
    ## mesh fixes
    bpy.ops.object.editmode_toggle()
    # cleanup
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.delete_loose()
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.remove_doubles()
    # fix normals
    #bpy.ops.mesh.select_all(action='SELECT')
    #bpy.ops.mesh.normals_make_consistent(inside=False) # recalculate normals
    # remove degenerate elements
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.dissolve_degenerate()
    bpy.ops.object.editmode_toggle()


# select export scene
#C.scene.active = bpy.data.scenes['exp']
#C.scenes['exp'] = True
name = 'exp' if 'exp' in bpy.data.scenes else 'Scene'
#name = 'Scene.003'
#name = 'Scene.001'
bpy.data.screens['Default'].scene = bpy.data.scenes[name]
C.screen.scene = bpy.data.scenes[name]
sce = C.scene

# activate all layers
#bpy.ops.view3d.layers(list(range(20)), False, True)
#sce.layers = [False] * 20
#sce.layers[0] = True
#sce.layers = [False] * 20
#sce.layers[0] = True
#layer = sce.layers[0]
#print(dir(layer))
bpy.ops.object.select_all(action='DESELECT')
for i in range(1, 20):
    sce.layers[i] = False

# export layers
# 0 - static geometry, walls etc
# 1 - static objects?
# 2 - additional decor objects?

sce.layers[0] = True
sce.layers[2] = True
sce.layers[4] = True
sce.layers[5] = True
sce.layers[10] = True
sce.layers[18] = True # light layer
#bpy.ops.view3d.layers(nr=1, extend=False, toggle=True)

# apply modifiers and scale/rotation
for ob in [x for x in C.visible_objects if x.type in ('MESH', 'CURVE', 'FONT')]:
    print('->', ob.name)
    bpy.ops.object.select_all(action='DESELECT')
    ob.select = True
    sce.objects.active = ob
    #import pdb; pdb.set_trace()
    if ob.type in ('MESH', 'CURVE', 'FONT'):
        bpy.ops.object.convert(target='MESH', keep_original=False)
    bpy.ops.object.transform_apply(rotation=True, scale=True)

    ## mesh fixes
    cleanup_mesh()

    # uniform/cleanup uvmaps
    if ob.type == '0MESH' and ob.data.uv_textures.active:
        me = ob.data
        uvTexmap = me.uv_textures.active
        uvTexmap.name = 'uvmap'
        uvlist = [x.name for x in me.uv_textures]
        for uv in uvlist:
            print('->', uv, 'uvmap')
            if uv != 'uvmap':
                tex = me.uv_textures[uv]
                me.uv_textures.remove(tex)
                print('remove')

if opt_split:
    # remove objects with 0 dimensions
    bpy.ops.object.select_all(action='DESELECT')
    for ob in [x for x in C.visible_objects if x.type == 'MESH']:
        if 0 in (ob.dimensions.x, ob.dimensions.y) and len(ob.data.vertices) >= 3:
            ob.select = True
            print('0 xy dimensions in ' + ob.name + ', deleting')
            bpy.ops.object.delete()
    bpy.ops.object.select_all(action='DESELECT')


    """
    print("joining...")
    ret = bpy.ops.object.join()
    if ret != {'FINISHED'}:
        print('error: cant join objects')
        print('mesh:', C.active_object.name)
        #print('group:', group.name)
        quit(-1)
    """


    print('splitting...')
    for ob in [x for x in C.visible_objects if x.type in ('MESH')]:
        # optimization
        rect = [0, 0, map_sx, map_sy]
        if not bbox_in_rect(ob, rect):
            #print('hit')
            continue
        bpy.ops.object.select_all(action='DESELECT')
        ob.select = True
        sce.objects.active = ob
        print(ob)
        split_object(ob)
    print('done...')
    bpy.ops.object.select_all(action='DESELECT')



#1/0

if opt_split:
    # select regions and group them
    print('grouping...')
    for y in range(0, map_sy, map_cell_size):
        for x in range(0, map_sx, map_cell_size):
            # create group
            gName = 'g_auto_%s_%s' % (int(x/map_cell_size), int(y/map_cell_size))
            print(gName)
            grp = bpy.data.groups.new(gName)
            for ob in [x for x in C.visible_objects if x.type == 'MESH']:
                # skip groups
                if len(ob.users_group):
                    continue
                # only layer 1
                #if not ob.layers[0]:
                #    continue
                rect = [x, y, map_cell_size, map_cell_size]

                # optimization
                if not bbox_in_rect(ob, rect):
                    continue
                ret = select_by_rect(ob, rect)            

                #bpy.ops.group.create(name=gName)
                #bpy.ops.group.add_objects_to_active()
                if ret:
                    if not ob.name in grp.objects:
                        grp.objects.link(ob)

    # discard unused objects
    print('discarding...')
    bpy.ops.object.select_all(action='DESELECT')
    for ob in [x for x in C.visible_objects if x.type == 'MESH']:
        if len(ob.users_group) < 1 and \
            (not ob.layers[1]): # skip snow
            #print('discard ' + ob.name + ', deleting')
            sce.objects.unlink(ob)
            ob.user_clear()
            D.objects.remove(ob)
    bpy.ops.object.select_all(action='DESELECT')
    print('done')

save_tmp_file('out_stage1.blend')
#quit()

#ob =  [x for x in C.visible_objects if x.type in ('MESH')][3]
#sce.objects.active = ob
# fixme: must have pivot
root = bpy.data.objects.get("root")
if root:
    sce.objects.active = root
else:
    print("Warning, no root mesh")
    #exit(-1)
bpy.ops.object.select_all(action='DESELECT')

#1/0

for group in bpy.data.groups:
    print("processing group: ", group.name)
    prefix = group.name + '_'

    bpy.ops.object.select_all(action='DESELECT')
    #bpy.ops.object.select_by_type(type='MESH', extend=False)
    for ob in group.objects:
        if ob.is_visible(sce) and ob.type in ('MESH'):
            ob.select = True
            sce.objects.active = ob # fixme

    if len(group.objects) < 1:
        print('skipping seems empty group', group.name)
        continue

    if len(group.objects) > 1:
        print('joining group', group.name)

        ret = bpy.ops.object.join()
        if ret != {'FINISHED'}:
            print('error: cant join objects')
            print('mesh:', C.active_object.name)
            print('group:', group.name)
            quit(-1)


    # cleanup
    cleanup_mesh()

    if opt_remove_downfacing_faces:
        ## remove downfacing faces
        bpy.ops.object.editmode_toggle()
        bpy.ops.mesh.select_all(action='DESELECT')
        select_downfacing_faces()
        bpy.ops.mesh.delete(type='FACE')
        bpy.ops.object.editmode_toggle()
        # cleanup
        cleanup_mesh()

    #0/1
    ob = sce.objects.active
    me = ob.data
    texmapName = prefix + 'texmap'
    # add uv map
    #curr_uvtex = me.uv_textures.active
    bpy.ops.mesh.uv_texture_add()
    uvTexmap = me.uv_textures.active
    uvTexmap.name = texmapName
    #uvTexmap.active = True
    #uvTexmap.active_render = False

    # add image
    tex_size = opt_texture_size
    img = bpy.ops.image.new(name=texmapName, width=tex_size, height=tex_size, alpha=False)
    #me.uv_textures.active.data[0].image = bpy.data.images['texmap']
    # apply img to all faces
    uvtex = me.uv_textures.active.data
    for f in uvtex:
        f.image = bpy.data.images[texmapName]
        #f.image = img

    # unwrap
    bpy.ops.object.mode_set(mode='EDIT')
    #bpy.ops.mesh.select_all()
    #unselect_downfacing_faces()
    #bpy.ops.uv.smart_project(angle_limit=89, island_margin=0.2, user_area_weight=1.0)
    #bpy.ops.uv.smart_project(angle_limit=66, island_margin=0.2, user_area_weight=0)
    bpy.ops.uv.lightmap_pack(PREF_IMG_PX_SIZE=tex_size, PREF_APPLY_IMAGE=False,
            PREF_BOX_DIV=draft(1,48//2), PREF_MARGIN_DIV=0.2, PREF_CONTEXT='ALL_FACES')
    #bpy.ops.uv.lightmap_pack(PREF_IMG_PX_SIZE=256, PREF_APPLY_IMAGE=True)
    #me.uv_textures.active.data[0].image = bpy.data.images['texmap']
    #bpy.ops.uv.lightmap_pack(PREF_IMG_PX_SIZE=tex_size, PREF_APPLY_IMAGE=False,
    #        PREF_BOX_DIV=draft(1,48), PREF_MARGIN_DIV=0.2, PREF_CONTEXT='ALL_FACES')
    #bpy.ops.uv.select_all()
    #bpy.ops.uv.pack_islands(margin=0.1)
    bpy.ops.object.mode_set(mode='OBJECT')

    #1/0

    # bake
    def bake(type):
        #sce.render.bake_type = 'TEXTURE' # TEXTURE, NORMALS, AO, FULL
        sce.render.bake_type = type
        sce.render.bake_margin = 4 #2
        sce.render.filter_size = 1.5
        sce.render.pixel_filter_type = 'CATMULLROM'
        sce.render.use_antialiasing = True
        sce.render.use_shadows = True
        sce.render.use_bake_normalize = True # need for ao
        bpy.ops.object.bake_image()
        # fix needed for proper work of Image.copy() function
        bpy.data.images[texmapName].pack(as_png=True)

    # save ao image
    if opt_add_ao:
        bake('AO')
        img = bpy.data.images[texmapName]
        file = os.path.join(os.path.dirname(output), prefix + 'ao.png')
        print("saving ", file)
        img.save_render(file)

    #1/0
    scale_image(bpy.data.images[texmapName], *(opt_normal_map_size,)*2)
    # bake normals texture
    bake('NORMALS')
    #img_bump = bpy.ops.image.new(name='normalmap', width=sx, height=sx, alpha=False)
    img_bump = bpy.data.images[texmapName].copy()
    img_bump.name = prefix + 'normalmap'

    scale_image(bpy.data.images[texmapName], *(opt_texture_size,)*2)
    # bake diffuse texture
    bake('FULL')

    #1/0

    # Cleanup

    # remove original uvs
    print(list(me.uv_textures))
    uvTexmap.active = False
    uvlist = [x.name for x in me.uv_textures]
    for x in uvlist:
        print('->', x, texmapName)
        if x != texmapName:
            tex = me.uv_textures[x]
            me.uv_textures.remove(tex)
            print('remove')
    #me.uv_textures[0].active = True
    #bpy.ops.mesh.uv_texture_remove()


    # remove materials
    for x in ob.material_slots:
        bpy.ops.object.material_slot_remove()

    # add material
    mat = bpy.data.materials.new('wallmat')
    mat.diffuse_color = (.8,.8,.8)
    mat.diffuse_shader = 'LAMBERT' 
    mat.diffuse_intensity = 1.0 
    mat.specular_color = (.8,.8,.8)
    mat.specular_shader = 'BLINN'
    mat.specular_intensity = 0.5
    mat.specular_hardness = 10
    mat.alpha = 1
    mat.ambient = 1
    me.materials.append(mat)

    # Add color texture
    cTex = bpy.data.textures.new(prefix + 'ColorTex', type='IMAGE')
    cTex.image = bpy.data.images[texmapName]

    # add texture slot 
    mtex = mat.texture_slots.add()
    mtex.texture = cTex
    mtex.texture_coords = 'UV'
    mtex.use_map_color_diffuse = True 
    mtex.diffuse_color_factor = 1.0
    mtex.blend_type = 'MULTIPLY'

    # Add normal/bump texture
    bumpTex = bpy.data.textures.new(prefix + 'NormalTex', type='IMAGE')
    bumpTex.image = img_bump

    # add bump texture slot 
    mtex2 = mat.texture_slots.add()
    mtex2.texture = bumpTex
    mtex2.texture_coords = 'UV'
    mtex2.use_map_color_diffuse = False
    mtex2.use_map_normal = True 
    mtex2.blend_type = 'MULTIPLY'
    #1/0

#-- process snow scenery from layer #2
bpy.ops.object.select_all(action='DESELECT')
# skip other layers
#for i in range(0, 20):
#    sce.layers[i] = False
sce.layers[1] = True
#bpy.ops.object.select_all()
bpy.ops.object.select_by_layer(layers=2) # 2 == layer 2

# select all groups on all layers
for g in bpy.data.groups:
    for ob in g.objects:
        if not len(ob.material_slots) > 0:
            print ('overwork')
            continue
        if not 'wallmat' in ob.material_slots[0].name: # fixme
            print('skipping seems BAD object and group', ob.name, g.name)
            continue
        ob.select = True

#--

#output = '/tmp/ex.dae'
#bpy.ops.collada.export(filepath=output, applyModifiers=True)
# selected only
print("\nexporting to collada...")
bpy.ops.wm.collada_export(filepath=output, selected=True, apply_modifiers=True, \
       include_uv_textures=True, include_material_textures=True, use_texture_copies=True, \
       triangulate=True, active_uv_only=True)
       # breaks assimp?
       #export_transformation_type=2, export_transformation_type_selection='both')
print("** Saved to: " + output)
#sys.exit(0)

# Debug
save_tmp_file('out.blend')

print("all done")
