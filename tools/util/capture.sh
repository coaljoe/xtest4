#!/bin/sh

file=/home/j/media/capture/$(date +%F.%H%M%S).mkv
echo "** $file"

#ffmpeg -video_size 1440x900 -framerate 0.8 -f x11grab -i :0.0 -vf scale=iw/2:ih/2 -vcodec libx264 -crf 40 -preset medium -threads 0 $(date +%F.%H%M%S).mkv
#ffmpeg -video_size 1440x900 -framerate 1.5 -f x11grab -i :0.0 -vf scale=iw/2:ih/2 -vcodec libx264 -pix_fmt yuv422p -crf 40 -preset medium -threads 0 $(date +%F.%H%M%S).mkv
#ffmpeg -video_size `xdpyinfo | grep 'dimensions:'| awk '{print $2}'` -framerate 1.5 -f x11grab -i :0.0 -vf scale=iw/2:ih/2 -vcodec libx264 -pix_fmt yuv422p -crf 40 -preset medium -threads 0 $file
ffmpeg -video_size `xdpyinfo | grep 'dimensions:'| awk '{print $2}'` -framerate 0.5 -f x11grab -i :0.0 -vf scale=iw/2:ih/2 -vcodec libx264 -pix_fmt yuv422p -crf 41 -preset slow -x264opts subme=0 -threads 0 $file >/dev/null 2>&1 &

echo "** $file"
