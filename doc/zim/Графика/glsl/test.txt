Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2014-01-30T03:18:35+04:00

====== test ======

projecting texture
http://www.kickjs.org/example/shader_editor/shader_editor.html

attribute vec3 vertex;
attribute vec3 normal;
attribute vec2 uv1;
attribute vec4 tangent;

uniform mat4 _mv; // model-view matrix
uniform mat4 _mvProj; // model-view-projection matrix
uniform mat3 _norm; // normal matrix
uniform float _time; // time in seconds

varying vec2 uv;
varying vec3 n;

void main(void) {
	// compute position
	gl_Position = _mvProj * vec4(vertex, 1.0);

    vec4 v = _mvProj * vec4(vertex, 1.0);
    //vec4 v = _mv * _mvProj * vec4(vertex, 1.0);

	uv = vec2(v.x, v.y);
	//uv = vec2(-0.5 + v.x/2.0, 0.5 + v.y/2.0);

	// compute light info
	n = normalize(_norm * normal);
}
