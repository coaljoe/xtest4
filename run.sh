#!/bin/sh

cd `dirname $0`
export LD_LIBRARY_PATH=`pwd`
export PYTHONPATH=`pwd`:`pwd`/lib
export PYTHONPATH=/home/j/build/pycegui/cegui/build/lib:$PYTHONPATH
export LD_LIBRARY_PATH=/home/j/build/pycegui/cegui/build/lib:$LD_LIBRARY_PATH

if [ "$1" == "-hud" ]; then
    shift
    vram=`echo 128*1024^2 | bc`
    export GALLIUM_HUD="fps+cpu:100;requested-VRAM+requested-GTT:$vram;draw-calls"
fi

if [ "$1" == "-trace" ]; then
    shift
    export DUBUG=1
    apitrace trace -o trace python2 -OO ./src/main2.py $*
elif [ "$1" == "-profile" ]; then
    shift
    python2 ./src/profile.py $*
else
    #python2 ./src/main2.py $* 2>&1 | tee run.log
    nohup ~/util/boost.sh 20 2>&1 >/dev/null &
    python2 -OO ./src/main2.py $*
fi

