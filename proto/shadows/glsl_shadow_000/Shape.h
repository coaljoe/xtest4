#ifndef __SHAPE_H_
#define __SHAPE_H_

#include <fstream>
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GL/glut.h>
#include "Transform.h"

#if DEBUG
#define glError() { \
        GLenum err = glGetError(); \
        while (err != GL_NO_ERROR) { \
                    fprintf(stderr, "glError: %s caught at %s:%u\n", (char *)gluErrorString(err), __FILE__, __LINE__); \
                    err = glGetError(); \
                } \
}
#else
#define glError() 
#endif

struct ShaderObject
{
    GLuint programObject;

    // uniform
    GLuint mModelLocation;
    GLuint mCameraViewLocation;
    GLuint mCameraProjLocation;
    GLuint mLightViewLocation;
    GLuint mLightProjLocation;
    GLuint shadowMapLocation;

    GLuint colorLocation;

    // attribute
    GLuint vertexLocation;
    GLuint normalLocation;
};



class Ground
{
private:
    const ShaderObject *shObj;
    GLuint vboId[3];
    GLuint numIndices;

    GLfloat matrix[16];
    GLfloat color[4];

public:
    Ground(const ShaderObject *obj)
        : shObj(obj)
    {
        color[0] = 1.0f; color[1] = 1.0f; color[2] = 1.0f; color[3] = 1.0f;
        sl_LoadIdentityf(matrix);
    }

    void setup()
    {
        GLuint numVertices;

        const GLfloat scale = 10.0f;

        const GLfloat vertices[] = {
            -scale, 0.0f, -scale,
            +scale, 0.0f, -scale,
            +scale, 0.0f, +scale,
            -scale, 0.0f, +scale
        };
        const GLfloat normals[] = {
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f
        };
        const GLuint indices[] = {
            0, 2, 1,
            0, 3, 2
        };
        numVertices = 4;
        numIndices = 6;

        ////////////////////////////////////////////////////////////////

        glGenBuffers(3, vboId);
        glBindBuffer(GL_ARRAY_BUFFER, vboId[0]);
        glBufferData(GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), vertices, GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, vboId[1]);
        glBufferData(GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), normals, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId[2]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices*sizeof(GLuint), indices, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    void render()
    {
        glUniformMatrix4fv(shObj->mModelLocation, 1, GL_FALSE, matrix);
        glUniform4fv(shObj->colorLocation, 1, color);

        // vertex
        glBindBuffer(GL_ARRAY_BUFFER, vboId[0]);
        glVertexAttribPointer(shObj->vertexLocation, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*3, 0); 
        glEnableVertexAttribArray(shObj->vertexLocation);

        // normal
        glBindBuffer(GL_ARRAY_BUFFER, vboId[1]);
        glVertexAttribPointer(shObj->normalLocation, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*3, 0); 
        glEnableVertexAttribArray(shObj->normalLocation);

        // index
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId[2]);

        // draw
        glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0);
    }

    void setPosition(float x, float y, float z) {
        sl_LoadIdentityf(matrix);
        sl_Translatef(matrix, x, y, z);
    }

    void setColor(float r, float g, float b, float a) {
        color[0] = r;
        color[1] = g;
        color[2] = b;
        color[3] = a;
    }
};



class Cube
{
private:
    const ShaderObject *shObj;
    GLuint vboId[3];
    GLuint numIndices;

    GLfloat matrix[16];
    GLfloat color[4];

public:
    Cube (const ShaderObject *obj)
        : shObj(obj)
    {
        color[0] = 1.0f; color[1] = 1.0f; color[2] = 1.0f; color[3] = 1.0f;
        sl_LoadIdentityf(matrix);
    }

    void setup()
    {
        GLuint numVertices;
        GLfloat vertices[] = {
            -0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f, +0.5f,
            +0.5f, -0.5f, +0.5f,
            +0.5f, -0.5f, -0.5f,
            -0.5f, +0.5f, -0.5f,
            -0.5f, +0.5f, +0.5f,
            +0.5f, +0.5f, +0.5f,
            +0.5f, +0.5f, -0.5f,
            -0.5f, -0.5f, -0.5f,
            -0.5f, +0.5f, -0.5f,
            +0.5f, +0.5f, -0.5f,
            +0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f, +0.5f,
            -0.5f, +0.5f, +0.5f,
            +0.5f, +0.5f, +0.5f,
            +0.5f, -0.5f, +0.5f,
            -0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f, +0.5f,
            -0.5f, +0.5f, +0.5f,
            -0.5f, +0.5f, -0.5f,
            +0.5f, -0.5f, -0.5f,
            +0.5f, -0.5f, +0.5f,
            +0.5f, +0.5f, +0.5f,
            +0.5f, +0.5f, -0.5f
        };
        GLfloat normals[] = {
            +0.0f, -1.0f, +0.0f,
            +0.0f, -1.0f, +0.0f,
            +0.0f, -1.0f, +0.0f,
            +0.0f, -1.0f, +0.0f,
            +0.0f, +1.0f, +0.0f,
            +0.0f, +1.0f, +0.0f,
            +0.0f, +1.0f, +0.0f,
            +0.0f, +1.0f, +0.0f,
            +0.0f, +0.0f, -1.0f,
            +0.0f, +0.0f, -1.0f,
            +0.0f, +0.0f, -1.0f,
            +0.0f, +0.0f, -1.0f,
            +0.0f, +0.0f, +1.0f,
            +0.0f, +0.0f, +1.0f,
            +0.0f, +0.0f, +1.0f,
            +0.0f, +0.0f, +1.0f,
            -1.0f, +0.0f, +0.0f,
            -1.0f, +0.0f, +0.0f,
            -1.0f, +0.0f, +0.0f,
            -1.0f, +0.0f, +0.0f,
            +1.0f, +0.0f, +0.0f,
            +1.0f, +0.0f, +0.0f,
            +1.0f, +0.0f, +0.0f,
            +1.0f, +0.0f, +0.0f
        };
        GLuint indices[] = {
            0, 2, 1,
            0, 3, 2,
            4, 5, 6,
            4, 6, 7,
            8, 9, 10,
            8, 10, 11,
            12, 15, 14,
            12, 14, 13,
            16, 17, 18,
            16, 18, 19,
            20, 23, 22,
            20, 22, 21
        };
        numIndices = 36;
        numVertices = 24;

        glGenBuffers(3, vboId);
        glBindBuffer(GL_ARRAY_BUFFER, vboId[0]);
        glBufferData(GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), vertices, GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, vboId[1]);
        glBufferData(GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), normals, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId[2]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices*sizeof(GLuint), indices, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    void render()
    {
        glUniformMatrix4fv(shObj->mModelLocation, 1, GL_FALSE, matrix);
        glUniform4fv(shObj->colorLocation, 1, color);

        // vertex
        glBindBuffer(GL_ARRAY_BUFFER, vboId[0]);
        glVertexAttribPointer(shObj->vertexLocation, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*3, 0); 
        glEnableVertexAttribArray(shObj->vertexLocation);

        // normal
        glBindBuffer(GL_ARRAY_BUFFER, vboId[1]);
        glVertexAttribPointer(shObj->normalLocation, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*3, 0); 
        glEnableVertexAttribArray(shObj->normalLocation);

        // index
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId[2]);

        // draw
        glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0);
    }

    void setPosition(float x, float y, float z) {
        sl_LoadIdentityf(matrix);
        sl_Translatef(matrix, x, y, z);
    }

    void setColor(float r, float g, float b, float a) {
        color[0] = r;
        color[1] = g;
        color[2] = b;
        color[3] = a;
    }
};


class Sphere
{
private:
    const ShaderObject *shObj;
    GLuint vboId[3];
    GLuint numIndices;

    GLfloat matrix[16];
    GLfloat color[4];

public:
    Sphere(const ShaderObject *shaderObject)
        : shObj(shaderObject)
    {
        color[0] = 1.0f; color[1] = 1.0f; color[2] = 1.0f; color[3] = 1.0f;
        sl_LoadIdentityf(matrix);
    }

    void setup()
    {
        GLfloat radius = 0.5;
        GLuint numSlices = 50;

        GLuint numParallels = numSlices;
        GLuint numVertices = ( numParallels + 1 ) * ( numSlices + 1 );
        numIndices = numParallels * numSlices * 6;
        GLfloat angleStep = (2.0f * M_PI) / ((GLfloat) numSlices);

        GLfloat* vertices = (GLfloat*)malloc(3*numVertices*sizeof(GLfloat));
        GLfloat* normals = (GLfloat*)malloc(3*numVertices*sizeof(GLfloat));
        GLuint* indices = (GLuint*)malloc(numIndices*sizeof(GLuint));

        for (unsigned i=0; i<numParallels+1; ++i)
        {
            for (unsigned j = 0;j<numSlices+1; ++j)
            {
                GLuint vertexIndex = ((GLuint)i * (numSlices + 1) + j) * 3; 
                GLuint normalIndex = ((GLuint)i * (numSlices + 1) + j) * 3; 

                vertices[vertexIndex + 0] = radius * sinf (angleStep * (GLfloat)i) * sinf (angleStep * (GLfloat)j);
                vertices[vertexIndex + 1] = radius * cosf (angleStep * (GLfloat)i);
                vertices[vertexIndex + 2] = radius * sinf (angleStep * (GLfloat)i) * cosf (angleStep * (GLfloat)j);

                normals[normalIndex + 0] = vertices[vertexIndex + 0] / radius;
                normals[normalIndex + 1] = vertices[vertexIndex + 1] / radius;
                normals[normalIndex + 2] = vertices[vertexIndex + 2] / radius;
            }
        }

        GLuint* indexBuf = indices;
        for (unsigned i=0; i<numParallels ; ++i) {
            for (unsigned j=0; j<numSlices; ++j) {
                *indexBuf++  = i * (numSlices + 1) + j;
                *indexBuf++ = (i + 1) * (numSlices + 1) + j;
                *indexBuf++ = (i + 1) * (numSlices + 1) + (j + 1);

                *indexBuf++ = i * (numSlices + 1) + j;
                *indexBuf++ = (i + 1) * (numSlices + 1) + (j + 1);
                *indexBuf++ = i * (numSlices + 1) + (j + 1);
            }
        }

        ////////////////////////////////////////////////////////////////

        glGenBuffers(3, vboId);
        glBindBuffer(GL_ARRAY_BUFFER, vboId[0]);
        glBufferData(GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), vertices, GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, vboId[1]);
        glBufferData(GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), normals, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId[2]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices*sizeof(GLuint), indices, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        delete [] vertices;
        delete [] normals;
        delete [] indices;
    }

    void render()
    {
        glUniformMatrix4fv(shObj->mModelLocation, 1, GL_FALSE, matrix);
        glUniform4fv(shObj->colorLocation, 1, color);

        // vertex
        glBindBuffer(GL_ARRAY_BUFFER, vboId[0]);
        glVertexAttribPointer(shObj->vertexLocation, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*3, 0); 
        glEnableVertexAttribArray(shObj->vertexLocation);

        // normal
        glBindBuffer(GL_ARRAY_BUFFER, vboId[1]);
        glVertexAttribPointer(shObj->normalLocation, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*3, 0); 
        glEnableVertexAttribArray(shObj->normalLocation);

        // index
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId[2]);

        glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0);
    }

    void setPosition(float x, float y, float z) {
        sl_LoadIdentityf(matrix);
        sl_Translatef(matrix, x, y, z);
    }

    void setColor(float r, float g, float b, float a) {
        color[0] = r;
        color[1] = g;
        color[2] = b;
        color[3] = a;
    }

};




#endif /* __SHAPE_H_ */
