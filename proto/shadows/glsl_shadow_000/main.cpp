#include "Shape.h"


const float renderWidth  = 640.0f;
const float renderHeight = 480.0f;

const float shadowMapRatio = 4.0f;
//const float shadowMapWidth = (renderWidth * shadowMapRatio);
//const float shadowMapHeight = (renderHeight * shadowMapRatio);
const float shadowMapWidth = 1024.0f;
const float shadowMapHeight = 1024.0f;


static ShaderObject shObj;
static Cube cube(&shObj);
//static Sphere sphere(&shObj);
static Ground ground(&shObj);

static GLuint fboId;
static GLuint depthTextureId;

static GLfloat mCameraView[16];
static GLfloat mCameraProj[16];
static GLfloat mLightView[16];
static GLfloat mLightProj[16];


void init();
void initGLSL();
void display();
void reshape(int w, int h);
void timer(int t);
GLuint LoadShader(GLenum type, const char *fileName);


void initGLSL()
{
    ///////////////////////////////////////////////////////////////////////////
    // loadShader
    //
    GLuint vertexShader;
    GLuint fragmentShader;

    vertexShader   = LoadShader(GL_VERTEX_SHADER, "simple.vert");
    fragmentShader = LoadShader(GL_FRAGMENT_SHADER, "simple.frag");

    shObj.programObject = glCreateProgram();

    glAttachShader(shObj.programObject,vertexShader);
    glAttachShader(shObj.programObject,fragmentShader);
    glLinkProgram(shObj.programObject);

    ///////////////////////////////////////////////////////////////////////////
    // Texture for ShadowMap
    //
    glGenTextures(1, &depthTextureId);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, depthTextureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32,
            (int)shadowMapWidth, (int)shadowMapHeight, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
    glBindTexture(GL_TEXTURE_2D, 0);


    ///////////////////////////////////////////////////////////////////////////
    // FBO for ShadowMap
    //
    glGenFramebuffers(1, &fboId);
    glBindFramebuffer(GL_FRAMEBUFFER, fboId);

    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTextureId, 0);

    GLenum FBOstatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(FBOstatus != GL_FRAMEBUFFER_COMPLETE)
        printf("GL_FRAMEBUFFER_COMPLETE failed, CANNOT use FBO\n");

    glBindFramebuffer(GL_FRAMEBUFFER, 0);


    ///////////////////////////////////////////////////////////////////////////
    // for Fragment
    //
    glBindFragDataLocation(shObj.programObject, 0, "fragColor");

    ///////////////////////////////////////////////////////////////////////////
    // attribute
    //
    shObj.vertexLocation = glGetAttribLocation(shObj.programObject, "a_vertex");
    shObj.normalLocation = glGetAttribLocation(shObj.programObject, "a_normal");

    ///////////////////////////////////////////////////////////////////////////
    // Uniform
    //
    shObj.shadowMapLocation = glGetUniformLocation(shObj.programObject,"ShadowMap");
    shObj.mModelLocation = glGetUniformLocation(shObj.programObject,"mModel");
    shObj.mCameraViewLocation = glGetUniformLocation(shObj.programObject,"mCameraView");
    shObj.mCameraProjLocation = glGetUniformLocation(shObj.programObject,"mCameraProj");
    shObj.mLightViewLocation = glGetUniformLocation(shObj.programObject,"mLightView");
    shObj.mLightProjLocation = glGetUniformLocation(shObj.programObject,"mLightProj");

    shObj.colorLocation = glGetUniformLocation(shObj.programObject,"color");

    ///////////////////////////////////////////////////////////////////////////
    // Setup objects
    //
    ground.setup();
    cube.setup();
    //sphere.setup();
}


void drawObjects(void)
{
    // draw ground
    ground.setColor(0.6f, 0.5f, 0.5f, 1.0f);
    ground.setPosition(0.0f, 0.0f, 0.0f);
    ground.render();

    // draw box(red)
    cube.setColor(1.0f, 0.0f, 0.0f, 1.0f);
    cube.setPosition(0.0f, 2.0f, 0.0f);
    cube.render();

    // draw box(white)
    cube.setColor(1.0f, 1.0f, 1.0f, 1.0f);
    cube.setPosition(-1.0f, 0.5f, -1.0f);
    cube.render();

    cube.setColor(1.0f, 1.0f, 1.0f, 1.0f);
    cube.setPosition(-1.0f, 0.5f, 1.0f);
    cube.render();

    cube.setColor(1.0f, 1.0f, 1.0f, 1.0f);
    cube.setPosition(1.0f, 0.5f, 1.0f);
    cube.render();

    cube.setColor(1.0f, 1.0f, 1.0f, 1.0f);
    cube.setPosition(1.0f, 0.5f, -1.0f);
    cube.render();

    //// draw sphere(yellow)
    //sphere.setColor(1.0f, 1.0f, 0.0f, 1.0f);
    //sphere.setPosition(2.0f, 2.0f, -2.0f);
    //sphere.render();
}


void display(void) 
{
    // light position
    static float lightAngle = 0.0f;
    static float light_pos[3];
    light_pos[0] = 15.0f * cos(lightAngle*M_PI/180.0f);
    light_pos[1] = 30.0f;
    light_pos[2] = 15.0f * sin(lightAngle*M_PI/180.0f);
    lightAngle += 0.5f;
    if (lightAngle >= 360.0f) lightAngle = 0.0f;

    // ------------------------------------------------------------------------
    glUseProgram(shObj.programObject);
    // ------------------------------------------------------------------------

    ///////////////////////////////////////////////////////////////////////////
    // first pass
    ///////////////////////////////////////////////////////////////////////////

    glViewport(0, 0, (int)(shadowMapWidth), (int)(shadowMapHeight));

    sl_LoadIdentityf(mLightProj);
    sl_Perspectivef(mLightProj, 30.0f, (GLfloat)shadowMapWidth/(GLfloat)shadowMapHeight, 1.0, 40000.0);
    sl_LoadIdentityf(mLightView);
    sl_LookAtf(mLightView, light_pos[0], light_pos[1], light_pos[2], 0.0f, 0.0f, 0.0f, 0.0, 1.0, 0.0);

    glCullFace(GL_FRONT);
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); 

    glBindFramebuffer(GL_FRAMEBUFFER, fboId);
    {
        glClear(GL_DEPTH_BUFFER_BIT);

        glEnable(GL_POLYGON_OFFSET_FILL);
        glPolygonOffset(1.1, 4.0);
        //glPolygonOffset(10.0, 4.0);

        glUniformMatrix4fv(shObj.mCameraViewLocation, 1, GL_FALSE, mLightView);
        glUniformMatrix4fv(shObj.mCameraProjLocation, 1, GL_FALSE, mLightProj);

        //glUniformMatrix4fv(shObj.mLightViewLocation, 1, GL_FALSE, mLightView);
        //glUniformMatrix4fv(shObj.mLightProjLocation, 1, GL_FALSE, mLightProj);

        drawObjects();

        glDisable(GL_POLYGON_OFFSET_FILL);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE); 
    glCullFace(GL_BACK);

    ///////////////////////////////////////////////////////////////////////////
    // second pass
    ///////////////////////////////////////////////////////////////////////////

    glViewport(0, 0, (int)renderWidth, (int)renderHeight);

    sl_LoadIdentityf(mCameraProj);
    sl_Perspectivef(mCameraProj, 45.0, (GLfloat)renderWidth/(GLfloat)renderHeight, 1.0, 1000.0);
    sl_LoadIdentityf(mCameraView);
    sl_LookAtf(mCameraView, 5.0f, 6.0f, 7.0f, 0.0f, 1.0f, 0.0f, 0.0, 1.0, 0.0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUniformMatrix4fv(shObj.mCameraViewLocation, 1, GL_FALSE, mCameraView);
    glUniformMatrix4fv(shObj.mCameraProjLocation, 1, GL_FALSE, mCameraProj);

    glUniformMatrix4fv(shObj.mLightViewLocation, 1, GL_FALSE, mLightView);
    glUniformMatrix4fv(shObj.mLightProjLocation, 1, GL_FALSE, mLightProj);

    glUniform1i(shObj.shadowMapLocation, 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, depthTextureId);

    drawObjects();

    // ------------------------------------------------------------------------
    glUseProgram(0);
    // ------------------------------------------------------------------------

    glutSwapBuffers();
}


void init()
{
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.2f,0.2f,0.2f,1.0f);

    glEnable(GL_CULL_FACE);
}


 
GLuint LoadShader(GLenum type, const char *fileName)
{
    GLuint shader;
    GLint compiled;
 
    std::fstream inputFile(fileName);
    std::istreambuf_iterator<char> dataBegin(inputFile);
    std::istreambuf_iterator<char> dataEnd;
    std::string fileData(dataBegin, dataEnd);
    const char *file = fileData.c_str();
 
    shader = glCreateShader(type);
    if (shader == 0) return 0;
 
    glShaderSource(shader, 1, &file, NULL);
    glCompileShader(shader);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (!compiled) {
        GLint infoLen = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
        if (infoLen > 1) {
            char* infoLog = new char[sizeof(char)*infoLen];
            glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
            std::cerr << "Error compiling shader: " << fileName << "\n" << infoLog << "\n";
            delete [] infoLog;
        }
        glDeleteShader(shader);
        return 0;
    }
    return shader;
}
 
void timer(int t)
{
    glutPostRedisplay();
    glutTimerFunc(t, timer, 17);
}
 

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowPosition(100,100);
    glutInitWindowSize((int)renderWidth, (int)renderHeight);
    glutCreateWindow("GLSL Shadow mapping");

    glutDisplayFunc(display);
    glutTimerFunc(100, timer, 17);

    GLenum err;
    err = glewInit();
    if (err != GLEW_OK) {
        std::cerr << "GLEW error : " << glewGetErrorString(err) << "\n";
        exit(1);
    }   

    initGLSL();
    init();

    glutMainLoop();
}
