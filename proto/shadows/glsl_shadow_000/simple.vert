#version 130

in vec3 a_vertex;

out vec4 v_shadowCoord;

uniform mat4 mModel;

uniform mat4 mLightView;
uniform mat4 mLightProj;

uniform mat4 mCameraView;
uniform mat4 mCameraProj;

const mat4 bias = mat4(
    0.5, 0.0, 0.0, 0.0, 
    0.0, 0.5, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.5, 0.5, 0.5, 1.0
);


void main()
{
    v_shadowCoord = bias * mLightProj * mLightView * mModel * vec4(a_vertex.xyz,1.0);

    gl_Position = mCameraProj * mCameraView * mModel * vec4(a_vertex.xyz, 1.0);
}
