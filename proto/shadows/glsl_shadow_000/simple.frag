#version 130

in vec4 v_shadowCoord;

out vec4 fragColor;

uniform sampler2D ShadowMap;
uniform vec4 color;

void main()
{
    vec4 shadowCoordWdiv = v_shadowCoord / v_shadowCoord.w;
    //shadowCoordWdiv.z += 0.00005;
    //shadowCoordWdiv.z += 0.00042;
    float shadow = 1.0;
    if (v_shadowCoord.w > 0.0) {
        float bias = 0.000042;
        //float bias = 0;
        float distanceFromLight = textureProj(ShadowMap, shadowCoordWdiv).z;
        shadow = (distanceFromLight < shadowCoordWdiv.z-bias) ? 0.5 : 1.0;
    }
    fragColor = shadow * color;
}
