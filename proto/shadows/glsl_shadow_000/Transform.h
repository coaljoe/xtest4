#ifndef __TRANSFORM_H_
#define __TRANSFORM_H_

#include <GL/gl.h>

// vector

GLfloat sl_Amountf(const GLfloat *v);
void sl_Normalizef(GLfloat *v);
GLfloat glusDotf(const GLfloat v0[3], const GLfloat v1[3]);
void glusCrossf(GLfloat result[3], const GLfloat v0[3], const GLfloat v1[3]);


// Matrix

void sl_Scalef( GLfloat *result, GLfloat sx, GLfloat sy, GLfloat sz);
void sl_Translatef( GLfloat *result, GLfloat tx, GLfloat ty, GLfloat tz);
void sl_Rotatef( GLfloat *result, GLfloat angle, GLfloat x, GLfloat y, GLfloat z);
void sl_MultMatrixf( GLfloat *result, const GLfloat *srcA, const GLfloat *srcB); 
void sl_LoadIdentityf(GLfloat *result);

// ModelView

void sl_Frustumf(
    GLfloat *result,
    float left, float right,
    float bottom, float top,
    float nearZ, float farZ);

void sl_Perspectivef(
    GLfloat *result,
    float fovy, float aspect,
    float nearZ, float farZ);

void sl_Orthof(
    GLfloat *result,
    float left, float right,
    float bottom, float top,
    float nearZ, float farZ);

void sl_LookAtf(
    GLfloat *result,
    float ex, float ey, float ez,
    float tx, float ty, float tz,
    float ux, float uy, float uz);

#endif /* __TRANSFORM_H_ */
