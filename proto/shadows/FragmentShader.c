uniform sampler2DShadow ShadowMap;

varying vec4 ShadowCoord;

void main()
{	
	vec4 shadowCoordinateWdivide = ShadowCoord / ShadowCoord.w ;
	
	// Used to lower moiré pattern and self-shadowing
	shadowCoordinateWdivide.z += 0.0005;
	//shadowCoordinateWdivide.z += 0.01;
	
	
	/*float distanceFromLight = shadow2DProj(ShadowMap,shadowCoordinateWdivide.st).z;*/
	
	
         /*float shadow = 1.0;*/
         /*if (ShadowCoord.w > 0.0)*/
                 /*shadow = distanceFromLight < shadowCoordinateWdivide.z ? 0.5 : 1.0 ;*/

        float shadow = shadow2DProj(ShadowMap, ShadowCoord).z;
	//shadow.z += 0.0005;
  	
	
  	gl_FragColor =	 shadow * gl_Color;
  
}

