[VertexShader]
varying vec3 v_V;
varying vec3 v_N;

void main() {
	gl_Position = ftransform();
	v_V = (gl_ModelViewMatrix * gl_Vertex).xyz;
	v_N = gl_NormalMatrix * gl_Normal;
}
[FragmentShader]
varying vec3 v_V;
varying vec3 v_N;
//http://help.chaosgroup.com/vray/help/200R1/examples_vrayglsl.htm

void main() {
	vec3 N = normalize(v_N);
	vec3 V = normalize(v_V);
	vec3 R = reflect(V, N);
	vec3 L = normalize(vec3(gl_LightSource[0].position));

	// color ramp
	vec4 c1 = vec4(1.0, 1.0, 1.0, 0);
	vec4 c2 = vec4(0.515, 0.527, 1.0, 0);

	// specular	
	vec4 spec_color = vec4(1.0, 1.0, 1.0, 0);
	float spec_intensity = 0.5;
	float shininess = 5.0;
	
	// ramp calcs
	const float amount = 1.0;
	vec3 normal = (gl_FrontFacing) ? N : -N;
	vec3 direction = V;
	float dot_dn = clamp(-dot(direction, normal), 0.0, 1.0);
	float mix_value = pow(dot_dn, 1.0 / amount);

	// mixing
	vec4 ambient = vec4(0.1, 0.1, 0.1, 0);
	vec4 diffuse = mix(c1, c2, mix_value) * max(dot(L, N), 0.0);
	vec4 specular = spec_color * spec_intensity * pow(max(dot(R, L), 0.0), shininess);

	gl_FragColor = ambient + diffuse + specular;
}
