[VertexShader]
varying vec3 v_V;
varying vec3 v_N;

void main() {
	gl_Position = ftransform();
	v_V = (gl_ModelViewMatrix * gl_Vertex).xyz;
	v_N = gl_NormalMatrix * gl_Normal;
}
[FragmentShader]
varying vec3 v_V;
varying vec3 v_N;
const float amount = 0.8;

void main() {
	vec3 N = normalize(v_N);
	vec3 V = normalize(v_V);
	vec3 R = reflect(V, N);
	vec3 L = normalize(vec3(gl_LightSource[0].position));

	vec4 c1 = vec4(0.5, 0.1, 0.1, 0);
	vec4 c2 = vec4(0.1, 0.0, 0.5, 0);
	
	vec3 normal = (gl_FrontFacing) ? N : -N;
	vec3 direction = V;
	
	float dot_dn = clamp(-dot(direction, normal), 0.0, 1.0);
	float mix_value = 1.0 - pow(dot_dn, 1.0 / amount);

	vec4 ambient = gl_FrontMaterial.ambient;
	vec4 diffuse = mix(c1, c2, mix_value) * max(dot(L, N), 0.0);
	vec4 specular = gl_FrontMaterial.specular * pow(max(dot(R, L), 0.0), gl_FrontMaterial.shininess);

	gl_FragColor = ambient + diffuse + specular;
}
