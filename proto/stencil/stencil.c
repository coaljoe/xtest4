/*
 * (c) Copyright 1995-1999, Igor Tarasov
 * FidoNet: 2:5020/370.2 620.20 1103.5
 * Inet: itarasov@rtuis.miem.edu.ru
 * Phone: (095)942-50-97
 */




#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>



void resize(int width,int height)
{
   glViewport(0,0,width,height);
   glMatrixMode( GL_PROJECTION );
   glLoadIdentity();
   glOrtho(-5,5, -5,5, 2,12);   
   gluLookAt( 0,0,5, 0,0,0, 0,1,0 );
   glMatrixMode( GL_MODELVIEW );
}    



void display(void)
{
 glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);


 // ��������� ���� ���������
 glEnable(GL_STENCIL_TEST);

 // ������ ��� � ��������� ������ ��������� ���������
 // � ��� �����, ��� �������� ���
 glStencilFunc(GL_ALWAYS, 1, 0);
 glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
 glutSolidCube(2.5);

 // ��������� ������ ��������� ��������
 // � ��� �����, ��� ����� ��������� ���
 glStencilFunc(GL_ALWAYS, 2, 0);
 glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
 glutSolidSphere(1.5, 8, 8);

 // ������� ������� ����� � �������
glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

// ��������� ���� ��������� � ������ ������� �����
glDisable(GL_STENCIL_TEST);
glColor3d(1,0,0);
glutSolidSphere(0.5, 8, 8);
//����� ���
glColor3d(0,0,1);
glutSolidTorus(0.15, 0.6, 8, 8);
// ������� ���, ���������� �� 90 �������� ������������ ������
glColor3d(0,1,0);
glPushMatrix();
glRotated(90, 1,0,0);
glutSolidTorus(0.15, 0.6, 8, 8);
glPopMatrix();

 // ��������� ���� ��������� � ������ ������� ���
 glEnable(GL_STENCIL_TEST);
 glStencilFunc(GL_EQUAL, 1, 255);
 glColor3d(1,1,1);
 glutSolidCube(2.5);

 // ������� �����
glRotated(3, 1,0,0);
glRotated(5, 0,1,0);
glRotated(7, 0,0,1);

 glutSwapBuffers();
 //sleep(0.5);
 usleep(100000);
}



void main(int argc, char **argv)
{
float pos[4] = {3,3,3,1};
float dir[3] = {-1,-1,-1};

    GLfloat mat_specular[] = {1,1,1,1};

    glutInit(&argc, argv);
    glutInitWindowSize(512,512);
    glutInitDisplayMode( GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE | GLUT_STENCIL);
    glutCreateWindow( "Glaux Template" );
    glutIdleFunc(display);
    glutReshapeFunc(resize);



    glEnable(GL_DEPTH_TEST);

    glEnable(GL_COLOR_MATERIAL);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    glLightfv(GL_LIGHT0, GL_POSITION, pos);
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, dir);

	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialf(GL_FRONT, GL_SHININESS, 128.0);

    /*
    * Enter your cod here
    */
    glutMainLoop();
}
