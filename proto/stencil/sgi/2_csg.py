import OpenGL 
OpenGL.ERROR_ON_COPY = True 
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import sys, time 
from math import sin,cos,sqrt,pi
from OpenGL.constants import GLfloat
vec4 = GLfloat_4

#### csg

# fixes up the depth buffer with A's depth values
def fixup(a):
    # fix up the depth buffer
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE)
    glEnable(GL_DEPTH_TEST)
    glDisable(GL_STENCIL_TEST)
    glDepthFunc(GL_ALWAYS)
    a()

    # reset depth func
    glDepthFunc(GL_LESS)


def inside(a, b, face, test):
    # draw A into depth buffer, but not into color buffer
    glEnable(GL_DEPTH_TEST)
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE)
    glCullFace(face)
    a()
    
    # use stencil buffer to find the parts of A that are inside of B
    # by first incrementing the stencil buffer wherever B's front faces
    # are...
    
    glDepthMask(GL_FALSE)
    glEnable(GL_STENCIL_TEST)
    glStencilFunc(GL_ALWAYS, 0, 0)
    glStencilOp(GL_KEEP, GL_KEEP, GL_INCR)
    glCullFace(GL_BACK)
    b()

    # ...then decrement the stencil buffer wherever B's back faces are
    glStencilOp(GL_KEEP, GL_KEEP, GL_DECR)
    glCullFace(GL_FRONT)
    b()

    # now draw the part of A that is inside of B
    glDepthMask(GL_TRUE)
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE)
    glStencilFunc(test, 0, 1)
    glDisable(GL_DEPTH_TEST)
    glCullFace(face)
    a()

    # reset stencil test
    glDisable(GL_STENCIL_TEST)


#  boolean A subtract B (draw wherever A is and B is NOT)
#  algorithm: find where a is inside B, then find where
#             the BACK faces of B are NOT in A
def sub(a, b):
    inside(a, b, GL_FRONT, GL_NOTEQUAL)
    #if 1  # set to 0 for faster, but incorrect results
    fixup(b)
    #endif
    inside(b, a, GL_BACK, GL_EQUAL)

def _and(a, b):
    inside(a, b, GL_BACK, GL_NOTEQUAL)
    #if 1  /* set to 0 for faster, but incorrect results */
    fixup(b)
    #endif
    inside(b, a, GL_BACK, GL_NOTEQUAL)

#### csg

YELLOWMAT = 1
BLUEMAT = 2

def init():
   yellow_diffuse = vec4(0.7, 0.7, 0.0, 1.0)
   yellow_specular = vec4(1.0, 1.0, 1.0, 1.0)

   blue_diffuse = vec4(0.1, 0.1, 0.7, 1.0)
   blue_specular = vec4(0.1, 1.0, 1.0, 1.0)

   position_one = vec4(1.0, 1.0, 1.0, 0.0)

   glNewList(YELLOWMAT, GL_COMPILE)
   glMaterialfv(GL_FRONT, GL_DIFFUSE, yellow_diffuse)
   glMaterialfv(GL_FRONT, GL_SPECULAR, yellow_specular)
   glMaterialf(GL_FRONT, GL_SHININESS, 64.0)
   glEndList()

   glNewList(BLUEMAT, GL_COMPILE)
   glMaterialfv(GL_FRONT, GL_DIFFUSE, blue_diffuse)
   glMaterialfv(GL_FRONT, GL_SPECULAR, blue_specular)
   glMaterialf(GL_FRONT, GL_SHININESS, 45.0)
   glEndList()

   glLightfv(GL_LIGHT0, GL_POSITION, position_one)
   glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE)

   glEnable(GL_LIGHT0)
   glEnable(GL_LIGHTING)
   glDepthFunc(GL_LESS)
   glEnable(GL_DEPTH_TEST)

   #glEnable(GL_COLOR_MATERIAL)
   glEnable(GL_CULL_FACE)

   glClearColor(0.1, 0.5, 0.5, 0.0)

# Draw a sphere in a diamond-shaped section in the
# middle of a window with 2 torii.

def display():
    #glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT)
   glEnable(GL_DEPTH_TEST)
   #glutSolidSphere (0.5, 15, 15)

   def bg():
       # draw bg
       glCallList (BLUEMAT)
       glPushMatrix()
       glTranslatef(0, 0, -10)
       glutSolidCube(5)
       glPopMatrix()

   def a():
      glCallList (BLUEMAT)
      glPushMatrix()
      glTranslatef(.7, 0, .4)
      glutSolidSphere (0.5, 15, 4)
      glPopMatrix()

   def b():
      glCallList (YELLOWMAT)
      glPushMatrix()
      glTranslatef(0.5, 0, 0)
      glutSolidSphere (0.5, 15, 4)
      glPopMatrix()


   bg()

   glPushMatrix()

   #_and(a, b)
   #a()
   #b()
   sub(a, b)
   #sub(b, a)
   glPopMatrix()

   """
   glClearStencil(0x0)
   glEnable(GL_STENCIL_TEST)
   glClear(GL_STENCIL_BUFFER_BIT)
   #glStencilFunc (GL_ALWAYS, 0x1, 0x1)
   #glStencilOp (GL_REPLACE, GL_REPLACE, GL_REPLACE)

   glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE)
   glDepthMask(GL_FALSE)
   # draw blue sphere where the stencil is 1
   glStencilFunc (GL_ALWAYS, 0x1, 0x1)
   #glStencilOp (GL_REPLACE, GL_REPLACE, GL_REPLACE)
   glStencilOp (GL_KEEP, GL_KEEP, GL_REPLACE)
   glCallList (BLUEMAT)
   glutSolidSphere (0.5, 15, 15)

   glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE)
   glDepthMask(GL_TRUE)
   glCallList (YELLOWMAT)
   # draw the tori where the stencil is not 1 
   glStencilFunc (GL_NOTEQUAL, 0x1, 0x1)
   glPushMatrix()
   glTranslatef(0.5, 0, 0)
   glutSolidSphere (0.5, 15, 15)
   glPopMatrix()
   """

   """
   # draw blue sphere where the stencil is 1
   glStencilFunc (GL_EQUAL, 0x1, 0x1)
   glStencilOp (GL_KEEP, GL_KEEP, GL_KEEP)
   glCallList (BLUEMAT)
   glutSolidSphere (0.5, 15, 15)

   # draw the tori where the stencil is not 1 
   glStencilFunc (GL_NOTEQUAL, 0x1, 0x1)
   glPushMatrix()
   glRotatef (45.0, 0.0, 0.0, 1.0)
   glRotatef (45.0, 0.0, 1.0, 0.0)

   glCallList (YELLOWMAT)
   glutSolidTorus (0.275, 0.85, 15, 15)
   glPushMatrix()
   glRotatef (90.0, 1.0, 0.0, 0.0)
   glutSolidTorus (0.275, 0.85, 15, 15)
   glPopMatrix()
   glPopMatrix()
   """

   #glDisable(GL_STENCIL_TEST)
   #glEnable(GL_DEPTH_TEST)

   # draw bg
   #bg()

   glutPostRedisplay()
   glutSwapBuffers()
   #print 'test'

#  Whenever the window is reshaped, redefine the 
#  coordinate system and redraw the stencil area.

def reshape(w, h):
   glViewport(0, 0, w, h)

   glMatrixMode(GL_PROJECTION)
   glLoadIdentity()
   gluPerspective(45.0, w/h, 0.1, 100)
   glMatrixMode(GL_MODELVIEW)
   glLoadIdentity()
   #glRotatef(-30,-10,-30,1)
   #glTranslatef(4.0, 0.0, -5.0)
   glTranslatef(0.0, 0.0, -5.0)

def keyboard(k, x, y):
    if ord(k) == 27: # Escape
        sys.exit(0)
    else:
        return

# Main Loop
# Be certain to request stencil bits.

def main():
   glutInit([])
   glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL)
   glutInitWindowSize (400, 400)
   glutInitWindowPosition (100, 100)
   glutCreateWindow ("test")
   init()
   glutReshapeFunc(reshape)
   glutDisplayFunc(display)
   glutKeyboardFunc(keyboard)
   glutMainLoop()

main()
