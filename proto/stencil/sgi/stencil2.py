import OpenGL 
OpenGL.ERROR_ON_COPY = True 
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import sys, time 
from math import sin,cos,sqrt,pi
from OpenGL.constants import GLfloat
vec4 = GLfloat_4


YELLOWMAT = 1
BLUEMAT = 2

def init():
   yellow_diffuse = vec4(0.7, 0.7, 0.0, 1.0)
   yellow_specular = vec4(1.0, 1.0, 1.0, 1.0)

   blue_diffuse = vec4(0.1, 0.1, 0.7, 1.0)
   blue_specular = vec4(0.1, 1.0, 1.0, 1.0)

   position_one = vec4(1.0, 1.0, 1.0, 0.0)

   glNewList(YELLOWMAT, GL_COMPILE);
   glMaterialfv(GL_FRONT, GL_DIFFUSE, yellow_diffuse);
   glMaterialfv(GL_FRONT, GL_SPECULAR, yellow_specular);
   glMaterialf(GL_FRONT, GL_SHININESS, 64.0);
   glEndList();

   glNewList(BLUEMAT, GL_COMPILE);
   glMaterialfv(GL_FRONT, GL_DIFFUSE, blue_diffuse);
   glMaterialfv(GL_FRONT, GL_SPECULAR, blue_specular);
   glMaterialf(GL_FRONT, GL_SHININESS, 45.0);
   glEndList();

   glLightfv(GL_LIGHT0, GL_POSITION, position_one);

   glEnable(GL_LIGHT0);
   glEnable(GL_LIGHTING);
   glEnable(GL_DEPTH_TEST);

# Draw a sphere in a diamond-shaped section in the
# middle of a window with 2 torii.

def display():
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   #glutSolidSphere (0.5, 15, 15);

   glClearStencil(0x0);
   glEnable(GL_STENCIL_TEST);
   glClear(GL_STENCIL_BUFFER_BIT);
   #glStencilFunc (GL_ALWAYS, 0x1, 0x1);
   #glStencilOp (GL_REPLACE, GL_REPLACE, GL_REPLACE);

   glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
   glDepthMask(GL_FALSE)
   # draw blue sphere where the stencil is 1
   glStencilFunc (GL_ALWAYS, 0x1, 0x1);
   #glStencilOp (GL_REPLACE, GL_REPLACE, GL_REPLACE);
   glStencilOp (GL_KEEP, GL_KEEP, GL_REPLACE);
   glCallList (BLUEMAT);
   glutSolidSphere (0.5, 15, 15);

   glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
   glDepthMask(GL_TRUE)
   glCallList (YELLOWMAT);
   # draw the tori where the stencil is not 1 
   glStencilFunc (GL_NOTEQUAL, 0x1, 0x1);
   glPushMatrix();
   glTranslatef(0.5, 0, 0);
   glutSolidSphere (0.5, 15, 15);
   glPopMatrix()

   """
   # draw blue sphere where the stencil is 1
   glStencilFunc (GL_EQUAL, 0x1, 0x1);
   glStencilOp (GL_KEEP, GL_KEEP, GL_KEEP);
   glCallList (BLUEMAT);
   glutSolidSphere (0.5, 15, 15);

   # draw the tori where the stencil is not 1 
   glStencilFunc (GL_NOTEQUAL, 0x1, 0x1);
   glPushMatrix();
   glRotatef (45.0, 0.0, 0.0, 1.0);
   glRotatef (45.0, 0.0, 1.0, 0.0);

   glCallList (YELLOWMAT);
   glutSolidTorus (0.275, 0.85, 15, 15);
   glPushMatrix();
   glRotatef (90.0, 1.0, 0.0, 0.0);
   glutSolidTorus (0.275, 0.85, 15, 15);
   glPopMatrix();
   glPopMatrix()
   """

   glDisable(GL_STENCIL_TEST);
   glCallList (BLUEMAT);

   glPushMatrix()
   glTranslatef(0, 0, -1);
   glutSolidCube(2)
   glPopMatrix()

   glutSwapBuffers()
   glutPostRedisplay()

#  Whenever the window is reshaped, redefine the 
#  coordinate system and redraw the stencil area.

def reshape(w, h):
   glViewport(0, 0, w, h);

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(45.0, w/h, 0.1, 100);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   glTranslatef(0.0, 0.0, -5.0);

def keyboard(k, x, y):
    if ord(k) == 27: # Escape
        sys.exit(0)
    else:
        return

# Main Loop
# Be certain to request stencil bits.

def main():
   glutInit(sys.argv);
   glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB 
                        | GLUT_DEPTH | GLUT_STENCIL);
   glutInitWindowSize (400, 400);
   glutInitWindowPosition (100, 100);
   glutCreateWindow ("test");
   init();
   glutReshapeFunc(reshape);
   glutDisplayFunc(display);
   glutKeyboardFunc(keyboard);
   glutMainLoop();

main()
