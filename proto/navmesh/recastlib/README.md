## Introduction
[![Build Status](https://secure.travis-ci.org/layzerar/recastlib.png?branch=master)](https://travis-ci.org/layzerar/recastlib)

Recastlib is a python interface for recastnavigation.

For more detail, please see https://code.google.com/p/recastnavigation.


## License
Zlib License
