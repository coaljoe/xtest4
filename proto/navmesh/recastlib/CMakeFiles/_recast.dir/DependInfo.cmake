# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/tmp/recastlib/Recast/Detour/Source/DetourAlloc.cpp" "/tmp/recastlib/CMakeFiles/_recast.dir/Recast/Detour/Source/DetourAlloc.cpp.o"
  "/tmp/recastlib/Recast/Detour/Source/DetourCommon.cpp" "/tmp/recastlib/CMakeFiles/_recast.dir/Recast/Detour/Source/DetourCommon.cpp.o"
  "/tmp/recastlib/Recast/Detour/Source/DetourNavMesh.cpp" "/tmp/recastlib/CMakeFiles/_recast.dir/Recast/Detour/Source/DetourNavMesh.cpp.o"
  "/tmp/recastlib/Recast/Detour/Source/DetourNavMeshBuilder.cpp" "/tmp/recastlib/CMakeFiles/_recast.dir/Recast/Detour/Source/DetourNavMeshBuilder.cpp.o"
  "/tmp/recastlib/Recast/Detour/Source/DetourNavMeshQuery.cpp" "/tmp/recastlib/CMakeFiles/_recast.dir/Recast/Detour/Source/DetourNavMeshQuery.cpp.o"
  "/tmp/recastlib/Recast/Detour/Source/DetourNode.cpp" "/tmp/recastlib/CMakeFiles/_recast.dir/Recast/Detour/Source/DetourNode.cpp.o"
  "/tmp/recastlib/python/detour.cpp" "/tmp/recastlib/CMakeFiles/_recast.dir/python/detour.cpp.o"
  "/tmp/recastlib/python/detour/query.cpp" "/tmp/recastlib/CMakeFiles/_recast.dir/python/detour/query.cpp.o"
  "/tmp/recastlib/python/dtmath.cpp" "/tmp/recastlib/CMakeFiles/_recast.dir/python/dtmath.cpp.o"
  "/tmp/recastlib/python/export.cpp" "/tmp/recastlib/CMakeFiles/_recast.dir/python/export.cpp.o"
  "/tmp/recastlib/python/loader.cpp" "/tmp/recastlib/CMakeFiles/_recast.dir/python/loader.cpp.o"
  "/tmp/recastlib/python/loader/sampletilemesh.cpp" "/tmp/recastlib/CMakeFiles/_recast.dir/python/loader/sampletilemesh.cpp.o"
  "/tmp/recastlib/python/loader/tilemesh.cpp" "/tmp/recastlib/CMakeFiles/_recast.dir/python/loader/tilemesh.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/python2.7"
  "Recast/DebugUtils/Include"
  "Recast/Detour/Include"
  "Recast/DetourCrowd/Include"
  "Recast/DetourTileCache/Include"
  "Recast/Recast/Include"
  "python"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
