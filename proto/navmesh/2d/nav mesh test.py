import pygame
from pygame.locals import*
pygame.init()

import time, random
from raubana_base import*


print """


   Hello! 

- Please wait while the nav mesh is being generated. After it's done,
  you can click on the map where you want to spawn an actor. 
  Then pressing in a different spot, the actor will create a path to that point.
  This is an excellent example of what can be done with non regular maps using
  a navigation mesh to optimise A* generated paths.

  A small note: this is not completly optimised, but that shows how
  using navigation meshes works all the better to imporve computing time!

- This entire demo is based off of the system built by ValvE (c) where
  a simple navigation mesh is quickly built ahead of time and saved with
  the map. Then when an npc needs to find a way to a location or object (say,
  a group of survivers :P ), it can generate an A* path, then as it moves, it
  can look ahead instead of simply following the path. This creates a fairly
  human like path and also optimises the path even more!

 
- Press the spacebar to see the actor think as he moves
 
 
- This was made by Dylan J. Raub 2010, aka Mr. Raubana


"""





##########################################################
def render_line(screen, screen_size, line, color, thickness=1):
  lines=[Line(Point(0,0),Point(0,screen_size[1])),
        Line(Point(0,0),Point(screen_size[0],0)),
        Line(Point(screen_size[0],0),Point(screen_size[0],screen_size[1])),
        Line(Point(0,screen_size[1]),Point(screen_size[0],screen_size[1]))]
  
  collides=[]
  for l in lines:
    p=line.intercect_line(l)
    if type(p)==Point:
      if p.x>=0 and p.y>=0 and p.x<=screen_size[0] and p.y<=screen_size[1]:
        no_match=True
        for p2 in collides:
          if p2.x==p.x and p2.y==p.y:
            no_match=False
        
        if no_match:
          collides.append(p)
          
  if len(collides)>=2:
    return pygame.draw.line(screen, color, [collides[0].x,collides[0].y],[collides[1].x,collides[1].y],thickness)
    

def Astar(startpos, endpos, grid, connections, world, randomise=False):
  started=time.time()
  #print startpos, endpos
  open_list=[]#pos,cost,prev_point
  closed_list=[]#pos,cost,prev_point
  closed_blocks=[]
  #start by finding the block we're starting in
  x=0
  currentBlock=None
  currentPoint=startpos
  endBlock=None
  while x<len(grid):
    if grid[x][0].collidepoint(startpos):
      currentBlock=grid[x]
    if grid[x][0].collidepoint(endpos):
      endBlock=grid[x]
    x+=1
    
  if currentBlock==None:
    if world.intercect_point(Point(startpos[0], startpos[1]), False):
      closest=None
      for b in grid:
        distance=((b[0].center[0]-startpos[0])**2+(b[0].center[1]-startpos[1])**2)**0.5
        if closest==None or closest[1]>distance:
          closest=[b,distance]
      currentBlock=closest[0]
    else:
      print "!!!The start possition's not in the world!"
      return None
      
  if endBlock==None: 
    if world.intercect_point(Point(endpos[0], endpos[1]), False):
      closest=None
      for b in grid:
        distance=((b[0].center[0]-endpos[0])**2+(b[0].center[1]-endpos[1])**2)**0.5
        if closest==None or closest[1]>distance:
          closest=[b,distance]
      endBlock=closest[0]
    else:
      print "!!!The end possition's not in the world!"
      return None
    
  if currentBlock[0]==endBlock[0]:
    return [startpos, endpos]
    
  #then put this point into the closed list
  closed_list.append([list(startpos),((startpos[0]-endpos[0])**2+(startpos[1]-endpos[1])**2)**0.5, None])
  closed_blocks.append(currentBlock)
  #next places any point on this block into the open list
  for c in connections:
    if currentBlock in c and c[2]!=startpos:
      distanceTraveled=((startpos[0]-currentPoint[0])**2+(startpos[1]-currentPoint[1])**2)**0.5
      distanceFromEnd=((c[2][0]-endpos[0])**2+(c[2][1]-endpos[1])**2)**0.5
      open_list.append([list(c[2]), distanceTraveled+distanceFromEnd, list(currentPoint)])
      #print "placing this point into the open list: ",c[2]
  #can now proceed into the loop untill a solution is found or it runs out of options
  reachedEnd=False
  run=0
  while not reachedEnd and open_list!=[]:
    #print run
    run+=1
    best=None
    for o in open_list:
      if best==None or (open_list[best][1]>o[1]):
        best = open_list.index(o)
    if best==None:
      #print "!!!couldn't find the best open one in the loop!"
      return None
    nextPoint=list(open_list[best][0])
    #at this point it has choosed which point it wants to go to, so it's time to figure out what block shares it
    #but first, lets put this point into the closed list
    closed_list.append(open_list.pop(best))
    
    #print "current block: ",currentBlock
    
    if currentBlock[0]==endBlock[0]:
      reachedEnd=True
    else: 
      #now finds the block
      #print "nextPoint=",nextPoint
      for c in connections:
        if list(nextPoint)==list(c[2]):
          if c[0] not in closed_blocks:
            currentBlock=c[0]
            #print "   Found block"
          elif c[1] not in closed_blocks:
            currentBlock=c[1]
            #print "   Found block"
          else:
            #print "ERROR! no open blocks!"
            #return None
            if not randomise:
              if c[1]==currentBlock:
                currentBlock=c[0]
              elif c[0]==currentBlock:
                currentBlock=c[1]
              else:
                pass
            else:
              if currentBlock in c:
                if random.randint(0,1)==0:
                  currentBlock=c[0]
                else:
                  currentBlock=c[1]
              else:
                pass
              
            
      closed_blocks.append(currentBlock)
      #places any point on this block (that's not already in a list) into the open list
      for c in connections:
        if currentBlock in c and list(c[2])!=list(nextPoint):
          Used=False
          for o in open_list:
            if list(c[2])==list(o[0]):
              Used=True
              #print "   Used..."
          for o in closed_list:
            if list(c[2])==list(o[0]):
              Used=True
              #print "   Used..."
          if not Used:
            #print "adding this point to the open list:", c[2]
            distanceTraveled=((c[2][0]-nextPoint[0])**2+(c[2][1]-nextPoint[1])**2)**0.5
            distanceFromEnd=((c[2][0]-endpos[0])**2+(c[2][1]-endpos[1])**2)**0.5
            open_list.append([list(c[2]), distanceTraveled+distanceFromEnd, list(nextPoint)])
      
      currentPoint=nextPoint
        
  #now creates the path (if one can be made)
  if reachedEnd:
    path=[[endpos,0,currentPoint]]
    while path[-1][2]!=None:
      for p in closed_list:
        if p[0]==path[-1][2]:
          path.append(p)
    path2=[]
    for p in path:
      path2.insert(0,p[0])
    #print "OPEN LIST:"
    #for p in open_list:
    #  print p
    #print "CLOSED LIST:"
    #for p in closed_list:
    #  print p
    #print "SOLUTION:"
    #for p in path2:
    #  print p
    print "Time: ",time.time()-started
    return path2
  else:
    #print "!!!Found no solution!"
    return None
    
        
        
 
 
class Actor(object):
  def __init__(self, world, pos):
    self.world = world
    
    self.pos=Point(pos[0], pos[1])
    self.path=[]
    self.vel=Vector(0,0)
    self.speed=3.0
    self.view_distance=250#must be at least twice the minimum of the grid
    self.current_target=None
    
    self.path_took=[[int(self.pos.x),int(self.pos.y)]]
  
  def set_path(self, path):
    #print "NEW PATH SET"
    self.path_took=[[int(self.pos.x),int(self.pos.y)]]
    if path==None:
      self.path=[]
    else:
      self.path=list(path)
    
  def update(self):
    if self.path!=[]:
      self.current_target=self.path[0]
      vel = Point(self.current_target[0], self.current_target[1])-self.pos
      vel = Vector(vel.x,vel.y)
      dist = vel.magn()
      works=True
      if dist<=self.speed*2:
        self.path.pop(0)
        #print "reached point"
        works=False
      if self.path==[]:
        #print "reached end"
        self.current_target=None
        works=False
      else:
        vel /= max(0.000001, dist)
        vel *= self.speed #speed
        self.vel = Lerp(vel, self.vel, max(0.9, min(0.99,dist/self.view_distance)))
      
      if works and self.path!=[self.current_target]:
        next_point=self.path[1]
        line=LineSegment(Point(self.pos.x,self.pos.y),Point(next_point[0], next_point[1]))
        dist2=((self.pos.x-next_point[0])**2+(self.pos.y-next_point[1])**2)**0.5
        if dist2<=self.view_distance and dist<=self.view_distance/2:
          lines=self.world.shape.get_linesegments()
          for l in lines:
            i=l.intercect_linesegment(line)
            if type(i)!=Line and i!=None and l.intercect_point(i)==True:
              works=False
          if works:
            #print "skipping to next point"
            self.path.pop(0)
          else:
            #print "Is close to next point, but can't see it yet!"
            pass
      
    else:
      self.vel=Vector(0,0)
    
    vel = Point(self.path_took[-1][0], self.path_took[-1][1])-self.pos
    vel = Vector(vel.x,vel.y)
    dist = vel.magn()
    if dist>10:
      self.path_took.append([int(self.pos.x),int(self.pos.y)])
      
  def move(self):
    self.pos+=self.vel
    
  def render(self):
    if len(self.path_took)>=2:
        self.world.rects.append(pygame.draw.lines(self.world.screen, [127,127,255],False,self.path_took, 3))
    self.world.rects.append(pygame.draw.circle(self.world.screen, [0,255,0], [int(self.pos.x), int(self.pos.y)], 5))
    if self.world.phase==9 and self.world.keys[K_SPACE]:
      if len(self.path)>=2:
        self.world.rects.append(pygame.draw.lines(self.world.screen, [127,64,0],False,self.path, 1))
      if self.current_target!=None:
        self.world.rects.append(pygame.draw.line(self.world.screen, [255,127,0],[self.pos.x, self.pos.y],self.current_target, 1))
    
    
      
    
      
      
  
  
  
  
  
  
  


class World(object):
  def __init__(self):
    #SET UP __INIT__
      self.screen_size = (800,600)
      self.screen = pygame.display.set_mode(self.screen_size)
    
      self.clock = pygame.time.Clock()
      self.framerate=30

      self.bg_color = [0,0,0]
      self.screen.fill(self.bg_color)
      
      self.font = pygame.font.Font(pygame.font.get_default_font(),10)


      #SET UP ENTITIES
      self.reset()
      self.run()
      
  def reset(self):
    l=[[Point(5,5), Point(20,5), Point(20,15), Point(30,15), Point(30,5),Point(75,5),Point(75,55),Point(5,55),Point(5,35), Point(15,40), Point(45,40), Point(45,30), Point(5,30)],
        [Point(10,10),Point(15,10),Point(15,15), Point(10,15)],
        [Point(60,30),Point(70,30),Point(65,45)]]
                        
    for pl in l:
      for x in xrange(len(pl)):
        pl[x]=pl[x]*10
    self.shape=Polygon(l)
    self.points=[]
    self.connections=[]
    self.grid_blocks=[]
    self.block_connections=[]
    self.test_scale=20
    self.max_size=100
    self.test_x=0
    self.test_y=0
    self.phase=0
    self.start_time=time.time()
    self.repeat_phase=False
    
    self.start=None
    self.end=None
    self.path=[]
    self.actor = None
    
    
    print "PHASE 0: Sample walking area..."
        
          

  def run(self):
      
      # RUN MAIN LOOP
      while True:
        self.clock.tick(self.framerate)
        self.events = pygame.event.get()
        self.keys=pygame.key.get_pressed()
        self.mouse_pos=pygame.mouse.get_pos()
        self.mouse_but = pygame.mouse.get_pressed()
        self.rects=[]
        
          
        #UPDATE
        if self.phase==0:#creates a point grid
          while self.phase==0:
            c=self.shape.intercect_point(Point(self.test_x, self.test_y), False)
            while self.phase==0 and (not self.shape.rect.collidepoint([self.test_x, self.test_y])  or not c):
              self.test_x+=self.test_scale
              if self.test_x>self.screen_size[0]:
                self.test_x=0
                self.test_y+=self.test_scale
                if self.test_y>self.screen_size[1]:
                  self.phase=1
                  self.test_x=0
                  self.test_y=0
                  print "PHASE 1: Connecting points..." 
              c=self.shape.intercect_point(Point(self.test_x, self.test_y), False)
            
            if c:
              self.points.append(Point(self.test_x, self.test_y))
              self.points[-1].connected_to=[]
              
              self.test_x+=self.test_scale
              if self.test_x>self.screen_size[0]:
                self.test_x=0
                self.test_y+=self.test_scale
                if self.test_y>self.screen_size[1]:
                  self.phase=1
                  self.test_x=0
                  self.test_y=0
                  print "PHASE 1: Connecting points..."  
          
          
              
        elif self.phase==1:#connnects points that are next to each other
          while self.test_x<len(self.points):
            p=self.points[self.test_x]
            for p2 in self.points:
              if p2!=p and (abs(p.x-p2.x)==self.test_scale and abs(p.y-p2.y)==0) or (abs(p.x-p2.x)==0 and abs(p.y-p2.y)==self.test_scale):
                if [p,p2] not in self.connections and [p2,p] not in self.connections:
                  works=True
                  line=LineSegment(p,p2)
                  lines=self.shape.get_linesegments()
                  for l in lines:
                    i=l.intercect_linesegment(line)
                    if type(i)!=Line and i!=None and l.intercect_point(i)==False:
                      works=False
                  if works:
                    self.connections.append([p,p2])
                    p.connected_to.append(p2)
                    p2.connected_to.append(p)
            
            self.test_x+=1
          self.phase=2
          self.test_x=0
          print "PHASE 2: Creating blocks from closed areas..."
            
            
        elif self.phase==2:#creates blocks from closed areas
          while self.phase==2:
            topleft=self.points[self.test_x]
            topright=None
            bottomright=None
            bottomleft=None
            for p in topleft.connected_to:
              if float(p.x) == float(topleft.x+self.test_scale) and float(p.y) == float(topleft.y):
                topright=p
            if topright!=None:
              for p in topright.connected_to:
                if float(p.x) == float(topleft.x+self.test_scale) and float(p.y) == float(topleft.y+self.test_scale):
                  bottomright=p
            if bottomright!=None:
              for p in bottomright.connected_to:
                if float(p.x) == float(topleft.x) and float(p.y) == float(topleft.y+self.test_scale):
                  bottomleft=p
            if bottomleft!=None:#this could be made into a rect
              works=True
              rect = [pygame.Rect([topleft.x,topleft.y, bottomright.x-topleft.x, bottomright.y-topleft.y]),0]
              for pl in self.shape.points:
                for p in pl:
                  if rect[0].collidepoint([p.x,p.y]):
                    works=False
              if works:
                self.grid_blocks.append(rect)
                p=self.points.pop(self.test_x)
                for c in self.connections:
                  if p in c:
                    self.connections.remove(c)
                for p2 in self.points:
                  if p in p2.connected_to:
                    p2.connected_to.remove(p)
                self.test_x-=1
            self.test_x+=1
            if self.test_x>=len(self.points):
              self.phase=3
              self.test_x=0
              print "PHASE 3: removing nodes, numbering blocks based on the number of neighbors..."
        
        elif self.phase==3:#removes all the node crap and finds the number of neighbors of each block
          self.points=[]
          self.connections=[]
          self.outer_blocks=[]
          self.inner_blocks=[]
          self.tinny_blocks=[]
          grid_blocks=[]
          
          while self.test_x<len(self.grid_blocks):
            b=self.grid_blocks[self.test_x]
            neighbors=0
            for b2 in self.grid_blocks:
              if b!=b2:
                if (b[0].top>=b2[0].top and b[0].bottom<=b2[0].bottom and b[0].right==b2[0].left) or (b[0].top>=b2[0].top and b[0].bottom<=b2[0].bottom and b[0].left==b2[0].right) or (b[0].left>=b2[0].left and b[0].right<=b2[0].right and b[0].top==b2[0].bottom) or (b[0].left>=b2[0].left and b[0].right<=b2[0].right and b[0].bottom==b2[0].top):
                  neighbors+=1
                elif (b2[0].top>=b[0].top and b2[0].bottom<=b[0].bottom and b[0].right==b2[0].left) or (b2[0].top>=b[0].top and b2[0].bottom<=b[0].bottom and b[0].left==b2[0].right) or (b2[0].left>=b[0].left and b2[0].right<=b[0].right and b[0].top==b2[0].bottom) or (b2[0].left>=b[0].left and b2[0].right<=b[0].right and b[0].bottom==b2[0].top):
                  neighbors+=1
            b[1]=neighbors
            if neighbors==4:
              self.inner_blocks.append(b)
            elif neighbors==3:
              self.outer_blocks.append(b)
            else:
              grid_blocks.append(b)
            self.test_x+=1
          self.phase=4
          self.test_x=0
          self.test_y=0
          self.grid_blocks=grid_blocks
          print "PHASE 4: Finding all ways to optimise... (This phase may take a couple minutes)"
          
        elif self.phase==4:
          best=None
          while self.phase==4:
            while self.test_x<len(self.outer_blocks):
              while self.test_y<len(self.outer_blocks):
                b=self.outer_blocks[self.test_x]
                b2=self.outer_blocks[self.test_y]
                if self.test_x!=self.test_y and b[0].top<=b2[0].top and b[0].bottom<=b2[0].bottom:
                  if (b[0].top==b2[0].top and b[0].bottom==b2[0].bottom and b[0].right==b2[0].left) or (b[0].left==b2[0].left and b[0].right==b2[0].right and b[0].bottom==b2[0].top):
                    if best==None or (b[1]<=b2[1] and b[1]<=best[0][1] and b2[1]<=best[1][1]):
                      best = [b,b2]
                self.test_y+=1
              self.test_y=0
              self.test_x+=1
              #print self.test_x
              
            self.test_x=0
            self.test_y=0
            
            if best==None:
              self.phase=5
              self.test_x=0
              self.test_y=0
              print "PHASE 5: ..."
            else:
              rect = pygame.Rect([best[0][0].left,best[0][0].top, best[1][0].right-best[0][0].left, best[1][0].bottom-best[0][0].top])
              best[0][0]=rect
              self.outer_blocks.remove(best[1])
              #self.phase=4
              self.test_x=0
              self.test_y=0
              best=None
            
        elif self.phase==5:#looks at all the ways it can simplify the grid
          while self.test_x<len(self.inner_blocks):
            while self.test_y<len(self.inner_blocks):
              b=self.inner_blocks[self.test_x]
              b2=self.inner_blocks[self.test_y]
              if self.test_x!=self.test_y and b[0].top<=b2[0].top and b[0].bottom<=b2[0].bottom:
                if b[0].top==b2[0].top and b[0].bottom==b2[0].bottom and b[0].right==b2[0].left:
                  rect = pygame.Rect([b[0].left,b[0].top, b2[0].right-b[0].left, b2[0].bottom-b[0].top])
                  #self.test_new_blocks.append(rect)
                  b[0]=rect
                  self.inner_blocks.pop(self.test_y)
                  self.test_x=0
                  self.test_y=-1
                elif b[0].left==b2[0].left and b[0].right==b2[0].right and b[0].bottom==b2[0].top:
                  rect = pygame.Rect([b[0].left,b[0].top, b2[0].right-b[0].left, b2[0].bottom-b[0].top])
                  #self.test_new_blocks.append(rect)
                  b[0]=rect
                  self.inner_blocks.pop(self.test_y)
                  self.test_x=0
                  self.test_y=-1
              self.test_y+=1
            self.test_y=0
            self.test_x+=1
          self.test_x=0
          self.test_y=0
          self.phase=6
          for b in self.inner_blocks:
            self.grid_blocks.append(b)
          for b in self.outer_blocks:
            self.grid_blocks.append(b)
          
          self.inner_blocks=[]
          self.outer_blocks=[]
          
          print "PHASE 6: ..."
          
        elif self.phase==6:
          while self.test_x<len(self.grid_blocks):
            while self.test_y<len(self.grid_blocks):
              b=self.grid_blocks[self.test_x]
              b2=self.grid_blocks[self.test_y]
              if self.test_x!=self.test_y and b[0].top<=b2[0].top and b[0].bottom<=b2[0].bottom:
                if b[0].top==b2[0].top and b[0].bottom==b2[0].bottom and b[0].right==b2[0].left:
                  rect = pygame.Rect([b[0].left,b[0].top, b2[0].right-b[0].left, b2[0].bottom-b[0].top])
                  #self.test_new_blocks.append(rect)
                  b[0]=rect
                  self.grid_blocks.pop(self.test_y)
                  self.test_x=0
                  self.test_y=-1
                elif b[0].left==b2[0].left and b[0].right==b2[0].right and b[0].bottom==b2[0].top:
                  rect = pygame.Rect([b[0].left,b[0].top, b2[0].right-b[0].left, b2[0].bottom-b[0].top])
                  #self.test_new_blocks.append(rect)
                  b[0]=rect
                  self.grid_blocks.pop(self.test_y)
                  self.test_x=0
                  self.test_y=-1
              self.test_y+=1
            self.test_y=0
            self.test_x+=1
          self.test_x=0
          self.test_y=0
          self.phase=7
          print "PHASE 7: Splicing any blocks that are too big"
        
        elif self.phase==7:
          while self.test_x<len(self.grid_blocks):
            
            b=self.grid_blocks[self.test_x]
            if b[0].width>=self.max_size*2:
              slices=b[0].width/self.max_size
              for x in xrange(slices):
                size=float(b[0].width)
                p1=x/float(slices)
                p2=(x+1)/float(slices)
                x1=(int(p1*size+b[0].left)/self.test_scale)*self.test_scale
                x2=(int(p2*size+b[0].left)/self.test_scale)*self.test_scale
                rect = pygame.Rect([x1,b[0].top, x2-x1, b[0].height])
                self.grid_blocks.append([rect,int(b[1])])
              self.test_x=-1
              self.grid_blocks.remove(b)
            elif b[0].height>=self.max_size*2:
              slices=b[0].height/self.max_size
              for x in xrange(slices):
                size=float(b[0].height)
                p1=x/float(slices)
                p2=(x+1)/float(slices)
                y1=(int(p1*size+b[0].top)/self.test_scale)*self.test_scale
                y2=(int(p2*size+b[0].top)/self.test_scale)*self.test_scale
                rect = pygame.Rect([b[0].left,y1, b[0].width, y2-y1])
                self.grid_blocks.append([rect,int(b[1])])
              self.test_x=-1
              self.grid_blocks.remove(b)
            self.test_x+=1
            
          self.test_x=0
          self.phase=8
          print "PHASE 8: Creating connections between neighbor blocks..."
          
        elif self.phase==8:
          for b in self.grid_blocks:
            for b2 in self.grid_blocks:
              if b[0].right==b2[0].left:
                if b[0].top>=b2[0].top and b[0].bottom<=b2[0].bottom:
                  #print "a1", b[0] ,b2[0]
                  self.block_connections.append([b,b2,list(b[0].midright)])
                elif b[0].top<=b2[0].top and b[0].bottom>=b2[0].bottom:
                  #print "a2", b[0] ,b2[0]
                  self.block_connections.append([b,b2,list(b2[0].midleft)])
                elif b[0].top>b2[0].top and b[0].bottom>b2[0].bottom and b[0].bottom<b2[0].top:
                  #print "b1", b[0] ,b2[0]
                  self.block_connections.append([b,b2,[b[0].right,(b[0].top+b2[0].bottom)/2]])
                elif b[0].top<b2[0].top and b[0].bottom<b2[0].bottom and b[0].top>b2[0].bottom:
                  #print "b2", b[0] ,b2[0]
                  self.block_connections.append([b,b2,[b[0].right,(b[0].bottom+b2[0].top)/2]])
              elif b[0].bottom==b2[0].top:
                if b[0].left>=b2[0].left and b[0].right<=b2[0].right:
                  #print "a1", b[0] ,b2[0]
                  self.block_connections.append([b,b2,list(b[0].midbottom)])
                elif b[0].left<=b2[0].left and b[0].right>=b2[0].right:
                  #print "a2", b[0] ,b2[0]
                  self.block_connections.append([b,b2,list(b2[0].midtop)])
                elif b[0].left>b2[0].left and b[0].right>b2[0].right and b[0].left<b2[0].right:
                  #print "b1", b[0] ,b2[0]
                  self.block_connections.append([b,b2,[(b[0].left+b2[0].right)/2,b[0].bottom]])
                elif b[0].left<b2[0].left and b[0].right<b2[0].right and b[0].right>b2[0].left:
                  #print "b2", b[0] ,b2[0]
                  self.block_connections.append([b,b2,[(b[0].right+b2[0].left)/2,b[0].bottom]])
                
                
                
          self.phase=9
          
          
          print "Done."
          print
          print "Total time: ",int(time.time()-self.start_time),"seconds"
          
        if self.actor!=None:
          self.actor.update()
          
        if self.phase==9:
          for event in self.events:
            if event.type==MOUSEBUTTONDOWN:
              if self.actor==None and self.shape.intercect_point(Point(self.mouse_pos[0],self.mouse_pos[1])):
                self.start=self.mouse_pos
                self.actor = Actor(self, self.start)
              else:
                self.start=[self.actor.pos.x, self.actor.pos.y]
                self.end=self.mouse_pos
                self.path=Astar(self.start, self.end, self.grid_blocks, self.block_connections, self.shape)
                self.actor.set_path(self.path)
              
          if self.actor!=None:
            self.actor.move()
              
        
        """
                else:
                  rect = pygame.Rect([b[0].left,b[0].top, b2[0].right-b[0].left, b2[0].bottom-b[0].top])
                  area=rect.width*rect.height-(b[0].width*b[0].height+b2[0].width*b2[0].height)
                  toBeRemoved=[b2]
                  for b3 in self.grid_blocks:
                    if b3!=b and b3!=b2 and b3[0].colliderect(rect) and b3[0].left>=b[0].left and b3[0].top>=b[0].top and b3[0].right<=b2[0].right and b3[0].bottom<=b2[0].bottom:
                      toBeRemoved.append(b3)
                      area-=b3[0].width*b3[0].height
                  if area==0:
                    self.test_new_blocks.append(rect)
                    x=0
                    while x < len(self.grid_blocks):
                      b3 = self.grid_blocks[x]
                      if b3 in toBeRemoved:
                        self.grid_blocks.pop(x)
                        x-=1
                      x+=1
                    b[0]=rect
                    self.test_x=-1
                    self.test_y=0  
        """           
        """    
        if self.phase==5:#looks at every combination and finds the best
          while True:
            if binary!="0"*(len(self.test_new_blocks)-1)+"2":
              #print binary
              test_blocks=[]
              for x in xrange(len(self.test_new_blocks)):
                if binary[x]=="1":
                  test_blocks.append(self.test_new_blocks[x])
              works=True#tests to find any of this group of blocks that run over others
              for t in test_blocks:
                for t2 in test_blocks:
                  if t!=t2 and t.colliderect(t2): 
                    works=False
              if works: 
                for b in self.grid_blocks:#finds what blocks from the original grid arn't affected and puts them back into play
                  works=True
                  for t in test_blocks:
                    if b[0].colliderect(t):
                      works=False
                  if works:
                    test_blocks.append(pygame.Rect(b[0]))
                  else:
                    pass
                if len(test_blocks)<best:#compares this example to the best so far
                  best = list(test_blocks)
                  break
            else:
              self.phase=6
              self.test_grid_blocks=[]
              self.grid_blocks=[]
              self.final_blocks=best
              print "Done!"
            self.final_blocks=best
            #adds one to the binary string
            x=0
            binary=str(int(binary[0])+1)+binary[1:]
            while x<len(binary):
              if binary[x]=="2" and x<len(binary)-1:
                binary=binary[:x]+"0"+binary[x+1:]
                binary=binary[:x+1]+str(int(binary[x+1])+1)+binary[x+2:]
              x+=1
          #adds one to the binary string
          x=0
          binary=str(int(binary[0])+1)+binary[1:]
          while x<len(binary):
            if binary[x]=="2" and x<len(binary)-1:
              binary=binary[:x]+"0"+binary[x+1:]
              binary=binary[:x+1]+str(int(binary[x+1])+1)+binary[x+2:]
            x+=1
        """
          
          
          
        #RENDER
        for point_list in self.shape.points:
          for x in xrange(len(point_list)):
            if x!=len(point_list)-1:
              p1=[point_list[x].x, point_list[x].y]
              p2=[point_list[x+1].x, point_list[x+1].y]
              self.rects.append(pygame.draw.line(self.screen, [255,255,255], p1, p2, 3))
            else:
              p1=[point_list[x].x, point_list[x].y]
              p2=[point_list[0].x, point_list[0].y]
              self.rects.append(pygame.draw.line(self.screen, [255,255,255], p1, p2, 3))
        
        if self.phase!=9 or (self.keys[K_SPACE]):
          for b in self.grid_blocks:
            self.rects.append(pygame.draw.rect(self.screen, [32,32,32], b[0]))
            pygame.draw.rect(self.screen, [0,0,0], b[0], 1)
            self.screen.blit(self.font.render(str(b[1]), True, [255,255,255]), b[0].topleft)
          for c in self.block_connections:
            self.rects.append(pygame.draw.circle(self.screen, [127,0,0], c[2], 3))
            
        if self.phase==4:   
          for b in self.outer_blocks:
            self.rects.append(pygame.draw.rect(self.screen, [127,127,255], b[0]))
            pygame.draw.rect(self.screen, [0,0,0], b[0], 1)
            self.screen.blit(self.font.render(str(b[1]), True, [255,255,255]), b[0].topleft)
            
        if self.phase==5:   
          for b in self.inner_blocks:
            self.rects.append(pygame.draw.rect(self.screen, [127,127,255], b[0]))
            pygame.draw.rect(self.screen, [0,0,0], b[0], 1)
            self.screen.blit(self.font.render(str(b[1]), True, [255,255,255]), b[0].topleft)
            
        img=self.font.render(str(self.mouse_pos), True, [255,255,255])
        rect = img.get_rect(topleft=[0,0])
        self.rects.append(rect)
        self.screen.blit(img, rect)
        
        img=self.font.render("fps: "+str(int(self.clock.get_fps())), True, [255,255,255])
        rect = img.get_rect(topright=[self.screen_size[0],0])
        self.rects.append(rect)
        self.screen.blit(img, rect)
          
        for c in self.connections:
          self.rects.append(pygame.draw.line(self.screen, [0,127,255], [c[0].x,c[0].y], [c[1].x,c[1].y],1))
          
        for p in self.points:
          self.rects.append(pygame.draw.circle(self.screen, [0,0,255], [int(p.x),int(p.y)], 2))
             
        if self.phase==0:
          self.rects.append(pygame.draw.circle(self.screen, [255,255,255], [self.test_x,self.test_y], 3))
          
        if self.phase==2:
          p=self.points[self.test_x]
          self.rects.append(pygame.draw.rect(self.screen, [255,255,255], [p.x,p.y,self.test_scale, self.test_scale], 1))
        
          
          
          
        if self.shape.intercect_point(Point(self.mouse_pos[0], self.mouse_pos[1])):
          self.rects.append(pygame.draw.circle(self.screen, [127,0,127], self.mouse_pos, 3,1))
          
        
        if self.start!=None:
          start = [int(x) for x in self.start]
          self.rects.append(pygame.draw.circle(self.screen, [255,255,0], start, 3))
        if self.end!=None:
          end = [int(x) for x in self.start]
          self.rects.append(pygame.draw.circle(self.screen, [255,0,0], end, 3))
        if self.actor!=None:
          self.actor.render()
         
          
        
        
        

        pygame.display.flip()

        for rect in self.rects:
          self.screen.fill(self.bg_color, rect)


        #EVENT HANDLER UPDATE
        for event in self.events:
          if event.type==KEYDOWN or event.type==QUIT:
            if event.type==QUIT or event.key==K_ESCAPE:
                pygame.quit()
                return


world = World()

    
    
