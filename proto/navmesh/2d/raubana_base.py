import pygame

class Point(object):
  def __init__(self,x,y):
    self.x=float(x)
    self.y=float(y)
    
  def __str__(self):
    return "Point: "+str(self.x)+","+str(self.y)
    
  def __list__(self):
    return [self.x,self.y]
    
  def __add__(self,a):
    t=type(a)
    if t==float or t==int:
      return Point(self.x+a,self.y+a)
    elif t==Vector:
      return Point(self.x+a.x,self.y+a.y)
    elif t==Point:
      return Point(self.x+a.x,self.y+a.y)
    else:
      raise "TypeError! Couldn't add type "+str(t)+" to Point!"
      
  def __sub__(self,a):
    t=type(a)
    if t==float or t==int:
      return Point(self.x-a,self.y-a)
    elif t==Vector:
      return Point(self.x-a.x,self.y-a.y)
    elif t==Point:
      return Vector(self.x-a.x,self.y-a.y)
    else:
      raise "TypeError! Couldn't subtract type "+str(t)+" from Point!"
      
  def __mul__(self,a):
    t=type(a)
    if t==float or t==int:
      return Point(self.x*a,self.y*a)
    elif t==Vector:
      raise "TypeError! Couldn't multiply type "+str(t)+" to Point!"
    elif t==Point:
      raise "TypeError! Couldn't multiply type "+str(t)+" to Point!"
    else:
      raise "TypeError! Couldn't multiply type "+str(t)+" to Point!"
  
  def __div__(self,a):
    t=type(a)
    if t==float or t==int:
      return Point(self.x/a,self.y/a)
    elif t==Vector:
      raise "TypeError! Couldn't divide type "+str(t)+" from Point!"
    elif t==Point:
      raise "TypeError! Couldn't divide type "+str(t)+" from Point!"
    else:
      raise "TypeError! Couldn't divide type "+str(t)+" from Point!"
      
  def magn(self, point):
    import math
    x=point.x-self.x
    y=point.y-self.y
    return math.sqrt((x**2)+(y**2))
    
      
    
class Vector(object):
  def __init__(self,x,y):
    self.x=float(x)
    self.y=float(y)
    
  def __str__(self):
    return "Vector: "+str(self.x)+","+str(self.y)
    
  def __list__(self):
    return [self.x,self.y]
  
  def __add__(self,a):
    t=type(a)
    if t==float or t==int:
      return Vector(self.x+a,self.y+a)
    elif t==Vector:
      return Vector(self.x+a.x,self.y+a.y)
    elif t==Point:
      return Point(self.x+a.x,self.y+a.y)
    else:
      raise "TypeError! Couldn't add type "+str(t)+" to Vector!"
      
  def __sub__(self,a):
    t=type(a)
    if t==float or t==int:
      return Vector(self.x-a,self.y-a)
    elif t==Vector:
      return Vector(self.x-a.x,self.y-a.y)
    elif t==Point:
      raise "TypeError! Couldn't subtract type "+str(t)+" from Vector!"
    else:
      raise "TypeError! Couldn't subtract type "+str(t)+" from Vector!"
      
  def __mul__(self,a):
    t=type(a)
    if t==float or t==int:
      return Vector(self.x*a,self.y*a)
    elif t==Vector:
      raise "TypeError! Couldn't multiply type "+str(t)+" to Vector!"
    elif t==Point:
      raise "TypeError! Couldn't multiply type "+str(t)+" to Vector!"
    else:
      raise "TypeError! Couldn't multiply type "+str(t)+" to Vector!"
      
  def __div__(self,a):
    t=type(a)
    if t==float or t==int:
      return Vector(self.x/a,self.y/a)
    elif t==Vector:
      raise "TypeError! Couldn't divide type "+str(t)+" from Vector!"
    elif t==Point:
      raise "TypeError! Couldn't divide type "+str(t)+" from Vector!"
    else:
      raise "TypeError! Couldn't divide type "+str(t)+" from Vector!"
      
  def normalize(self):
    magn = self.magn()
    if magn!=0:
      self.x/=magn
      self.y/=magn
      
  def magn(self):
    import math
    return math.sqrt((self.x**2)+(self.y**2))
    
    

class Line(object):
  def __init__(self, point1, point2):
    if point1!=point2:
      self.points=[point1,point2]
      if point1.x==point2.x:
        self.vertical=True
      else:
        self.vertical=False
        
      if point1.y==point2.y:
        self.horizontal=True
      else:
        self.horizontal=False
        
      if not self.vertical:
        self.slant_height=float(self.points[0].y-self.points[1].y)/(self.points[0].x-self.points[1].x)
        self.y_int=self.points[0].y-self.slant_height*float(self.points[0].x)
    else:
      raise "Error! The two points given are the same!"
      
  def intercect_point(self, point):
    if not(self.vertical or self.horizontal):
      y=self.slant_height*point.x+self.y_int
      if y==point.y:
        return True
      else:
        return False
    elif self.vertical:
      if point.x==self.points[0].x:
        return True
      else:
        return False
    elif self.horizontal:
      if point.y==self.points[0].y:
        return True
      else:
        return False
      
  def intercect_line(self, line):
    #formula one
    if not (self.horizontal or self.vertical) and not (line.horizontal or line.vertical):
      if self.slant_height==line.slant_height:
        if self.y_int==line.y_int:
          return Line(self.points[0], self.points[1])
        else:
          return None
      else:
        if self.slant_height>line.slant_height:
          m= self.slant_height-line.slant_height
          x= line.y_int-self.y_int
        else:
          m= line.slant_height-self.slant_height
          x= self.y_int-line.y_int
        x=x/m
        y=self.slant_height*x+self.y_int
        return Point(x,y)
  
    #formula two
    if (self.horizontal and line.vertical) or (self.vertical and line.horizontal):
      if self.horizontal:
        y=self.points[0].y
        x=line.points[0].x
      else:
        y=line.points[0].y
        x=self.points[0].x
        
      return Point(x,y)
      
    #formula three
    if self.vertical and line.vertical:
      if self.points[0].x==line.points[0].x:
        return Line(self.points[0], self.points[1])
      else:
        return None
        
    #formula four
    if self.horizontal and line.horizontal:
      if self.points[0].y==line.points[0].y:
        return Line(self.points[0], self.points[1])
      else:
        return None
     
    #formula five
    if (self.vertical and not (line.vertical or line.horizontal)) or (line.vertical and not (self.vertical or self.horizontal)):
      if self.vertical:
        vert = self
        other = line
      else:
        vert = line
        other = self
        
      y=other.slant_height*vert.points[0].x+other.y_int
      return Point(vert.points[0].x, y)
   
    #formula six
    if (self.horizontal and not (line.vertical or line.horizontal)) or (line.horizontal and not (self.vertical or self.horizontal)):
      if self.horizontal:
        hori = self
        other = line
      else:
        hori = line
        other = self
        
      x=(hori.points[0].y-other.y_int)/other.slant_height
      return Point(x, hori.points[0].y)
      
    #if the program gets to this point, a problem occured...
    raise "Error in the code! Tell programmer!"
    
    
class LineSegment(object):
  def __init__(self, point1, point2):
    line=Line(point1, point2)
    self.line=line
    self.points=line.points
    self.vertical = line.vertical
    self.horizontal = line.horizontal
    if not self.vertical:
      self.slant_height=line.slant_height
      self.y_int=line.y_int
      
  
  def intercect_point(self, point):
    intercect = self.line.intercect_point(point)
    intX=point.x
    intY=point.y
    
    if intercect:
      MinX=self.points[0].x
      MaxX=self.points[0].x
      MinY=self.points[0].y
      MaxY=self.points[0].y
      
      if self.points[1].x<MinX:MinX=self.points[1].x
      if self.points[1].x>MaxX:MaxX=self.points[1].x
      if self.points[1].y<MinY:MinY=self.points[1].y
      if self.points[1].y>MaxY:MaxY=self.points[1].y
      
      if intX>=MinX and intX<=MaxX and intY>=MinY and intY<=MaxY:
        return True
      else:
        return False
        
    
    
  def intercect_linesegment(self, lineseg):
    intercect = self.line.intercect_line(lineseg.line)
    works=True
    
    if type(intercect)==Line:
      return self.line
    
    if type(intercect)==Point:
      intX=intercect.x
      intY=intercect.y
      for seg in [self, lineseg]:
        MinX=seg.points[0].x
        MaxX=seg.points[0].x
        MinY=seg.points[0].y
        MaxY=seg.points[0].y
        
        if seg.points[1].x<MinX:MinX=seg.points[1].x
        if seg.points[1].x>MaxX:MaxX=seg.points[1].x
        if seg.points[1].y<MinY:MinY=seg.points[1].y
        if seg.points[1].y>MaxY:MaxY=seg.points[1].y
        
        if intX>=MinX and intX<=MaxX and intY>=MinY and intY<=MaxY:
          pass
        else:
          works=False
    
      if works:
        return intercect
      
      

class Polygon(object):
  def __init__(self, point_lists):
    self.points=point_lists#it's important to remember that these are multiple lists, in case the polygon is hollow
    MinX=self.points[0][0].x
    MaxX=self.points[0][0].x
    MinY=self.points[0][0].y
    MaxY=self.points[0][0].y
    
    for point_list in self.points:
      for p in point_list:
        if p.x<MinX:MinX=p.x
        if p.x>MaxX:MaxX=p.x
        if p.y<MinY:MinY=p.y
        if p.y>MaxY:MaxY=p.y
        
    self.rect = pygame.Rect([MinX, MinY, MaxX-MinX, MaxY-MinY])
    
  def get_linesegments(self):
    lines=[]
    for point_list in self.points:
      for x in xrange(len(point_list)):
        if x!=len(point_list)-1:
          lines.append(LineSegment(point_list[x],point_list[x+1]))
        else:
          lines.append(LineSegment(point_list[x],point_list[0]))
    return lines
    
  
  def intercect_point(self, point, use_edge=True):
    if not self.rect.collidepoint([point.x, point.y]):
      return False
      
    lines=self.get_linesegments()
        
      
    for l in lines:
      if l.intercect_point(point):
        return use_edge
      
    L=LineSegment(point, Point(self.rect.right, point.y))
    
    nodes=[]
    
    for l in lines:
      collide = L.intercect_linesegment(l)
      if type(collide)!=None:
        if type(collide)==Point:
          skip = False
          if float(collide.x)==float(l.points[0].x) and float(collide.y)==float(l.points[0].y):
            p=l.points[1]
          elif float(collide.x)==float(l.points[1].x) and float(collide.y)==float(l.points[1].y):
            p=l.points[0]
          else:
            nodes.append(collide)
            skip=True
            
          if not skip:
            if p.y<point.y and collide.x>=point.x:
              nodes.append(collide)
            else:
              pass
        elif type(collide)==Line:
          pass
    
    return (len(nodes)%2)==1

          
    
      
    
    
    
    
    
    
    



def Lerp(a, b, p):
  return(a+(b-a)*p)
      

