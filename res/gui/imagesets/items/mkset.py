#!/usr/bin/env python2
import os
from glob import glob
txt = """\
<Imageset autoScaled="" imagefile="items/_mont.png" name="items" nativeHorzRes="1024" nativeVertRes="576" version="2">
%s
</Imageset>\
"""
#<Image height="64" name="bottle" width="64" xPos="64" yPos="0" />

files = list(glob('inv_*.png'))
files.sort()
lines = []
i = 0
for f in files:
    if not f.startswith('inv_'):
        continue
    name = f.replace('inv_', '').replace('.png', '')
    s = """    <Image height="64" name="%s" width="64" xPos="%s" yPos="0" />""" % (name, i * 64)
    #print s
    #print f
    lines.append(s)
    i += 1

print txt % "\n".join(lines)
