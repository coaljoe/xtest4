struct Material {
    vec3 diffuse;
    vec3 specular; // .1, .1, .1
    float hardness; // 50
};
struct Light {
    float intensity; // 1.0
};

uniform Material mat;
uniform Light light; // sun
uniform bool use_shadows = false;
const vec3 ambient = vec3(0.1, 0.1, 0.1);
const float diffuse_intensity = 1.0;
const float specular_intensity = 1.0;
const float shadow_intensity = 0.4;
const int MAX_LIGHTS = 8;

varying vec3 L; // light position
varying vec3 N;
varying vec3 V;
varying vec3 H;
varying vec4 position_in_object_coordinates;

#ifdef _TEXTURE_MAP
uniform sampler2D texMap;
//varying vec2 UV;
#endif

#ifdef _NORMAL_MAP
uniform sampler2D normalMap;
//varying vec2 UV;
#endif

// shadows
uniform sampler2D shadowMap;
varying vec4 shadowCoord;

void main() {
    vec3 l = normalize(L); // light vec
    vec3 n = normalize(N); // normal vec
    vec3 v = normalize(-V); // view vec (eyeVec)
    vec3 h = normalize(L + V);  // half vec
    //vec3 h = normalize(H);

    // out
    vec3 color;
    vec3 diffuse = vec3(0.0, 0.0, 0.0);
    vec3 specular = vec3(0.0, 0.0, 0.0);

#ifdef _NORMAL_MAP
    n = texture2D(normalMap, gl_TexCoord[0].st).rgb * 2.0 - 1.0;
#endif

    float NdotL = max(dot(n,l), 0.0);
    float NdotH = max(dot(n,h), 0.0);

    //if (NdotL > 0.0) {
        diffuse = mat.diffuse * light.intensity * NdotL;
        //diffuse = clamp(diffuse, 0.0, 1.0);

        specular = mat.specular * pow(NdotH, mat.hardness); // 1.0
        //specular = mat.specular * pow(NdotH, mat.hardness) * (NdotL > 0.0 ? 1.0 : 0.0);
        //specular = clamp(specular, 0.0, 1.0);
    //}

    color = ambient + diffuse * diffuse_intensity + specular * specular_intensity;

#ifdef _TEXTURE_MAP
    color *= texture2D(texMap, gl_TexCoord[0].st).rgb;
#endif

if (use_shadows) {
    float shadow = 1.0;
    float bias  = 0.005;
    if (texture2D( shadowMap, shadowCoord.xy ).z  <  shadowCoord.z-bias) {
        shadow = 1.0 - shadow_intensity;
    }
    color *= shadow;
}

    //if (color.b < 0.1) discard;
    //if (color.rgb == vec3(1,1,1)) discard;
    //if (color.r > 0.2) discard;

    if (position_in_object_coordinates.z > 0.0) 
    {
       discard; // stop processing the fragment 
          // if y coordinate is positive
    }
    if (gl_FrontFacing) // are we looking at a front face?
    {
       gl_FragColor = vec4(0.0, 1.0, 0.0, 1.0); // yes: green
    }
    else
    {
       gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0); // no: red
    }

    gl_FragColor.rgb = color;
}

/* vim:set ft=glsl: */
