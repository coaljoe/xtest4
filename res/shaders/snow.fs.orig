struct Material {
    vec3 diffuse;
    vec3 specular; // .1, .1, .1
    float hardness; // 50
};
struct Light {
    float intensity; // 1.0
};

uniform Material mat;
uniform Light light; // sun
uniform bool use_shadows = false;
const vec3 ambient = vec3(0.1, 0.1, 0.1);
const float diffuse_intensity = 1.0;
const float specular_intensity = 1.0;
const float shadow_intensity = 0.4;
const int MAX_LIGHTS = 8;

varying vec3 L; // light position
varying vec3 N;
varying vec3 V;
varying vec3 H;

#ifdef _TEXTURE_MAP
uniform sampler2D texMap;
//varying vec2 UV;
#endif

#ifdef _NORMAL_MAP
uniform sampler2D normalMap;
//varying vec2 UV;
#endif

// shadows
uniform sampler2D shadowMap;
varying vec4 shadowCoord;

void main() {
    vec3 l = normalize(L); // light vec
    vec3 n = normalize(N); // normal vec
    vec3 v = normalize(-V); // view vec (eyeVec)
    vec3 h = normalize(L + V);  // half vec
    //vec3 h = normalize(H);

    // out
    vec3 color;

#ifdef _NORMAL_MAP
    n = texture2D(normalMap, gl_TexCoord[2].st).rgb * 2.0 - 1.0;
#endif

    float NdotL = max(dot(n,l), 0.0);
    float NdotH = max(dot(n,h), 0.0);

    // diffuse color ramp
    vec4 c1 = vec4(1.0, 1.0, 1.0, 0);
    vec4 c2 = vec4(0.515, 0.527, 1.0, 0);

    // specular	
    vec4 spec_color = vec4(1.0, 1.0, 1.0, 0);
    float spec_intensity = 0.1;
    float shininess = 200.0;
    
    // ramp calcs
    const float amount = 1.0;
    vec3 normal = (gl_FrontFacing) ? n : -n;
    vec3 direction = v;
    float dot_dn = clamp(-dot(direction, normal), 0.0, 1.0);
    float mix_value = pow(dot_dn, 1.0 / amount);

    /*
    vec4 ambient = vec4(0.1, 0.1, 0.1, 0);
    vec4 diffuse = mix(c1, c2, mix_value) * NdotL;
    vec4 specular = spec_color * spec_intensity * pow(NdotL, shininess);

    color = vec3(ambient + diffuse + specular);
    */

    vec4 ambient = vec4(0.1, 0.1, 0.1, 0);
    vec4 diffuse = mix(c1, c2, mix_value) * (NdotL * 0.8);
    vec4 specular = spec_color * spec_intensity * pow(NdotL, shininess);

    color = vec3(ambient + diffuse);
    //color = vec3(ambient + diffuse + specular);

#ifdef _TEXTURE_MAP
    color *= texture2D(texMap, gl_TexCoord[0].st).rgb;
#endif

if (use_shadows) {
    float shadow = 1.0;
    float bias  = 0.009; // 0.005;
    if (texture2D( shadowMap, shadowCoord.xy ).z  <  shadowCoord.z-bias) {
        shadow = 1.0 - shadow_intensity;
    }
    color *= shadow;
}


    gl_FragColor.rgb = color;
}

/* vim:set ft=glsl: */
