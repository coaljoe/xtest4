uniform vec3 f_diffuse;
uniform vec3 f_specular; // .1, .1, .1
uniform float f_hardness; //50
float f_ambient_intencity = 1.0;
float f_diffuse_intencity = 1.1;
float f_specular_intencity = 1.1;

varying vec3 L;
varying vec3 N;
varying vec3 V;
varying vec3 H;

#ifdef _TEXTURE_MAP
uniform sampler2D texMap;
//varying vec2 UV;
#endif

#ifdef _NORMAL_MAP
uniform sampler2D normalMap;
//varying vec2 UV;
#endif

void main() {
    vec3 l = normalize(L); // light vec
    vec3 n = normalize(N); // normal vec
    vec3 v = normalize(V); // view vec (eyeVec)
    //vec3 h = normalize(l + v);  // half vec
    vec3 h = normalize(H);

    // out
    vec4 color;

#ifdef _NORMAL_MAP
    n = texture2D(normalMap, gl_TexCoord[0].st).rgb * 2.0 - 1.0;
#endif

    float NdotL = max(dot(n,l), 0.0);
    float NdotH = max(dot(n,h), 0.0);

    vec4 ambient = vec4(0.1, 0.1, 0.1, 1);
    vec4 diffuse = vec4(f_diffuse, 1) * NdotL;
    vec4 specular = vec4(f_specular, 1) * pow(NdotH, f_hardness);

    color = max(ambient * f_ambient_intencity, diffuse * f_diffuse_intencity) + specular * f_specular_intencity;

#ifdef _TEXTURE_MAP
    //color += diffuse * NdotL;
    //color = diffuse * texture2D(texMap, gl_TexCoord[0].st);
    color = color * texture2D(texMap, gl_TexCoord[0].st);

    //gl_FragColor = amb + texture2D(texMap, gl_TexCoord[0].st) * diff;
#endif

    gl_FragColor = color;
}

/* vim:set ft=glsl: */
