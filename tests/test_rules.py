#!/usr/bin/env python
from src.rpgsys.rules import *

def test():
    s_handgun = Specialism(name='Handgun', type='actions')
    s_leadership = Specialism(name='Leadership', type='ego')
    s_strategic = Specialism(name='Strategic', type='wits')
    s_deduction = Specialism(name='Deduction', type='wits')
    s_gunner = Specialism(name='Gunner', type='actions')

    c = camilla = Character('Camilla', actions=d6, wits=d10, ego=d8, status='Exile',
                   background="Jacob's daughter")
    #c.specAction.append(s_handgun)
    c.specWits.extend([s_strategic, s_deduction])
    c.specEgo.append(s_leadership)
    c.list()

    c = npc1 = Character('Gunner1', actions=d10, wits=d6, ego=d8)
    c.specAction.extend([s_gunner, s_handgun])
    c.specEgo.append(s_leadership)
    c.list()

    npc1.performAttack(camilla)

if __name__=='__main__':
    test()