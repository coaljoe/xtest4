from __future__ import division, absolute_import
from collections import OrderedDict
from pprint import pprint
from src.dialogue import Dialogue, DialogueFinishedError

class TestDialogue(object):
    def setUp(self):
        pass
    def test_simple(self):
        def cond_pc_is_female(d):
            return True if d.pc.info.gender == 'F' else False
        data = OrderedDict((
            #('A', 1),
            ## section 1
            ('WELCOME', {
                'say':
                [
                    #{'say': "welcome m'lady", 'cond': cond_pc_is_female},
                    {'say': "welcome sir"},
                    {'say': "impossible input", 'cond': lambda: False},
                ],
                'replies':
                [
                    {'reply': 'let me in',
                        'goto': 'SMALLTALK'},
                    {'reply': 'thanks',
                        'goto': 'SMALLTALK'},
                    {'reply': 'no way!',
                        'goto': 'exit'},
                    #{'goto': 'DEFAULT_SECTION'}, # finally
                ],
                #'say': "have a nice day"
            }),
            ## section 2
            ('SMALLTALK', {
                'say': 'would you like a cup of tea?',
                'replies': [
                    {'reply': 'no thanks', 'set_var': ['something', 1], 'goto': 'exit'},
                    {'reply': 'yes please', 'set_var': {'something': 1}, 'goto': 'exit'},
                    {'reply': 'eval', 'eval': "x.vars.something += 1", 'goto': 'exit'},
                    {'reply': 'exit', 'goto': 'exit'},
                    #{'say': 'die!', 'vars_cond': lambda vars: vars.something > 0},
                    #{'goto': 'exit'}
                ]
            }),
            #('Z', 1)
        ))
        d = Dialogue(data, npcName='Good NPC', pcName='Player')
        pprint(d.data.items())
        #print d.data.items()
        assert d 
        d.iterate()
        d.replyIdx(0)
        d.iterate()
        d.replyIdx(3)
        d.iterate()
        assert d.finished
        try:
            d.iterate()
        except DialogueFinishedError:
            print "\n>> dialog was finished without errors."
        #d.iterate()
        #d.update()
        #d.iterate()
        #d.update()
