#!/usr/bin/env python
from time import time
import sys
sys.argv.extend(['-l', 'empty'])
from src.lib.aux3d import aux3d as aux
from src.main2 import game, xapp

def test():
    m = list(aux.scene.loadScene('res/models/armature.dae'))[0]
    print m.mesh.armature
    m.mesh.playAnimation('default')

    tend = time() + 2
    def running_check():
        return True if time() < tend else False
    update(running_check)

def update(check):
    while xapp.running:
        dt = xapp.dt
        if xapp.step():
            game.update(dt)
            xapp.flip()
        if not check():
            break        

def quit():
    xapp.w.has_exit = True
    game.quit()

if __name__=='__main__':
    test()
