#!/usr/bin/env python
from time import time
import sys
sys.argv.extend(['-l', 'empty'])
from src.audio import Audio, Sfx
from src.main2 import game, xapp

def test():
    #au = Audio(game)
    #au.init_audio()
    au = game.audio
    sfx1 = au.add_sound('res/items/gun/snd_use.wav')
    assert sfx1
    sfx1.play()

    tend = time() + 2
    def running_check():
        return True if time() < tend else False
    update(running_check)
    au.free()

def update(check):
    while xapp.running:
        dt = xapp.dt
        if xapp.step():
            game.update(dt)
            xapp.flip()
        if not check():
            break        

def quit():
    xapp.w.has_exit = True
    game.quit()

if __name__=='__main__':
    test()
