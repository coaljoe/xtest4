#!/usr/bin/env python
from __future__ import division, absolute_import
import sys
sys.argv.extend(['-l', 'empty'])

from src.vector3 import Vector3
#from src.game import Game
from src.main2 import game, xapp
#from src.rpgsys.rules import * 
from src.item import Weapon, item_factory
from src.npc import NPC
from src.character import Character, AlreadyEquippedError, SlotError

#game = Game()
print game
#exit()


def test():
    #### setup ####
    print '\n** begin test\n'

    char = game.char
    print char
    print char.name
    print char.info.name
    
    # give gun to char
    w = Weapon.factory('g17')
    w.rounds = w.maxrounds
    #w.reload()
    inv = char.inventory
    inv.addItem(w)
    #char.pickup_item(w)
    #inv.listItems()
    # set item
    char.item1 = w
    char.item = w
    assert char.weapon

    # kid npc
    npc = NPC.factory('kid')
    npc.pos = char.pos + Vector3(2, 0, 0)
    npc.spawn()

    # give armor to npc
    it = item_factory('boots')
    npc.equip.wear(it)
    try:
        npc.equip.wear(it)
    except AlreadyEquippedError:
        print "already equipped... do nothing"
    print npc.equip.armor
    print npc.equip.slots
    print npc.equip.used_slots
    assert npc.armor

    #import pdb; pdb.set_trace()

    #### run ####
    if char.canAttack(npc):
        char.setTarget(npc)
    else:
        print "can't attack npc"
        exit()

    def check():
        return npc.isAlive
    update(check)

    print char.stats.items_use
    quit()


def update(check):
    while xapp.running:
        dt = xapp.dt
        if xapp.step():
            game.update(dt)
            xapp.flip()
        if not check():
            break        
    
def quit():
    xapp.w.has_exit = True
    game.quit()

if __name__=='__main__':
    test()
