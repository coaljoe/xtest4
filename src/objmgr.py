from __future__ import division, absolute_import
from weakref import WeakSet, WeakValueDictionary
from src.event import event

class ObjMgr(object):
    def __init__(self, game):
        self.game = game
        self.objs = WeakSet()
        self.objs_map = WeakValueDictionary()

        event.sub('ev_obj_spawn', self.onObjSpawn)
        event.sub('ev_obj_respawn', self.onObjRespawn)
        event.sub('ev_obj_destroy', self.onObjDestroy)

    def id2obj(self, id):
        pass

    def onObjSpawn(self, obj):
        if obj.spawned:
            return
        assert obj not in self.objs
        self.objs.add(obj)
        self.objs_map[obj.name] = obj
        obj.spawned = True

    def onObjRespawn(self, obj):
        # register object if not spawned
        if not obj.spawned:
            self.onObjSpawn(obj)

    def onObjDestroy(self, obj):
        #print 'on_destroy', obj
        self.objs.remove(obj)
        del self.objs_map[obj.name]
        
    def update(self, dt):
        for x in self.objs:
            x.update(dt)

