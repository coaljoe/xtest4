from __future__ import division, absolute_import
import os.path, fnmatch, glob
from xml.etree.ElementTree import ElementTree as ET
from weakref import WeakSet, WeakValueDictionary
from src.vector3 import Vector3
from src.lib.aux3d import aux3d as aux

def remove_namespace(doc, namespace):
    """Remove namespace in the passed document."""
    ns = u'{%s}' % namespace
    nsl = len(ns)
    for elem in doc.getiterator():
        if elem.tag.startswith(ns):
            elem.tag = elem.tag[nsl:]

class Level(object):
    def __init__(self, name):
        self.name = name
        self.dirname = self.name
        
        # load any '.dae' file or levelname.dae
        dir = os.path.join('res', 'levels', self.name)
        filepath = None
        for fp in glob.glob(os.path.join(dir, '*.dae')):
            filepath = fp
            if os.path.basename(fp) == self.name + '.dae':
                break

        self.filepath = filepath
        self.size = 50

        self.meta = {}
        self.strawberries = WeakSet()
        self.strawberries_total = 0
        self.nodes = WeakSet()
        self.navmesh = None
        self.selbox_nodes = []
        self.selbox_nodes_map = WeakValueDictionary()
        self.pf = None

    def spawn(self):
        #app.resetScene()
        #app.setDefaultScene()

        # load level.json
        leveljson = os.path.join(self.path, 'level.json')
        if os.path.exists(leveljson):
            pass

        # get data from level xml file
        tree = ET(); tree.parse(self.filepath)
        remove_namespace(tree, 'http://www.collada.org/2005/11/COLLADASchema')
        root = tree.getroot()
        
        ## process 'EMPTY' nodes
        for el in tree.findall('library_visual_scenes/visual_scene/node[@type="NODE"]'):
            # skip non-empty node types
            if len(el) > 1:
                continue

            namestr = el.get('name')
            assert namestr is not None
            print el, namestr
            ## TransRotLoc
            #postxt = el.find('translate').text
            #pos = Vector3([float(x) for  x in postxt.split()])
            ## 4x4 matrix
            postxt = el.find('matrix').text.split()
            pos = Vector3(*[float(x) for x in (postxt[3], postxt[7], postxt[11])])
            self.meta[namestr] = {'pos': Vector3(pos.x, pos.z, -pos.y)} # convert to map-space
            #print postxt, pos
            #print el
            #exit()
        #print self.meta; exit()
            
        # free
        tree = None

        # spawn scene
        self.nodes = aux.scene.loadScene(self.filepath, static=True)
        #print(list(self.nodes)); exit()

        # create pathfinder
        for x in self.nodes:
            if x.name == 'navmesh':
                self.navmesh = x
                x.visible = False

        filepath = os.path.join(self.path, 'navmesh.bin')
        if os.path.exists(filepath):
            from src.pathfind import PathFinder
            self.pf = PathFinder()
            self.pf.load_mesh(filepath) # XXX fixme

        # set the snow materials
        #mn = aux.scene.getMeshNode('Plane_002')
        for mn in [x for x in aux.scene.meshes if x.mesh.material.name]:
            if mn.mesh.material.name.startswith('snow'):
                print mn.name
                mn.mesh.setMaterial('materials/snow.json')

        # create selbox nodes
        for mn in [x for x in aux.scene.meshes if x.mesh.material.name]:
            if mn.mesh.material.name.startswith('selbox_mat'):
                #print mn.name
                #print mn.mesh.material.name
                mn.static = False
                #mn.linked_obj = mn # selflinked
                mn.linked_obj = None
                self.selbox_nodes.append(mn)
                self.selbox_nodes_map[mn.name] = mn
                aux.scene.removeMeshNode(mn)

        #print self.selboxes
        #exit()

    def free(self):
        #aux.scene.free()
        for nd in self.nodes:
            aux.scene.meshes.remove(nd) # fixme
            nd.free()
        self.nodes.clear()
        #for ob in self.objs: ob.destroy()
        #self.objs.clear()
  
    def update(self, dt):
        pass

    @property
    def path(self):
        return os.path.join('res', 'levels', self.dirname)


def getLevelsList():
    l = []
    path = os.path.join('res', 'levels')
    for d in os.listdir(path):
        if os.path.isdir(os.path.join(path, d)):
            l.append(d)
    l.sort()
    return l
