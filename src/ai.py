from __future__ import division, absolute_import
from random import randint, uniform
from src.util import Timer, Singleton

class AIMgr(object):
    __metaclass__ = Singleton
    def __init__(self):
        self.ais = []
    def addAI(self, ai):
        self.ais.append(ai)
    def update(self, dt):
        for ai in self.ais:
            ai.update(dt)

class BaseAI(object):
    pass

class WanderingAI(BaseAI):
    def __init__(self, char):
        super(WanderingAI, self).__init__()
        self.idling_time_range = (2, 30)
        self.walking_time_range = (0.2, 8)
        self.walking_speed_range = (0.25, 0.75) # doto make step change

        self.delay = 0.0
        self.walk = True
        self.char = char
        self.timer = Timer()
        self.timer.start()

        # register
        AIMgr().addAI(self)

    @property
    def is_walking(self):
        return True if self.char.vel > 0 else False

    def update(self, dt):
        if self.char.isDead:
            return

        # update timer
        self.timer.update(dt)
        if self.timer.dt > self.delay:
            #print 'bong'
            delay_time = self.idling_time_range if self.is_walking \
                         else self.walking_time_range
            self.delay = uniform(*delay_time)
            self.timer.reset()
        else:
            return

        # update char props
        if self.is_walking:
            self.char.vel = 0
        else:
            self.char.dir = (self.char.dir + randint(0, 180)) %  360
            self.char.vel = uniform(*self.walking_speed_range)
            #print self.char.dir
            #print self.char.vel
