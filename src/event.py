from __future__ import division, absolute_import
from datetime import datetime
from src.lib.aux3d.contrib import message
from src.util import Singleton
#import zmq

class Event(object):
    __metaclass__ = Singleton
    """Game's event subsystem
    """
    def __init__(self):
        self.start_dt = datetime.now()
        #self.ctx = zmq.Context(1)
        #self.socket = self.ctx.socket(zmq.PULL)
        #self.socket.bind('tcp://127.0.0.1:5555')
        pass

    @property
    def dt_ms(self):
        dt = datetime.now() - self.start_dt
        return round(dt.seconds + (dt.microseconds / 1000000), 4) # fixme ?

    def sub(self, evname, handler):
        #print ': event.sub', evname, self.dt_ms
        message.sub(evname, handler)

    def pub(self, evname, *args, **kw):
        #print ': event.pub', evname, self.dt_ms
        message.pub(evname, *args, **kw)

    def update(self, dt):
        pass
        """
        data = None
        try:
            data = self.socket.recv(flags=zmq.NOBLOCK)
        except zmq.ZMQError:
            return

        ev_name = data.split(" ")[0]
        args = data.split(" ")[1:]
        self.pub(ev_name, *args)
        """

event = Event()

