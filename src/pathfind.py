from __future__ import division, absolute_import
from contrib.recast import recast as dt
from src.vector3 import Vector3

class PathFinder(object):
    def __init__(self):
        self.navmesh = None
        self.filter = None
        self.query = None

    def load_mesh(self, fn):
        print "Create NavMesh Object From File"
        self.navmesh = dt.dtLoadSampleTileMesh(fn)
        self.filter = dt.dtQueryFilter()
        self.query = dt.dtNavMeshQuery()

        print "Init NavMesh Object"
        status = self.query.init(self.navmesh, 2048)
        if dt.dtStatusFailed(status):
            self.fail(-1, status)

    def find_straight_path(self, p1, p2):
        print 'pf: find straight path', p1, p2
        print "Fix The Input Data"
        polyPickExt = dt.dtVec3(1000.0, 1.0, 1000.0) # fixme: navmesh size?
        startPos = dt.dtVec3(*p1)
        endPos = dt.dtVec3(*p2)
        #startPos = dt.dtVec3(1,1,1)
        #endPos = dt.dtVec3(2,2,2)
        #startPos = dt.dtVec3(2, .5, 20)
        #endPos = dt.dtVec3(4, .5, 20)
        #print 'startPos', startPos
        #print 'endPos', endPos

        status, out = self.query.findNearestPoly(startPos, polyPickExt, self.filter)
        if dt.dtStatusFailed(status):
            self.fail(-2, status)
        startRef = out["nearestRef"]
        _startPt = out["nearestPt"]

        status, out = self.query.findNearestPoly(endPos, polyPickExt, self.filter)
        if dt.dtStatusFailed(status):
            self.fail(-3, status)
        endRef = out["nearestRef"]
        _endPt = out["nearestPt"]

        print "Get Path Reference List"
        status, out = self.query.findPath(startRef, endRef, startPos, endPos, self.filter, 32)
        if dt.dtStatusFailed(status):
            self.fail(-4, status)
        pathRefs = out["path"]

        status, fixEndPos = self.query.closestPointOnPoly(pathRefs[-1], endPos)
        if dt.dtStatusFailed(status):
            self.fail(-5, status)
        print "Get Path Point List"
        status, out = self.query.findStraightPath(startPos, fixEndPos, pathRefs, 32, 0)
        if dt.dtStatusFailed(status):
            self.fail(-6, status)
        straightPath = out["straightPath"]
        straightPathFlags = out["straightPathFlags"]
        straightPathRefs = out["straightPathRefs"]

        print "Print Search Result"
        print "The input data:"
        print "\tstart pos:", startPos
        print "\tend pos: ", endPos

        print "The fixed input data:"
        print "\tstart point(in poly):", _startPt
        print "\tend pos(in poly):", _endPt
        print "\tend pos(fixed):", fixEndPos

        print "The final output path:"
        print "\tstraight path:", straightPath
        print "\tstraight path flags:", straightPathFlags
        print "\tstraight path refs:", straightPathRefs

        return [[Vector3(*x) for x in straightPath], straightPathFlags]

    def fail(self, rv=None, st=None):
        print 'pf fail'
        print 'rt', rv, 'st:', st
        #exit(-1)
