from __future__ import division, absolute_import
import PyCEGUI
from PyCEGUI import USize, UDim, UVector2
from .components import GuiWindow
from .util import cleanup_container

class Dialogue(GuiWindow):
    def __init__(self, gui):
        super(Dialogue, self).__init__(gui)
        # windows properties
        self.name = 'dialogue'
        self.modal_visible = True

        self.dialogue = None

        win = self.win = self.gui.wmgr.loadLayoutFromFile("dialogue.layout")
        win.setVisible(False)
        self.gui.root.addChild(win)
        self._wins.append(win)

        self.portraitImage = self.win.getChildRecursive('PortraitImage')
        self.sayText = self.win.getChildRecursive('SayText')
        self.repliesContainer = self.win.getChildRecursive('RepliesContainer')

    
    def reset(self):
        cleanup_container(self.repliesContainer)

    def refresh(self):
        #self.repliesContainer.cleanupChildren()
        cleanup_container(self.repliesContainer)

    def showFor(self, dialogue):
        self.dialogue = dialogue
        self.dialogue.reset()
        self.reset()
        self.iterate()
        self.show()

    def iterate(self):
        d = self.dialogue
        d.iterate()
        if d.finished:
            self.hide()
            return False
        self.refresh()
        npc = self.dialogue.npc
        if npc.info.portrait:
            self.portraitImage.setProperty("Image", npc.info.portrait)
        #self.portraitImage.subscribeEvent('MouseClick', self.close_cb)
        self.sayText.setProperty("Text", d.getSayText())
        for i, reply in enumerate(d.getReplies()):
            w = label = self.gui.wmgr.createWindow("WindowsLook/StaticText","ReplyLabel" + str(i))
            w.setProperty("Text", ' ' + reply.text)
            w.setSize(USize(UDim(0,375),UDim(0,15)));
            w.setPosition(UVector2(UDim(0,40), UDim(0,15*i)))
            w.setProperty("TextColours", "tl:FF96FFFF tr:FF96FFFF bl:FF96FFFF br:FF96FFFF")
            w.setProperty("BackgroundColours", "tl:FF222299 tr:FF222299 bl:FF222299 br:FF222299")
            w.setProperty("FrameEnabled", "False")
            w.setProperty("BackgroundEnabled", "False")
            w.setUserData(i)
            w.subscribeEvent('MouseClick', self.reply_click_cb)
            w.subscribeEvent('MouseEntersArea', self.reply_mouse_over_cb)
            w.subscribeEvent('MouseLeavesArea', self.reply_mouse_out_cb)
            self.repliesContainer.addChild(label)

        return True

    def onShow(self):
        pass

    def close(self):
        self.hide()

    def close_cb(self, e):
        self.close()

    def reply_click_cb(self, e):
        print 'bong'
        idx = e.window.getUserData()
        self.dialogue.replyIdx(idx)
        self.iterate()

    def reply_mouse_over_cb(self, e):
        try: e.window.setProperty("BackgroundEnabled", "True")
        except: pass
    def reply_mouse_out_cb(self, e):
        try: e.window.setProperty("BackgroundEnabled", "False")
        except: pass
