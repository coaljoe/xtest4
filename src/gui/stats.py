from __future__ import division, absolute_import
import PyCEGUI
from .notebook import NotebookWindow
from .components import GuiWindow
from src.util import format_approx_number
from .util import cleanup_container

class Stats(NotebookWindow):
    def __init__(self, gui):
        super(Stats, self).__init__(gui)
        self.name = 'inventory'
        self.modal_visible = True

        win = self.win = self.gui.wmgr.loadLayoutFromFile("stats.layout")
        win.setVisible(False)
        self.gui.root.addChild(win)
        self._wins.append(win)

        w = self.win.getChildRecursive("NextPageButton")
        w.subscribeEvent('MouseClick', self.next_page_cb)
        w = self.win.getChildRecursive("PrevPageButton")
        w.subscribeEvent('MouseClick', self.prev_page_cb)

        self.reset()

    def reset(self):
        ## cleanup
        w = self.killsText = self.win.getChildRecursive('KillsText')
        w.setProperty('Text', '')
        #cleanup_container(w)

    def show(self):
        super(Stats, self).show()
        self.reset()
        self.char = self.game.char

        self.refresh()

    def refresh(self):
        s = ''
        for k, v in self.char.stats.kills.items():
            s += "%-36s %3s\n" % (k, v)
        self.killsText.setProperty('Text', s)

    def onShow(self):
        pass

    def close(self):
        self.hide()

    def close_cb(self, e):
        self.close()

    def next_page_cb(self, e):
        self.hide()
        self.gui.notebook.show()

    def prev_page_cb(self, e):
        self.hide()
        self.gui.inventory.show()
