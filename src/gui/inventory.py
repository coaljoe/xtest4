from __future__ import division, absolute_import
from collections import OrderedDict
#import trollius
#from trollius import From
import PyCEGUI
from PyCEGUI import USize, UDim, UVector2, CoordConverter
from src.equip import SlotError
from src.util import Timer
from .components import GuiWindow, ItemGrid
from .notebook import NotebookWindow
from src.util import format_approx_number, longtext
from .util import cleanup_container


class Inventory(NotebookWindow):
    slot_names = 'head hands body back legs foots item1 item2'.split()
    def __init__(self, gui):
        super(Inventory, self).__init__(gui)
        self.name = 'inventory'
        self.modal_visible = True
        
        self.holdTimer = Timer()
        self.shed_gen = None

        win = self.win = self.gui.wmgr.loadLayoutFromFile("inventory.layout")
        win.setVisible(False)
        self.gui.root.addChild(win)
        self._wins.append(win)

        ## cleanup ip
        ip = self.win.getChildRecursive('ItemsPane')
        cleanup_container(ip.getChildAtIdx(0))

        self.ig = ItemGrid(ip, self.gui, 5, 8, name='IG')
        #self.lg.mouse_double_click_cb = self.lg_mouse_double_click_cb
        #self.ig.mouse_click_cb = self.ig_mouse_click_cb
        self.ig.mouse_down_cb = self.ig_mouse_down_cb
        self.ig.mouse_up_cb = self.ig_mouse_up_cb
        self.ig.mouse_over_cb = self.ig_mouse_over_cb
        self.ig.mouse_out_cb = self.ig_mouse_out_cb

        w = self.win.getChildRecursive("NextPageButton")
        w.subscribeEvent('MouseClick', self.next_page_cb)
        w = self.win.getChildRecursive("PrevPageButton")
        w.subscribeEvent('MouseClick', self.prev_page_cb)

        self.equipButtonStates = {}

        for slot_name in self.slot_names:
            w = self.win.getChildRecursive(slot_name.capitalize() + "Button")
            w.setUserData(slot_name)
            w.setWantsMultiClickEvents(True)
            w.setMouseAutoRepeatEnabled(True)
            #w.subscribeEvent('Clicked', self.equip_button_clicked_cb)
            w.subscribeEvent('MouseEntersArea', self.equip_button_mouse_over_cb)
            w.subscribeEvent('MouseLeavesArea', self.equip_button_mouse_out_cb)
            w.subscribeEvent('MouseButtonDown', self.equip_button_mouse_down_cb)            
            w.subscribeEvent('MouseButtonUp', self.equip_button_mouse_up_cb)
            w.setAutoRepeatRate(0.01)
            
            self.equipButtonStates[slot_name] = {'pressed': False}

        self.reset()
        
    def setEquipButton(self, name, it):
        w = self.win.getChildRecursive(name.capitalize() + "Frame")
        w.setProperty('Text', '')
        w = self.win.getChildRecursive(name.capitalize() + "Button")
        w = w.getChildRecursive("StaticImage")
        w.setProperty('Image', 'items/' + it.inv_icon)
    
    def unsetEquipButton(self, name):
        w = self.win.getChildRecursive(name.capitalize() + "Frame")
        w.setProperty('Text', name)
        w = self.win.getChildRecursive(name.capitalize() + "Button")
        w = w.getChildRecursive("StaticImage")
        w.setProperty('Image', '')
        w = self.win.getChildRecursive(name.capitalize() + "StatsText")
        w.setProperty('Text', '')
    
    def setEquip(self, name, it):
        self.setEquipButton(name, it)
        d = OrderedDict()
        for prop, shortcut in [('insulation', 'In'), ('bulk', 'Bk'),
                               ('modifier', 'AM'), ('durability', 'Dr')]:
            v = getattr(it, prop)
            if prop == 'insulation' and v:
                d[shortcut] = v
            elif prop == 'bulk' and abs(v) > 1.0:
                d[shortcut] = int(v)
            elif prop == 'modifier' and v < 0:
                d[shortcut] = v
            elif prop == 'durability' and v < 1.0:
                #d[shortcut] = str(int(v * 100)) + '%'
                #d[shortcut] = int(v * 10)
                d[shortcut] = int(v * 100)
                #print d; exit()
        s = ''
        # have % sign in whole column
        have_percent_c1 = False
        have_percent_c2 = False
        rows = []
        for j in xrange(0, len(d.items()), 2):
            c1 = d.items()[j]
            try:
                c2 = d.items()[j+1]
            except IndexError:
                c2 = (' ', '  ')
            #if v > 0:
            #    v = ' ' + str(v)
            #s += " %-3s %s" % (v, k)
            #s += '{0:<3d}  {1}'.format(v, k)
            i = 1
            for c in (c1, c2):
                k, v = c
                vs = str(v)
                v_adj = ''
                zc1 = 0 # % in cell
                zc2 = 0 #
                if '-' not in vs:
                    v_adj = ' ' + vs
                else:
                    v_adj = vs
                if '%' in v_adj:
                    global zc1, zc2
                    if i == 1:
                        have_percent_c1 = True
                        zc1 = 1
                    elif i == 2:
                        have_percent_c2 = True
                        zc2 = 1
                if i == 1:
                    c1 = (k, v_adj, zc1)
                elif  i == 2:
                    c2 = (k, v_adj, zc2)
                i += 1
            print c1, c2
            rows.append((c1, c2))

        print rows
        for c1, c2 in rows:
            s += '{0:<{zc1}}{1:<3}{2:<3}{3:>{zc2}}\n'.format(c1[1], c1[0], c2[1], c2[0],
                                                             zc1=4 if not have_percent_c1 else 5,
                                                             zc2=3 if not have_percent_c2 else 4 - c2[2])
                                                             
        w = self.win.getChildRecursive(name.capitalize() + "StatsText")
        #print d, s; exit()
        w.setProperty('Text', s)
        
    
    def unsetEquip(self, name):
        self.unsetEquipButton(name)
        
    def setItem(self, name, it):
        self.setEquipButton(name, it)
    
    def unsetItem(self, name):
        self.unsetEquipButton(name)
        
    def hlEquip(self, name):
        w = self.win.getChildRecursive(name.capitalize() + "Frame")
        #w.setProperty('Text', '')
        w.setProperty('BackgroundEnabled', 'True')
        w.setProperty('BackgroundColours', 'tl:FF4d7cfb tr:FF4d7cfb bl:FF4d7cfb br:FF4d7cfb')
        
    def unhlEquip(self):
        for name in self.slot_names:
            w = self.win.getChildRecursive(name.capitalize() + "Frame")
            #w.setProperty('Text', name)
            w.setProperty('BackgroundEnabled', 'False')
        
    def reset(self):
        for slot_name in self.slot_names:
            self.unsetEquip(slot_name)

    def show(self):
        super(Inventory, self).show()
        self.char = self.game.char
        self.cont = self.char.inventory

        self.refresh()

    def refresh(self):
        self.reset()

        self.ig.clear()
        self.ig.appendItems(self.cont.items)

        equip = self.char.equip

        for it in equip.used_slots:
            self.setEquip(it.slot, it)
        
        for slot_name in ['item1', 'item2']:
            it = getattr(equip, slot_name, None)
            if it:
                #it.slot = slot_name
                self.setItem(slot_name, it)

        print self.ig.len
        print self.ig.items
        weightLabel = self.win.getChildRecursive('WeightLabel')
        weightLabel.setProperty("Text", 'Weight (kg): ' + format_approx_number(self.ig.totalWeight, ''))
        bulkLabel = self.win.getChildRecursive('BulkLabel')
        bulkLabel.setProperty("Text", 'Bulk (L): ' + format_approx_number(self.ig.totalBulk, ''))

    def showItemDescription(self, it):
        desc = it.name + "\n\n" + (it.long_description or it.description+'...')
        descText = self.win.getChildRecursive('DescriptionText')
        descText.setProperty("Text", desc)

    def hideItemDescription(self):
        descText = self.win.getChildRecursive('DescriptionText')
        descText.setProperty("Text", '')

    def onShow(self):
        pass

    def close(self):
        self.hide()

    def close_cb(self, e):
        self.close()
        
    def slot_name2item(self, slot_name):
        it = None
        for x in self.char.equip.items:
            if x.slot == slot_name:
                it = x
        return it

    def ig_mouse_click_cb(self, e):
        print 'BONG'
        idx = e.window.getUserData()
        print 'zz'
        it = self.ig.getItemBtnIdx(idx)
        equip = self.game.char.equip
        if e.button == PyCEGUI.LeftButton:
            if it.item_type != 'equip':
                equip.putReplaceItem(it, slot_name='item1')
                self.refresh()
            else:
                try:
                    equip.putItem(it)
                except SlotError:
                    print 'cant item put, slot error', it
                finally:
                    self.refresh()
        elif e.button == PyCEGUI.MiddleButton:
            print 'no middle click'
        elif e.button == PyCEGUI.RightButton:
            if it.item_type != 'equip':
                equip.putReplaceItem(it, slot_name='item2')
                self.refresh()
        print '-> done'
                
    def ig_mouse_up_cb(self, e):
        idx = e.window.getUserData()
        print 'pong up!', idx
        #print self.ig.buttonStates        
        self.ig.buttonStates[idx]['pressed'] = False
        print '->>', self.ig.buttonStates[idx]['pressed']
        
    def ig_mouse_down_cb(self, e):
        print 'bong!'
        idx = e.window.getUserData()
        it = self.ig.getItemBtnIdx(idx)
        slot_name = it.slot
        #self.ig_mouse_click_cb(e)
        #return
        self.holdTimer.reset()
        self.holdTimer.start()
        self.ig.buttonStates[idx]['pressed'] = True
        def test():
            print '->>', self.ig.buttonStates[idx]['pressed']
            while self.holdTimer.dt < 0.2:
                print 'dong', self
                yield

            print '->>', self.ig.buttonStates[idx]['pressed']
            print 'done!!!!!'
            # equip button was unpressed
            if not self.ig.buttonStates[idx]['pressed']:
                self.ig_mouse_click_cb(e)
            else:
                print 'show submenu!'
                #w = self.ig._w.getChildRecursive(slot_name.capitalize() + "Button") # XXX not Button
                w = self.ig.getItemButtonByIdx(idx)
                print w
                print w.getUserData()
                #exit()
                self.gui.item_submenu.showFor(self, w, it, xoff=410, yoff=180) # XXX fixme
                #self.refresh()
        self.shed_gen = test()
        

    def ig_mouse_over_cb(self, e):
        #print 'PONG'
        idx = e.window.getUserData()
        it = self.ig.getItemBtnIdx(idx)
        self.showItemDescription(it)
        print it.item_type
        if it.item_type == 'equip':
            self.hlEquip(it.slot)
        else:
            # any other item can be put in itemx slots
            #print it.item_type
            #self.hlEquip('item1')
            #self.hlEquip('item2')
            pass

    def ig_mouse_out_cb(self, e):
        self.unhlEquip()
        #pass
        #self.hideItemDescription()

    def equip_button_clicked_cb(self, e):
        slot_name = e.window.getUserData()
        it = self.slot_name2item(slot_name)        
        print slot_name
        print it
        self.char.equip.takeItem(it)
        #self.char.equip.swapItem(item, self.char.inventory)
        # unset slot for autoslot items
        if it.slot in ['item1', 'item2']:
            it.slot = None
            
        self.equipButtonStates[slot_name]['pressed'] = False
        self.refresh()

    def equip_button_mouse_over_cb(self, e):
        slot_name = e.window.getUserData()
        it = self.slot_name2item(slot_name)
        if it:
            self.showItemDescription(it)

    def equip_button_mouse_out_cb(self, e):
        pass
        #self.hideItemDescription()
    
    """    
    @trollius.coroutine
    def greet_every_two_seconds(self, slot_name):
        print '->>'. self.equipButtonStates[slot_name]['pressed']
        yield From(trollius.sleep(2))
        print 'show menu!'
        print '->>', self.equipButtonStates[slot_name]['pressed']
    """
        
    def equip_button_mouse_up_cb(self, e):
        slot_name = e.window.getUserData()
        print 'pong!'
        print '->>', self.equipButtonStates[slot_name]['pressed']
        self.equipButtonStates[slot_name]['pressed'] = False
    
    def equip_button_mouse_down_cb(self, e):
        print 'bong!'
        slot_name = e.window.getUserData()
        it = self.slot_name2item(slot_name)
        self.holdTimer.reset()
        self.holdTimer.start()
        self.equipButtonStates[slot_name]['pressed']= True
        #loop = trollius.get_event_loop()
        #loop.run_until_complete(self.greet_every_two_seconds(slot_name))        
        def test(slot_name):
            print '->>', self.equipButtonStates[slot_name]['pressed']
            while self.holdTimer.dt < 0.2:
                print 'dong', self
                yield

            print '->>', self.equipButtonStates[slot_name]['pressed']
            print 'done!!!!!'
            # equip button was unpressed
            if not self.equipButtonStates[slot_name]['pressed']:
                self.equip_button_clicked_cb(e)
            else:
                print 'show submenu!'
                w = self.win.getChildRecursive(slot_name.capitalize() + "Frame") # XXX not Button
                self.gui.item_submenu.showFor(self, w, it)
                #self.refresh()
                print 'done'
        self.shed_gen = test(slot_name)

    def next_page_cb(self, e):
        self.hide()
        self.gui.stats.show()

    def prev_page_cb(self, e):
        self.hide()
        self.gui.notebook.show()

    def update(self, dt):
        self.holdTimer.update(dt)
        if self.shed_gen:
            try: self.shed_gen.next()
            except StopIteration: pass