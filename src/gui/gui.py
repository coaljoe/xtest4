from __future__ import division, absolute_import
from OpenGL.GL import *
import PyCEGUI
import PyCEGUIOpenGLRenderer
from PyCEGUI import USize, UDim, UVector2
from src.lib.aux3d.contrib.glfw import *
from src.lib.aux3d import aux3d as aux
from src.conf import conf
from src.event import event
from src.util import Singleton
CEGUI_PATH = "res/gui/"
from .notebook import Notebook
from .barter import Barter
from .dashboard import DashBoard
from .dialogue import Dialogue
from .inventory import Inventory
from .item_submenu import ItemSubMenu
from .stats import Stats
from .util import GlfwToCeguiKey

def hide_parent_window_cb(e):
    print 'test'
    #e.window.getParent().setVisible(False)
    e.window.getParent().hide()
    return True


class Gui(object):
    __metaclass__ = Singleton
    def __init__(self, game):
        self.game = game
        self.width = None
        self.height = None
        PyCEGUIOpenGLRenderer.OpenGLRenderer.bootstrapSystem()
        self.setup()
        self._visible = True
        self.restore_windows_list = []
        self.disabled_windows_list = []
        self.mx = None
        self.my = None
        
        aux.renderer.use_gui = True
        aux.renderer.render_gui = self.draw
        self.visible = False
        #glfwSetInputMode(self.w._glfw_window, GLFW_CURSOR, G       
        
    def setup(self):
        # resources
        rp = PyCEGUI.System.getSingleton().getResourceProvider()
 
        rp.setResourceGroupDirectory("schemes", CEGUI_PATH + "schemes")
        rp.setResourceGroupDirectory("imagesets", CEGUI_PATH + "imagesets")
        rp.setResourceGroupDirectory("fonts", CEGUI_PATH + "fonts")
        rp.setResourceGroupDirectory("layouts", CEGUI_PATH + "layouts")
        rp.setResourceGroupDirectory("looknfeels", CEGUI_PATH + "looknfeel")
        rp.setResourceGroupDirectory("schemas", CEGUI_PATH + "xml_schemas")
 
        PyCEGUI.ImageManager.setImagesetDefaultResourceGroup("imagesets")
        PyCEGUI.Font.setDefaultResourceGroup("fonts")
        PyCEGUI.Scheme.setDefaultResourceGroup("schemes")
        PyCEGUI.WidgetLookManager.setDefaultResourceGroup("looknfeels")
        PyCEGUI.WindowManager.setDefaultResourceGroup("layouts")
 
        parser = PyCEGUI.System.getSingleton().getXMLParser()
        if parser.isPropertyPresent("SchemaDefaultResourceGroup"):
            parser.setProperty("SchemaDefaultResourceGroup", "schemas")     
 
        # gui
        PyCEGUI.SchemeManager.getSingleton().createFromFile("WindowsLook.scheme")
        #PyCEGUI.SchemeManager.getSingleton().create("WindowsLook.scheme")
        #PyCEGUI.FontManager.getSingleton().create("gui.font");
        #PyCEGUI.System.getSingleton().setDefaultFont("gui_font")
        #PyCEGUI.System.getSingleton().setDefaultMouseCursor("WindowsLook", "MouseArrow")

        """
        # PyCEGUI need fix from http://cegui.org.uk/mantis/view.php?id=996

        ctx = PyCEGUI.System.getSingleton().getDefaultGUIContext()
        print ctx
        m = ctx.getMouseCursor()
        print m
        ctx.getMouseCursor().setDefaultImage("WindowsLooks/MouseArrow")
        """

        self.wmgr = PyCEGUI.WindowManager.getSingleton()
        self.root = self.wmgr.createWindow("DefaultWindow", "root")
        self.ctx = PyCEGUI.System.getSingleton().getDefaultGUIContext()
        #self.root.setProperty("MousePassThroughEnabled", 'True')
        #self.root.setProperty("MouseInputPropagationEnabled", 'True')
        self.ctx.setRootWindow(self.root)

        # resize cb
        #PyCEGUI.System.getSingleton().notifyDisplaySizeChanged(PyCEGUI.Sizef(self.width, self.height))
 
        # windows
        """
        unitprod = PyCEGUI.WindowManager.getSingleton().loadLayoutFromFile("unitprod.layout")
        win = unitprod.getChildRecursive("CloseButton")
        #win.subscribeEvent(PyCEGUI.PushButton.EventClicked, hide_parent_window_cb, "");
        unitprod.setVisible(True)
        self.root.addChild(unitprod)
        """

        self.notebook = Notebook(self)
        self.barter = Barter(self)
        self.dashboard = DashBoard(self)
        self.dialogue = Dialogue(self)
        self.inventory = Inventory(self)
        self.item_submenu = ItemSubMenu(self)
        self.stats = Stats(self)


        # windows
        self.windows = [self.notebook, self.barter, self.dashboard, self.dialogue,
                        self.inventory, self.stats, self.item_submenu]

        if conf.vars.enable_dashboard:
            self.dashboard.show()

        win = self.wmgr.createWindow("WindowsLook/Tooltip","TipWindow" )
        #win = self.wmgr.createWindow("WindowsLook/StaticText","TipWindow" )
        win.setPosition(UVector2(UDim(0.5,0), UDim(0.5,0)))
        win.setProperty("Text", "notext")
        #win.setModalState(True)
        #win.setVisible(False)
        #self.ctx.getRootWindow().addChild(win)
        self.tipWin = win
        self.root.addChild(self.tipWin)

    def __del__(self):
        PyCEGUIOpenGLRenderer.OpenGLRenderer.destroySystem()

    def show_tip(self, text, pos=(0.5, 0.5)):
        print 'pos:', pos
        x, y = pos
        win = self.tipWin
        win.setProperty("Text", text)
        win.setPosition(UVector2(UDim(x,0), UDim(y,0)))
        win.setSize(USize(UDim(0,100),UDim(0,25)));
        #win.setVisible(True)
        win.show()
        self.root.addChild(win) # must be called, fixme ?
        #print 'SHOW!'

    def hide_tip(self):
        self.tipWin.setProperty("Text", 'hide')
        self.tipWin.hide()
        #print 'HIDE!'

    def draw(self):
        PyCEGUI.System.getSingleton().renderAllGUIContexts()

    def draw_(self):
        # fix
        #glPopAttrib()
        glPushMatrix()

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity()
        #glBindBuffer(GL_ARRAY_BUFFER,0);
        #glActiveTexture(0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glOrtho(0,self.width,0,self.height,-1,1)
        glDisable(GL_DEPTH_TEST)
        glDisable(GL_CULL_FACE)
        glDisable(GL_BLENDING)
        glDisable(GL_TEXTURE_2D)
        glDisable(GL_LIGHTING)
        #h3dClearOverlays();

        #glTranslatef(0, 0, 1)
        PyCEGUI.System.getSingleton().renderAllGUIContexts()

        glPopMatrix()
        # fix
        #glPushAttrib(GL_ALL_ATTRIB_BITS)

    def on_mouse_button(self, button, pressed):
        if button == GLFW_MOUSE_BUTTON_LEFT:
            if not pressed:
                self.ctx.injectMouseButtonUp(PyCEGUI.LeftButton)
            else:
                self.ctx.injectMouseButtonDown(PyCEGUI.LeftButton)
        elif button == GLFW_MOUSE_BUTTON_RIGHT:
            if not pressed:
                self.ctx.injectMouseButtonUp(PyCEGUI.RightButton)
            else:
                self.ctx.injectMouseButtonDown(PyCEGUI.RightButton)
        elif button == GLFW_MOUSE_BUTTON_MIDDLE:
            if not pressed:
                self.ctx.injectMouseButtonUp(PyCEGUI.MiddleButton)
            else:
                self.ctx.injectMouseButtonDown(PyCEGUI.MiddleButton)
 

    def on_mouse_pos(self, x, y, dx, dy):
        #print x, y
        self.mx, self.my = x, y
        self.ctx.injectMousePosition(x, y)
        
    def on_mouse_scroll(self, delta):
        #print delta
        self.ctx.injectMouseWheelChange(delta)

    def on_key_press(self, key):
        if not self.locked:
            return
        ckey = GlfwToCeguiKey(key)
        #print 'key', key, 'ckey', ckey
        self.ctx.injectKeyDown(ckey)
        if key < 127:
            self.ctx.injectChar(key)
        
    def on_key_release(self, key):
        if not self.locked:
            return
        ckey = GlfwToCeguiKey(key)
        self.ctx.injectKeyUp(ckey)

    def toggle(self):
        raise NotImplementedError
        self.visible = not self.visible
        for w in self.windows:
            if self.visible: w.show()
            else: w.hide()

    def toggle_window(self, name):
        for w in self.windows:
            print w.name, name
            if w.name == name:
                w.toggle()

    def store_windows(self):
        for w in self.windows:
            if w.visible:
                self.restore_windows_list.append(w.name)
            #w.hide(silent=True)
            w.hide()
            print w

    def restore_windows(self):
        for w in self.windows:
            if w.name in self.restore_windows_list:
                w.show(silent=True)
        self.restore_windows_list = []
        
    def store_nonmodal_windows(self):
        for w in self.windows:
            if w.visible:
                self.disabled_windows_list.append(w.name)
            w.setEnabled(False)

    def restore_nonmodal_windows(self):
        for w in self.windows:
            if w.name in self.restore_windows_list:
                w.setEnabled(True)
        self.restore_windows_list = []

    @property
    def visible(self): return self._visible
    @visible.setter
    def visible(self, val):
        self._visible = val
        if self._visible:
            event.pub('ev_gui_show')
        else:
            event.pub('ev_gui_hide')
        return self._visible

    @property
    def locked(self):
        for w in self.windows:
            if w.visible and not w._no_lock:
                return True
        return False

    # callbacks
    def hide_cb(self, e):
        raise NotImplementedError
        if self.visible:
            self.toggle()

    def update(self, dt):
        PyCEGUI.System.getSingleton().injectTimePulse(dt)
        for w in self.windows:
            if w.visible and not w._no_lock:
                w.update(dt)