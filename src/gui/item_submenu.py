from __future__ import division, absolute_import
from PyCEGUI import USize, UDim, UVector2, CoordConverter
from .components import GuiWindow

class ItemSubMenu(GuiWindow):    
    def __init__(self, gui):
        super(ItemSubMenu, self).__init__(gui)
        self.name = 'item_submenu'
        self.modal = False
        
        win = self.win = self.gui.wmgr.loadLayoutFromFile("item_submenu.layout")
        win.setVisible(False)
        self.gui.root.addChild(win)
        self._wins.append(win)
        
        self.item = None
        self.parentWin = None
        
        w = self.win
        w.subscribeEvent('MouseClick', self.close_cb)        
        #w = self.win.getChildRecursive("BackgroundImage")
        #w.subscribeEvent('MouseClick', self.close_cb)
        self.bg = self.win.getChildRecursive("BackgroundImage")
        self.itemImage = self.win.getChildRecursive("ItemImage")
        
        w = self.win.getChildRecursive("DropArea")
        w.subscribeEvent('MouseClick', self.drop_area_click_cb)
        
    def reset(self):
        pass

    def refresh(self):
        pass
    
    def showFor(self, parentWin, w, item, xoff=0, yoff=0):
        print  'showFor', w, item
        self.item = item
        self.parentWin = parentWin

        self.itemImage.setProperty('Image', 'items/' + self.item.inv_icon)
        
        """
        nb = self.win.getChildRecursive('BackgroundImage')
        _pos_win = nb.getPosition()
        pos_win = _pos_win.d_x.d_scale, _pos_win.d_y.d_scale
        w = self.win.getChildRecursive(slot_name.capitalize() + "Frame")
        _pos = w.getPosition()
        """
        
        sx, sy = 1024, 576
        nb_size = 770, 540
        pos_win = sx/2 - nb_size[0]/2, sy/2 - nb_size[1]/2
        
        #print CoordConverter.screenToWindow(nb, nb.getHeight())
        """
        #_pos = CoordConverter.asAbsolute(w.getHeight(), 0.0)
        #_pos = CoordConverter.asAbsolute(w.getSize().d_width, 0.0)
        _pos = CoordConverter.asAbsolute(w.getPosition().d_x, 0.0)
        print _pos
        z = w.getHeight().asAbsolute(576.0)
        print z
        exit()
        """
        _pos = w.getPosition()
        pos = _pos.d_x.d_offset, _pos.d_y.d_offset
        _size = w.getSize()
        size = _size.d_width.d_offset, _size.d_height.d_offset
              
        #wp = w.getParent().getParent()
        #_pos = wp.getPosition()
        #wp_pos = _pos.d_x.d_offset, _pos.d_y.d_offset
        #wp_pos = wp.getPosition().d_x.d_scale,  wp.getPosition().d_x.d_offset
        
        print pos_win
        #print wp_pos
        print pos
        pos = (pos_win[0] + xoff + pos[0], pos_win[1] + yoff + pos[1])
        
        self.show()
        #sm.setPos(self.gui.mx, self.gui.my)
        #sm.setPos(pos[0] + w/2, pos[1] - h/2)
        print pos, size
        #sm.setPos((pos[0] - size[0]/2), (pos[1] + size[1]/2))
        #self.setPos((pos[0] + 64/2) - w/2, pos[1])
        w, h = 158, 137
        p = (pos[0] + 64/2) - w/2, pos[1]
        self.bg.setPosition(UVector2(UDim(0, p[0]), UDim(0, p[1])))
        print 'done'

    def onShow(self):
        # XXX modal windows doesn't work, using DefaultWindow-hack from CEED instead
        pass
        
    def onHide(self):
        self.parentWin.refresh()

    def close(self):
        self.hide()

    def close_cb(self, e):
        self.close()
        
    def drop_area_click_cb(self, e):
        print 'drop item', self.item
        it = self.item
        # first takeoff item if its in equip
        if it in self.game.char.equip.items:
            self.game.char.equip.takeItem(it)
        self.game.char.inventory.removeItem(it)
        #self.game.char.dropItem(self.item)
        #self.game.char.equip.takeItem(self.item)
        self.close()