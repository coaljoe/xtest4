from __future__ import division, absolute_import
from src.lib.aux3d.contrib.glfw import *
import PyCEGUI

def cleanup_container(cont):
    dest_list = []
    for i in xrange(cont.getChildCount()):
        w = cont.getChildAtIdx(i)
        print w, w.getName()
        if cont.isChild(w):
            print 'destroy'
            dest_list.append(w)
            #cont.destroyChild(w)
            #w.destroy()
    for w in dest_list:
        w.destroy()
    True

def GlfwToCeguiKey(glfwKey):
    _map = {
        GLFW_KEY_ESCAPE     : PyCEGUI.Key.Escape,
        GLFW_KEY_F1        : PyCEGUI.Key.F1,
        GLFW_KEY_F2        : PyCEGUI.Key.F2,
        GLFW_KEY_F3        : PyCEGUI.Key.F3,
        GLFW_KEY_F4        : PyCEGUI.Key.F4,
        GLFW_KEY_F5        : PyCEGUI.Key.F5,
        GLFW_KEY_F6        : PyCEGUI.Key.F6,
        GLFW_KEY_F7        : PyCEGUI.Key.F7,
        GLFW_KEY_F8        : PyCEGUI.Key.F8,
        GLFW_KEY_F9        : PyCEGUI.Key.F9,
        GLFW_KEY_F10       : PyCEGUI.Key.F10,
        GLFW_KEY_F11       : PyCEGUI.Key.F11,
        GLFW_KEY_F12       : PyCEGUI.Key.F12,
        GLFW_KEY_F13       : PyCEGUI.Key.F13,
        GLFW_KEY_F14       : PyCEGUI.Key.F14,
        GLFW_KEY_F15       : PyCEGUI.Key.F15,
        GLFW_KEY_UP        : PyCEGUI.Key.ArrowUp,
        GLFW_KEY_DOWN      : PyCEGUI.Key.ArrowDown,
        GLFW_KEY_LEFT      : PyCEGUI.Key.ArrowLeft,
        GLFW_KEY_RIGHT     : PyCEGUI.Key.ArrowRight,
        GLFW_KEY_LEFT_SHIFT    : PyCEGUI.Key.LeftShift,
        GLFW_KEY_RIGHT_SHIFT    : PyCEGUI.Key.RightShift,
        GLFW_KEY_LEFT_CONTROL     : PyCEGUI.Key.LeftControl,
        GLFW_KEY_RIGHT_CONTROL     : PyCEGUI.Key.RightControl,
        GLFW_KEY_LEFT_ALT      : PyCEGUI.Key.LeftAlt,
        GLFW_KEY_RIGHT_ALT      : PyCEGUI.Key.RightAlt,
        GLFW_KEY_TAB       : PyCEGUI.Key.Tab,
        GLFW_KEY_ENTER     : PyCEGUI.Key.Return,
        GLFW_KEY_BACKSPACE : PyCEGUI.Key.Backspace,
        GLFW_KEY_INSERT   : PyCEGUI.Key.Insert,
        GLFW_KEY_DELETE       : PyCEGUI.Key.Delete,
        GLFW_KEY_PAGE_UP    : PyCEGUI.Key.PageUp,
        GLFW_KEY_PAGE_DOWN  : PyCEGUI.Key.PageDown,
        GLFW_KEY_HOME      : PyCEGUI.Key.Home,
        GLFW_KEY_END       : PyCEGUI.Key.End,
        GLFW_KEY_KP_ENTER  : PyCEGUI.Key.NumpadEnter,
    }
    return _map.get(glfwKey, PyCEGUI.Key.Unknown)

