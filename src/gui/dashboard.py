from __future__ import division, absolute_import
import PyCEGUI
from .components import GuiWindow

class DashBoard(GuiWindow):
    def __init__(self, gui):
        super(DashBoard, self).__init__(gui)
        self.name = 'dashboard'

        win = self.win = self.gui.wmgr.loadLayoutFromFile("hud_proto.layout")
        win.setVisible(False)
        self.gui.root.addChild(win)
        self._wins.append(win)
    
        # disable input locking
        self._no_lock = True

    def reset(self):
        pass

    def refresh(self):
        pass

    def onShow(self):
        pass

    def close(self):
        self.hide()

    def close_cb(self, e):
        self.close()
