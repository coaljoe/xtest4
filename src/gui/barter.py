from __future__ import division, absolute_import
import PyCEGUI
from .components import GuiWindow, ItemGrid
from .notebook import NotebookWindow
from src.util import format_approx_number
from .util import cleanup_container

class Barter(NotebookWindow):
    def __init__(self, gui):
        super(Barter, self).__init__(gui)
        self.name = 'barter'
        self.modal_visible = True

        win = self.win = self.gui.wmgr.loadLayoutFromFile("barter.layout")
        win.setVisible(False)
        self.gui.root.addChild(win)
        self._wins.append(win)

        w = self.win.getChildRecursive("CloseButton")
        w.subscribeEvent('Clicked', self.close_cb)
        #win.subscribeEvent('Clicked', hide_parent_window_cb)
        #win.subscribeEvent('Clicked', self.close_button_cb)

        self.wins = []

        # update rigth pane
        #rsp = self.win.getChildRecursive('RightScrollablePane')
        rsp = self.win.getChildRecursive('RightSidePane')
        lsp = self.win.getChildRecursive('LeftSidePane')

        ## cleanup lsp
        #lsp.cleanupChildren()
        cleanup_container(lsp.getChildAtIdx(0))

        """
        w1 = self.gui.wmgr.createWindow("WindowsLook/YellowButton","w1" )
        w1.setSize(USize(UDim(0,50),UDim(0,50)));
        lsp.addChild(w1)
        """

        self.rg = ItemGrid(rsp, self.gui, 5, 4, name='RG')
        #self.rg.mouse_double_click_cb = self.rg_mouse_double_click_cb
        self.rg.mouse_click_cb = self.rg_mouse_click_cb
        self.rg.mouse_over_cb = self.rg_mouse_over_cb
        self.rg.mouse_out_cb = self.xg_mouse_out_cb

        self.lg = ItemGrid(lsp, self.gui, 5, 4, name='LG')
        #self.lg.mouse_double_click_cb = self.lg_mouse_double_click_cb
        self.lg.mouse_click_cb = self.lg_mouse_click_cb
        self.lg.mouse_over_cb = self.lg_mouse_over_cb
        self.lg.mouse_out_cb = self.xg_mouse_out_cb

        self.reset()

    def reset(self):
        self.lcont = None
        self.rcont = None

    def showFor(self, lcont, rcont):
        self.reset()
        self.lcont = lcont
        self.rcont = rcont

        self.lg.clear()
        self.lg.appendItems(lcont.items)
        print self.lg.len
        print self.lg.items

        self.rg.clear()
        self.rg.appendItems(rcont.items)
        print self.rg.len
        print self.rg.items
        print self.rg.len_grouped
        #print self.rg.itemsCounter
        print self.rg.items_grouped
        #exit()
        self.refresh()
        self.show()

    def refresh(self):
        #self.lcont.items = self.lg.items
        #self.rcont.items = self.rg.items
        #rWeightLabel.setProperty("Text", 'Weight: ' + str(self.rg.totalWeight))
        #rWeightLabel.setProperty("Text", 'Weight: ' + format_approx_number(self.rg.totalWeight, 'kg'))
        for prefix in ['Right', 'Left']:
            ig = self.rg if prefix == 'Right' else self.lg
            weightLabel = self.win.getChildRecursive(prefix + 'WeightLabel')
            weightLabel.setProperty("Text", 'Weight (kg): ' + format_approx_number(ig.totalWeight, ''))
            bulkLabel = self.win.getChildRecursive(prefix + 'BulkLabel')
            bulkLabel.setProperty("Text", 'Bulk (L): ' + format_approx_number(ig.totalBulk, ''))

    def onShow(self):
        pass

    def close(self):
        self.lcont.setItems(self.lg.items)
        self.rcont.setItems(self.rg.items)
        #import pdb; pdb.set_trace()
        self.hide()

    def close_cb(self, e):
        self.close()

    def lg_mouse_double_click_cb(self, e):
        return self.xg_mouse_double_click_cb(self.lg, e)
    def rg_mouse_double_click_cb(self, e):
        return self.xg_mouse_double_click_cb(self.rg, e)
    def xg_mouse_double_click_cb(self, g, e):
        print 'PONG'
        idx = e.window.getUserData()
        it = g.getItemBtnIdx(idx)
        if e.button == PyCEGUI.LeftButton:
            it.apply()
        self.refresh()

    def lg_mouse_click_cb(self, e):
        return self.xg_mouse_click_cb(self.lg, self.rg, e)
    def rg_mouse_click_cb(self, e):
        return self.xg_mouse_click_cb(self.rg, self.lg, e)
    def xg_mouse_click_cb(self, g, oth_g,  e):
        print 'BONG'
        idx = e.window.getUserData()
        #self.lg.removeItemIdx(0)
        if e.button == PyCEGUI.LeftButton:
            g.swapItemsWith(idx, oth_g)
        elif e.button == PyCEGUI.RightButton:
            g.swapItemsWith(idx, oth_g, single=True)
        elif e.button == PyCEGUI.MiddleButton:
            it = g.getItemBtnIdx(idx)
            it.apply()
        self.refresh()

    def lg_mouse_over_cb(self, e):
        return self.xg_mouse_over_cb(self.lg, e)
    def rg_mouse_over_cb(self, e):
        return self.xg_mouse_over_cb(self.rg, e)
    def xg_mouse_over_cb(self, g, e):
        #print 'PONG'
        idx = e.window.getUserData()
        it = g.getItemBtnIdx(idx)
        desc = it.name + "\n\n" + (it.long_description or it.description+'...')
        descText = self.win.getChildRecursive('DescriptionText')
        descText.setProperty("Text", desc)

    def xg_mouse_out_cb(self, e):
        pass
        #descText = self.win.getChildRecursive('DescriptionText')
        #descText.setProperty("Text", '')


