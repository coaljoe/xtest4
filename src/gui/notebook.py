from __future__ import division, absolute_import
import PyCEGUI
from .components import GuiWindow
from src.util import Singleton
from src.lib.aux3d import aux3d as aux
from src.conf import conf

class NotebookWindow(GuiWindow):
    # rather hack: fixme
    initialized = False
    bg = None
    def __init__(self, gui):
        super(NotebookWindow, self).__init__(gui)

        if not NotebookWindow.initialized:
            bg = NotebookWindow.bg = self.gui.wmgr.loadLayoutFromFile("notebook_bg.layout")
            self.gui.root.addChild(bg)        
            bg.setVisible(False)
            NotebookWindow.initialized = True

        # set for every notebook window
        NotebookWindow.bg.subscribeEvent('MouseClick', self.bg_mouse_click_cb)
        
        self._wins.append(NotebookWindow.bg)
        
    def show(self):
        super(NotebookWindow, self).show()
        self.gui.game.gui_halt = True
        aux.renderer.effect_number = conf.vars['gui_notebookbg_effect_number']
        
    def hide(self):
        super(NotebookWindow, self).hide()
        self.gui.game.gui_halt = False
        aux.renderer.effect_number = 0
        
    def bg_mouse_click_cb(self, e):
        self.hide()
    
    """
    def next_page_cb(self, e):
        super(NotebookWindow, self).next_page_cb(e)
        print 'play sound:  next_page'

    def prev_page_cb(self, e):
        super(NotebookWindow, self).prev_page_cb(e)
        print 'play sound:  prev_page'
    """

class Notebook(NotebookWindow):
    def __init__(self, gui):
        super(Notebook, self).__init__(gui)
        self.name = 'notebook'
        self.modal_visible = True

        win = self.win = self.gui.wmgr.loadLayoutFromFile("notebook.layout")
        win.setVisible(False)
        self.gui.root.addChild(win)
        self._wins.append(win)

        w = self.win.getChildRecursive("NextPageButton")
        w.subscribeEvent('MouseClick', self.next_page_cb)
        w = self.win.getChildRecursive("PrevPageButton")
        w.subscribeEvent('MouseClick', self.prev_page_cb)
        w = self.win.getChildRecursive("StatsButton")
        w.subscribeEvent('Clicked', self.hide_cb)
        w = self.win.getChildRecursive("InventoryButton")
        w.subscribeEvent('Clicked', self.inventory_button_cb)

        self.items = []
        self.a1 = None

    def refresh(self):
        # update stats
        char = self.game.char; ci = char.info
        role = 'Leader'
        w = self.win.getChildRecursive('CharDescLabel')
        w.setProperty('Text', ci.name + ', ' + ci.gender + ', ' + str(ci.age) + '\n' + role)

        w = self.win.getChildRecursive('CharStatsLabel')
        w.setProperty('Text', 'Ac %s\n' % str(char.actions) + \
                              'Wt %s\n' % str(char.wits) + \
                              'Eg %s\n' % str(char.ego))

        self.itemsTextColours = PyCEGUI.ColourRect(PyCEGUI.Colour(.3,.3,.3,1))
        self.itemsSelectColours = PyCEGUI.ColourRect(PyCEGUI.Colour(1,1,1,0.3))

        w = self.win.getChildRecursive('StateList')
        w.resetList()
        for i, name in enumerate(['Undamaged', 'Freezing', 'Tired', 'Drunk']):
            item = PyCEGUI.ListboxTextItem("state" + str(i))
            item.setText(name)
            item.setTextColours(self.itemsTextColours)
            item.setSelectionColours(self.itemsSelectColours)
            w.addItem(item)

        w = self.win.getChildRecursive('TraitsList')
        w.resetList()
        a1 = self.a1 = PyCEGUI.ListboxTextItem("SSSS")
        a1.setText("Short")
        a1.setTextColours(self.itemsTextColours)
        a1.setSelectionColours(self.itemsSelectColours)
        a1.setSelected(True)
        w.addItem(a1)
        #w.ensureItemIsVisible(0)

        w = self.win.getChildRecursive('TraitsList')
        for i, name in enumerate(['Bad sight', 'Lucky']):
            item = PyCEGUI.ListboxTextItem("trait" + str(i))
            item.setText(name)
            item.setTextColours(self.itemsTextColours)
            item.setSelectionColours(self.itemsSelectColours)
            #item.setSelectionBrushImage("TaharezLook", "ListboxSelectionBrush")
            w.addItem(item)


    def onShow(self):
        self.refresh()

    def next_page_cb(self, e):
        print 'bong'
        self.hide()
        self.gui.inventory.show()

    def prev_page_cb(self, e):
        self.hide()
        self.gui.stats.show()

    def inventory_button_cb(self, e):
        self.hide()
        self.gui.inventory.show()
