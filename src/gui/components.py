from __future__ import division, absolute_import
#from collections import Counter
import PyCEGUI
from PyCEGUI import USize, UDim, UVector2

class ItemGrid(object):
    def __init__(self, _w, gui, width, height, name=None):
        self._w = _w
        self.gui = gui
        self.name = 'grid' if name is None else name
        self.items = []
        #self.itemsCounter = Counter()
        self.width = width
        self.height = height
        self.btn_idx = []


        self.root = w = self.gui.wmgr.createWindow("WindowsLook/Static","%s_root" % self.name)
        w.setProperty("FrameEnabled", 'False')
        w.setProperty("BackgroundEnabled", 'False')
        self._w.addChild(w)

        self.reset()
        self.build()

    def appendItem(self, it, refresh=True):
        self.items.append(it)
        if refresh:
            self.refresh()

    def appendItems(self, list):
        for it in list:
            self.appendItem(it, refresh=False)
        self.refresh()

    def getItemBtnIdx(self, idx):
        ii = self.btn_idx[idx][0]
        return self.items[ii]

    def removeItem(self, it):
        self.items.remove(it)
        self.refresh()

    def removeItemIdx(self, idx):
        it = self.items.pop(idx)
        self.refresh()
        return it

    def removeItemBtnIdx(self, idx):
        # convert button index to item's index
        ii = self.btn_idx[idx].pop()
        return self.removeItemIdx(ii)

    def removeAllItemsBtnIdx(self, idx):
        # remove all items at btn idx
        iis = list(self.btn_idx[idx])
        self.btn_idx[idx] = []
        #print iis, self.items, len(self.items)
        r = []
        #for i, ii in enumerate(iis):
        #    r.append(self.removeItemIdx(ii-i))
        for i in iis:
            r.append(self.items[i])
        self.items = [i for j, i in enumerate(self.items) if j not in iis]
        self.refresh()
        return r

    def swapItemsWith(self, idx, oth, single=False):
        if not single:
            l = self.removeAllItemsBtnIdx(idx)
            oth.appendItems(l)
        else:
            it = self.removeItemBtnIdx(idx)
            oth.appendItem(it)

    def add_button(self, x, y, idx, image, typename, amt=''):
        w = self.gui.wmgr.createWindow("WindowsLook/ItemButton","%s_ib_%s_%s" % (self.name, x, y))
        w.setSize(USize(UDim(0,66),UDim(0,66)))
        w1 = self.gui.wmgr.createWindow("WindowsLook/StaticImage","%s_ib_%s_%s_img" % (self.name, x, y))
        w1.setSize(USize(UDim(0,64),UDim(0,64)))
        w1.setProperty("MousePassThroughEnabled", 'True')
        #w1.setProperty("MouseInputPropagationEnabled", 'True')
        w1.setProperty("BackgroundEnabled", 'False')
        w1.setProperty("FrameEnabled", 'False')
        w1.setProperty("Image", image)
        w.addChild(w1)
        w1.setPosition(UVector2(UDim(0,1), UDim(0,1)))
        w2 = self.gui.wmgr.createWindow("WindowsLook/StaticText","%s_ib_%s_%s_amt_label" % (self.name, x, y))
        w2.setSize(USize(UDim(0,15),UDim(0,15)))
        w2.setProperty("MousePassThroughEnabled", 'True')
        #w2.setProperty("MouseInputPropagationEnabled", 'True')
        w2.setProperty("BackgroundEnabled", 'False')
        w2.setProperty("FrameEnabled", 'False')
        #w2.setProperty("TextColours", 'tl:a0000000 tr:a0000000 bl:a0000000 br:a0000000')
        w2.setProperty("TextColours", 'tl:ffa66060 tr:ffa66060 bl:ffa66060 br:ffa66060')
        w2.setProperty("HorzFormatting", 'RightAligned')
        w2.setProperty("Font", 'Terminus-12')
        w2.setProperty("Text", str(amt))
        w.addChild(w2)
        w2.setPosition(UVector2(UDim(0,48), UDim(0,51)))
        self.root.addChild(w)
        self.itemButtons.append(w)
        w.setPosition(UVector2(UDim(0,65*x), UDim(0,65*y)))

        w.setUserData(idx)
        w.setWantsMultiClickEvents(True)
        w.subscribeEvent('Clicked', self.click_cb)
        w.subscribeEvent('MouseClick', self.mouse_click_cb)
        w.subscribeEvent('MouseDoubleClick', self.mouse_double_click_cb)
        w.subscribeEvent('MouseEntersArea', self.mouse_over_cb)
        w.subscribeEvent('MouseLeavesArea', self.mouse_out_cb)
        w.subscribeEvent('MouseButtonDown', self.mouse_down_cb)
        w.subscribeEvent('MouseButtonUp', self.mouse_up_cb)
        #w.subscribeEvent('Activated', self.mouse_over_cb)

        ii = [self.items.index(_) for _ in self.items if _.item_typename == typename] # hack?
        self.btn_idx.insert(idx, ii)
        
        self.buttonStates.setdefault(idx, {})
        #print self.buttonStates

    def build(self):
        sx, sy = self.width, self.height
        for y in xrange(sy):
            # hbars
            if y > 0:
                w = self.gui.wmgr.createWindow("WindowsLook/Static","%s_sep_y%s" % (self.name, y))
                w.setProperty("FrameEnabled", 'False')
                w.setProperty("BackgroundColours", 'tl:26000000 tr:26000000 bl:26000000 br:26000000')
                w.setPosition(UVector2(UDim(0,0), UDim(0,65*y)))
                w.setSize(USize(UDim(1,0), UDim(0,1)))
                self.root.addChild(w)

            # vbars
            if y == 1:
                for x in xrange(1, sx):
                    w = self.gui.wmgr.createWindow("WindowsLook/Static","%s_sep_x%s" % (self.name, x))
                    w.setProperty("FrameEnabled", 'False')
                    w.setProperty("BackgroundColours", 'tl:26000000 tr:26000000 bl:26000000 br:26000000')
                    w.setPosition(UVector2(UDim(0,65*x), UDim(0,0)))
                    w.setSize(USize(UDim(0,1), UDim(0,65*sy)))
                    self.root.addChild(w)

            for x in xrange(sx):
                idx = y * self.width + x
                #print idx, self.items, self.len
                if idx >= self.len_grouped:
                    continue
                g_it = self.items_grouped[idx]
                im = "items/" + g_it.inv_icon
                amt = self.len_typename(g_it.item_typename)
                self.add_button(x, y, idx, im, g_it.item_typename, amt if amt > 1 else '')
                
    def reset(self):
        self.buttonStates = {}
        self.itemButtons = []
        
    def clear(self):
        self.items = []
        self.refresh()
                
    def refresh(self):
        # cleanup
        cont = self.root
        dest_list = []
        for i in xrange(cont.getChildCount()):
            w = cont.getChildAtIdx(i)
            #print w, w.getName()
            if cont.isChild(w):
                #print 'destroy'
                dest_list.append(w)
                #cont.destroyChild(w)
                #w.destroy()
        for w in dest_list:
            w.destroy()
            
        self.reset()
        
        # rebuild
        self.build()

    def update(self):
        self.rebuild()

    def click_cb(self, e): pass
    def mouse_click_cb(self, e): pass
    def mouse_double_click_cb(self, e): pass
    def mouse_over_cb(self, e): pass
    def mouse_out_cb(self, e): pass
    def mouse_down_cb(self, e): pass
    def mouse_up_cb(self, e): pass

    def getItemButtonByIdx(self, idx):
        for b in self.itemButtons:
            if b.getUserData() == idx:
                return b

    def len_typename(self, typename):
        return len([_ for _ in self.items if _.item_typename == typename])

    @property
    def len(self): return len(self.items)
    @property
    def items_grouped(self):
        r = []
        for it in self.items:
            if it.item_typename not in [_.item_typename for _ in r]:
                r.append(it)
        return r
    @property
    def len_grouped(self): return len(self.items_grouped)
    @property
    def totalWeight(self):
        return round(sum([it.weight for it in self.items]), 2)
    @property
    def totalBulk(self):
        return round(sum([it.bulk for it in self.items]), 2)


class GuiWindow(object):
    def __init__(self, gui):
        #super(GuiWindow, self).__init__()
        self.gui = gui
        self.name = None
        self._wins = []
        self.visible = False
        self._no_lock = False # fixme
        self.modal_visible = False
        self.modal = False

    def __repr__(self):
        return "<GuiWindow %s>" % self.name

    def show(self, silent=False):
        if self.visible:
            if not silent:
                print 'already visible'
            return
        if self.modal_visible:
            self.gui.store_windows()
        elif self.modal:
            self.gui.store_nonmodal_windows()
        self.onShow()
        for w in self._wins:
            w.show()
        #self.gui.root.addChild(self._win)
        self.visible = True

    def hide(self, silent=False):
        if not self.visible:
            if not silent:
                print 'already hidden'
            return
        if self.modal_visible:
            self.gui.restore_windows()
        elif self.modal:
            self.gui.restore_nonmodal_windows()
        self.onHide()
        for w in self._wins:
            w.hide()
        self.visible = False

    def toggle(self):
        if self.visible: self.hide()
        else: self.show()
        
    def setPos(self, x, y):
        for w in self._wins:
            w.setPosition(UVector2(UDim(0,x), UDim(0,y)))
            
    def setEnabled(self, v):
        for w in self._wins:
            w.setEnabled(v)

    @property
    def game(self): return self.gui.game

    # overrides
    def onShow(self): pass
    def onHide(self): pass

    # callbacks
    def hide_cb(self, e): self.hide()
        
    # override
    def update(self, dt): pass