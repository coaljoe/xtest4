from __future__ import division, absolute_import
import os, json, argparse
from src.util import Singleton, DotDict

vars = {'disable_views': False}
data_path = os.path.join(os.path.dirname(__file__), '..', 'data') # FIXME
media_path = os.path.join(os.path.dirname(__file__), '..', 'media')
root_path = os.path.normpath(os.path.join(os.path.dirname(__file__), '..'))
src_path = os.path.join(os.path.dirname(__file__))

class Conf(object):
    __metaclass__ = Singleton
    def __init__(self):
        self.data_path = data_path
        self.media_path = media_path
        self.src_path = src_path
        # default vars
        self.vars = DotDict({
            'blender_path': 'blender',
            'nosound': False,
            'simple_audio': False,
            'enable_gui': True,
            'enable_dashboard': True,
            'enable_environment': False,
            #'resolution': (800, 600), # default resolution 4:3
            'resolution': (1024, 576), # default resolution 16:9
            'gui_notebookbg_effect_number': 0,
            })
        # run args
        self.args = None
        self.conffile = os.path.join(root_path, 'config.json')
        print 'using conf:', self.conffile

    def parse_args(self):
        ap = argparse.ArgumentParser()
        ap.add_argument("--level", "-l", type=str, help="level to play", metavar="NAME")
        ap.add_argument("-res", "--resolution", dest='res', type=str, help="resolution", metavar="WxH")
        ap.add_argument("-fs", "--fulscreen", action='store_true', dest='fs', help="full screen")
        ap.add_argument("-fs0", action='store_true', dest='fs0', help="full screen with current resolution")
        #ap.add_argument('-x', '--width', type=int, dest='width', help='window width', default=800)
        #ap.add_argument('-y', '--height', type=int, dest='height', help='window height', default=600)
        ap.add_argument("--nosound", action='store_true', help="disable sound")
        ap.add_argument("-d", "--debug", action='store_true', help="enable debug", default=False)
        ap.add_argument("-vars", dest='vars', nargs='+', help="pass conf.vars")
        self.args = ap.parse_args()

        if self.args.res:
            val = self.args.res.split('x')
            self.vars.resolution = int(val[0]), int(val[1])
            
        if self.args.vars:
            s = 'self.vars.update({%s})' % " ".join(self.args.vars)
            eval(s)

    def load(self):
        if os.path.exists(self.conffile):
            self.vars.update(json.load(open(self.conffile, 'r')))
        else:
            self.save()

    def save(self):
        json.dump(self.vars, open(self.conffile, 'w+'), indent=2)

conf = Conf()
