from __future__ import division, absolute_import
from time import time
from random import random
from math import *
from datetime import datetime, timedelta
from src.lib.aux3d import aux3d as aux
from src.event import event
from src.vector3 import Vector3
from src.geom import lerp, lerp_dict
from src.util import Timer
from src.conf import conf

dawn_vars = {
            'sound': 'dawn.ogg',
            'light_intensity': 0.7,
            'uniforms': {
                'ambient': (.4,)*3,
                'diffuse_intensity': 1.0,
                'specular_intensity': 1.0,
                'shadow_intensity': 0.2,
            }
            }
noon_vars = {
            'sound': 'noon.ogg',
            'light_intensity': 1.0,
            'uniforms': {
                'ambient': (.6,)*3,
                'diffuse_intensity': 1.0,
                'specular_intensity': 1.0,
                'shadow_intensity': 0.2,
            }
            }
dusk_vars = {
            'sound': 'dusk.ogg',
            'light_intensity': 0.5,
            'uniforms': {
                'ambient': (.5, .2, .2,),
                'diffuse_intensity': 1.0,
                'specular_intensity': 0.2,
                'shadow_intensity': 0.2,
            }
            }
night_vars = {
            'sound': 'night.ogg',
            'light_intensity': 0.2,
            'uniforms': {
                'ambient': (.1,) * 3,
                'diffuse_intensity': 0.5,
                'specular_intensity': 0.1,
                'shadow_intensity': 0.5,
            }
            }

class Environment(object):
    def __init__(self, game):
        self.game = game
        self.enabled = conf.vars['enable_environment']
        self.update_scene_timer = Timer()

        self.reset()
        self.setDefault()

        # events
        event.sub('ev_location_start', self.onLocationStart)

    def setDefault(self):
        self.daytime = time()
        self.time = datetime(2001, 1, 1, 12, 00)
        self.weather_type = 'snow'
        self.wind_speed = 1
        self.time_of_day = 'noon'
        self.temperature = -10

    def updateScene(self):
        s = self.game.scene
        #s.light.diffuse = Vector3(random(), random(), random())
        s.light.intensity = 1.0
        """
        aux.renderer.shader_uniforms.update({
            'ambient': (.6,)*3,
            'diffuse_intensity': 1.0,
            'specular_intensity': 1.0,
            'shadow_intensity': 0.2,
            })
        """
        h, m = self.time.hour, self.time.minute
        pnames = ['dawn', 'noon', 'dusk', 'night']
        pranges = [(0, 10), (10, 15), (15, 20), (20, 24)]
        pname = None
        if h in range(0, 10):
            pname = 'dawn'
        elif h in range(10, 15):
            pname = 'noon'
        elif h in range(15, 20):
            pname = 'dusk'
        elif h in range(20, 24):
            pname = 'dusk'
        else:
            print 'bad h', h
            raise Error
        pnext = pnames[(pnames.index(pname) + 1) % len(pnames)]
        prange = [x for x in pranges if h in range(*x)][0]
        d_cur = globals()[pname + '_vars']
        d_next = globals()[pnext + '_vars']
        #print prange, max(prange), d_cur, d_next
        #t = h / max(prange)
        #print h, m
        t = ((h*60)+m) / (max(prange)*60) # minutes
        #v = lerp_dict(d_cur, d_next, t)
        s.light.intensity = lerp(d_cur['light_intensity'], d_next['light_intensity'], t)
        #aux.renderer.shader_uniforms.update(d_cur['uniforms'])
        aux.renderer.shader_uniforms.update(lerp_dict(d_cur['uniforms'], d_next['uniforms'], t))
       
        if self.time_of_day != pname:
            self.time_of_day = pname
            print 'time of day:', self.time_of_day

    def reset(self):
        self.update_scene_timer.start()

    def onLocationStart(self):
        print 'Env.onLocationStart'

    def update(self, dt):
        if not self.enabled: return
        if self.update_scene_timer.dt >= 0.2:
            #print 'bang'
            self.updateScene()
            self.update_scene_timer.reset()
        self.time += timedelta(minutes=10*dt)
        self.update_scene_timer.update(dt)
