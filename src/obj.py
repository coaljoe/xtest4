from __future__ import division, absolute_import
#from game import Game
from itertools import count
from src.lib.aux3d.node import Transform
from src.event import event
from src import util

class Obj(Transform):
    typename = "obj"
    obj_counter = count()
    def __init__(self, **kw):
        super(Obj, self).__init__(**kw)
        self.name = util.get_autoname(self.typename)
        self.obj_id = self.obj_counter.next()
        self.spawned = False

    def __repr__(self):
        return "<Obj " + self.name + ">"

    def spawn(self):
        print "::obj spawn", self
        event.pub('ev_obj_spawn', self)
        self.spawned = True

    def respawn(self):
        print "::obj respawn", self
        event.pub('ev_obj_respawn', self)
        self.spawned = True

    def destroy(self):
        event.pub('ev_obj_destroy', self)

    #def onObjClick(self):
        #event.pub('ev_obj_click', self)

    def free(self):
        pass

    def update(self, dt):
        pass
