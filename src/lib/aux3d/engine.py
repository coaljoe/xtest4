from __future__ import division, absolute_import
from .vector3 import Vector3
from .render.render import *
from .scene import Scene
from .conf import conf
from .util import Singleton
from .resource import ResMgr

class Aux3d(object):
    __metaclass__ = Singleton
    def __init__(self):
        self.renderer = Renderer(self)
        
        self.resmgr = ResMgr()
        self.rm = self.resmgr

        self.scene = Scene()
        self.sm = self.scene
        self.renderer.scene = self.scene

        # XXX config
        self.conf = conf
        conf.compressed_textures = True
        conf.max_anisotropy = 8
        conf.frustum_culling = False

    def init(self, width, height):
        self.renderer.init(width, height)

    def deinit(self):
        pass

    def toggleDebugDraw(self):
        self.renderer.debugdraw = not self.renderer.debugdraw

    def render(self, cam=None):
        active_cam = self.scene.getActiveCamera()
        #import pdb; pdb.set_trace()
        self.renderer.render(active_cam if cam == None else cam)

    def update(self, dt):
        self.scene.update(dt)


aux3d = Aux3d()
