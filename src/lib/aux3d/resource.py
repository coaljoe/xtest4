from __future__ import division, absolute_import
from .log import log
from .mesh import Mesh
from .vector3 import Vector3

class Res(object):
    def __init__(self, type, loc):
        self.type = type
        self.loc = loc
        self.loaded = False

    def load(self):
        pass


class ResMgr(object):
    def __init__(self):
        self.ress = []

    def addRes(self, type, loc):
        res = Res(type, loc)
        self.ress.append(res)
        return res

    def addMesh(self, mdlLoc, matLoc):
        #mdl = self.addRes('mesh', mdlLoc)
        #mat = self.addRes('material', matLoc)
        m = Mesh()
        m.load(mdlLoc)
        m.setMaterial(matLoc)
        return m

