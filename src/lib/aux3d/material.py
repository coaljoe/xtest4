from __future__ import division, absolute_import
import json
from OpenGL.GL import *
from .vector3 import Vector3
from .shader import Shader
from .texture import Texture

class Material(object):
    _shader_cache = {}
    _shader_id_max = -1
    def __init__(self, path):
        self.name = None
        self.diffuse = Vector3(1, 1, 1)
        self.specular = Vector3(1, 1, 1)
        self.ambient = Vector3(.1, .1, .1)
        self.hardness = 50.0
        self.texture = None
        self.lightMap = None
        self.normalMap = None

        self.shader = None
        self.shader_id = None
        self._path = path
        self.load(path)


    def load(self, path):
        d = json.load(open('res/' + path))

        # fixme
        if 'diffuse' in d:
            self.diffuse = Vector3(*d['diffuse'])

        vs_path = d['shaders'][0]
        fs_path = d['shaders'][1]

        shader_hash = hash(vs_path + fs_path)
        if shader_hash not in Material._shader_cache:
            _shader = Shader(vs_path, fs_path, version=120)
            Material._shader_id_max += 1
            Material._shader_cache[shader_hash] = _shader

        self.shader_id = Material._shader_id_max
        self.shader = Material._shader_cache[shader_hash]
        self.shader.compile()

        # a shortcut
        self._shader = self.shader._shader

    def setTexture(self, path):
        self.texture = Texture(path, mag_filter=GL_LINEAR)
        self.shader.vs_macro += "#define _TEXTURE_MAP 1\n"
        self.shader.fs_macro += "#define _TEXTURE_MAP 1\n"
        self.texture.bind()
        self.shader.compile()
        self.texture.unbind()

    def setNormalMap(self, path):
        self.normalMap = Texture(path, mipmaps=True, anisotropy=False);#, compress=False)
        #self.normalMap = Texture(path);#, compress=False)
        self.shader.vs_macro += "#define _NORMAL_MAP 1\n"
        self.shader.fs_macro += "#define _NORMAL_MAP 1\n"
        self.normalMap.bind()
        self.shader.compile()
        self.normalMap.unbind()

    def setLightMap(self, path):
        self.lightMap = Texture(path)

    def free(self):
        if self.shader: self.shader.free()
        if self.texture: self.texture.free()
        if self.normalMap: self.normalMap.free()
        if self.lightMap: self.lightMap.free()
