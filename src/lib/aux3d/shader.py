from __future__ import division
from OpenGL.GL import *
from OpenGL.GL.shaders import *
import os

class Shader(object):
    def __init__(self, vs_path, fs_path, version=120):
        self.version = version
        self.vs_path = vs_path
        self.fs_path = fs_path

        self.vs_macro = ""
        self.fs_macro = ""
        self.vs_source = ""
        self.fs_source = ""
        self._shader = None
        self.load()

        self._uniform_locs_cache = {}

    def load(self):
        self.vs_macro = "#version %s\n" % self.version
        self.fs_macro = "#version %s\n" % self.version

        def _add_includes(_path):
            lines = open(_path).read().splitlines()
            for i, line in enumerate(lines):
                s = line.rstrip().split(' ')
                if s[0] == '#include':
                    filepath = s[1].strip('"')
                    #print "including: %s" %  filepath
                    txt = open(os.path.dirname(_path) + '/' + filepath).read().splitlines()
                    txt.insert(0, "\n/*#### %s ####*/" % filepath)
                    lines[i:i+1] = txt
                    #lines.insert(i, txt)
            return "\n".join(lines)

        self.vs_source = _add_includes('res/' + self.vs_path)
        self.fs_source = _add_includes('res/' + self.fs_path)

        self.compile()

    def compile(self):
        try:
            self._shader = compileProgram(
                compileShader(self.vs_macro + self.vs_source, GL_VERTEX_SHADER),
                compileShader(self.fs_macro + self.fs_source, GL_FRAGMENT_SHADER))
        except RuntimeError, e:
            print "\n", e[0], "Dump saved to: /tmp/dump.glsl"
            f = open('/tmp/dump.glsl', 'w+')
            f.write("\n".join(e[1]))
            f.close()
            exit(-1)

    def bind(self):
        glUseProgram(self._shader)

    def unbind(self):
        glUseProgram(0)

    def passUniform(self, t, name, *vals):
        if name not in self._uniform_locs_cache:
            _loc = glGetUniformLocation(self._shader, name)
            self._uniform_locs_cache[name] = _loc
        loc = self._uniform_locs_cache[name]
        t(loc, *vals)

    def passUniformAuto(self, name, val):
        if type(val) in (float, int):
            self.passUniform(glUniform1f, name, val)
        elif len(val) == 3:
            self.passUniform(glUniform3f, name, *val)
        else:
            raise NotImplementedError

    def free(self):
        glDeleteProgram(self._shader)
