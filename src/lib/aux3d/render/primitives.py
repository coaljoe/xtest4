from __future__ import division, absolute_import
import OpenGL
from OpenGL.GL import *
from OpenGL.GLU import *

def draw_aabb(aabb):
    min, max = aabb[0],aabb[1]
    glDisable(GL_TEXTURE_2D)
    glColor3f(1.0, 0.0, 0.0)

    mode = GL_LINE_LOOP

    #--- pos z
    glBegin(mode)
    glVertex3f(min.x,min.y,max.z)
    glVertex3f(max.x,min.y,max.z)
    glVertex3f(max.x,max.y,max.z)
    glVertex3f(min.x,max.y,max.z)
    glEnd()

    #--- pos x
    glBegin(mode)
    glVertex3f(max.x,min.y,max.z)
    glVertex3f(max.x,min.y,min.z)
    glVertex3f(max.x,max.y,min.z)
    glVertex3f(max.x,max.y,max.z)
    glEnd()

    #---- pos y
    glBegin(mode)
    glVertex3f(min.x,max.y,max.z)
    glVertex3f(max.x,max.y,max.z)
    glVertex3f(max.x,max.y,min.z)
    glVertex3f(min.x,max.y,min.z)
    glEnd()

    #--- neg z
    glBegin(mode)
    glVertex3f(min.x,min.y,min.z)
    glVertex3f(min.x,max.y,min.z)
    glVertex3f(max.x,max.y,min.z)
    glVertex3f(max.x,min.y,min.z)
    glEnd()

    #--- neg y
    glBegin(mode)
    glVertex3f(min.x,min.y,min.z)
    glVertex3f(max.x,min.y,min.z)
    glVertex3f(max.x,min.y,max.z)
    glVertex3f(min.x,min.y,max.z)
    glEnd()

    #--- neg x
    glBegin(mode)
    glVertex3f(min.x,min.y,min.z)
    glVertex3f(min.x,min.y,max.z)
    glVertex3f(min.x,max.y,max.z)
    glVertex3f(min.x,max.y,min.z)
    glEnd()

def draw_cube(sx=1, sy=1, sz=1):
    glBegin(GL_QUADS)
    glVertex3f( sx, sy,-sz)
    glVertex3f(-sx, sy,-sz)
    glVertex3f(-sx, sy, sz)
    glVertex3f( sx, sy, sz)
    
    glVertex3f( sx,-sy, sz)
    glVertex3f(-sx,-sy, sz)
    glVertex3f(-sx,-sy,-sz)
    glVertex3f( sx,-sy,-sz)
    
    glVertex3f( sx, sy, sz)
    glVertex3f(-sx, sy, sz)
    glVertex3f(-sx,-sy, sz)
    glVertex3f( sx,-sy, sz)
    
    glVertex3f( sx,-sy,-sz)
    glVertex3f(-sx,-sy,-sz)
    glVertex3f(-sx, sy,-sz)
    glVertex3f( sx, sy,-sz)
    
    glVertex3f(-sx, sy, sz)
    glVertex3f(-sx, sy,-sz)
    glVertex3f(-sx,-sy,-sz)
    glVertex3f(-sx,-sy, sz)
    
    glVertex3f( sx, sy,-sz)
    glVertex3f( sx, sy, sz)
    glVertex3f( sx,-sy, sz)
    glVertex3f( sx,-sy,-sz)
    glEnd()

def draw_frustum(f):
    glLineWidth(2.5)
    glColor3f(1,0,0)     # top (near)
    glBegin(GL_LINE_LOOP)
    glVertex3fv(f[0])
    glVertex3fv(f[1])
    glVertex3fv(f[2])
    glVertex3fv(f[3])
    glEnd()

    glColor3f(0,1,0)     # bottom (far)
    glBegin(GL_LINE_LOOP)
    glVertex3fv(f[4])
    glVertex3fv(f[5])
    glVertex3fv(f[6])
    glVertex3fv(f[7])
    glEnd()

    glColor3f(0,0,1)     # sides in
    glBegin(GL_LINES)
    glVertex3fv(f[0])
    glVertex3fv(f[4])

    glVertex3fv(f[1])
    glVertex3fv(f[5])

    glVertex3fv(f[2])
    glVertex3fv(f[6])

    glVertex3fv(f[3])
    glVertex3fv(f[7])
    glEnd()


