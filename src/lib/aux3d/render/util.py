from __future__ import division, absolute_import
import OpenGL
from OpenGL.GL import *
from OpenGL.GLU import *

def createTexture(width, height, depth=False):
    tex_id = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, tex_id)

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)

    if not depth:
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, None)
    else:
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, None)
        #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL)
        #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE)

    glBindTexture(GL_TEXTURE_2D, 0)

    return tex_id

def id2c(id):
    r = id // 65536
    g = (id - r * 65536) // 256
    b = (id - r * 65536 - g * 256)
    return (r / 255., g / 255., b / 255.)

def c2id(r, g, b):
    return b + g * 256 + r * 256 * 256

def draw_grid(size=10, cell_size=1):
    glPushAttrib(GL_CURRENT_BIT)
    glLineWidth(1.0)
    n, s = size, cell_size
    glBegin(GL_LINES);
    for i in xrange(0, n, s):
        if i==0:  glColor3f(.3,.3,.6)
        else: glColor3f(.25,.25,.25) 
        glVertex3f(i,0,0)
        glVertex3f(i,0,n)
        if i==0: glColor3f(.6,.3,.3)
        else: glColor3f(.25,.25,.25)
        glVertex3f(0,0,i)
        glVertex3f(n,0,i)
    glEnd()
    glPopAttrib()

