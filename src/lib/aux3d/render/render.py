from __future__ import division, absolute_import
from ..conf import conf
import math
from random import random
import OpenGL
if conf.debug:
    OpenGL.ERROR_ON_COPY = True
    OpenGL.FULL_LOGGING = False # XXX slow
from OpenGL.GL import *
from OpenGL.GL.framebufferobjects import *
from OpenGL.GL.shaders import *
from OpenGL.GLU import *
import numpy as np
from .primitives import *
from .util import *
from ..shader import Shader

class RenderTarget(object):
    def __init__(self, name, width, height, depth=False, only_color=False):
        self.name = name
        self.depth = depth
        self.width = width
        self.height = height
        self.only_color = only_color

        # gl
        self.fbo_id = None
        self.tex_id = None

        self.create()
        #event.on_rendertarget_create()

    def create(self):
        # create a color texture
        self.tex_id = createTexture(self.width, self.height, self.depth)

        # tune the texture
        glBindTexture(GL_TEXTURE_2D, self.tex_id)
        if self.depth:
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        else:
            # need for fxaa
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)

        # create FBO
        self.fbo_id = glGenFramebuffersEXT(1)
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, self.fbo_id)

        if self.only_color:
            glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, self.tex_id, 0)
        else:
            # depth buffer
            rb_depth = glGenRenderbuffers(1)
            glBindRenderbuffer(GL_RENDERBUFFER, rb_depth)
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, self.width, self.height)
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rb_depth)

            # attach the texture
            if not self.depth:
                glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, self.tex_id, 0)
            else:
                glDrawBuffer(GL_NONE)
                glReadBuffer(GL_NONE)
                glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, self.tex_id, 0)
        #glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, self.tex_id, 0)

        checkFramebufferStatus()
        status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT)
        if status != GL_FRAMEBUFFER_COMPLETE_EXT:
            print 'bad fbo. status =',  status
            exit()

        # unbind
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)
        glBindRenderbuffer(GL_RENDERBUFFER, 0)

    def bind(self):
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, self.fbo_id)

    def unbind(self):
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)


class Renderer(object):
    def __init__(self, aux):
        self.scene = None
        self.aux = aux # aux
        self.debugdraw = False
        self.showbuffers = False

        self._setup()
        self._curmat = None # fixme
        self._curcam = None # fixme
        self.shadowTex = None
        self.shadowMatrix = None
        self._use_shadowMap = False
        self.use_shadows = False
        self.use_fxaa = True
        self.use_sharpen_pass = True
        self.use_selbuf = False
        self.use_gui = False
        self.effect_number = 0

        self.camera_shader = None
        self.frame = 0
        self.width = None
        self.height = None

        self.shader_uniforms = {}

    def _setup(self):
        pass
        """
        glClearColor(0.1,0.1,0.1,1.)
        glClearDepth(1.0)
        glEnable(GL_CULL_FACE)
        glEnable(GL_DEPTH_TEST)
        glDepthFunc(GL_LEQUAL)
        """
    def init(self, width, height):
        # fixme, temporary
        self.camera_shader = Shader('shaders/camera_fsquad.vs',
                                    'shaders/camera_fsquad.fs', version=130)
        self.camera_shader.compile()

        self.camera_pass2 = Shader('shaders/camera_fsquad.vs',
                                   'shaders/camera_pass2.fs', version=130)
        self.camera_pass2.compile()

        self.tmp_rt = RenderTarget('tmp_rt', width, height)

        if self.use_selbuf:
            self.selbuf_rt = RenderTarget('selbuf_rt', width//8, height//8, only_color=True)

    def set_camera(self, cam):
        self._curcam = cam
        # important
        sx, sy = (cam.rt.width, cam.rt.height) if cam.rt else (self.width, self.height)
        glViewport(0,0,sx,sy)

        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()

        if cam.ortho:
            glOrtho(*cam.ortho_planes + [cam.znear, cam.zfar])
        else:
            gluPerspective(cam.fov * 180/math.pi, cam.aspect, cam.znear, cam.zfar)

        # set camera position
        # transform order: SRT
        glRotatef(cam.rot.x, -1, 0, 0) # pitch
        glRotatef(cam.rot.y, 0, -1, 0) # heading
        glRotatef(cam.rot.z, 0, 0, -1) # roll
        glTranslatef(*-cam.pos)
        #glTranslatef(1, 1, -10)
        #glRotatef(90, 1, 0, 0)
        """
        if cam.lookTarget:
            pos = cam.pos
            at = cam.lookTarget
            gluLookAt(pos[0], pos[1], pos[2], at[0], at[1], at[2], 0, 1, 0)
        """

        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        """
        cam = transform(camera.position, camera.transformation)
        at = transform(camera.lookat, camera.transformation)
        gluLookAt(cam[0], cam[2], -cam[1],
                   at[0],  at[2],  -at[1],
                       0,      1,       0)
        """

        if conf.frustum_culling:
            # /* Get the current PROJECTION matrix from OpenGL */
            #proj = glGetFloatv( GL_PROJECTION_MATRIX).flatten()
            proj = glGetFloatv( GL_PROJECTION_MATRIX)
            proj = np.hstack(proj)

            # /* Get the current MODELVIEW matrix from OpenGL */
            #modl = glGetFloatv( GL_MODELVIEW_MATRIX).flatten()
            modl = glGetFloatv( GL_MODELVIEW_MATRIX)
            modl = np.hstack(modl)
            #print proj, proj[12]
            #exit()
            cam.updateFrustum(proj, modl)

        # save camera matrix
        cam._proj_matrix = glGetDoublev(GL_PROJECTION_MATRIX)
        cam._mv_matrix = glGetDoublev(GL_MODELVIEW_MATRIX)
        cam._vp = glGetIntegerv(GL_VIEWPORT)

    def set_material(self, mat):
        #XXX FIXME add error check code
        shader = mat.shader
        shader.bind()
        # pass global shader uniforms
        for k,v in self.shader_uniforms.iteritems():
            shader.passUniformAuto(k, v) # fixme
        if self.shader_uniforms:
            print self.shader_uniforms
        # pass parameters to shader
        shader.passUniform(glUniform3f, 'mat.diffuse', *mat.diffuse)
        shader.passUniform(glUniform3f, 'mat.specular', *mat.specular)
        shader.passUniform(glUniform1f, 'mat.hardness', mat.hardness)
        #print self.sm.light0.pos
        #cam = self.sm.light0.camera
        #cam = self.sm.lights[0].camera
        #glUniform3f(glGetUniformLocation(mat._shader, 'lightPos'), cam.x, cam.y, cam.z)
        shader.passUniform(glUniform3f, 'lightPos', self.scene.light0.x, self.scene.light0.y, self.scene.light0.z)
        shader.passUniform(glUniform1f, 'light.intensity', self.scene.light0.intensity)
        #glUniform4f(glGetUniformLocation(mat._shader, 'eyePos'), self.sm.cam0.x, self.sm.cam0.y, self.sm.cam0.z, 1)
        if self._use_shadowMap:
            shader.passUniform(glUniform1i, 'use_shadows', 1)


        # texture
        if mat.texture:
            glActiveTexture(GL_TEXTURE0)
            glBindTexture(GL_TEXTURE_2D, mat.texture.tex_id)
            shader.passUniform(glUniform1i, 'texMap', 0)
            glActiveTexture(GL_TEXTURE0)

        # lightmap
        if mat.lightMap:
            glActiveTexture(GL_TEXTURE1)
            glBindTexture(GL_TEXTURE_2D, mat.lightMap.tex_id)
            shader.passUniform(glUniform1i, 'lightMap', 1)
            glActiveTexture(GL_TEXTURE0)

        # normalmap
        if mat.normalMap:
            glActiveTexture(GL_TEXTURE2)
            glBindTexture(GL_TEXTURE_2D, mat.normalMap.tex_id)
            shader.passUniform(glUniform1i, 'normalMap', 2)
            glActiveTexture(GL_TEXTURE0)

        # shadow
        if self._use_shadowMap:
            glActiveTexture(GL_TEXTURE3)
            glBindTexture(GL_TEXTURE_2D, self.shadowMap)
            shader.passUniform(glUniform1i, 'shadowMap', 3)
            shader.passUniform(glUniformMatrix4fv, 'shadowMatrix', 1, False, self.shadowMatrix)

            glActiveTexture(GL_TEXTURE0) # unset texture unit
            #glBindTexture(GL_TEXTURE_2D, 0)

        #self.lightmapTex.bind()
        self._curmat = mat

    def render_fsquad(self, width=1, height=1, z=0):
        glPushMatrix()
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(0, 1, 1, 0, -1, 1)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        glColor3f(0, 1, 0)
        #glBindTexture(GL_TEXTURE_2D, img)
        glTranslatef(0, 0, z);
        glBegin(GL_QUADS)
        glTexCoord2f(0,1); glVertex2f(0,0);
        glTexCoord2f(0,0); glVertex2f(0,height);
        glTexCoord2f(1,0); glVertex2f(width,height);
        glTexCoord2f(1,1); glVertex2f(width,0);
        glEnd()
        glPopMatrix()

    def render_mesh(self, mesh):
        #glActiveTexture(GL_TEXTURE0)
        #glPushMatrix()
        #if mesh.matrix != None:
        #    glMultMatrixf(mesh.matrix)

        glEnableClientState(GL_VERTEX_ARRAY)
        glEnableClientState(GL_NORMAL_ARRAY)

        glBindBuffer(GL_ARRAY_BUFFER, mesh.gl_data["vertices"])
        glVertexPointer(3, GL_FLOAT, 0, None)

        glBindBuffer(GL_ARRAY_BUFFER, mesh.gl_data["normals"])
        glNormalPointer(GL_FLOAT, 0, None)

        if mesh.hasUVs: # fixme
            for i in xrange(len(mesh.uvs)):
                glEnableClientState(GL_TEXTURE_COORD_ARRAY)
                glClientActiveTexture(GL_TEXTURE0 + i)
                glBindBuffer(GL_ARRAY_BUFFER, mesh.gl_data["texcoords" + str(i)])
                glTexCoordPointer(2, GL_FLOAT, 0, None)
                #glEnableClientState(GL_TEXTURE_COORD_ARRAY)

        if self._curmat and self._curmat.normalMap:
            if 'tangents' not in mesh.gl_data:
                print 'Error: no tangents for mesh with normalmap; (check mesh normals/data)'
                print 'hint: assimp cant create tangents for mesh with no normals'
                print 'hint: check assimp log for errors'
                print 'mesh: ', mesh.name
                exit(-1)
            glEnableClientState(GL_TEXTURE_COORD_ARRAY)
            glClientActiveTexture(GL_TEXTURE7)
            glBindBuffer(GL_ARRAY_BUFFER, mesh.gl_data["tangents"])
            glTexCoordPointer(3, GL_FLOAT, 0, None)

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.gl_data["faces"])
        glDrawElements(GL_TRIANGLES, mesh.gl_data['num_faces'] * 3, GL_UNSIGNED_SHORT, None)

        glDisableClientState(GL_VERTEX_ARRAY)
        glDisableClientState(GL_NORMAL_ARRAY)
        glDisableClientState(GL_TEXTURE_COORD_ARRAY)

        glBindBuffer(GL_ARRAY_BUFFER, 0)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)

        #glPopMatrix()

    def render_debug(self):
        if not self.debugdraw:
            return
        draw_grid()

        # fixme: only static objects supported
        for mesh_node in self.scene.meshes:
            if not mesh_node.visible:
                continue
            #glPushMatrix()
            #glTranslatef(*mesh_node.pos)
            draw_aabb(mesh_node.aabb)
            #glPopMatrix()

        for mesh_node in self.scene.meshes:
            if self._curcam.canSee(mesh_node):
                glPushMatrix()
                glTranslatef(*mesh_node.aabb_center)
                #print mesh_node.pos
                #if not str(mesh_node.mesh.material.name).startswith('snow'):
                glColor3f(0.0, 0.0, 1.0)
                quadratic = gluNewQuadric()
                gluQuadricDrawStyle(quadratic, GLU_LINE)
                gluSphere(quadratic, mesh_node.getBBoxRadius(), 6, 6)
                glPopMatrix()

        glPushMatrix()
        glTranslatef(*self.scene.light0.pos)

        # draw light
        glColor3f(1., 1., 1.)
        quadratic = gluNewQuadric()
        gluQuadricDrawStyle(quadratic, GLU_FLAT)
        gluSphere(quadratic, .08, 6, 6)

        glPopMatrix()

    def render_scene(self, disable_materials=False, _selbuf_color_ids=False):
        # save model matrix and apply node transformation
        #glPushMatrix()
        #m = node.transformation.transpose() # OpenGL row major
        #glMultMatrixf(m)

        # node: no speed up for shader_id based sorting
        sorted_meshes = sorted(self.scene.meshes, key=lambda m: m.mesh.material.name, reverse=True)

        prev_mat = None
        for mesh_node in sorted_meshes:
            if not mesh_node.visible:
                continue
            if conf.frustum_culling and (not self._curcam.canSee(mesh_node)):
                continue
            glPushMatrix()
            # transform order: TRS
            glTranslatef(*mesh_node.pos)
            if mesh_node.rot.length:
                #print mesh_node.name, mesh_node.rot
                glRotatef(mesh_node.rot.x, 1, 0, 0) # pitch
                glRotatef(mesh_node.rot.y, 0, 1, 0) # heading
                glRotatef(mesh_node.rot.z, 0, 0, 1) # roll
            if mesh_node.scale.length:
                glScalef(*mesh_node.scale)
            ## models pretransforms
            glRotatef(90, -1, 0, 0) # fixme: model's pretransform rotation matrix
            #if mesh_node.mesh._matrix != None:
            #    glMultMatrixf(mesh_node.mesh._matrix)
            #draw_aabb(mesh_node.aabb)
            mat = mesh_node.mesh.material
            if mat and not disable_materials:
                # XXX temporary disabled because of bug
                #if (prev_mat is None) or prev_mat.name != mat.name or (mat.name is None):
                if 1:
                    self.set_material(mat)
                #elif prev_mat:
                #    print prev_mat._path,  mat._path
                #    print prev_mat.name,  mat.name
                #    exit()
                prev_mat = mat
            elif _selbuf_color_ids:
                #glColor3f(random(), random(), random())
                glColor3f(*id2c(mesh_node.selbuf_id))
            try:
                self.render_mesh(mesh_node.mesh)
            except Exception as e:
                print "\nError: cant render mesh: '%s', node: '%s'\n" \
                        % (mesh_node.mesh.name, mesh_node.name)
                #print repr(e)
                import traceback; traceback.print_exc()
                exit(-1)
            glPopMatrix()

        # unset material
        glUseProgram(0)
        glBindTexture(GL_TEXTURE_2D, 0)

        #for child in node.children:
        #    self.recursive_render(child)

    def render_gui(self):
        # overrideable
        pass

    def render_shadow_pass(self):
        lcam = self.scene.lights[0].camera
        #import pdb; pdb.set_trace()

        glCullFace(GL_FRONT)
        #glCullFace(GL_BACK)
        glDepthFunc(GL_LESS)

        self.set_camera(lcam)

        # bind framebuffer
        lcam.rt.bind()
        #glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE)
        #glDepthMask(GL_TRUE)
        glClear(GL_DEPTH_BUFFER_BIT)

        glEnable(GL_POLYGON_OFFSET_FILL)
        glPolygonOffset(1,1)

        self.render_scene(disable_materials=True)

        glDisable(GL_POLYGON_OFFSET_FILL)

        lcam.rt.unbind()
        glBindFramebuffer(GL_FRAMEBUFFER, 0)

        ### calculate texture matrix
        #self.light.load_matrices()
        model_view = glGetDoublev(GL_MODELVIEW_MATRIX);
        projection = glGetDoublev(GL_PROJECTION_MATRIX);        
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        bias = [ 0.5, 0.0, 0.0, 0.0, 
                 0.0, 0.5, 0.0, 0.0,
                 0.0, 0.0, 0.5, 0.0,
                 0.5, 0.5, 0.5, 1.0]
        # bias*lightProjection*lightViewMatrix
        glLoadMatrixd(bias)
        glMultMatrixd(projection)
        glMultMatrixd(model_view)

        #glUniformMatrix4fv(self.shader.uniforms['shadowMatrix'], 1, False, glGetFloatv(GL_MODELVIEW_MATRIX))
        self.shadowMatrix = glGetFloatv(GL_MODELVIEW_MATRIX)
        #--    

        # finally
        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE)
        self._use_shadowMap = True

    def render_selbuf_pass(self, cam):
        self.set_camera(cam)
        glViewport(0,0,self.selbuf_rt.width,self.selbuf_rt.height) # fix viewport
        self.selbuf_rt.bind()
        glClearColor(0,0,0,1)
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)

        self.render_scene(disable_materials=True, _selbuf_color_ids=True)

        self.selbuf_rt.unbind()
        glBindFramebuffer (GL_FRAMEBUFFER, 0)

    def render(self, cam):
        if self.use_selbuf:
            #if self.frame % 2 == 0: ## cache misses ?
            self.render_selbuf_pass(cam)

        if self.use_shadows:
            glEnable(GL_DEPTH_TEST)
            glEnable(GL_CULL_FACE)
            #if self.frame % 16 == 0:
            self.render_shadow_pass()
            glDisable(GL_CULL_FACE)

        if cam.rt:
            cam.rt.bind()

        glBindTexture(GL_TEXTURE_2D, 0)
        glActiveTexture(GL_TEXTURE0)
        #self._setup()
        #glClearColor(0.1,0.1,0.1,1.)
        #glClearDepth(1.0)
        glEnable(GL_DEPTH_TEST)
        glDepthFunc(GL_LEQUAL)
        #glDepthFunc(GL_LESS)

        #wireframe = False
        #twosided = True
        #glPolygonMode(GL_FRONT_AND_BACK, GL_LINE if wireframe else GL_FILL)
        #glDisable(GL_CULL_FACE) if twosided else glEnable(GL_CULL_FACE)

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)

        glEnable(GL_CULL_FACE)
        glCullFace(GL_BACK)
        self.set_camera(cam)

        #f = cam._frustum
        #f = list(np.hstack(f))
        #print f
        #f = (list(f[5][0:3]), list(f[4][0:3]))
        #print f
        #draw_frustum(f)

        self.render_scene()
        self.render_debug()

        glDisable(GL_CULL_FACE)

        #frustum = ((-2,-1,-1), (2,-1,-1), (2,1,-1),(-2,1,-1), # top of frustum at z=-1
           #(-20,-10,-10), (20,-10,-10),(20,10,-10), (-20,10,-10))
        #draw_frustum(frustum)

        if self.showbuffers:
            glEnable(GL_TEXTURE_2D)
            glBindTexture(GL_TEXTURE_2D, self.shadowMap)
            self.render_fsquad(width=0.2, height=0.2, z=1)

        if cam.rt:
            cam.rt.unbind()
            if cam.active:

                self.tmp_rt.bind()
                # clear tmp_rt texture
                glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
                
                # pass1
                # render the cam.rt texture with fsquad shader
                glEnable(GL_TEXTURE_2D)
                self.camera_shader.bind()

                glActiveTexture(GL_TEXTURE0)
                glBindTexture(GL_TEXTURE_2D, cam.rt.tex_id)
                self.camera_shader.passUniform(glUniform1i, 'fboTex', 0)
                self.camera_shader.passUniform(glUniform1i, 'use_fxaa', self.use_fxaa)

                self.render_fsquad()

                glUseProgram(0)
                glBindTexture(GL_TEXTURE_2D, 0)

                self.tmp_rt.unbind()

                #glBindFramebuffer(GL_READ_FRAMEBUFFER, cam.rt.fbo_id)
                #glBindTexture(GL_TEXTURE_2D, cam.rt.tex_id);
                #glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, cam.rt.width, cam.rt.height)
                #glBindFramebuffer(GL_READ_FRAMEBUFFER, 0)

                # pass2
                # render to screen
                glEnable(GL_TEXTURE_2D)
                self.camera_pass2.bind()

                glActiveTexture(GL_TEXTURE0)
                glBindTexture(GL_TEXTURE_2D, self.tmp_rt.tex_id)
                #glBindTexture(GL_TEXTURE_2D, cam.rt.tex_id)
                self.camera_pass2.passUniform(glUniform1i, 'fboTex', 0)
                if self.effect_number:
                    self.camera_pass2.passUniform(glUniform1i, 'use_sharpen_pass', False)
                    self.camera_pass2.passUniform(glUniform1i, 'effect_number', self.effect_number)
                else:
                    self.camera_pass2.passUniform(glUniform1i, 'use_sharpen_pass', self.use_sharpen_pass)
                    self.camera_pass2.passUniform(glUniform1i, 'effect_number', self.effect_number)

                self.render_fsquad()

                glUseProgram(0)
                glBindTexture(GL_TEXTURE_2D, 0)

        if self.use_gui:
            glDisable(GL_DEPTH_TEST)
            self.render_gui()


        # unbind FBOs
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)
        self.frame += 1
