from __future__ import division
from .conf import conf
import os.path, logging, ctypes
from pprint import pprint
import OpenGL
if conf.debug:
    OpenGL.ERROR_ON_COPY = True
    OpenGL.FULL_LOGGING = True
    try:
        from OpenGL.arrays import numpymodule
        numpymodule.NumpyHandler.ERROR_ON_COPY = True
    except: pass
from OpenGL.GL import *
#from OpenGL.arrays import vbo
#from OpenGL.arrays import ArrayDatatype as ADT
import numpy as np
from contrib.pyassimp import core as pyassimp
from contrib.pyassimp.postprocess import *
from contrib.pyassimp.helper import *
pyassimp.logger.setLevel(logging.WARNING)
from .sceneloader import SceneLoader
from .material import Material
from .vector3 import Vector3


class Mesh(object):
    _max_id = 0
    def __init__(self):
        self.name = None
        self.gl_data = {}
        self._matrix = None
        self.material = None
        self.uvs = None
        self.aabb = None

        self.path = None
        self.hasUVs = False
        self.hasTangents = False

        self.static = False

        # set id
        self.id = Mesh._max_id
        Mesh._max_id += 1

    def setMaterial(self, path):
        self.material = Material(path)

    def load(self, path):
        self.path = 'res/' + path
        sl = SceneLoader()
        sl.load(self.path)
        self.fromMesh(sl.scene.meshes[0])
        sl.free()

    def fromMesh(self, mesh, _aabb_matrix=None):
        """ creade mesh from pyassimp's mesh.
        """
        #self.name = mesh.name #fixme
        props = mesh.material.properties
        # convert props keys from (k, 0L) to k
        #props = dict([(k[0],v) for k,v in props.items()])
        print mesh.name
        print props
        #pprint(props)
        #print 'diffuse' in props
        #print 'diffuse' in props.keys()

        # default model material
        self.material = Material('materials/model.json' if not self.static else
                                 'materials/static.json')
        self.material.name = props['name']
        self.material.diffuse = Vector3(*props['diffuse'][:3])
        self.material.specular = Vector3(*props["specular"][:3])
        self.material.hardness = float(props["shininess"])

        # properties semantics
        sem_diffuse = 1
        sem_normal = 6

        _file = dict(props.items()).get('file') # fixme
        if 'file' in props.keys(): #and _file != 'lightmap.png':
            print '\n_file', _file, '\n'
            file = os.path.dirname(self.path) + '/' + _file
            #import pdb; pdb.set_trace()

            # diffuse texture
            if ('mapmodeu', sem_diffuse) in props:
                self.material.setTexture(file)
                # diffuse + bumpmaping
                if ('mapmodeu', sem_normal) in props:
                    filepath = os.path.dirname(self.path) + '/' + props[('file', sem_normal)]
                    self.material.setNormalMap(filepath)
            # only normalmap XXX deprecated
            elif ('mapmodeu', sem_normal) in props:
                print 'deprecated error'
                exit(-1)
                #self.material.setNormalMap(file)
                #if _file == 'baked.png':
                #    print mesh.texturecoords
                #    print mesh.tangents
                #    #exit()

        if len(mesh.tangents) > 0:
            #print 'mesh.tangents\n', mesh.tangents
            self.hasTangents = True

        tcs = mesh.texturecoords
        #tcs = tcs[::-1] # flip
        #print tcs, type(tcs), tcs.dtype
        #print '-'*30
        if len(tcs) > 0:
            self.hasUVs = True
            uvs = []
            for i in range(len(tcs)):
                uvs.append(np.delete(tcs[i],2,1)) # convert to 2d

            self.uvs = uvs
            #print self.uvs
            #self.tcs = np.array(self.tcs, numpy.float32).reshape(-1,2)
            #print self.tcs, type(self.tcs)
            #exit()
        else:
            print("no texture coordinates")

        #print("uv-component-count:" + str(len(mesh.numuvcomponents)))
        #print tcs, type(tcs), tcs.size
        #print mesh.vertices, type(mesh.vertices)
        #print mesh.material

        # find aabb
        # borrowed from: https://pyrr.readthedocs.org/en/latest/_modules/pyrr/aabb.html

        #print mesh.vertices, type(mesh.vertices)
        points = mesh.vertices
        aabb = [Vector3(*numpy.amin( points, axis = 0 )),
                Vector3(*numpy.amax( points, axis = 0 ))]

        self.aabb = aabb
        #import pdb; pdb.set_trace()
        #print 'mesh aabb:', aabb
        #exit()

        # multiply aabb by _aabb_matrix if presented
        if _aabb_matrix is not None:
            assert type(_aabb_matrix) is np.ndarray
            min = np.dot(_aabb_matrix, np.append(self.aabb[0], 1.))
            max = np.dot(_aabb_matrix, np.append(self.aabb[1], 1.))
            self.aabb = [Vector3(*min[:3]), Vector3(*max[:3])]

        self.gl_data["num_faces"] = len(mesh.faces)
        self.prepare_gl_buffers(mesh)

    #def fromAssimp(self, node):
        #""" creade mesh from pyassimp's node.
        #"""
        #mesh = node.meshes[0]
        #self.prepare_gl_buffers(mesh)
        #self.gl_data["num_faces"] = len(mesh.faces)
        #self.matrix = node.transformation.transpose()

    def prepare_gl_buffers(self, mesh):
        """ Creates 3 buffer objets for each mesh, 
        to store the vertices, the normals, and the faces
        indices.
        """

        # Fill the buffer for vertex positions
        self.gl_data["vertices"] = glGenBuffers(1)
        self.gl_data["num_vertices"] = len(mesh.vertices)
        glBindBuffer(GL_ARRAY_BUFFER, self.gl_data["vertices"])
        glBufferData(GL_ARRAY_BUFFER, mesh.vertices, GL_STATIC_DRAW)

        # Fill the buffer for normals
        self.gl_data["normals"] = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.gl_data["normals"])
        glBufferData(GL_ARRAY_BUFFER, mesh.normals, GL_STATIC_DRAW)

        if self.hasUVs:
            # Fill the buffers for texcoords
            for i in xrange(len(self.uvs)):
                k = "texcoords" + str(i)
                self.gl_data[k] = glGenBuffers(1)
                glBindBuffer(GL_ARRAY_BUFFER, self.gl_data[k])
                glBufferData(GL_ARRAY_BUFFER, self.uvs[i], GL_STATIC_DRAW)
                #glBufferData(GL_ARRAY_BUFFER, ADT.arrayByteCount(self.tcs), ADT.voidDataPointer(self.tcs), GL_STATIC_DRAW)

            #print 'uvs:',self.uvs[1]
        
            #glVertexAttribPointer(self.gl_data['texcoords'], 2, GL_FLOAT, GL_FALSE, 2 * 4, 0)
            #glEnableVertexAttribArray(self.gl_data['texcoords'])

        if self.hasTangents:
            self.gl_data["tangents"] = glGenBuffers(1)
            glBindBuffer(GL_ARRAY_BUFFER, self.gl_data["tangents"])
            glBufferData(GL_ARRAY_BUFFER, mesh.tangents, GL_STATIC_DRAW)

        # Fill the buffer for vertex positions
        self.gl_data["faces"] = glGenBuffers(1)
        self.gl_data["num_faces"] = len(mesh.faces)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.gl_data["faces"])
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.faces.astype(np.ushort), GL_STATIC_DRAW)

        # Unbind buffers
        glBindBuffer(GL_ARRAY_BUFFER,0)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0)

    def free(self):
        # free material
        if self.material: self.material.free()
        # remove buffers
        buffers = [self.gl_data["vertices"], self.gl_data["normals"], self.gl_data["faces"]]
        if self.hasTangents: buffers += [self.gl_data["tangents"]]
        glDeleteBuffers(len(buffers), buffers)

    def get_triangles(self):
        PyBuffer_FromMemory = ctypes.pythonapi.PyBuffer_FromMemory
        PyBuffer_FromMemory.restype = ctypes.py_object
        ret = []
        faces = v_co = None

        ## vertex coordinates
        glBindBuffer(GL_ARRAY_BUFFER, self.gl_data["vertices"]) # data from vbo
        vp = glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY)
        buffer = PyBuffer_FromMemory( 
            ctypes.c_void_p(vp), self.gl_data["num_vertices"] * ctypes.sizeof(ctypes.c_float) * 3
        )
        #print self.gl_data["num_vertices"], len(buffer)
        v_co = np.frombuffer(buffer, np.float32)

        glUnmapBuffer(GL_ARRAY_BUFFER)
        glBindBuffer(GL_ARRAY_BUFFER,0)

        ## face indexes
        glBindBuffer(GL_ARRAY_BUFFER, self.gl_data["faces"]) # data from vbo
        vp = glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY)
        buffer = PyBuffer_FromMemory( 
            ctypes.c_void_p(vp), self.gl_data["num_faces"] * ctypes.sizeof(ctypes.c_ushort) * 3
        )
        #print self.gl_data["num_vertices"], len(buffer)
        faces = np.frombuffer(buffer, np.ushort)

        glUnmapBuffer(GL_ARRAY_BUFFER)
        glBindBuffer(GL_ARRAY_BUFFER,0)

        print len(faces), faces
        print len(v_co), v_co
        print self.gl_data["num_faces"], self.gl_data["num_vertices"]
        for i in xrange(0, len(faces), 3):
            face = faces[i:i+3] # face index
            v1 = v_co[face[0]:face[0]+3]
            v2 = v_co[face[1]:face[1]+3]
            v3 = v_co[face[2]:face[2]+3]
            ret.append((v1, v2, v3))

        return ret

