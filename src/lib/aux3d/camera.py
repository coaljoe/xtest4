from __future__ import division, absolute_import
from math import *
import numpy as np
from .vector3 import Vector3
from .render.render import RenderTarget
from .node import Node

class CameraNode(Node):
    activeCam = None
    def __init__(self, *args, **kw):
        Node.__init__(self, *args, **kw)
        self.fov = 45
        self.aspect = 1
        self.znear = 0.1
        self.zfar = 100
        self.ortho = False
        self.target = Vector3(0, 0, 0)
        self.lookTarget = None

        self.rt = None
        self.ortho_planes = None
        self._frustum = np.zeros((6,4), 'f')
        self._zoom = 1

        #self.size = None

        self._active = False
        self._disable_culling = False
        self._proj_matrix = None
        self._mv_matrix = None
        self._vp = None

    def setupView(self, fov, aspect, znear, zfar, ortho=False):
        self.fov = fov
        self.aspect = aspect
        self.znear = znear
        self.zfar = zfar
        self.ortho = ortho

        # reset scale
        self.zoom = 1
    
    @property
    def active(self):
        #return True
        return CameraNode.activeCam == self

    @active.setter
    def active(self, val):
        CameraNode.activeCam = self

    @property
    def zoom(self): return self._zoom

    @zoom.setter
    def zoom(self, zoom):
        self._zoom = zoom

        if self.ortho:
            ortho_scale = self.fov
            aspect_ratio = self.aspect
            xaspect = aspect_ratio
            yaspect = 1.0
            xaspect = xaspect * ortho_scale / (aspect_ratio*2)
            yaspect = yaspect * ortho_scale / (aspect_ratio*2)
            zoom = self._zoom
            self.ortho_planes = [-xaspect * zoom, xaspect * zoom, -yaspect * zoom, yaspect * zoom]

    def addRenderTarget(self, *args, **kw):
        self.rt = RenderTarget(*args, **kw)

    def updateFrustum(self, proj, modl):
        """ // I found this code here: http://www.markmorley.com/opengl/frustumculling.html
        // and decided to make it part of
        // the camera class just in case I might want to rotate
        // and translate the projection matrix. This code will
        // make sure that the Frustum is updated correctly but
        // this member is computational expensive with:
        // 82 muliplications, 72 additions, 24 divisions, and
        // 12 subtractions for a total of 190 operations. Ouch! """

        # /* Get the current PROJECTION matrix from OpenGL */
        #proj = glGetFloatv( GL_PROJECTION_MATRIX);

        # /* Get the current MODELVIEW matrix from OpenGL */
        #modl = glGetFloatv( GL_MODELVIEW_MATRIX);

        # /* Combine the two matrices (multiply projection by modelview) */
        # Careful, Note, that replication is simple scalars is OK, but replicate of objects
        # and lists is very bad.
        clip = [None,] * 16
        # clip = Numeric.zeros ( (16), 'f')
        clip[ 0] = modl[ 0] * proj[ 0] + modl[ 1] * proj[ 4] + modl[ 2] * proj[ 8] + modl[ 3] * proj[12];
        clip[ 1] = modl[ 0] * proj[ 1] + modl[ 1] * proj[ 5] + modl[ 2] * proj[ 9] + modl[ 3] * proj[13];
        clip[ 2] = modl[ 0] * proj[ 2] + modl[ 1] * proj[ 6] + modl[ 2] * proj[10] + modl[ 3] * proj[14];
        clip[ 3] = modl[ 0] * proj[ 3] + modl[ 1] * proj[ 7] + modl[ 2] * proj[11] + modl[ 3] * proj[15];

        clip[ 4] = modl[ 4] * proj[ 0] + modl[ 5] * proj[ 4] + modl[ 6] * proj[ 8] + modl[ 7] * proj[12];
        clip[ 5] = modl[ 4] * proj[ 1] + modl[ 5] * proj[ 5] + modl[ 6] * proj[ 9] + modl[ 7] * proj[13];
        clip[ 6] = modl[ 4] * proj[ 2] + modl[ 5] * proj[ 6] + modl[ 6] * proj[10] + modl[ 7] * proj[14];
        clip[ 7] = modl[ 4] * proj[ 3] + modl[ 5] * proj[ 7] + modl[ 6] * proj[11] + modl[ 7] * proj[15];

        clip[ 8] = modl[ 8] * proj[ 0] + modl[ 9] * proj[ 4] + modl[10] * proj[ 8] + modl[11] * proj[12];
        clip[ 9] = modl[ 8] * proj[ 1] + modl[ 9] * proj[ 5] + modl[10] * proj[ 9] + modl[11] * proj[13];
        clip[10] = modl[ 8] * proj[ 2] + modl[ 9] * proj[ 6] + modl[10] * proj[10] + modl[11] * proj[14];
        clip[11] = modl[ 8] * proj[ 3] + modl[ 9] * proj[ 7] + modl[10] * proj[11] + modl[11] * proj[15];

        clip[12] = modl[12] * proj[ 0] + modl[13] * proj[ 4] + modl[14] * proj[ 8] + modl[15] * proj[12];
        clip[13] = modl[12] * proj[ 1] + modl[13] * proj[ 5] + modl[14] * proj[ 9] + modl[15] * proj[13];
        clip[14] = modl[12] * proj[ 2] + modl[13] * proj[ 6] + modl[14] * proj[10] + modl[15] * proj[14];
        clip[15] = modl[12] * proj[ 3] + modl[13] * proj[ 7] + modl[14] * proj[11] + modl[15] * proj[15];

        # ### Use a shortened name to reference to our camera's Frustum (does 
        # ### not copy anything, just a ref to make code less wordy
        Frustum = self._frustum

        # /* Extract the numbers for the RIGHT plane */
        Frustum[0][0] = clip[ 3] - clip[ 0];
        Frustum[0][1] = clip[ 7] - clip[ 4];
        Frustum[0][2] = clip[11] - clip[ 8];
        Frustum[0][3] = clip[15] - clip[12];

        # /* Normalize the result */
        t = (sqrt( Frustum[0][0] * Frustum[0][0] + \
        Frustum[0][1] * Frustum[0][1] + Frustum[0][2] * Frustum[0][2] ));
        Frustum[0][0] /= t;
        Frustum[0][1] /= t;
        Frustum[0][2] /= t;
        Frustum[0][3] /= t;

        # /* Extract the numbers for the LEFT plane */
        Frustum[1][0] = clip[ 3] + clip[ 0];
        Frustum[1][1] = clip[ 7] + clip[ 4];
        Frustum[1][2] = clip[11] + clip[ 8];
        Frustum[1][3] = clip[15] + clip[12];

        # /* Normalize the result */
        t = sqrt( Frustum[1][0] * Frustum[1][0] + Frustum[1][1] * Frustum[1][1] + Frustum[1][2] * Frustum[1][2] );
        Frustum[1][0] /= t;
        Frustum[1][1] /= t;
        Frustum[1][2] /= t;
        Frustum[1][3] /= t;

        # /* Extract the BOTTOM plane */
        Frustum[2][0] = clip[ 3] + clip[ 1];
        Frustum[2][1] = clip[ 7] + clip[ 5];
        Frustum[2][2] = clip[11] + clip[ 9];
        Frustum[2][3] = clip[15] + clip[13];

        # /* Normalize the result */
        t = sqrt( Frustum[2][0] * Frustum[2][0] + Frustum[2][1] * Frustum[2][1] + Frustum[2][2] * Frustum[2][2] );
        Frustum[2][0] /= t;
        Frustum[2][1] /= t;
        Frustum[2][2] /= t;
        Frustum[2][3] /= t;

        # /* Extract the TOP plane */
        Frustum[3][0] = clip[ 3] - clip[ 1];
        Frustum[3][1] = clip[ 7] - clip[ 5];
        Frustum[3][2] = clip[11] - clip[ 9];
        Frustum[3][3] = clip[15] - clip[13];

        # /* Normalize the result */
        t = sqrt( Frustum[3][0] * Frustum[3][0] + Frustum[3][1] * Frustum[3][1] + Frustum[3][2] * Frustum[3][2] )
        Frustum[3][0] /= t;
        Frustum[3][1] /= t;
        Frustum[3][2] /= t;
        Frustum[3][3] /= t;

        # /* Extract the FAR plane */
        Frustum[4][0] = clip[ 3] - clip[ 2];
        Frustum[4][1] = clip[ 7] - clip[ 6];
        Frustum[4][2] = clip[11] - clip[10];
        Frustum[4][3] = clip[15] - clip[14];

        # /* Normalize the result */
        t = sqrt( Frustum[4][0] * Frustum[4][0] + Frustum[4][1] * Frustum[4][1] + Frustum[4][2] * Frustum[4][2] )
        Frustum[4][0] /= t;
        Frustum[4][1] /= t;
        Frustum[4][2] /= t;
        Frustum[4][3] /= t;

        # /* Extract the NEAR plane */
        Frustum[5][0] = clip[ 3] + clip[ 2];
        Frustum[5][1] = clip[ 7] + clip[ 6];
        Frustum[5][2] = clip[11] + clip[10];
        Frustum[5][3] = clip[15] + clip[14];

        # /* Normalize the result */
        t = sqrt( Frustum[5][0] * Frustum[5][0] + Frustum[5][1] * Frustum[5][1] + Frustum[5][2] * Frustum[5][2] );
        Frustum[5][0] /= t;
        Frustum[5][1] /= t;
        Frustum[5][2] /= t;
        Frustum[5][3] /= t;

        return

    def isSphereInFrustum(self, pos, radius):
        for i in xrange(6):
            f = self._frustum
            if (f[i][0] * pos.x + f[i][1] * pos.y + f[i][2] * pos.z + f[i][3] <= radius):
                return False
        return True

    def canSee(self, node):
        return self.isSphereInFrustum(node.aabb_center, node.getBBoxRadius())

