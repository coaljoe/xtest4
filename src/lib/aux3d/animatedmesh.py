from __future__ import division
from math import sin, acos, sqrt
from contrib.pyassimp.structs import Quaternion
from .mesh import Mesh
from .armature import Armature
from .vector3 import Vector3
from src.util import Timer # fixme

def quat_interpolate(pStart, pEnd, pFactor):
  # calc cosine theta
  cosom = pStart.x * pEnd.x + pStart.y * pEnd.y + pStart.z * pEnd.z + pStart.w * pEnd.w

  # adjust signs (if necessary)
  end = pEnd
  if(cosom < 0.0):
    cosom = -cosom;
    end.x = -end.x; #Reverse all signs
    end.y = -end.y;
    end.z = -end.z;
    end.w = -end.w;

  # Calculate coefficients
  sclp = sclq = 0.0
  if((1.0 - cosom) > 0.0001): # 0.0001 -> some epsillon
    # Standard case (slerp)
    omega = acos(cosom); # extract theta from dot product's cos theta
    sinom = sin(omega);
    sclp = sin( (1.0 - pFactor) * omega) / sinom;
    sclq = sin( pFactor * omega) / sinom;
  else:
    #Very close, do linear interp (because it's faster)
    sclp = 1.0 - pFactor;
    sclq = pFactor;

  pOut = Quaternion();
  pOut.x = sclp * pStart.x + sclq * end.x;
  pOut.y = sclp * pStart.y + sclq * end.y;
  pOut.z = sclp * pStart.z + sclq * end.z;
  pOut.w = sclp * pStart.w + sclq * end.w;
  return pOut

def quat_normalize(q):
  # compute the magnitude and divide through it
  mag = sqrt(q.x*q.x + q.y*q.y + q.z*q.z + q.w*q.w);
  if mag:
    invMag = 1.0/mag;
    q.x *= invMag;
    q.y *= invMag;
    q.z *= invMag;
    q.w *= invMag;
  return q

def quat_print(q):
  print 'Quat:', q.x, q.y, q.z, q.w


class AnimatedMesh(Mesh):
  def __init__(m):
    m.armature = None
    m.anim = None
    m.play_lock = False
    m.timer = Timer()
    super(AnimatedMesh, m).__init__()

  def fromMesh(m, mesh, **kw):
    super(AnimatedMesh, m).fromMesh(mesh, **kw)

    # add armature
    if len(mesh.bones) > 0:
      m.armature = Armature(mesh)

  def findRotationKey(m, time, nodeAnim):
    """return previos key of the time"""
    keys = nodeAnim.rotationkeys
    for i in range(len(keys)-1):
      if time < keys[i+1].time:
        print time, keys[i].time, keys[i+1].time
        return i, keys[i]

  def calcInterpolatedRotation(m, time, nodeAnim):
    """
    // we need at least two values to interpolate...
    if (pNodeAnim->mNumRotationKeys == 1) {
      Out = pNodeAnim->mRotationKeys[0].mValue;
      return;
    }
    """

    RotationIndex = m.findRotationKey(time, nodeAnim)[0]
    NextRotationIndex = (RotationIndex + 1)
    assert(NextRotationIndex < len(nodeAnim.rotationkeys))

    DeltaTime = nodeAnim.rotationkeys[NextRotationIndex].mTime - nodeAnim.rotationkeys[RotationIndex].mTime
    Factor = (time - nodeAnim.rotationkeys[RotationIndex].mTime) / DeltaTime
    assert(Factor >= 0.0 and Factor <= 1.0)

    StartRotationQ = nodeAnim.rotationkeys[RotationIndex].mValue
    EndRotationQ = nodeAnim.rotationkeys[NextRotationIndex].mValue

    q = Quaternion()
    Out = quat_interpolate(StartRotationQ, EndRotationQ, Factor)

    Out = quat_normalize(Out)

    #print Out
    return Out

  def fromAnimation(m, anim):
    print 'anim', type(anim)
    print 'props:'
    print anim.name
    print anim.duration
    print anim.tickspersecond
    print anim.channels
    c0 = anim.channels[0] # NodeAnim
    print c0
    #print dir(c0)
    print c0.name
    print c0.positionkeys
    print c0.rotationkeys
    for qk in c0.rotationkeys:
      print '->', qk.time, qk.value
    print m.findRotationKey(0, c0)
    print m.findRotationKey(0.05, c0)
    print m.findRotationKey(0.7, c0)
    print m.findRotationKey(0.65, c0)
    print m.findRotationKey(0.67, c0)
    q = m.calcInterpolatedRotation(0.1, c0)
    print q
    quat_print(q)
    m.anim = anim
    #exit()

  def playAnimation(m, name):
    print 'playing animation "%s"' % name
    anim = m.anim
    c0 = anim.channels[0] # NodeAnim
    for qk in c0.rotationkeys:
      print '->', qk.time, qk.value
    m.timer.reset()
    m.timer.start()
    m.play_lock = True

  def stopAnimation(m):
    print 'stopping animation'
    m.play_lock = False
    
  def update(m, dt):
    #print 'update animatedmesh'
    
    # update animation
    if m.play_lock:
      m.timer.update(dt)
      anim = m.anim
      c0 = anim.channels[0] # NodeAnim
      ret = m.findRotationKey(m.timer.time, c0)
      if ret:
        print ret
      else:
        m.stopAnimation()
#end