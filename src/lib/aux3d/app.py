#!/usr/bin/python
from __future__ import division, absolute_import
import os, sys
from time import time
from OpenGL.GL import *
from .contrib.glfw import *
from .engine import aux3d as aux


class Window(object):
    width = 800
    height = 600
    fullscreen = False
    name = 'app'
    _glfw_initialized = False

    def __init__(self, app):
        self._app = app
        self._glfw_window = None

        self.mouse_prev_x = 0
        self.mouse_prev_y = 0
        self.has_exit = False

        self._init_glfw()
        #glEnable(GL_MULTISAMPLE);
        # opengl 2.1
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2)
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1)
        #glfwWindowHint(GLFW_WINDOW_NO_RESIZE, True)
        #glfwWindowHint(glfw.FSAA_SAMPLES, 8)
        glfwWindowHint(GLFW_SAMPLES, 0)
        glfwWindowHint(GLFW_RESIZABLE, 0)
        #glfw.CreateWindow(800, 600, 8, 8, 8, 8, 24, 8, glfw.WINDOW)
        monitor = None
        if self.fullscreen:
            monitor = glfwGetPrimaryMonitor()
        glfwWindowHint(GLFW_RED_BITS, 8)
        glfwWindowHint(GLFW_GREEN_BITS, 8)
        glfwWindowHint(GLFW_BLUE_BITS, 8)
        glfwWindowHint(GLFW_ALPHA_BITS, 0)
        glfwWindowHint(GLFW_DEPTH_BITS, 24)
        glfwWindowHint(GLFW_STENCIL_BITS, 0)

        window = glfwCreateWindow(self.width, self.height, self.name, monitor, None)
        if not window:
            glfwTerminate()
            sys.exit()
        else:
            self._glfw_window = window
            print 'glfw window successfully created...'

        glfwMakeContextCurrent(window)


        #glfw.SwapInterval(0)
        glfwSetWindowPos(self._glfw_window, 100, 100)
        #glfw.Disable(glfw.MOUSE_CURSOR)
        
        # important
        glClearColor(0.0, 0.0, 0.0, 1.0)
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        glMatrixMode(GL_MODELVIEW)

        #glfw.Enable(glfw.STICKY_KEYS)
        #glfw.Disable(glfw.STICKY_KEYS)
        #glfw.Enable(GLFW_KEY_REPEAT)
        #glfwSetInputMode(GLFW_KEY_REPEAT)
        #glfw.SetWindowRefreshCallback(self.on_refresh)
        #glfwSetWindowSizeCallback(self._glfw_window, self.on_resize)
        glfwSetMouseButtonCallback(self._glfw_window, self._on_mouse_button)
        glfwSetScrollCallback(self._glfw_window, self._on_mouse_scroll)
        glfwSetCursorPosCallback(self._glfw_window, self._on_mouse_pos)
        glfwSetKeyCallback(self._glfw_window, self.on_key)
        #print 'bong'
        #import pdb; pdb.set_trace()

        self.event_handlers = [self]

    @classmethod
    def _init_glfw(cls):
        if not cls._glfw_initialized:
            glfwInit()
            cls._glfw_initialized = True

    @classmethod
    def get_video_resolution(cls):
        cls._init_glfw() 
        monitor = glfwGetPrimaryMonitor()
        w, h = glfwGetVideoMode(monitor)[0:2]
        return w, h
 

    #def on_resize(self, window, width, height):
    #    self.width = width
    #    self.height = height

    def on_mouse_pos(self, x, y, dx, dy):pass
    def _on_mouse_pos(self, window, x, y):
        #print x, y
        dx = x - self.mouse_prev_x
        dy = y - self.mouse_prev_y
        self.mouse_prev_x = x
        self.mouse_prev_y = y

        #self.on_mouse_pos(x, y, dx, dy)
        # fixme
        for h in self.event_handlers:
            if hasattr(h, 'on_mouse_pos'):
                h.on_mouse_pos(x, y, dx, dy)

    def on_mouse_button(self, button, action): pass
    def _on_mouse_button(self, window, button, action, mods):
        # fixme
        for h in self.event_handlers:
            if hasattr(h, 'on_mouse_button'):
                h.on_mouse_button(button, action)
                
    def on_mouse_scroll(self, delta): pass
    def _on_mouse_scroll(self, window, xoff, yoff):
        # fixme
        for h in self.event_handlers:
            if hasattr(h, 'on_mouse_scroll'):
                h.on_mouse_scroll(yoff)

    # override
    def on_key_press(self, key):
        pass
    def on_key_release(self, key):
        pass
    def on_key(self, window, key, scancode, action, mods):
        if action == GLFW_PRESS:
            for h in self.event_handlers:
                if hasattr(h, 'on_key_press'):
                    h.on_key_press(key)
        elif action == GLFW_RELEASE:
            for h in self.event_handlers:
                if hasattr(h, 'on_key_release'):
                    h.on_key_release(key)

    def isPressed(self, key):
        if type(key) != int:
            #key = ord(key.encode('latin-1').upper())
            key = ord(key.upper())
        #assert self._glfw_window != None
        #assert type(key) == int
        v = True if glfwGetKey(self._glfw_window, key) == GLFW_PRESS else False
        #if v: print key
        return v


class App(object):
    def __init__(self, windowCls):
        self.w = windowCls(self)

    def init(self):
        self.dt = 0.0
        self.lastTime = None
        self.frame = 0
        self.fps = 0

        aux.init(self.w.width, self.w.height)

        # engine options
        self.opt = None

        # resources
        self.res = None

        # load resources from disk

        # camera
        self.camera = aux.scene.addCameraNode("cam")
        self.camera.setupView(10, self.w.width / self.w.height, 0.1, 100, ortho=True)
        #self.camera.setupView(50, self.w.width / self.w.height, 0.1, 100, ortho=False)
        self.camera.addRenderTarget("camRT", self.w.width, self.w.height, depth=False)
        #self.camera.size = [self.w.width, self.w.height]

    def step(self):
        if self.w.has_exit:
            return False

        thisTime = time()
        if self.lastTime is None:
            self.lastTime = thisTime
        dt = (thisTime - self.lastTime)
        self.lastTime = thisTime
        self.dt = dt
        #print dt

        self._mainloopUpdate(dt)
        self._mainloopRender(dt)

        #h3d.finalizeFrame()
        #glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        
        """
        glPopAttrib()

        glBegin(GL_QUADS)
        glVertex3f(0, 0, 0)
        glVertex3f(0.1, 0.2, 0.3)
        glVertex3f(0.1, 0.2, 0.3)
        glEnd()

        glPushAttrib(GL_ALL_ATTRIB_BITS)
        """
        #glfw.SwapBuffers()

        weight_ratio = 0.8
        self.last_fps = self.fps
        if dt > 0:
            self.fps = (1/dt) * (1 - weight_ratio) + self.last_fps * weight_ratio

        self.frame += 1
        if self.frame % 60 == 0 and dt:
            #glfw.SetWindowTitle(str(int(1/dt)))
            glfwSetWindowTitle(self.w._glfw_window, str(int(self.fps)))
	
        return True

    def begin_custom_gl(self):
        glPopAttrib()

    def end_custom_gl(self):
        glPushAttrib(GL_ALL_ATTRIB_BITS)

    def flip(self):
        glfwPollEvents()
        glfwSwapBuffers(self.w._glfw_window)

    @property
    def running(self):
        return True if not self.w.has_exit else False

    def mainloop(self):
        ret = self.step()
        if not ret:
            aux.deinit()
            print "exiting..."
            #exit(0)

    def _mainloopUpdate(self, dt):
        aux.update(dt)

    def _mainloopRender(self, dt):
        aux.render()


def main():
    app = App(Window)
    app.init()
    app.mainloop()


if __name__ == '__main__':
    main()

