from __future__ import division, absolute_import
from math import *
from random import randint
import numpy as np
from .log import log
from .vector3 import Vector3
from src import geom

class Transform(object):
    def __init__(self, **kw):
        super(Transform, self).__init__(**kw)
        self.rot = Vector3(0, 0, 0)
        self.pos = Vector3(0, 0, 0)
        self.scale = Vector3(1, 1, 1)

    def transform(self, tx, ty, tz, rx, ry, rz, sx=None, sy=None, sz=None):
        self.pos = Vector3(tx, ty, tz)
        self.rot = Vector3(rx, ry, rz)
        if sx and sy and sz:
            self.scale = Vector3(sx, sy, sz)

    def lookAt2d(self, x, z):
        deg = geom.deg_between(self.pos.x, self.pos.z, x, z)
        self.rot.y = deg
        """
        #rotateZ
        a = radians(deg)
        x = self.rot.x * cos(a) - self.rot.z * sin(a)
        y = self.rot.y
        z = self.rot.x * sin(a) + self.rot.z * cos(a)
        self.rot = Vector(x, y, z)
        #self.rot.rotateZ(radians(deg))
        """

    def get_translation_matrix(self):
        return np.matrix([
            [1, 0, 0, self.pos.x],
            [0, 1, 0, self.pos.y],
            [0, 0, 1, self.pos.z],
            [0, 0, 0, 1]])

    def get_rotation_matrix(self):
        ax, ay, az = [radians(_) for _ in self.rot]
        #print self.rot, az, -az
        #az = -az
        rotx = np.matrix([
            [1, 0,       0,       0],
            [0, cos(ax),-sin(ax), 0],
            [0, sin(ax), cos(ax), 0],
            [0, 0,       0,       1]])
        roty = np.matrix([
            [cos(ay),  0, sin(ay), 0],
            [0,        1, 0,       0],
            [-sin(ay), 0, cos(ay), 0],
            [0,        0, 0,       1]])
        rotz = np.matrix([
            [cos(az),-sin(az), 0,  0],
            [sin(az), cos(az), 0,  0],
            [0,       0,       1,  0],
            [0,       0,       0,  1]])
        m = rotz * roty * rotx
        return m

    def get_scale_matrix(self):
        return np.matrix([
            [self.scale.x, 0,             0,            0],
            [0,             self.scale.y, 0,            0],
            [0,             0,            self.scale.z, 0],
            [0,             0,            0,            1]])

    def get_transform_matrix(self):
        # build transform matrix (aka model matrix)
        # m = Tm * Rm * Sm
        m = self.get_translation_matrix() * \
            self.get_rotation_matrix() * \
            self.get_scale_matrix()
        #print m
        #exit()
        return m

    @property
    def x(self): return self.pos.x
    @property
    def y(self): return self.pos.y
    @property
    def z(self): return self.pos.z
    @property
    def rx(self): return self.rot.x
    @property
    def ry(self): return self.rot.y
    @property
    def rz(self): return self.rot.z


class Node(Transform):
    def __init__(self, name=None):
        Transform.__init__(self)
        self.name = name

        self.parent = None
        self.child = None
        self.visible = True
        self.static = False

    #@override
    def free(self):
        raise NotImplementedError

class Bbox(object):
    def __init__(self):
        self.min = Vector3(0, 0, 0)
        self.max = Vector3(1, 1, 1)

class MeshNode(Node):
    def __init__(self, mesh, *args, **kw):
        Node.__init__(self, *args, **kw)
        self.mesh = mesh
        #self._matrix = np.identity(4)
        self._aabb_matrix = np.identity(4)

        #self.bbox = Bbox()
        #self.aabb = [Vector3(0, 0, 0), Vector3(1, 1, 1)]

        # min/max point
        self._aabb = mesh.aabb
        #self.aabb_size = self.aabb[1] - self.aabb[0]
        #self.aabb_center = (self.aabb[0] + self.aabb[1]) * 0.5
        self._aabb_cache = None

        # set selbuf id
        self.selbuf_id = randint(0, 2**24)

        # obj backlink fixme
        self.linked_obj = None

    @property
    def aabb(self):
        if self.static and self._aabb_cache is not None:
            return self._aabb_cache
        #if self._aabb_cache:
        #    return self._aabb_cache
        #print self.mesh.aabb
        #print self.matrix
        #print self.get_translation_matrix()
        #m = self.get_translation_matrix()
        m = self.get_transform_matrix()
        min = np.dot(m, np.append(self.mesh.aabb[0], 1.)).getA1()
        max = np.dot(m, np.append(self.mesh.aabb[1], 1.)).getA1()
        aabb = [Vector3(*min[:3]), Vector3(*max[:3])]
        self._aabb_cache = aabb
        #print 'aabb', aabb
        return aabb

    @property
    def aabb_center(self):
        #halfsize = (self.aabb[1] - self.aabb[0]) * 0.5
        center = (self.aabb[1] + self.aabb[0]) * 0.5
        return center

    def getBBoxRadius(self):
        #return max(map(lambda x: abs(x.length), self.aabb))
        halfsize = (self.aabb[1] - self.aabb[0]) * 0.5
        return halfsize.length()

    """
    @property
    def matrix(self):
        return self._matrix

    @matrix.setter
    def matrix(self, m):
        # convert transform matrix to pos, rot, scale
        #self.pos = Vector3(m[0], m[5], m[10])
        #self.pos = Vector3(m[0][3], m[1][3], m[2][3])
        self.pos = Vector3(m[3][0], m[3][1], m[3][1])
        self._matrix = m
        print m
    """

    def free(self):
        self.mesh.free()

    def __repr__(self):
        return self.__class__.__name__ + '(%s)' % self.name


class LightNode(Node):
    def __init__(self, *args, **kw):
        Node.__init__(self, *args, **kw)
        self.castShadow = False
        self.onlyShadow = False

        self.diffuse = Vector3(1, 1, 1)
        self.specular = Vector3(1, 1, 1)
        self.attenuation = Vector3(1, 1, 1)
        self.intensity = 1

        self.camera = None

    def setCamera(self, cam):
        self.camera = cam

# XXX fixme
from .camera import CameraNode
