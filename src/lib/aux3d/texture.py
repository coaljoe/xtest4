from __future__ import division, absolute_import
import os
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GL.EXT.texture_filter_anisotropic import *
from OpenGL.GL.EXT.texture_compression_s3tc import *
from OpenGL.GL.ARB.texture_compression import *
from PIL import Image
from .contrib import dds as dds
from .conf import conf
from .event import event

class Texture(object):
    def __init__(self, path=None, compress=None, mipmaps=False, anisotropy=True, mag_filter=GL_LINEAR):
        # gl
        self.gl = {}
        self.tex_id = None
        self.compress = compress
        self.mipmaps = mipmaps
        self.anisotropy = anisotropy
        self.mag_filter = mag_filter

        if path:
            self.load(path)
            event.pub('ev_texture_add', self)
    
    def load(self, path):
        ext = os.path.splitext(path)[1]
        im = ix = iy = None
        if ext != '.dds':
            im = Image.open(path)
            ix, iy, image = im.size[0], im.size[1], im.convert("RGB").tostring("raw", "RGB", 0, -1)
        #try:
        #except SystemError:
        #    ix, iy, image = im.size[0], im.size[1], im.tostring("raw", "RGBX", 0, -1)
        min_filter = None

        self.tex_id = glGenTextures(1)
        glBindTexture(GL_TEXTURE_2D, self.tex_id)
        glPixelStorei(GL_UNPACK_ALIGNMENT,1)
        if self.mipmaps:
            glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE)
            min_filter = GL_LINEAR_MIPMAP_LINEAR
        if ext == '.dds':
            #ok = dds.load_dds(path)
            dec = dds.DDSImageDecoder()
            ix, iy, format, size, data = dec.decode(path)
            print ix, iy, format, size, len(data)
            glCompressedTexImage2D(GL_TEXTURE_2D, 0, format, ix, iy, 0, size, data)
        elif self.compress or (self.compress == None and conf.compressed_textures):
            #glHint(GL_TEXTURE_COMPRESSION_HINT_ARB, GL_NICEST) ## slow?
            #glCompressedTexImage2D(GL_TEXTURE_2D, 0, GL_COMPRESSED_RGB_S3TC_DXT1_EXT, ix, iy, 0, ix*iy, image)
            #glTexImage2D(GL_TEXTURE_2D, 0, GL_COMPRESSED_RGB_S3TC_DXT1_EXT, ix, iy, 0, GL_RGB, GL_UNSIGNED_BYTE, image)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_COMPRESSED_RGB_ARB, ix, iy, 0, GL_RGB, GL_UNSIGNED_BYTE, image)
        else:
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, ix, iy, 0, GL_RGB, GL_UNSIGNED_BYTE, image)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, self.mag_filter)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_filter or GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        if self.anisotropy:
            maxAniso = min(conf.max_anisotropy, glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT))
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAniso)
        glBindTexture(GL_TEXTURE_2D, 0)

    def bind(self):
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, self.tex_id)

    def unbind(self):
        glBindTexture(GL_TEXTURE_2D, 0)

    def free(self):
        glDeleteTextures([self.tex_id])
        event.pub('ev_texture_remove', self)


class TextureMgr(object):
    def __init__(self, sm):
        self.sm = sm
        self.textures = []

        event.sub('ev_texture_add', self.onTextureAdd)
        event.sub('ev_texture_remove', self.onTextureRemove)

    def onTextureAdd(self, t):
        self.textures.append(t)

    def onTextureRemove(self, obj):
        self.textures.remove(t)
