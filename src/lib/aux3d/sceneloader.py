from __future__ import division
from math import *
from weakref import WeakSet
from OpenGL.GL import *
from OpenGL.GLU import *
import numpy as np
#import logging; logging.basicConfig(level=logging.DEBUG)
from contrib.pyassimp import core as pyassimp
from contrib.pyassimp.structs import Matrix4x4, Quaternion
from contrib.pyassimp.postprocess import *
from contrib.pyassimp.helper import *
pyassimp.logger.setLevel(logging.WARNING)
from .vector3 import Vector3

class SceneLoader(object):
    def __init__(self):
        self.scene = None
        self.anims = None
        self.path = None
        self.static = False
        self.nodes = WeakSet()

    def load(self, path, static=False):
        self.path = path
        self.static = static
        #self.scene = scene = pyassimp.load(path, processing=aiProcessPreset_TargetRealtime_MaxQuality)
        #self.scene = scene = pyassimp.load(path, processing=aiProcessPreset_TargetRealtime_Fast)
        #self.scene = scene = pyassimp.load(path, processing=aiProcessPreset_TargetRealtime_Quality)
        self.scene = scene = pyassimp.load(path, processing=aiProcessPreset_TargetRealtime_Quality
                | aiProcess_OptimizeMeshes | aiProcess_RemoveRedundantMaterials
        ##        | aiProcess_OptimizeGraph
        ##        | aiProcess_PreTransformVertices
                )
        #self.scene = scene = pyassimp.load(path, processing=aiProcessPreset_TargetRealtime_Quality | aiProcess_PreTransformVertices |
        #        aiProcess_OptimizeMeshes | aiProcess_OptimizeGraph | aiProcess_RemoveRedundantMaterials)
        print " meshes: %d" % len(scene.meshes)
        print " total faces: %d" % sum([len(mesh.faces) for mesh in scene.meshes])
        print "  materials: %d" % len(scene.materials)
        self.bb_min, self.bb_max = get_bounding_box(scene)
        print "  bounding box:" + str(self.bb_min) + " - " + str(self.bb_max)

        scene_center = [(a + b) / 2. for a, b in zip(self.bb_min, self.bb_max)]

        #for index, mesh in enumerate(scene.meshes):
        #    print index, mesh

        if self.scene.animations:
            self.anims = self.scene.animations
            print "Animations", self.anims

    def spawn(self, sm):
        """ Spawn all elements in scene
        """
        from .mesh import Mesh
        from .animatedmesh import AnimatedMesh

        """
        for index, mesh in enumerate(self.scene.meshes):
            print index, mesh
            m = Mesh()
            m.fromAssimp(mesh)
            n = MeshNode(m)
            sm.addMeshNode(n)
        """

        # root node's transform matrix
        rootMatrix = self.scene.rootnode.transformation
        #print rootMatrix
        #rootMatrix = np.rot90(np.flipud(rootMatrix)).transpose()
        #rootMatrix = np.flipud(np.rot90(rootMatrix))
        #rootMatrix = np.array([
                #[1, 0, 0, 0],
                #[0, 0, 1, 0],
                #[0, -1, 0, 0],
                #[0, 0, 0, 1]]).astype(np.float32)
        #print rootMatrix
        #exit()
        #rootMatrix = np.flipud(rootMatrix)
        """
        rootMatrix[0][0] = 1
        rootMatrix[1] = 0 
        rootMatrix[2] = 0 
        rootMatrix[1][1] =  1
        rootMatrix[2][2] =  1
        """
        #print rootMatrix

        def recur_node(node,level = 0):
            print("  " + "\t" * level + "- " + str(node))
            for mesh in node.meshes:
                isAnimated = self.anims and len(self.anims) > 0
                m = Mesh() if not isAnimated else AnimatedMesh()
                m.name = node.name
                m.path = self.path # fixme
                m.static = self.static # fixme

                # fixme: currently need for proper aabb caltuation
                #matrix1 = np.dot(rootMatrix, matrix).transpose().copy().astype(np.float32)
                #matrix1 = np.dot(rootMatrix, node.transformation).transpose().copy().astype(np.float32)
                matrix1 = rootMatrix
                #n._aabb_matrix = matrix1
                m.fromMesh(mesh, _aabb_matrix=matrix1) 

                if isAnimated:
                    print self.anims
                    print type(self.anims[0])
                    m.fromAnimation(self.anims[0])

                # fixme: model's pretransform matrix
                matrix = node.transformation
                #m._matrix = np.dot(rootMatrix, matrix).transpose().copy().astype(np.float32)
                matrix = np.dot(rootMatrix, matrix)
                #matrix = np.dot(matrix, rootMatrix)
                m._matrix = matrix
                #print m.name
                #print m._matrix
                #exit()

                # calculate final model matrix
                matrix = node.transformation
                matrix = np.dot(rootMatrix, matrix)
                #matrix = np.dot(matrix,  rootMatrix)
                m1 = Matrix4x4(*matrix.flatten())
                print dir(node)
                scale, q, pos = pyassimp.decompose_matrix(m1)
                rot = Vector3(0, 0, 0)
                #rot.y = atan2(2 * q.x * q.w + 2 * q.y * q.z, 1 - 2 * (sqz  + sqw)) # Yaw 
                #rot.x = asin(2 * (q.x * q.z - q.w * q.y)) # Pitch 
                #rot.z = atan2(2 * q.x * q.y + 2 * q.z * q.w, 1 - 2 * (sqy + sqz)) # Roll

                # normalize
                #len = q.w*q.w + q.x*q.x + q.y*q.y + q.z*q.z
                #factor = 1.0 / sqrt(len)
                ##q = q * factor
                #q = Quaternion(q.w*factor, q.x*factor, q.y*factor, q.z*factor)

                #if not (q.w*q.w + q.x*q.x, q.y*q.y + q.z*q.z) == 1:
                    #print "not normalized"
                    #print q.x, q.y, q.z, q.w
                    #exit(-1)
                # radians
                rx = atan2((2 * q.y * q.z) - (2 * q.x * q.w), (2 * q.x * q.x) + ((2 * q.y * q.y) - 1))
                ry = -asin(((2 * q.y * q.w) + (2 * q.x * q.z))) # theta
                rz = atan2((2 * q.z * q.w) - (2 * q.x * q.y), (2 * q.x * q.x) + ((2 * q.w * q.w) - 1)) # phi
                rot = Vector3(degrees(rx), degrees(ry), degrees(rz))
                #print pos, rot, scale
                #print pos.x, pos.y, pos.z
                #exit()
                #matrix1 = np.dot(rootMatrix, matrix).transpose().copy().astype(np.float32)
                #m.matrix = np.dot(rootMatrix, matrix).transpose().copy().astype(np.float32)
                n = sm.addMeshNode(m)
                n.name = node.name
                n.static = self.static

                n.scale = Vector3(scale.x, scale.y, scale.z)
                n.rot = Vector3(rot.x - 90, rot.y, rot.z) # node rotation - pretransform
                n.pos = Vector3(pos.x, pos.y, pos.z)
                #print n.name
                #print n.scale, n.rot, n.pos
                #print node.transformation
                #print rootMatrix
                #print m.matrix
                #print m._matrix
                #exit()

                # add light map (fixme)
                #if self.static:
                #    file = os.path.join(os.path.dirname(self.path), 'lightmap.png')
                #    #print file; exit()
                #    if os.path.exists(file):
                #        m.material.setLightMap(file)

                self.nodes.add(n)
            for child in node.children:
                recur_node(child, level + 1) 

        recur_node(self.scene.rootnode)

    def free(self):
        pyassimp.release(self.scene)

