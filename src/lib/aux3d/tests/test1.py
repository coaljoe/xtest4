#!/usr/bin/env python
from __future__ import division, absolute_import
import os, os.path, sys
sys.path.insert(0, os.path.join('..', '..'))
#sys.path.insert(0, '..')
#print sys.path
sys.dont_write_bytecode = True
debug = True
from math import *
from pprint import pprint
import logging
logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.ERROR)
import OpenGL
if not debug:
    OpenGL.ERROR_CHECKING = False
    OpenGL.ERROR_ON_COPY = False
    OpenGL.FULL_LOGGING = False
from OpenGL.GL import *
from aux3d.contrib.glfw import *
from aux3d import aux3d as aux
from aux3d import app
from aux3d.vector3 import Vector3

class Window(app.Window):
    def on_key_press(self, key):
        if key in (GLFW_KEY_ESCAPE, GLFW_KEY_F12) or glfwGetKey('q') == GLFW_PRESS:
            self.has_exit = True
        elif key == GLFW_KEY_F7:
            aux.toggleDebugDraw()
        elif key == GLFW_KEY_F8:
            aux.renderer.showbuffers = not aux.renderer.showbuffers
              
class XApp(app.App):
    def __init__(self, windowCls):
        # init base App
        app.App.__init__(self, windowCls)
        self.init()

        # setup main camera
        c = self.camera
        c.setupView(10, self.w.width / self.w.height, -20, 80, ortho=True)
        #c.transform(0, 10, 0,  -90 + -5, 160, -5)
        #c.transform(10, 5, 5,  -90 + -5, 160, -5)
        c.transform(10, 10, 10,  -35.264, 45, 0)
        c.active = True

        self.test()

    def test(self):
        #aux.loadScene("res/models/box/box.dae", static=True)
        #aux.loadScene("res/models/box/box.dae", static=False)
        #aux.loadScene("res/models/testscene.dae")
        #aux.loadScene("res/models/box.dae")
        aux.loadScene("res/levels/test_plane/test_plane.dae")
        #self.sceneNode = aux.addSceneNode("res/models/bath/bathroom.dae")
        #self.sceneNode.setMaterial('materials/blue.json')
        self.light = aux.addLightNode("light1")

    def _mainloopUpdate(self, dt):
        app.App._mainloopUpdate(self, dt)


xapp = XApp(Window)

rot = 0
light_rad = 5

print 'loop...'
while xapp.running:
    dt = xapp.dt
    global rot
    rot = (rot + 15 * dt) % 360    
    xapp.light.pos.x = cos(radians(rot)) * light_rad
    xapp.light.pos.z = sin(radians(rot)) * light_rad

    if xapp.step():
        #xapp.begin_custom_gl()
        #xapp.end_custom_gl()
        xapp.flip()

print "exiting..."
