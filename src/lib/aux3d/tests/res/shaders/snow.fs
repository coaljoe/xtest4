#include "uniforms.fs"
#include "lighting.fs"


void main() {
    vec3 color;

    //color = ambient + calcPhongLight(L, N, V, mat.diffuse, mat.specular,  mat.hardness,
    //                                 light.intensity);

    /** custom shading **/
    vec3 l = normalize(L); // light vec
    vec3 n = normalize(N); // normal vec
    vec3 v = normalize(-V); // view vec (eyeVec)
    //vec3 h = normalize(L + V);  // half vec
    vec3 h = H;


#ifdef _NORMAL_MAP
    n = texture2D(normalMap, gl_TexCoord[2].st).rgb * 2.0 - 1.0;
#endif

    float NdotL = max(dot(n,l), 0.0);
    float NdotH = max(dot(n,h), 0.0);

    // diffuse color ramp
    vec3 c1 = vec3(1.0, 1.0, 1.0);
    vec3 c2 = vec3(0.515, 0.527, 1.0);

    // specular	
    vec3 spec_color = vec3(1.0, 1.0, 1.0);
    const float spec_intensity = 0.1;
    const float shininess = 200.0;
    
    // ramp calcs
    const float amount = 1.0;
    vec3 normal = (gl_FrontFacing) ? n : -n;
    vec3 direction = v;
    float dot_dn = clamp(-dot(direction, normal), 0.0, 1.0);
    //float mix_value = pow(dot_dn, 1.0 / amount);
    float mix_value = dot_dn;

    /*
    vec4 ambient = vec4(0.1, 0.1, 0.1, 0);
    vec4 diffuse = mix(c1, c2, mix_value) * NdotL;
    vec4 specular = spec_color * spec_intensity * pow(NdotL, shininess);

    color = vec3(ambient + diffuse + specular);
    */

    //vec4 ambient = vec4(0.1, 0.1, 0.1, 0);
    const float _light_intensity = 0.8;
    const float _ambient_intensity = 0.4;
    vec3 diffuse = mix(c1, c2, mix_value) * (NdotL * (light.intensity*_light_intensity)); //0.8);
    vec3 specular = spec_color * spec_intensity * pow(NdotL, shininess);

    color = (ambient*_ambient_intensity) + diffuse;
    //color = vec3(ambient + diffuse + specular);

#ifdef _TEXTURE_MAP
    color *= texture2D(texMap, gl_TexCoord[0].st).rgb;
#endif
    
    // show receiving less shadow
    color *= calcShadow(shadow_intensity/2);

    gl_FragColor.rgb = color;
}

/* vim:set ft=glsl: */
