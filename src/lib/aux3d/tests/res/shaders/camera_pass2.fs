uniform sampler2D fboTex;
uniform bool use_sharpen_pass = true;

//vec2 buffersize = textureSize(fboTex, 0).xy;
//float step_w = 1.0/buffersize.x;
//float step_h = 1.0/buffersize.y;
const float step_w = 1.0/800;
const float step_h = 1.0/600;

const vec2 offset[9] = vec2[9](
    vec2(-step_w, -step_h),
    vec2(0.0, -step_h),
    vec2(step_w, -step_h),
    vec2(-step_w, 0.0),
    vec2(0.0, 0.0),
    vec2(step_w, 0.0),
    vec2(-step_w, step_h),
    vec2(0.0, step_h),
    vec2(step_w, step_h));

/* SHARPEN KERNEL
 0 -1  0
-1  5 -1
 0 -1  0
*/

const float kernel[9] = float[9](
    float(0.0), float(-0.1), float(0.0),
    float(-0.1), float(1.4), float(-0.1),
    float(0.0), float(-0.1), float(0.0));

void main()
{
    vec3 color;

    if (use_sharpen_pass)
    {
        vec3 sum = vec3(0.0);
        int i;
        for (i=0; i<9; i++) {
            vec3 _color = texture2D(fboTex, gl_TexCoord[0].st + offset[i]).rgb;
            sum += _color * kernel[i];
        }

        color = sum;
    }
    else {
        color = texture2D(fboTex, gl_TexCoord[0].st).rgb;
    }

    gl_FragColor.rgb = color;
}

/* vim:set ft=glsl: */
