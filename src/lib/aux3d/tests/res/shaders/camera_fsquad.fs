#include "lib/fxaa.fs"
uniform sampler2D fboTex;
uniform vec2 ScreenPos;
uniform int effect_number = 0;
uniform bool use_fxaa = true;
uniform bool gamma_correction = true;

void main()
{
    vec3 color;
    //color = vec3(0.0,0.0,1.0);

    // test
    //vec3 c2 = texture2D(fboTex, ScreenPos).rgb;
    //vec3 c2 = texture2D(fboTex, gl_TexCoord[0].st).rgb;
    //color = (color + c2) / 2;
    //color = vec3(color.r, max(color.g,  color.b), color.b);

    if (use_fxaa) {
        //vec2 buffersize = vec2(800,600);
        vec2 buffersize = textureSize(fboTex, 0).xy;

        vec2 rcpFrame; 
        rcpFrame.x = 1.0 / buffersize.x;
        rcpFrame.y = 1.0 / buffersize.y;
 
        vec2 pos = gl_FragCoord.xy / buffersize.xy;

        // Only used on FXAA Quality.
        // Choose the amount of sub-pixel aliasing removal.
        // This can effect sharpness.
        //   1.00 - upper limit (softer)
        //   0.75 - default amount of filtering
        //   0.50 - lower limit (sharper, less sub-pixel aliasing removal)
        //   0.25 - almost off
        //   0.00 - completely off
        //float QualitySubpix = 0.75;
        float QualitySubpix = 0.3;
        
        // The minimum amount of local contrast required to apply algorithm.
        //   0.333 - too little (faster)
        //   0.250 - low quality
        //   0.166 - default
        //   0.125 - high quality 
        //   0.033 - very high quality (slower)
        //float QualityEdgeThreshold = 0.033;
        //float QualityEdgeThreshold = 0.125;
        float QualityEdgeThreshold = 0.13;

        // skips dark areas from processing (note green luma)
        float QualityEdgeThresholdMin = 0.0625; //0.02;//0.0625; // ?
  
        float dummy1 = 0;
        vec4 dummy4 = vec4(0);
        color = vec3(FxaaPixelShader(pos, dummy4, fboTex, fboTex, fboTex,
                                     rcpFrame, dummy4, dummy4, dummy4,
                                     QualitySubpix, QualityEdgeThreshold, QualityEdgeThresholdMin,
                                     dummy1, dummy1, dummy1, dummy4));
    }
    else {
        color = texture2D(fboTex, gl_TexCoord[0].st).rgb;
    }


    /*** post processing ***/

    // night vision
    if(effect_number == 1)
    {
        color *= 0.3;
        //color *= 0.05;

        const float brightness = 4;
        float lum = dot(vec3(0.30, 0.59, 0.11), color);
        color = vec3(0.5, 0.6, 0.5) * max(.1, 1.0 - (lum*brightness));
    }
    else if(effect_number == 2)
    {
        float oExtinction = 0.4;
        color.r = mix(16.0/255.0, gl_FragColor.r ,oExtinction);
    }
    // sharpen
    else if(effect_number == 3)
    {
        const float step_w = 1.0/800;
        const float step_h = 1.0/600;

        const vec2 offset[9] = vec2[9](
            vec2(-step_w, -step_h),
            vec2(0.0, -step_h),
            vec2(step_w, -step_h),
            vec2(-step_w, 0.0),
            vec2(0.0, 0.0),
            vec2(step_w, 0.0),
            vec2(-step_w, step_h),
            vec2(0.0, step_h),
            vec2(step_w, step_h));

        /* SHARPEN KERNEL
         0 -1  0
        -1  5 -1
         0 -1  0
        */

        const float kernel[9] = float[9](
            float(0.), float(-.1), float(0.),
            float(-.1), float(1.4), float(-.1),
            float(0.), float(-.1), float(0.));

        vec3 sum = vec3(0.0);
        int i;
        for (i=0; i<9; i++) {
            // fixme chain post-processing
            vec3 _color = texture2D(fboTex, gl_TexCoord[0].st + offset[i]).rgb;
            sum += _color * kernel[i];
        }

        color = sum;
    }

    if(gamma_correction)
    {
        float gamma;
        //gamma = 2.2; // sRGB
        gamma = 0.8;
        color.rgb = pow(color.rgb, vec3(1.0/gamma));
    }

    
    gl_FragColor.rgb = color;
}

/* vim:set ft=glsl: */
