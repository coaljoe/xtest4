struct Material {
    vec3 diffuse;
    vec3 specular; // .1, .1, .1
    float hardness; // 50
};
struct Light {
    float intensity; // 1.0
};

uniform Material mat;
uniform Light light; // sun
uniform bool use_shadows = false;
const vec3 ambient = vec3(.6);
const float diffuse_intensity = 1.0;
const float specular_intensity = 1.0;
const float shadow_intensity = 0.2;
const int MAX_LIGHTS = 8;

varying vec3 L; // light position
varying vec3 N;
varying vec3 V;
varying vec3 H;

#ifdef _TEXTURE_MAP
uniform sampler2D texMap;
//varying vec2 UV;
#endif

#ifdef _NORMAL_MAP
uniform sampler2D normalMap;
//varying vec2 UV;
#endif

#define _SHADOWS 1
#ifdef _SHADOWS
// shadows
uniform sampler2D shadowMap;
varying vec4 shadowCoord;
#endif

/* vim:set ft=glsl: */
