uniform vec3 lightPos;

varying vec3 N;
varying vec3 L;
varying vec3 V;
varying vec3 H;

#ifdef _TEXTURE_MAP
//vec2 vertexUV;
//vec2 UV;
#endif

void main(void)
{
    // vertex position
    vec3 p = vec3(gl_ModelViewMatrix * gl_Vertex);

    N = normalize(gl_NormalMatrix*gl_Normal);
    V = p;
    L = vec3(gl_ModelViewMatrix*(vec4(lightPos,1)-gl_Vertex));
    H = normalize(L + normalize(V));

#ifdef _NORMAL_MAP
    vec3 n = normalize (gl_NormalMatrix * gl_Normal);
    vec3 t = normalize (gl_NormalMatrix * gl_MultiTexCoord1.xyz);
    //vec3 t = normalize (gl_NormalMatrix * tangent);
    //vec3 t = normalize (gl_NormalMatrix * (gl_Color.rgb - 0.5));
    vec3 b = cross (n,t) * gl_MultiTexCoord1.w;

    mat3 tbn = mat3(t,b,n);

    L = normalize(L) * tbn;
    L = normalize(L);

    H = normalize(L + normalize(V));
    H = H * tbn;
#endif

    gl_TexCoord[0] = gl_MultiTexCoord0;
    //gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;
    //UV = vertexUV;
    gl_Position = ftransform();
}

/* vim: set ft=glsl: */
