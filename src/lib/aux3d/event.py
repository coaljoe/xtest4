from __future__ import division, absolute_import
from .contrib import message
from .util import Singleton
#import zmq

class Event(object):
    __metaclass__ = Singleton
    """event subsystem
    """
    def __init__(self):
        #self.ctx = zmq.Context(1)
        #self.socket = self.ctx.socket(zmq.PULL)
        #self.socket.bind('tcp://127.0.0.1:5555')
        pass

    def sub(self, evname, handler):
        message.sub(evname, handler)

    def pub(self, evname, *args, **kw):
        message.pub(evname, *args, **kw)

    def update(self, dt):
        pass
        """
        data = None
        try:
            data = self.socket.recv(flags=zmq.NOBLOCK)
        except zmq.ZMQError:
            return

        ev_name = data.split(" ")[0]
        args = data.split(" ")[1:]
        self.pub(ev_name, *args)
        """

event = Event()

