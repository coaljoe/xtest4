from __future__ import division
from contrib.pyassimp import core as pyassimp
from contrib.pyassimp.structs import Matrix4x4
from .vector3 import Vector3

class BoneInfo(object):
  """BoneInfo"""
  def __init__(m):
    m.name = None
    m.pos = None
#end

class Armature(object):
  def __init__(m, mesh):
    m.bonesinfo = []
    m.fromMesh(mesh)

  def fromMesh(m, mesh):
    print 'creating armature from mesh', mesh
    assert mesh.bones
    print mesh.bones
    for bone in mesh.bones:
      bi = BoneInfo()
      bi.name = bone.name
      m1 = Matrix4x4(*bone.offsetmatrix.flatten())
      scale, q, pos = pyassimp.decompose_matrix(m1)
      bi.pos = Vector3(pos.x, pos.y, pos.z)
      m.bonesinfo.append(bi)
      #print b.weights
      #print dir(bone)
    print m.bonesinfo
    #exit(-1)
#end