from __future__ import division, absolute_import
import os.path
import numpy as np
from .log import log
from .mesh import Mesh
from .node import MeshNode, LightNode, CameraNode
from .animatedmesh import AnimatedMesh
from .sceneloader import SceneLoader
from src import geom
    
class Scene(object):
    """Scene and SceneManager functions
    """
    def __init__(self):
        self.nodes = []
        self.meshes = [] # mesh nodes
        self.animatedmeshes = []
        self.lights = []
        self.cameras = []
        self.light0 = None
        self.cam0 = None # active camera
    @property
    def _meshes(self):
        return [x.mesh for x in self.meshes]
    @property
    def visible_nodes(self, type=None):
        return [n for n in self.nodes if n.visible]

    def dump(self):
        print 'meshes:', self.meshes
        print 'entities:', self.entities
        for en in self.entities.values():
            print "id:", en.id, "pos:", en.pos

    def addCameraNode(self, name):
        cam = CameraNode(name)

        # set active cam if its the first camera
        if len(self.cameras) < 1:
            cam.active = True

        self.cameras.append(cam)
        return cam

    """
    def setActiveCamera(self, cam):
        # set active camera
        cam.active = True
        self.cam0 = cam

    def isActiveCamera(self, cam):
        return self.cam0 == cam
    """

    def addLightNode(self, name):
        l = self.light0 = LightNode(name)
        self.lights.append(l)
        return l

    def addMeshNode(self, mesh):
        """Create MeshNode from Mesh and add it to scene
        """
        n = MeshNode(mesh)
        self.meshes.append(n)
        if isinstance(mesh, AnimatedMesh):
            self.animatedmeshes.append(mesh)
        return n

    def removeMeshNode(self, mn):
        """Create MeshNode from Mesh and add it to scene
        """
        n = self.meshes.pop(self.meshes.index(mn))
        assert n
        if isinstance(mn.mesh, AnimatedMesh):
            self.animatedmeshes.pop(self.animatedmeshes.index(mn.mesh))
        return n # ??

    def _addMeshNode(self, meshLoc, materialLoc):
        m = Mesh()
        m.load(meshLoc)
        m.setMaterial(materialLoc)
        n = MeshNode(m)
        self.meshes.append(n)
        return m

    def loadScene(self, loc, static=False):
        sl = SceneLoader()
        sl.load(loc, static=static)
        sl.spawn(self)
        sl.free()
        return sl.nodes

    #deprecated
    def addNode(self, res):
        if isinstance(res, Mesh):
            n = MeshNode(res)
            self.meshes.append(n)
            return n
        else:
            print 'unsupportet'
            exit(1)

    def getActiveCamera(self):
        return [x for x in self.cameras if x.active][0]

    def getMeshNode(self, name):
        for x in self.meshes:
            if x.name == name:
                return x

    def findRayNodesIntersection(self, ray, nodes=None, exclude_static=True):
        if nodes is None: nodes = self.meshes
        for mn in nodes:
            if not mn.visible or (mn.static and exclude_static):
                continue
            #print mn
            #if mn.name == 'Plane': continue
            _aabb = mn.aabb
            aabb = np.array([
                [_aabb[0].x, _aabb[0].y, _aabb[0].z],
                [_aabb[1].x, _aabb[1].y, _aabb[1].z],
            ])
            res = geom.ray_intersect_aabb(ray, aabb)
            if res is not None:
                return mn

        return False

    def free(self):
        for m in self.meshes:
            m.free()
        self.meshes = []

    def update(self, dt):
        # update animated meshes
        for mesh in self.animatedmeshes:
            mesh.update(dt)


