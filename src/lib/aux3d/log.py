from __future__ import division
import logging
import logging.config 

# setup logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    #filename='/tmp/meshloader.log',
                    #filemode='w',
                    )
log = logging.getLogger('MeshLoader')
#log.setLevel(logging.INFO)
log.warning('test warning')