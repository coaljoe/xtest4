from __future__ import division, absolute_import

class UnitManager(object):
    def __init__(self, game):
        self.units = []
        self.destroyedUnits = []
        self.game = game

    def onUnitSpawn(self, sender, **kw):
        obj=sender
        self.units.append(obj)
        self.game.field.setCellEmpty(obj.x, obj.y, True)

    def onUnitDestroy(self, sender, **kw):
        u=sender
        print u
        print self.units
        self.units.remove(u)
        self.destroyedUnits.append(u)

    def onFieldCellActivate(self, sender, px, py, oldx, oldy):
        #print 'LOL3'
        #print x1, y1, x2, y2, sender
        pass

    def onUnitStep(self, sender):
        obj=sender
        #print 'onUnitStep', obj
        #print obj.x, obj.y, obj.path[-1].x, obj.path[-1].y
        self.game.field.setCellEmpty(obj.x, obj.y, False) # clear current node
        self.game.field.setCellEmpty(obj.path[-1].x, obj.path[-1].y, True) # fill next node

    def createUnit(self, typename):
        """Create unit object without spawn"""
        from src import units
        u = getattr(units, typename)()
        return u

    def getPlayerUnits(self, player):
        return [u for u in self.units if u.player == player]

    def update(self, dt):
        for x in self.units:
            x.update(dt)

