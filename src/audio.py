from __future__ import division, absolute_import
import os, sys, time, wave
from src.lib.openal import al, alc
from src.event import event
from src.util import Singleton
from src.vector3 import Vector3

class Sfx(object):
    def __init__(self, fname=None):
        self.loop = False
        self.pos = Vector3(0, 0, 0)
        self.buf = None
        self.source = None
        # backlink
        self.linked_obj = None

        if fname:
            self.load(fname)

    def load(self, fname):
        wavefp = wave.open(fname)
        channels = wavefp.getnchannels()
        bitrate = wavefp.getsampwidth() * 8
        samplerate = wavefp.getframerate()
        wavbuf = wavefp.readframes(wavefp.getnframes())
        formatmap = {
            (1, 8) : al.AL_FORMAT_MONO8,
            (2, 8) : al.AL_FORMAT_STEREO8,
            (1, 16): al.AL_FORMAT_MONO16,
            (2, 16) : al.AL_FORMAT_STEREO16,
        }
        alformat = formatmap[(channels, bitrate)]

        self.source = source = al.ALuint(0)
        al.alGenSources(1, source)

        al.alSourcef(source, al.AL_PITCH, 1)
        al.alSourcef(source, al.AL_GAIN, 1)
        al.alSource3f(source, al.AL_POSITION, *self.pos)
        al.alSource3f(source, al.AL_VELOCITY, 0, 0, 0)
        al.alSourcei(source, al.AL_LOOPING, self.loop)

        self.buf = buf = al.ALuint(0)
        al.alGenBuffers(1, buf)

        al.alBufferData(buf, alformat, wavbuf, len(wavbuf), samplerate)
        al.alSourceQueueBuffers(source, 1, buf)

    def play(self):
        if self.linked_obj:
            self.pos = self.linked_obj.pos
        al.alSource3f(self.source, al.AL_POSITION, *self.pos)
        al.alSourcePlay(self.source)

    def free(self):
        al.alDeleteSources(1, self.source)
        al.alDeleteBuffers(1, self.buf)


class Audio(object):
    __metaclass__ = Singleton
    def __init__(self, game):
        self.game = game
        self.lock = False
        self.sources = {}
        self.bufs = {}
        self.sfxs = []

        self.init_audio()

        #event.sub('ev_weapon_use', self.onWeaponUse)
        #event.sub('ev_weapon_reload', self.onWeaponReload)
        #event.sub('ev_snd_play', self.onSndPlay)

    def init_audio(self):
        self.device = alc.alcOpenDevice(None)
        self.context = alc.alcCreateContext(self.device, None)
        alc.alcMakeContextCurrent(self.context)

    def add_sound(self, fname):
        if not os.path.exists(fname):
            print "warning: no sound file", fname
            #exit(-1)
            return False
        s = Sfx(fname)
        self.sfxs.append(s)
        return s

    def add_sound1(self, name, fname):
        wavefp = wave.open(fname)
        channels = wavefp.getnchannels()
        bitrate = wavefp.getsampwidth() * 8
        samplerate = wavefp.getframerate()
        wavbuf = wavefp.readframes(wavefp.getnframes())
        formatmap = {
            (1, 8) : al.AL_FORMAT_MONO8,
            (2, 8) : al.AL_FORMAT_STEREO8,
            (1, 16): al.AL_FORMAT_MONO16,
            (2, 16) : al.AL_FORMAT_STEREO16,
        }
        alformat = formatmap[(channels, bitrate)]

        source = al.ALuint(0)
        al.alGenSources(1, source)

        al.alSourcef(source, al.AL_PITCH, 1)
        al.alSourcef(source, al.AL_GAIN, 1)
        al.alSource3f(source, al.AL_POSITION, 10, 0, 0)
        al.alSource3f(source, al.AL_VELOCITY, 0, 0, 0)
        al.alSourcei(source, al.AL_LOOPING, 0)

        self.sources[name] = source

        buf = al.ALuint(0)
        al.alGenBuffers(1, buf)

        al.alBufferData(buf, alformat, wavbuf, len(wavbuf), samplerate)
        al.alSourceQueueBuffers(source, 1, buf)
        self.bufs[name] = buf

    def play_sound(self, name):
        source = self.sources[name]
        al.alSourcePlay(source)

    def delete_sound(self, name):
        source = self.sources[name]
        buf = self.buf[name]
        al.alDeleteSources(1, source)
        al.alDeleteBuffers(1, buf)

    def deinit_audio(self):
        alc.alcDestroyContext(self.context)
        alc.alcCloseDevice(self.device)

    def free(self):
        [s.free() for s in self.sfxs]
        self.deinit_audio()
        
    def onSndPlay(self, opts):
        pos = opts['obj'].pos
        self.sfx[opts['name']].play(pos)
        
    def update(self, dt):
        _to_ctypes = lambda seq, dtype: (len(seq) * dtype)(*seq)
        cam = self.game.scene.camera
        #upvec = (0.0, 1.0, 0.0)
        pos = (cam.pos.x, cam.pos.y, cam.pos.z)
        ori = (cam.target.x - cam.pos.x,
               cam.target.y - cam.pos.y,
               cam.target.z - cam.pos.z,
               0.0, 1.0, 0.0) # upvec

        al.alListenerfv(al.AL_POSITION, _to_ctypes(pos, al.ALfloat))
        al.alListenerfv(al.AL_ORIENTATION, _to_ctypes(ori, al.ALfloat))

