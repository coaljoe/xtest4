#!/usr/bin/env python
from __future__ import division, absolute_import
import sys
sys.path.insert(0, '..')
sys.dont_write_bytecode = True
from math import *
from OpenGL.GL import *
from src.lib import glfw
from src.lib import h3d
from src.view import app
from src.vector3 import Vector3

class Window(app.Window):
    def on_key_press(self, key):
        if key == glfw.KEY_ESC:
            self.has_exit = True
        #elif key == glfw.KEY_SPACE:
        #    self._app._freeze = not self._app._freeze
        elif key == glfw.KEY_F7:
            self._app._debugViewMode = not self._app._debugViewMode
        elif key == glfw.KEY_F8:
            self._app._wireframeMode = not self._app._wireframeMode
        elif key == glfw.KEY_F9:
            self._app._showStats = True
            self._app._statMode = 1 - self._app._statMode

    def on_mouse_pos(self, x, y, dx, dy):
        #print 'lel', dx, dy, x,y
        # Look left/right
        self._app._ry -= dx / 100.0 * 30.0
        # Loop up/down but only in a limited range
        self._app._rx = max(-90, min(90, self._app._rx + dy / 100.0 * 30.0))


    def on_mouse_button(self, button, pressed):
        pass


class CharacterView(object):
    def __init__(self, m):
        self.m = m

        class H3DRes: pass
        h3dres = H3DRes()
        self._h3dres = h3dres

        # by default its man
        h3dres.man = h3d.addResource(h3d.ResTypes.SceneGraph, "models/man/man.scene.xml", 0)
        h3dres.manAnim = h3d.addResource(h3d.ResTypes.Animation, "animations/man.anim", 0)
        h3d.utils.loadResourcesFromDisk('Content')

        #print h3d.getResParamStr(h3dres.man, h3d.ResTypes.Material, 0, h3d.MatRes.MatClassStr, 0)
        h3d.getResParamStr(h3dres.man, h3d.ResTypes.Material, 0, h3d.MatRes.MatClassStr, "dynamic")

        self.model = h3dres.man
        self.anim = h3dres.manAnim
        self.animTime = 0

    def spawn(self):
        h3dres = self._h3dres
        m=self.m
        #m.pos.y = 0

        self.node = h3d.addNodes(h3d.RootNode, self.model)
        h3d.setNodeTransform(self.node, m.pos.x, m.pos.y, m.pos.z, 0, 0, 0, 1, 1, 1)
        h3d.setupModelAnimStage(self.node, 0, self.anim, 0, "", False)

    def update(self, dt):
        m=self.m
        h3d.setNodeTransform(self.node, m.pos.x, m.pos.y, m.pos.z, 0, 180 - m.dir, 0, 1, 1, 1)

        # update anim
        if m.vel_x or m.vel_y:
            self.animTime += m.accel * dt
            h3d.setModelAnimParams(self.node, 0, self.animTime * 32.0, 1)
            h3d.updateModel(self.node, h3d.ModelUpdateFlags.Animation | h3d.ModelUpdateFlags.Geometry)

class Character(object):
    def __init__(self):
        self.pos = Vector3()
        self.dir = 0
        self.vel_x = 0
        self.vel_y = 0
        self.accel = 1.5

    def move(self, dir):
        if dir == 'up':
            self.vel_y = -self.accel
            self.dir = 0
        elif dir == 'down':
            self.vel_y = self.accel
            self.dir = 180
        elif dir == 'left':
            self.vel_x = -self.accel
            self.dir = -90
        elif dir == 'right':
            self.vel_x = self.accel
            self.dir = 90

    def stop(self):
        self.vel_x = 0
        self.vel_y = 0

    def update(self, dt):
        self.pos.x += float(self.vel_x * dt)
        self.pos.z += float(self.vel_y * dt)


class XApp(app.App):
    def __init__(self, windowCls):
        app.App.__init__(self, windowCls)

        self._x = 10.0
        self._y = 10.0
        self._z = 10.0
        self._rx = -35.264
        self._ry = 45
        self._rz = 0
        self._velocity = 50.0
        
        self._freeze = False
        self._showStats = False
        self._debugViewMode = False
        self._wireframeMode = False

        self._statMode = 0

        self.char = None

    def _h3dAddResources(self):
        h3dres = self._h3dres

        # Pipelines
        #h3dres.hdrPipe = h3d.addResource(h3d.ResTypes.Pipeline, "pipelines/hdr.pipeline.xml", 0)
        h3dres.forwardPipe = h3d.addResource(h3d.ResTypes.Pipeline, "pipelines/forward.pipeline.xml", 0)
        # Overlays
        h3dres.fontMat = h3d.addResource(h3d.ResTypes.Material, "overlays/font.material.xml", 0)
        h3dres.panelMat = h3d.addResource(h3d.ResTypes.Material, "overlays/panel.material.xml", 0)
        h3dres.logoMat = h3d.addResource(h3d.ResTypes.Material, "overlays/logo.material.xml", 0)
        # Environment
        #h3dres.env = h3d.addResource(h3d.ResTypes.SceneGraph, "models/sphere/sphere.scene.xml", 0)
        # Knight
        #h3dres.knight = h3d.addResource(h3d.ResTypes.SceneGraph, "models/knight/knight.scene.xml", 0)
        h3dres.man = h3d.addResource(h3d.ResTypes.SceneGraph, "models/man/man.scene.xml", 0)
        # Terrain
        #h3dres.terrain = h3d.addResource(h3d.ResTypes.SceneGraph, "terrains/terrain1/terrain1.scene.xml", 0)
        #h3dres.matRes = h3d.addResource(h3d.ResTypes.Material, "terrains/terrain1/terrain1.material.xml", 0);

        h3dres.scene = h3d.addResource(h3d.ResTypes.SceneGraph, "models/testscene/testscene.scene.xml", 0)

        h3d.utils.loadResourcesFromDisk('Content')

    def _h3dSetupScene(self):
        h3dres = self._h3dres

        self._env = h3d.addNodes(h3d.RootNode, h3dres.scene)
        h3d.setNodeTransform(self._env, 0, 0, 0, 0, 0, 0, 1, 1, 1)

        self._knight = h3d.addNodes(h3d.RootNode, h3dres.man)
        h3d.setNodeTransform(self._knight, 0, 0, 0, 0, 180, 0, 1, 1, 1)

        # Add terrain
        #self.terrain = h3d.addNodes(h3d.RootNode, h3dres.terrain)
        # Set sun direction for ambient pass
        #h3d.setMaterialUniform(h3dres.matRes, "sunDir", 1, -1, 0, 0 );
        #MeshQualityF = 10002
        #SkirtHeightF = 10003
        #h3d.setNodeParamF(self.terrain, 10002, 0, 100)
        #h3d.setNodeParamF(self.terrain, 10003, 0, 0.1)

        self._light = h3d.addLightNode(h3d.RootNode, 'Light1', 0, 'LIGHTING', 'SHADOWMAP')
        h3d.setNodeTransform(self._light, 10, 10, 10, -30, 60, 0, 1, 1, 1)
        h3d.setNodeParamF(self._light, h3d.Light.RadiusF, 0, 100)
        h3d.setNodeParamF(self._light, h3d.Light.FovF, 0, 90)
        h3d.setNodeParamI(self._light, h3d.Light.ShadowMapCountI, 3)
        h3d.setNodeParamF(self._light, h3d.Light.ShadowMapBiasF, 0, 0.001)
        h3d.setNodeParamF(self._light, h3d.Light.ColorF3, 0, .5)
        h3d.setNodeParamF(self._light, h3d.Light.ColorF3, 1, .5)
        h3d.setNodeParamF(self._light, h3d.Light.ColorF3, 2, .5)

    def _mainloopUpdate(self, dt):
        app.App._mainloopUpdate(self, dt)

        w = self.w
        #h3d.setNodeTransform(self.camera, self._x, self._y, self._z, 0, 0, 0, 1, 1, 1)
        h3d.setNodeTransform(self.camera, self._x, self._y, self._z, self._rx, self._ry, self._rz, 1, 1, 1)
        #h3d.setNodeTransform(self.rttcam, self._x, self._y, self._z, self._rx, self._ry, self._rz, 1, 1, 1)
        #print self._rx, self._ry, self._x, self._y, self._z

        if self._debugViewMode:
            h3d.setOption(h3d.Options.DebugViewMode, 1.0)
        else:
            h3d.setOption(h3d.Options.DebugViewMode, 0.0)

        if self._wireframeMode:
            h3d.setOption(h3d.Options.WireframeMode, 1.0)
        else:
            h3d.setOption(h3d.Options.WireframeMode, 0.0)

        curVel = self._velocity * dt
        if self.w.isPressed(glfw.KEY_LSHIFT):
            curVel *= 5
        if self.w.isPressed('w'):
            self._x -= sin(radians(self._ry)) * cos(-radians(self._rx)) * curVel
            self._y -= sin(-radians(self._rx)) * curVel
            self._z -= cos(radians(self._ry)) * cos(-radians(self._rx)) * curVel
        elif self.w.isPressed('s'):
            self._x += sin(radians(self._ry)) * cos(-radians(self._rx)) * curVel
            self._y += sin(-radians(self._rx)) * curVel
            self._z += cos(radians(self._ry)) * cos(-radians(self._rx)) * curVel
        if self.w.isPressed('a'):
            self._x += sin(radians(self._ry - 90)) * curVel
            self._z += cos(radians(self._ry - 90)) * curVel
        elif self.w.isPressed('d'):
            self._x += sin(radians(self._ry + 90)) * curVel
            self._z += cos(radians(self._ry + 90)) * curVel

        if self.w.isPressed(glfw.KEY_UP):
            self.char.move('up')
        elif self.w.isPressed(glfw.KEY_DOWN):
            self.char.move('down')
        elif self.w.isPressed(glfw.KEY_LEFT):
            self.char.move('left')
        elif self.w.isPressed(glfw.KEY_RIGHT):
            self.char.move('right')
        else:
            self.char.stop()

xapp = XApp(Window)
xapp.init()

rot = 0
light_pos = Vector3(0, 0, 1)
light_rad = 10

ch = Character()
chv = CharacterView(ch)

chv.spawn()

xapp.char = ch

while xapp.running:
    dt = xapp._dt
    global rot
    rot = (rot + 10 * dt) % 360    
    light_pos.x = cos(radians(rot)) * light_rad
    light_pos.y = sin(radians(rot)) * light_rad
    #print light_pos.x
    h3d.setNodeTransform(xapp._light, light_pos.x, 10, light_pos.y, -30, (60 - rot) % 360, 0, 1, 1, 1)

    ch.update(dt)
    chv.update(dt)

    xapp.step()
    #xapp.begin_custom_gl()
    #xapp.end_custom_gl()
    xapp.flip()
