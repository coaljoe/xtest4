from __future__ import division, absolute_import
from src.lib.aux3d import aux3d as aux
from src.event import event

class ItemMgr(object):
    def __init__(self):
        self.items = []

        event.sub('ev_item_spawn', self.onItemSpawn)

    def onItemSpawn(self, it):
        print 'ItemMgr onItemSpawn', it
        self.items.append(it)
