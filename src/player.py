from __future__ import division, absolute_import
from src.character import Character

class BasePlayer(Character):
    def __init__(self):
        super(BasePlayer, self).__init__()
        self.money = 0
    def update(self, dt):
        super(BasePlayer, self).update(dt)
        pass


class Player(BasePlayer):
    def __init__(self):
        super(Player, self).__init__()
        self.money = 1000

