from __future__ import division, absolute_import
from weakref import WeakSet
from src.lib.aux3d import aux3d as aux
from src.event import event
from src import util

class ItemContainer(object):
    """Abstract item container
    """
    typename='itemcontainer'
    def __init__(self, name=None, cont_name='Unnamed container'):
        self.items = WeakSet()
        self.name = name or util.get_autoname(self.typename)
        self._cont_name = cont_name
        self.maxweight = None
        self.maxbulk = None

    def addItem(self, it):
        if it in self.items:
            print 'item %s is already in %s; not adding' % (it, self._cont_name)
            raise ItemAlreadyInContainerError
            return False
        it.container = self
        self.items.add(it)
        event.pub('ev_container_add_item', it)
        return True

    def removeItem(self, it):
        if not it in self.items:
            print 'no such item %s in %s; do nothing' % (it, self._cont_name)
            raise NoSuchItemError
            return False
        it.container = None
        self.items.remove(it)
        event.pub('ev_container_remove_item', it)
        return True

    def swapItem(self, it, oth):
        self.removeItem(it)
        oth.addItem(it)
        return True
        
    def setItems(self, l):
        self.items = WeakSet(l)
        # update container links
        for it in self.items:
            it.container = self

    def listItems(self):
        print 'list items:'
        for it in self.items:
            print ' -', it.name, it.weight
            print 'props:'
            it.list_props()

    def putItem(self, it):
        raise NotImplementedError 

    def takeItem(self, it):
        raise NotImplementedError 

    @property
    def totalWeight(self):
        return round(sum([it.weight for it in self.items]), 2)
    @property
    def totalBulk(self):
        return round(sum([it.bulk for it in self.items]), 2)

class Inventory(ItemContainer):
    def __init__(self):
        ItemContainer.__init__(self, name='inventory', cont_name='Inventory')


class ItemAlreadyInContainerError(Exception):
    pass

class NoSuchItemError(Exception):
    pass
