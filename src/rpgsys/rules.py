from __future__ import division, absolute_import
from random import randint, uniform
from src.util import TaskSchedule

class BaseRuleset(object):
    pass

class Dice(int):
    def __str__(t):
        return 'd%s' % int(t)
    def roll(t):
        return randint(0, int(t))

d6 = Dice(6) # below average
d8 = Dice(8) # average
d10 = Dice(10) # good/primary
#print d10.roll(), d10

class Specialism(object):
    def __init__(t, name, type, description=None):
        t.name = name
        t.type = type
        t.modifier = +2
        t.description = description

    def __str__(t):
        return '%s +%s (%s)' % (t.name, t.modifier, t.type.capitalize())

    def __repr__(t):
        return str(t)

class Weapon(object):
    def __init__(t, **kw):
        t.special = []
        t.range = 'long'
        t.modifier = 0
        t.useAPCost = 0
        t.reloadAPCost = 0
        t._wearPoints = 0.0 # real
        t.wearPointsInc = 0.5

    def use(t):
        t._wearPoints += t.wearPointsInc
        print 'rpgsys: weapon is now %s points of wear' % t.wearPoints

    @property
    def wearPoints(t): return int(t._wearPoints)
    #@wearPoints.setter
    #def wearPoints(t, v): t._wearPoints += v

class Armor(object):
    def __init__(t, **kw):
        t.special = []
        t.modifier = 0

class Character(object):
    def __init__(t, name=None, actions=None, wits=None, ego=None, status='-', role_name=None, background=None, **kw):
        super(Character, t).__init__(**kw)
        #### attributes
        # combat and dexterity
        t.actions = actions
        # intelligence and perception
        t.wits = wits
        # social skills
        t.ego = ego
        #### secondary attributes
        t.hits = None
        t.warmth = 5.0
        #### specialisms
        t.specAction = []
        t.specWits = []
        t.specEgo = []
        t.name = name
        t.status = status
        t.role_name = role_name
        t.ap = 10 # fixme
        #t.update_warmth_task = TaskSchedule(t._update_warmth, 10, start_delayed=True)
        t.update_warmth_task = TaskSchedule(t._update_warmth, 60)
        #print 'initialized, exit'
        #exit()

    def spawn(t):
        t.update_warmth_task.start()

    def load(t):
        """Same as load info from dict/file/json, for post-initialization
        """
        #super(Character, t).load()
        t.maxhits = (t.actions + t.wits) / 2 # avg, static(?)
        t.hits = t.maxhits

    def performAttack(t, opp):
        dmg = 0
        #assert type(opp) is not type
        #print opp
        #print dir(opp)
        #print type(opp)
        #print opp.actions
        #print t.actions
        #exit()
        """
        dv = opp.actions.roll()
        res = (t.actions.roll() + t.weapon.modifier) - dv
        """
        t = Task(dft=opp.actions)
        res = t.resolve(t.actions, modifier=t.weapon.modifier)
        if res > 0:
            dmg = res
            armor_modifier = opp.equip.armor_modifier
            msg = 'successful attack, damage = %s' % dmg
            # apply armor modifier (fixme)
            if armor_modifier < 0:
                dmg += armor_modifier
                msg += " (reduced by %s by armor)" % armor_modifier
            if dmg > 0:
                opp.takeDamage(dmg)
            else:
                msg += " NO DAMAGE!"
            print msg
        else:
            print 'failed attack, no damage (res %s)' % res

    def takeDamage(t, amt):
        print "HP =", t.hp
        t.hp -= amt
        if t.checkDead:
            print t, "is dead, hp =", t.hp
            t.die()

    def die(t):
        print "::rules.Character die"
        t.hits = 0

    def list(t):
        print '\n', t.name
        print t.status
        print '\nActions:', t.actions
        print 'Wits:', t.wits
        print 'Ego:', t.ego
        print '\nSpecialisms\n'
        print 'Action:', t.specAction
        print 'Wits:', t.specWits
        print 'Ego:', t.specEgo
        print 
        """
            t._insulation = 0.0 # base insulation value
            t.effective_insulation = 0.0 # total insulation value
        def get_insulation_area(t):
            if not t.slot:
                print "err: item's slot was not specified"
                exit()
            # units: 'heads'
            slot_area_map = {
                'head': 1.0,
                'face': 0.5,
                'neck': 0.5,
                'body': 8.0,
                'legs': 6.0,
                'foots': 2.0,
                'hands': 1.0,
            }
            return slot_area_map[t.slot]
        @property
        def insulation(t): return t._insulation
        @insulation.setter
        def insulation(t, v):
            t._insulation = v
            t.effective_insulation = v * t.get_insulation_area()
        """

    def _update_warmth(t):
        #XXX fixme curretly only for PC characters
        #TODO probably should use effective cold rating based also on surfaces area?
        if not t.pc:
            return
        print '_update_warmth'
        from src.game import Game
        temperature = Game().location.temperature
        # digitized temperature rating
        temp = abs(int(round(temperature/10.0))) # 16 -> 1.6 -> 2
        print temp, temperature
        cold_rating = 0
        for it, slot_name in set(t.equip.wear_slotsnames):
            slot_cold_rating = 0
            if it is None:
                slot_cold_rating = temp
            elif it.insulation < temp:
                slot_cold_rating = temp - it.insulation
            if slot_cold_rating:
                print 'msg: your %s is/are cold (slot_cold_rating: %s)' % (slot_name, slot_cold_rating)
            else:
                print 'msg: your %s is/are FINE' % slot_name
            cold_rating += slot_cold_rating
        print 'cold_rating:', cold_rating
        t.warmth -= cold_rating/10
        print 'wamrth:', t.warmth

    def update(t, dt):
        if t.checkDead:
            return
        t.update_warmth_task.update(dt)
        if t.warmth <= 0:
            t.die()

    @property
    def hp(t): return t.hits
    @hp.setter
    def hp(t, v): t.hits = v
    @property
    def checkDead(t): return True if t.hp <= 0 else False

class Task(object):
    """Task resolution
    """
    def __init__(t, dft):
        t.dftTable = {
            'easy': 2,
            'medium': 4,
            'hard': 7,
            'veryHard': 10,
            'impossible': 14,
        }
        t.dft = dft if type(dft) is Dice else Dice(t.dftTable[dft])

    def resolve(t, dice, modifier=0):
        ret = 0
        while ret == 0:
            # reroll if tie
            ret = (dice.roll() + modifier) - t.dft.roll()
        return ret

class Ruleset(BaseRuleset):
    def __init__(t): 
        pass

if __name__ == '__main__':
    s_handgun = Specialism(name='Handgun', type='actions')
    s_leadership = Specialism(name='Leadership', type='ego')
    s_strategic = Specialism(name='Strategic', type='wits')
    s_deduction = Specialism(name='Deduction', type='wits')
    s_gunner = Specialism(name='Gunner', type='actions')

    c = camilla = Character('Camilla', actions=d6, wits=d10, ego=d8, status='Exile',
                   background="Jacob's daughter")
    #c.specAction.append(s_handgun)
    c.specWits.extend([s_strategic, s_deduction])
    c.specEgo.append(s_leadership)
    c.list()

    c = npc1 = Character('Gunner1', actions=d10, wits=d6, ego=d8)
    c.specAction.extend([s_gunner, s_handgun])
    c.specEgo.append(s_leadership)
    c.list()

    npc1.performAttack(camilla)
