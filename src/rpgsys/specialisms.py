from __future__ import division, absolute_import
from .rules import Specialism
from src.util import longtext

leadership = Specialism(name='Leadership', type='ego', description=longtext(\
                        """You are charming and able to manipulate others to your will,
                           whether that's to persuade them to sell you a weapon for a lower price,
                           or to get a mercenary to join your cause.
                        """))

handgun = Specialism(name='Handgun', type='actions')
strategic = Specialism(name='Strategic', type='wits')
deduction = Specialism(name='Deduction', type='wits')
gunner = Specialism(name='Gunner', type='actions')
