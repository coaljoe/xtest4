from __future__ import division, absolute_import
from math import sin, cos, radians
from src.lib.aux3d import aux3d as aux
from src.contrib import fsm
from src.vector3 import Vector3
from src.event import event
from src.obj import Obj
from src import geom
from src.inventory import Inventory
from src.util import Timer
from src.rpgsys import rules
from src.equip import CharacterEquip
from src.stats import CharacterStats

class States:
    idle = 0
    moving = 1
    attacking = 2
    reloading = 3
    dead = 4

class CharacterFSM(fsm.StateMachine):
    def __init__(self, obj):
        fsm.StateMachine.__init__(self)
        self.obj = obj
        self.dt = None
        self.reload_timer = Timer()
        self.attack_interval_timer = Timer()

    @fsm.state
    def idle_state():
        def start(self):
            self.obj.state = States.idle
            self.obj.vel = 0
            event.pub('ev_character_state_switch', self.obj)
        def check(self):
            pass

        return (start, check)

    @fsm.state
    def attacking_state():
        def start(self):
            self.obj.state = States.attacking
            self.attack_interval_timer.start()
            # wind it up, fixme
            self.attack_interval_timer.start_time -= self.obj.weapon.attackInterval
            event.pub('ev_character_state_switch', self.obj)
        def check(self):
            obj=self.obj
            self.attack_interval_timer.update(self.dt)
            if obj.target.isDead:
                # unset target
                obj.setTarget(None)
            if obj.target is None:
                self.attack_interval_timer.reset()
                return "idle"
            if obj.weapon.needReload:
                return "reloading"
            # check is weapon is ready
            if self.attack_interval_timer.dt > obj.weapon.attackInterval:
                if obj.use_item(obj.weapon):
                    obj.performAttack(obj.target)
                    if obj.target.isDead:
                        k = obj.target.stats_kills_group_name
                        obj.stats.kills.setdefault(k, 0)
                        obj.stats.kills[k] += 1
                    self.attack_interval_timer.reset()

        return (start, check)

    @fsm.state
    def reloading_state():
        def start(self):
            obj=self.obj
            self.obj.state = States.reloading
            self.reload_timer.start()
            obj.weapon.reload()
            obj.ap -= obj.weapon.reloadAPCost
            event.pub('ev_character_state_switch', self.obj)
        def check(self):
            obj=self.obj
            self.reload_timer.update(self.dt)
            if self.reload_timer.dt > obj.weapon.reloadTime:
                self.reload_timer.reset()
                return "attacking" # fixme

        return (start, check)

    @fsm.state
    def dead_state():
        def start(self):
            obj=self.obj
            obj.state = States.dead
            obj.vel = 0
            obj.hp = 0
            obj.path = obj.nextp = None
            event.pub('ev_character_state_switch', obj)
        def check(self):
            pass

        return (start, check)

class CharacterInfo(object):
    def __init__(self):
        self.name = None
        self.age = None
        self.gender = None
        self.description = None
        self.long_description = None
        self.background = None
        self.portrait = None

class Character(Obj, rules.Character):
    def __init__(self, **kw):
        super(Character, self).__init__(**kw)
        self.inventory = Inventory()
        self.info = CharacterInfo()
        self.stats = CharacterStats()
        self.equip = CharacterEquip(self)
        # active item
        self.item = None
        self.target = None
        self.attackRange = None
        
        self.dir = -90 # x - 90 degrees = facing towards z+ axis
        self.vel = 0
        self.accel = 1.5 # walk speed
        self.path = []
        self.nextp = None
        self._spawned = False
        self._move_lock = False
        self.res_loc = None
        
        self.pc = False
        self.npc = False

        self.state = States.idle
        self.fsm = CharacterFSM(self)

    def move(self, dir):
        self.vel = self.accel
        self._move_lock = True
        if dir == 'up':
            self.dir = 90
        elif dir == 'down':
            self.dir = -90
        elif dir == 'left':
            self.dir = -180
        elif dir == 'right':
            self.dir = 0

    def stop_move(self):
        if self._move_lock:
            self.vel = 0
            self._move_lock = False

    def pickup_item(self, it):
        print 'picking up item', it.name
        if self.inventory.addItem(it): print 'success'
        else: print 'fail'

    def drop_item(self, it):
        print 'dropping item', it.name
        if self.inventory.removeItem(it):
            print 'success'
            print 'todo: place item near character (update its position)'
        else: print 'fail'

    def use_item(self, it):
        ret = it.use()
        if ret:
            self.stats.items_use.setdefault(it.item_typename, 0)
            self.stats.items_use[it.item_typename] += 1
            return True
        else:
            return False
        
    def canAttack(self, obj):
        if self.weapon is None:
            return False
        if not self.weapon.isArmed:
            print 'weapon is not armed'
            return False
        if not geom.in_radius(obj.pos.x, obj.pos.z, \
            self.weapon.attackRange, self.pos.x, self.pos.z):
            print 'target is too far' 
            return False
        if isinstance(obj, Character):
            if obj.isAlive: return True
        return False

    def canTalkWith(self, obj):
        return True
     
    def setTarget(self, obj):
        # warn: private method?
        assert self.canAttack(obj)
        self.target = obj
        self.attackTarget()
    
    def takeDamage(self, amt):
        super(Character, self).takeDamage(amt)

    def die(self):
        super(Character, self).die()
        self.fsm.set_state('dead')
        event.pub('ev_character_dead', self)
        
    def attackTarget(self):
        self.fsm.set_state('attacking')
    
    def follow_path(self, path):
        print "follow path:", path
        self.path = path

    def _handle_path(self, dt):
        if not self.nextp:
            self.nextp = self.path.pop()
            print "moving to:", self.nextp
            self.lookAt2d(self.nextp.x, self.nextp.z)
            self.vel = self.accel
        #if round(self.pos.x) == round(self.nextp.x) and \
        #   round(self.pos.z) == round(self.nextp.z):
        #print 'proc', self.path
        if geom.in_radius(self.pos.x, self.pos.z, .5, \
                          self.nextp.x, self.nextp.z):
            print "arrived to:", self.nextp
            self.nextp = None
            self.vel = 0
        else:
            pass
        
    def spawn(self, pos=None):
        super(Character, self).spawn()
        self.respawn(pos)
        self._spawned = True
        event.pub('ev_character_spawn', self)

    def respawn(self, pos=None):
        super(Character, self).respawn()
        if pos:
            self.pos = pos
        event.pub('ev_character_respawn', self)

    def update(self, dt):
        # not using super because of Obj.update interference?
        #super(Character, self).update(dt)
        rules.Character.update(self, dt)

        self.fsm.dt = dt
        self.fsm.update()

        if self.path or self.nextp:
            self._handle_path(dt)

        if self.vel:
            #print self.dir
            # move forward
            dirx = cos(radians(self.dir))
            dirz = sin(radians(self.dir))
            self.pos.x += dirx * self.vel * dt
            self.pos.z -= dirz * self.vel * dt

        # update items positions in inventory, needed for sound (fixme)
        for it in self.inventory.items:
            it.pos =  self.pos

    @property
    def item1(self): return self.equip.item1
    @property
    def item2(self): return self.equip.item2
    @property
    def weapon(self): return self.item
    @property
    def armor(self): return self.equip.armor
    @property
    def isDead(self): return self.state == States.dead
    @property
    def isAlive(self): return not self.isDead
    @property
    def dir(self): return self.rot.y
    @dir.setter
    def dir(self, val): self.rot.y = val
