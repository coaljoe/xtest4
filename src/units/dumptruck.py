from __future__ import division, absolute_import
import time
from src import game
from src.unit import Unit, States

class DumpTruck(Unit):
    typename = 'dump_truck'
    def __init__(self, x, y):
        Unit.__init__(self, x, y)
        self.dumpBodyAngle = 0
        self.dumpBodyAngleInc = .1
        self.maxSpeed = 50
        self.maxDamage = 200

    def _handle_unloading(self, dt=0):
        print 'unloading!'
        inc = self.dumpBodyAngleInc
        self.state = States.unloading

        # raise dump body
        while not self.dumpBodyAngle + inc > 45:
            self.dumpBodyAngle = min(45, self.dumpBodyAngle + inc)
            #print 'raise angle =', self.dumpBodyAngle
            yield

        #self.handle_func = None
        #print 'w00t', self.handle_func

        # wait for unloading
        time.sleep(1)

        # lower dump body
        while not self.dumpBodyAngle - inc < 0:
            self.dumpBodyAngle = max(0, self.dumpBodyAngle - inc)
            #print 'lower angle =', self.dumpBodyAngle
            yield

        #queue = [raiseDumpBody, waitForUnloading, lowerDumpBody]

        # switching state back to idle
        print 'finally', self.state
        self.state = States.idle

    def unloadCargoState(self):
        #if self.state != States.idle
        #    return False

        self.state = States.unloading
        #self.handle_func = self._handle_unloading()

        game.Game().shed_gen = self._handle_unloading()

