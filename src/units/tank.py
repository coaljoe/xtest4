from __future__ import division, absolute_import
from src import geom
from src.unit import Unit
from src.game import Game, timer
event = Game().event

class Tank(Unit):
    typename = 'tank'
    reloadTime = 2
    maxDamage = 200
    maxSpeed = 20
    fireRange = 50
    bulletSpeed = 0.02

    def __init__(self, *args, **kw):
        Unit.__init__(self, *args, **kw)
        self.turretAngle = 0 # deg, local
        self.turretAngleInc = .1
        self.lastFireTime = 0
        self.waitForHitTime = 0

    def _handle_aiming(self, dt=0):

        # find angle between target and unit
        a = geom.deg_between(self.pos.x, self.pos.z,
                             self.target.pos.x, self.target.pos.z)
        a -= self.dir
        self.turretAngle = a

    def fire(self):
        print "FIRE"
        print self.target.damageLevel
        self.lastFireTime = timer.time
        self.waitForHitTime = timer.time + geom.dist(self.xy, self.target.xy) * self.bulletSpeed
        event.pub('ev_unit_fire', self)

    def waitForHit(self):
        if(timer.time > self.waitForHitTime):
            self.target.takeDamage(50)
            self.waitForHitTime = 0

    def canFire(self):
        if (timer.time > self.lastFireTime + self.reloadTime) and \
           (timer.time > self.waitForHitTime):
            return True
        else:
            return False

