from __future__ import division, absolute_import
from src.units.tank import Tank

class LightTank(Tank):
    typename = 'lighttank'
    maxSpeed = 50

    def __init__(self, *args, **kw):
        Tank.__init__(self, *args, **kw)
