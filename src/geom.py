from __future__ import division, absolute_import
from math import atan2, sin, cos, degrees, radians, sqrt
import numpy as np

def deg_between(x1, y1, x2, y2):
    return degrees(atan2(y2 - y1, x2 - x1))

def in_rect(x, y, rx, ry, rw, rh):
    return True if x >= rx and x <= rx + rw and y >= ry and y <= ry + rh else False

def in_radius(x, y, r, cx=0, cy=0):
    """
    cx, cy - center of circle
    x, y - point
    r - radius
    """
    x -= cx
    y -= cy
    return True if x**2 + y**2 <= r**2 else False


def point_on_radius(x, y, cx, cy, r):
    """
    x, y - (A) unit
    cx, cy - (B) target
    r - radius
    """

    # angle from B to A
    a = deg_between(cx, cy, x, y)
    ar = radians(a)

    print 'a:', a
    
    # point on circle
    px, py = cx + r * cos(ar), \
             cy + r * sin(ar)

    print px, py

    return int(px), int(py)

def lerp(a, b, t):
    return a+(b-a)*t

def lerp_list(a, b, t):
    for k,v in zip(a,b):
        yield lerp(k, v, t)

def lerp_dict(a, b, t):
    d = {}
    for k in a.keys():
        if hasattr(a[k], '__iter__'):
            v = list(lerp_list(a[k], b[k], t))
        else:
            v = lerp(a[k], b[k], t)
        d[k] = v
    return d

def vec2add(a, b):
    return tuple(map(lambda x,y: x+y, a, b))

def dist(t1, t2):
    return sqrt((t2[0] - t1[0])**2 + (t2[1] - t1[1])**2)

def intersect(vec1, vec2, vec3, ray, orig, clip=1):
    v1 = vec1.copy()
    v2 = vec2.copy()
    v3 = vec3.copy()
    dir = ray.normalize()
    orig = orig.copy()

    # find vectors for two edges sharing v1
    e1 = v2 - v1
    e2 = v3 - v1

    # begin calculating determinant - also used to calculated U parameter
    pvec = dir.cross(e2)

    # if determinant is near zero, ray lies in plane of triangle
    det = e1.dot(pvec)

    if (-1E-6 < det < 1E-6):
        return None

    inv_det = 1.0 / det

    # calculate distance from v1 to ray origin
    tvec = orig - v1

    # calculate U parameter and test bounds
    u = tvec.dot(pvec) * inv_det
    if (clip and (u < 0.0 or u > 1.0)):
        return None

    # prepare to test the V parameter
    qvec = tvec.cross(e1)

    # calculate V parameter and test bounds
    v = dir.dot(qvec) * inv_det

    if (clip and (v < 0.0 or u + v > 1.0)):
        return None

    # calculate t, ray intersects triangle
    t = e2.dot(qvec) * inv_det

    dir = dir * t
    pvec = orig + dir

    return pvec

def ray_intersect_aabb(ray, aabb):
    """Calculates the intersection point of a ray and an AABB

    :param numpy.array ray1: The ray to check.
    :param numpy.array aabb: The Axis-Aligned Bounding Box to check against.
    :rtype: numpy.array
    :return: Returns a vector if an intersection occurs.
    Returns None if no intersection occurs.
    """
    """
    http://gamedev.stackexchange.com/questions/18436/most-efficient-aabb-vs-ray-collision-algorithms
    """
    # this is basically "numpy.divide( 1.0, ray[ 1 ] )"
    # except we're trying to avoid a divide by zero warning
    # so where the ray direction value is 0.0, just use infinity
    # which is what we want anyway
    direction = ray[1]
    dir_fraction = np.empty(3, dtype = ray.dtype)
    dir_fraction[direction == 0.0] = np.inf
    dir_fraction[direction != 0.0] = np.divide(1.0, direction[direction != 0.0])

    t1 = (aabb[0,0] - ray[0,0]) * dir_fraction[ 0 ]
    t2 = (aabb[1,0] - ray[0,0]) * dir_fraction[ 0 ]
    t3 = (aabb[0,1] - ray[0,1]) * dir_fraction[ 1 ]
    t4 = (aabb[1,1] - ray[0,1]) * dir_fraction[ 1 ]
    t5 = (aabb[0,2] - ray[0,2]) * dir_fraction[ 2 ]
    t6 = (aabb[1,2] - ray[0,2]) * dir_fraction[ 2 ]


    tmin = max(min(t1, t2), min(t3, t4), min(t5, t6))
    tmax = min(max(t1, t2), max(t3, t4), max(t5, t6))

    # if tmax < 0, ray (line) is intersecting AABB
    # but the whole AABB is behind the ray start
    if tmax < 0:
        return None

    # if tmin > tmax, ray doesn't intersect AABB
    if tmin > tmax:
        return None

    # t is the distance from the ray point
    # to intersection

    t = abs(tmin)
    point = ray[0] + (ray[1] * t)
    return point
