from __future__ import division, absolute_import
from src.util import DotDict
from src.game import timer, Game
"""Various production managers"""

typeInfo = {'tank': DotDict({
                'typename': 'tank',
                'cost': 150,
                'buildTime': 15}),
            'lighttank': DotDict({
                'typename': 'lighttank',
                'cost': 150,
                'buildTime': 15}),
            'dumptruck': DotDict({
                'typename': 'dumptruck',
                'cost': 50,
                'buildTime': 5}),
            'wheeledtank': DotDict({
                'typename': 'wheeledtank',
                'cost': 100500,
                'buildTime': 5}),
            'factory': DotDict({
                'prodTypenames': ['lighttank', 'tank', 'dumptruck', 'wheeledtank'],
                }),
            }

class ProdQueue(list):
    """Per-factory production queue"""
    def __init__(self, name, factory):
        list.__init__(self)
        self.name = name
        self.factory = factory
        self.factory_ti = typeInfo[self.factory.typename]

    def ableProduce(self, typename):
        if typename not in typeInfo:
            print "unknown typename:", typename
            return False
        if not typename in self.factory_ti.prodTypenames:
            return False
        else:
            return True

    def getProdTypenames(self):
        """Get List of typenames of possible production items"""
        return self.factory_ti.prodTypenames

    def getAvailableProduction(self):
        # add various resource checks
        return self.factory_ti.prodTypenames
    
    def canProduceNow(self, typename):
        # add resource checks
        ti = typeInfo[typename]
        if Game().player.money < ti.cost:
            return False
        else:
            return True

    def empty(self):
        return True if len(self) == 0 else False

class ProdQueueItem(object):
    def __init__(self, typename):
        self.typename = typename
        self.ti = typeInfo[typename]
        self.timeAdded = timer.time
        #self.timeComplete = timer.time + self.ti.buildTime

    def __cmp__(self, oth):
        return cmp(self.timeAdded, oth.timeAdded)

class ProductionManager(object):
    def __init__(self, game):
        self.game = game
        self.prodqs = {} # all prodques

    def addProdq(self, name, factory):
        """Register a production queue binded to a factory"""
        print "addProdq", name, factory
        self.prodqs[name] = ProdQueue(name, factory)
        print self.prodqs

    def buyUnit(self, typename, queue):
        if not typename in typeInfo:
            self.errmsg = 'invalid typename: %s' % typename
            return False
        
        ti = typeInfo[typename]

        if self.game.player.money < ti.cost:
            self.errmsg = 'no money'
            return False

        rec = ProdQueueItem(typename)
        self.prodqs[queue].append(rec)

        return True

    def update(self, dt):
        for q in self.prodqs.values():
            if q.empty(): continue

            item = q[0]
            produced = False

            if q.ableProduce(item.typename):
                # charge player
                self.game.player.money -= item.ti.cost
                obj = self.game.unitmgr.createUnit('Tank')
                q.factory.produce(obj)
                q.pop()
                produced = True

            if not produced:
                self.errmsg = 'cannot produce %s, skipping' % item.typename
                return

    def getAvailableProduction(self):
        """All currenlty available production of factories"""
        ret = set()
        for q in self.prodqs.values():
            ret.update(q.getProdTypenames())
        return list(ret)

    @property
    def errmsg(self):
        return self._errmsg

    @errmsg.setter
    def errmsg(self, val):
        print '-> errmsg: %s' % val
        self._errmsg = val

    @property
    def prodqs_list(self):
        return self.prodqs.values()

    ### events
    def _onBuildingSpawn(self, building):
        """auto add prodq when building is swawned"""
        if building.typename != 'factory':
            return
        self.addProdq(building.name, building)

    def _onBuildingDestroy(self):
        pass # todo: remove building's prodq

