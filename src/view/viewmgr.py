from __future__ import division, absolute_import 
from src.item import Item
from src.character import Character
from src.views.itemview import ItemView
from src.views.characterview import CharacterView
from src.event import event

class ViewMgr(object):
    def __init__(self, game):
        self.views = []
        self.game = game

        event.sub('ev_obj_spawn', self.onObjSpawn)

    def onObjSpawn(self, ob):
        if isinstance(ob, Item):
            v = ItemView(ob)
            v.spawn()
        elif isinstance(ob, Character):
            v = CharacterView(ob)
            v.spawn()
        self.views.append(v)

    def update(self, dt):
        for x in self.views:
            x.update(dt)
