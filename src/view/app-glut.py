#!/usr/bin/python
# *************************************************************************************************
#
# Horde3D
#   Next-Generation Graphics Engine
#
# Sample Application
# --------------------------------------
# Copyright (C) 2006-2008 Nicolas Schulz
#              2008 Florian Noeding
#
#
# This sample source file is not covered by the LGPL as the rest of the SDK
# and may be used without any restrictions
#
# *************************************************************************************************
from __future__ import division, absolute_import
import os, sys

"""
import pyglet
import pyglet.gl
from pyglet.gl import *
import pyglet.window
import pyglet.clock
"""
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
 
sys.path.append('../../Bindings/Python')
from src.lib import h3d

from optparse import OptionParser


class Window(object):
    def __init__(self, app):
        self._app = app

        self._width = 800
        self._height = 600
        self._mouse_prev_x = 0
        self._mouse_prev_y = 0
        self.has_exit = False

        glutInit(sys.argv)
        glutInitDisplayMode(GLUT_DEPTH|GLUT_DOUBLE|GLUT_RGBA)
        glutInitWindowSize(self._width, self._height)
        glutInitWindowPosition(100, 100)
        glutCreateWindow("glut app")
        #glutSetCursor(GLUT_CURSOR_NONE)
 
        #glutDisplayFunc(self.displayFunc)
        glutReshapeFunc(self.reshapeFunc)
        glutKeyboardFunc(self.on_key_press)
        #glutMouseFunc(self.mouseFunc)
        glutMotionFunc(self.mouseMotionFunc)
        glutPassiveMotionFunc(self.mouseMotionFunc)
        """
        pyglet.window.Window.__init__(self,
                resizable=True,
                config=config,
                width=width,
                height=height,
                caption=caption,
                vsync=options.vsync,
                )
        """

        # event dispatch
        #self.keyboard = pyglet.window.key.KeyStateHandler()
        #self.push_handlers(self.keyboard)

    def reshapeFunc(self, width, height):
        self._width = width
        self._height = height

        """
        glViewport(0, 0, width, height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(60.0, width / height, 1.0, 50.0)
        glMatrixMode(GL_MODELVIEW)
        """

    @property
    def camera(self):
        return self._app.camera

    def mouseMotionFunc(self, x, y):
        dx = x - self._mouse_prev_x
        dy = y - self._mouse_prev_y
        self._mouse_prev_x = x
        self._mouse_prev_y = y
        self.on_mouse_motion(self, x, y, dx, dy)

    def on_mouse_motion(self, x, y, dx, dy):
        pass

    def on_key_press(self, key, x, y):
        print 'lel'
        if key == GLUT_KEY_ESCAPE:
            self.has_exit = True


class App(object):
    def __init__(self, windowCls):
        self._windowCls = windowCls

    def init(self):
        self._parseOptions()

        self.w = self._windowCls(self)
        self._initHorde3D()
        self._dt = 0.0
        self._lastTime = glutGet(GLUT_ELAPSED_TIME)

    def _parseOptions(self):
        op = OptionParser()
        op.add_option('-x', '--width', type='int', dest='width', help='window width', default=800)
        op.add_option('-y', '--height', type='int', dest='height', help='window height', default=600)
        op.add_option('--vsync-on', dest='vsync', help='enable / disable vsync', action='store_true', default=False)
        op.add_option('--content', dest='contentPaths', help='path to content directory, may be specified several times', action='append', default=['Content'])
        op.add_option('--limit-fps', type='int', dest='fpsLimit', help='limit FPS to specified value; may improve keyboard / mouse reaction times for values below 100', default=None)
        op.add_option('--multiwindow', dest='multiWindow', help='enable support for multiple windows; disabled by default due to significant performance hit', default=False)

        options, args = op.parse_args()
        self._options = options
        self._args = args

        if not options.contentPaths:
            op.error('no content path set')
        else:
            options.contentPaths = '|'.join(options.contentPaths)

    def _initHorde3D(self):
        # init Horde3D
        glPushAttrib(GL_ALL_ATTRIB_BITS)

        h3d.init()

        glClearDepth(1.)
        glClearColor(0., 0., 0.5, 0.)
        glEnable(GL_DEPTH_TEST)
        
        # engine options
        h3d.setOption(h3d.Options.LoadTextures, 1)
        h3d.setOption(h3d.Options.TexCompression, 0)
        h3d.setOption(h3d.Options.FastAnimation, 0)
        h3d.setOption(h3d.Options.MaxAnisotropy, 0)
        h3d.setOption(h3d.Options.ShadowMapSize, 1024)

        # add resources
        class H3DRes:
            pass
        h3dres = H3DRes()
        self._h3dres = h3dres

        h3dres.forwardPipe = h3d.addResource(h3d.ResTypes.Pipeline, "pipelines/forward.pipeline.xml", 0)
        h3dres.fontMat = h3d.addResource(h3d.ResTypes.Material, "overlays/font.material.xml", 0)
        h3dres.logoMat = h3d.addResource(h3d.ResTypes.Material, "overlays/logo.material.xml", 0)

        # load resources from disk
        if not h3d.utils.loadResourcesFromDisk(self._options.contentPaths):
            print 'loading of some resources failed: See Horde3D_Log.html'

        # camera
        self.camera = h3d.addCameraNode(h3d.RootNode, "cam", h3dres.forwardPipe)
        h3d.setNodeTransform(self.camera, 0, 0, -100, 0, 0, 0, 1, 1, 1)

        width = self.w._width
        height = self.w._height
        h3d.setNodeParamI(self.camera, h3d.Camera.ViewportXI, 0)
        h3d.setNodeParamI(self.camera, h3d.Camera.ViewportYI, 0)
        h3d.setNodeParamI(self.camera, h3d.Camera.ViewportWidthI, width)
        h3d.setNodeParamI(self.camera, h3d.Camera.ViewportHeightI, height)
        #h3d.setupViewport(0, 0, width, height, True)

        h3d.setupCameraView(self.camera, 45, width / float(height), 0.1, 1000)

    def step(self):
        if self.w.has_exit:
            return False

        thisTime = glutGet(GLUT_ELAPSED_TIME)
        dt = (thisTime - self._lastTime) / 1000.0
        self._lastTime = thisTime
        self._dt = dt
        print dt

        #glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        #glLoadIdentity()
        self._mainloopUpdate(dt)

        self._mainloopRenderOverlays(dt)
        self._mainloopRender(dt)

        h3d.finalizeFrame()
        #glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        """
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        
        glBegin(GL_QUADS)
        glVertex3f(0, 0, 0)
        glVertex3f(0.1, 0.2, 0.3)
        glVertex3f(0.1, 0.2, 0.3)
        glEnd()
        """
        #glutPostRedisplay()
        #glutSwapBuffers()
        #h3d.utils.dumpMessages()


    def flip(self):
        #glutPostRedisplay()
        glutSwapBuffers()
        h3d.utils.dumpMessages()

    @property
    def running(self):
        return True if not self.w.has_exit else False

    def mainloop(self):
        self.step()

    def _mainloopUpdate(self, dt):
        pass

    def _mainloopRenderOverlays(self, dt):
        h3d.clearOverlays()

        x = self.w._width / float(self.w._height)
        verts = [
            x - 0.4, 0.8, 0.0, 1.0,
            x - 0.4, 1.0, 0.0, 0.0,
            x, 1.0, 1.0, 0.0,
            x, 0.8, 1.0, 1.0,
        ]
        h3d.showOverlays(verts, 1, 1, 1, 1, self._h3dres.logoMat, 0)


    def _mainloopRender(self, dt):
        h3d.render(self.camera)


def main():
    app = App(Window)
    app.init()
    app.mainloop()


if __name__ == '__main__':
    main()



