from __future__ import division, absolute_import
from OpenGL.GL import *
from src.lib import glfw

class States:
    idle = 0
    selection = 1

class Hud(object):
    def __init__(self, app):
        self.app = app
        self.state = 'idle'

        self.mx = 0
        self.my = 0

    def on_mouse_pos(self, x, y, dx, dy):
        self.mx = x
        self.my = y

    def on_mouse_button(self, button, pressed):
        if button == glfw.MOUSE_BUTTON_LEFT:
            if not pressed:
                PyCEGUI.System.getSingleton().injectMouseButtonUp(PyCEGUI.LeftButton)
            else:
                PyCEGUI.System.getSingleton().injectMouseButtonDown(PyCEGUI.LeftButton)
        elif button == glfw.MOUSE_BUTTON_RIGHT:
            if not pressed:
                PyCEGUI.System.getSingleton().injectMouseButtonUp(PyCEGUI.RightButton)
            else:
                PyCEGUI.System.getSingleton().injectMouseButtonDown(PyCEGUI.RightButton)
 

