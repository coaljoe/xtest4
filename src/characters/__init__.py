from __future__ import division, absolute_import
from .defaultcharacter import DefaultCharacter

def character_factory(typename):
    if typename == 'default':
        # create default character
        d = DefaultCharacter()
        print DefaultCharacter.mro()
        #print d.maxhits
        #print d.hits
        #exit()
        return d
    else:
        print 'unknown typename:', typename
        raise TypeError
