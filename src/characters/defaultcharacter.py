from __future__ import division, absolute_import
from src.item import item_factory
from src.rpgsys import rules
from src.rpgsys import specialisms
from src.character import Character


class DefaultCharacter(Character):
    """Default character
    """
    def __init__(self, **kw):
        super(DefaultCharacter, self).__init__(**kw)
        q=self
        q.res_loc = 'res/npcs/kid' # fix: res/chars/default
        q.pc = True # player's character
        # info
        q.info.name = 'Camilla'
        q.info.gender = 'F'
        q.info.age = 34
        q.info.description = 'Default character'
        # rules.Character
        q.actions = rules.d6
        q.wits = rules.d8
        q.ego = rules.d10
        q.specWit = [specialisms.strategic, specialisms.deduction]
        q.specEgo = [specialisms.leadership]
        q.load()

        ## wear default equipment
        it = item_factory('boots')
        self.inventory.addItem(it)
        self.equip.wear(it)

        # add another booos
        it2 = item_factory('boots')
        self.inventory.addItem(it2)
                
        # add backpack
        bp = item_factory('backpack')
        self.inventory.addItem(bp)
        
        # add gloves
        it = item_factory('gloves')
        self.inventory.addItem(it)

