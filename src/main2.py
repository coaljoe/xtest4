#!/usr/bin/env python
from __future__ import division, absolute_import
import sys
sys.path.insert(0, '..')
#sys.dont_write_bytecode = True
from math import *
from pprint import pprint
import logging
logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.ERROR)
from src.conf import conf
conf.load()
conf.parse_args()
import OpenGL
if not conf.args.debug:
    OpenGL.ERROR_CHECKING = False # XXX slow
    OpenGL.ERROR_ON_COPY = False
    OpenGL.FULL_LOGGING = False # XXX slow
from OpenGL.GL import *
from OpenGL.GLU import gluUnProject
import numpy as np
from src.geom import intersect
from src.event import event
from src.lib.aux3d.contrib.glfw import *
from src.lib.aux3d import aux3d as aux
from src.lib.aux3d import app
from src.vector3 import Vector3
_conf = aux.conf

class Window(app.Window):
    def on_key_press(self, key):
        if key in (GLFW_KEY_ESCAPE, GLFW_KEY_F12) or glfwGetKey('q') == GLFW_PRESS:
            self.has_exit = True
        elif key == GLFW_KEY_SPACE:
            #self._app._freeze = not self._app._freeze
            self._app._game.gui.toggle_window('notebook')
        elif key == GLFW_KEY_F7:
            aux.toggleDebugDraw()
        elif key == GLFW_KEY_F8:
            aux.renderer.showbuffers = not aux.renderer.showbuffers
            #self._app._wireframeMode = not self._app._wireframeMode
        elif key == GLFW_KEY_F9:
            aux.renderer.use_fxaa = not aux.renderer.use_fxaa
        elif key == GLFW_KEY_F10:
            aux.renderer.use_sharpen_pass = not aux.renderer.use_sharpen_pass
        elif key == GLFW_KEY_F11:
            from PIL import Image, ImageOps
            from datetime import datetime
            data = glReadPixels(0,0, self.width, self.height, GL_RGB, GL_UNSIGNED_BYTE)
            im = Image.frombuffer("RGB", (self.width, self.height), data, "raw", "RGB", 0, 0)
            im = ImageOps.flip(im)
            name = datetime.now().strftime("%Y%m%d-%H%M%S")
            im.save(name + ".png")

    #def on_mouse_pos(self, x, y, dx, dy):
    #    pass

    def on_mouse_button(self, button, pressed):
        pass


class XApp(app.App):
    def __init__(self, windowCls):
        # init base App
        app.App.__init__(self, windowCls)
        self.init()

        ## events
        self.w.event_handlers.append(self)

        ## tuning
        # dont need persp correction for glOrtho
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST)
            
        self._freeze = False
        self._showStats = False
        self._debugViewMode = False
        self._wireframeMode = False

        self._statMode = 0
        self._mx = 0
        self._my = 0
        self._game = None # fixme

        self._mouse_ray = None
        self._mouse_ray_dir = None
        self._sel_obj = None
        self._prev_sel_obj = None

    def test(self):
        #aux.loadScene("res/models/lev/lev.dae", static=False)
        #aux.loadScene("res/models/lev/landscape.dae", static=False)
        #aux.loadScene("res/models/bath/bathroom.dae", static=True)
        #aux.loadScene("res/models/bath/bathroom.blend")
        #aux.loadScene("res/models/bath/bath.blend")
        #aux.loadScene("res/models/box/box.dae")
        #aux.loadScene("res/models/testscene.dae")
        #aux.loadScene("res/models/box.dae")
        #self.sceneNode = aux.addSceneNode("res/models/bath/bathroom.dae")
        #self.sceneNode.setMaterial('materials/blue.json')
        pass

    def on_key_press(self, key):
        if key == ord(']'):
            self._game.playLocation(1)
        elif key == ord('['):
            self._game.playLocation(-1)

    def get_selected_obj(self):
        # mouse pos
        x = self._mx
        y = self._my
        # selection
        cam = aux.sm.getActiveCamera()
        vp = cam._vp
        winY = vp[3] - y
        aux.renderer.selbuf_rt.bind()
        #print x//2, winY//2
        c = glReadPixels(x//8, winY//8, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE)
        aux.renderer.selbuf_rt.unbind()
        #print type(c), ord(c[0])
        def c2id(r, g, b):
            return b + g * 256 + r * 256 * 256
        selbuf_id = c2id(ord(c[0]), ord(c[1]), ord(c[2]))
        print selbuf_id
        nd = None
        for mn in aux.scene.meshes:
            if mn.selbuf_id == selbuf_id:
                print mn.name
                nd = mn
                obj = mn.linked_obj
                if obj:
                    self._prev_sel_obj = self._sel_obj
                    self._sel_obj = obj
                    print '->', obj.name
                    return obj
        #else:
        #self._prev_sel_obj = None
        self._sel_obj = None

    def get_selected_obj_aabb(self):
        if self._mouse_ray is not None:
            _ray = np.array(self._mouse_ray)
            # append selbox nodes to meshnodes
            _nodes = aux.sm.meshes + self._game.location.level.selbox_nodes
            mn = aux.sm.findRayNodesIntersection(_ray, nodes=_nodes)
            if not mn:
                #self._prev_sel_obj = None
                self._sel_obj = None
                return
            print mn
            obj = mn.linked_obj # Obj (gameobject)
            if obj:
                self._prev_sel_obj = self._sel_obj
                self._sel_obj = obj
                print '->', obj.name
                return obj

    def on_mouse_pos(self, x, y, dx, dy):
        #print x, y
        self._mx = x
        self._my = y
        self._game.hud.mx = x
        self._game.hud.my = y

        if self._game.gui and self._game.gui.locked:
            return

        x, y = self._mx, self._my
        c = aux.sm.getActiveCamera()
        proj = c._proj_matrix
        mv = c._mv_matrix
        vp = c._vp
        winX, winY = x, vp[3] - y
        ###########
        p0 = Vector3(*gluUnProject(winX, winY, 0, mv, proj, vp))
        p1 = Vector3(*gluUnProject(winX, winY, 1, mv, proj, vp))
        ray_dir = (p1 - p0).normalize()
        ray = [p0, ray_dir] # origin, direction
        #print p0, p1
        self._mouse_ray = ray
        self._mouse_ray_dir = ray_dir
        #print 'ray =', ray

        ###########
        # update selection
        if self._game.frame % 2 == 0: # saving framerate
            #obj = self.get_selected_obj()
            obj = self.get_selected_obj_aabb()

        if self._sel_obj:
            #print self._sel_obj, self._prev_sel_obj
            if self._sel_obj != self._prev_sel_obj:
                print 'BANG', self._sel_obj
                event.pub('ev_obj_mouse_over', self._sel_obj)
                self._prev_sel_obj = self._sel_obj # hack to prevent furtur multiple firing
        else:
            if self._prev_sel_obj:
                event.pub('ev_obj_mouse_out', self._prev_sel_obj)
                self._prev_sel_obj = None
                self._sel_obj = None
            #print obj
            #for ob in game.objmgr.objs:
                #pass

    def on_mouse_button(self, button, down):
        if not down: return
        if self._game.gui and self._game.gui.locked:
            return

        # clicks on objects
        obj = self.get_selected_obj_aabb()
        if obj:
            event.pub('ev_obj_click', obj)
            return

        # get object coords
        x, y = self._mx, self._my
        c = aux.sm.getActiveCamera()
        proj = c._proj_matrix
        mv = c._mv_matrix
        vp = c._vp
        winX = x
        winY = vp[3] - y
        c.rt.bind()
        winZ = glReadPixels(x, winY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT)
        c.rt.unbind()
        obj_pos = Vector3(*gluUnProject(winX, winY, winZ, mv, proj, vp))
        print 'obj_pos =', obj_pos

        if obj_pos:
            ch = self._game.char

            lev = self._game.level
            if lev.pf:
                endPos = obj_pos
                endPos.y += 0.1
                path, flags = lev.pf.find_straight_path(ch.pos, endPos)
                print 'path:', path
                if len(path) > 1:
                    pos = path[-1]
                    #ch.lookAt2d(pos.x, pos.z)
                    _path = list(path)
                    _path.reverse()
                    _path.pop()
                    ch.follow_path(_path)
                    #ch.pos = path[-1]
                else:
                    print 'the path is too short'
            else:
                ch.pos = obj_pos


    def _mainloopUpdate(self, dt):
        app.App._mainloopUpdate(self, dt)
        scene = self._game.scene

        curVel = scene.camVelocity * dt
        if self.w.isPressed(GLFW_KEY_LEFT_SHIFT):
            curVel *= 5
        if self.w.isPressed('w') or self.w.isPressed(GLFW_KEY_KP_8):
            scene._px -= sin(radians(scene.camera.ry)) * curVel
            scene._pz -= cos(radians(scene.camera.ry)) * curVel
        elif self.w.isPressed('s') or self.w.isPressed(GLFW_KEY_KP_5) or self.w.isPressed(GLFW_KEY_KP_2):
            scene._px += sin(radians(scene.camera.ry)) * curVel
            scene._pz += cos(radians(scene.camera.ry)) * curVel
        if self.w.isPressed('a') or self.w.isPressed(GLFW_KEY_KP_4):
            scene._px -= sin(radians(scene.camera.ry + 90)) * curVel
            scene._pz -= cos(radians(scene.camera.ry + 90)) * curVel
        elif self.w.isPressed('d') or self.w.isPressed(GLFW_KEY_KP_6):
            scene._px += sin(radians(scene.camera.ry + 90)) * curVel
            scene._pz += cos(radians(scene.camera.ry + 90)) * curVel

        elif self.w.isPressed('1'):
            scene.camera.zoom += scene.camZoomSpeed * dt
        elif self.w.isPressed('2'):
            scene.camera.zoom -= scene.camZoomSpeed * dt

        if self.w.isPressed('z') or self.w.isPressed(GLFW_KEY_KP_DIVIDE):
            scene.camRot = (scene.camRot + scene.camRotSpeed * dt) % 360    
            #self._game.gui.dashboard.hide(silent=True)
        elif self.w.isPressed('x') or self.w.isPressed(GLFW_KEY_KP_MULTIPLY):
            scene.camRot = (scene.camRot - scene.camRotSpeed * dt) % 360    
            #self._game.gui.dashboard.hide(silent=True)
        else:
            pass
            #self._game.gui.dashboard.show(silent=True)

        if self.w.isPressed(GLFW_KEY_UP):
            self._game.char.move('up')
        elif self.w.isPressed(GLFW_KEY_DOWN):
            self._game.char.move('down')
        elif self.w.isPressed(GLFW_KEY_LEFT):
            self._game.char.move('left')
        elif self.w.isPressed(GLFW_KEY_RIGHT):
            self._game.char.move('right')
        else:
            self._game.char.stop_move()


if conf.args.fs0:
    w, h = Window.get_video_resolution()
    # monkeypatch resolution
    conf.vars.resolution = w, h
    Window.fullscreen = True
if 1:
    res = conf.vars.resolution
    Window.width = res[0]
    Window.height = res[1]
if conf.args.fs:
    Window.fullscreen = True

xapp = XApp(Window)
rot = 0
light_pos = Vector3(0, 0, 0)
light_rad = 3

print 'game import...'
from src.game import Game
game = Game()
xapp._game = game
## add window's event handlers (fixme)
xapp.w.event_handlers.append(game.scene)
if conf.vars.enable_gui:
    xapp.w.event_handlers.append(game.gui)

## update virtual res
virtual_res = 800
screen_res = xapp.w.width
k = screen_res / virtual_res
aux.sm.getActiveCamera().fov = 10 * k
aux.sm.getActiveCamera().zoom = game.scene.camDefaultZoom

## swawn level
game.playLocation(conf.args.level or 'lev')

scene = game.scene
#xapp.light.pos = Vector3(0.5, 2.5, 0.4)
#xapp.light.pos = Vector3(1.5, 1.5, .5)
scene.light.pos = Vector3(0, 15, 10)


def main():
    c = scene.shadowCam
    c0 = scene.camera
    prev_px = scene._px
    prev_pz = scene._pz
    dx, dy = 0, 0
    print prev_px, prev_pz, dx, dy
    #exit()
    while xapp.running:
        dt = xapp.dt
        global rot
        rot = (rot + 15 * dt) % 360    
        #light_pos.x = cos(radians(rot)) * light_rad
        #light_pos.z = sin(radians(rot)) * light_rad
        light_pos.x = aux.sm.getActiveCamera().x
        light_pos.z = aux.sm.getActiveCamera().z
        #print light_pos.x
        #xapp.light.transform(light_pos.x, 15, light_pos.z, -90, (60 - rot) % 360, 0, 1, 1, 1)

        # move sun with main camera
        #xapp.light.pos = Vector3(1, 2.5, 1)
        #xapp.light.pos.y = 0.3
        #xapp.light.pos.x = light_pos.x
        #xapp.light.pos.z = light_pos.z

        #xapp.camera.transform(light_pos.x, 1.2, light_pos.z, -90, (60 - rot) % 360, 0, 1, 1, 1)
        #xapp.char.pos.x = light_pos.x
        #xapp.char.pos.z = light_pos.z

        px = scene._px
        pz = scene._pz

        dx = px - prev_px
        dy = pz - prev_pz

        prev_px = px
        prev_pz = pz

        #print dx, dy, prev_px, prev_pz, xapp._px, xapp._pz
        #exit()
        c.pos.x += dx
        c.pos.z += dy

        # move
        #c.pos.x -= sin(radians(c.ry)) * dx
        #c.pos.z -= cos(radians(c.ry)) * dy

        #xapp.char.pos.x = xapp._px
        #xapp.char.pos.z = xapp._pz

        #print 'bong', game.char.rot.y
        #game.char.rot.y = (game.char.rot.y + (25 * dt)) % 360
        #print 'bong', game.char.dir
        #game.char.dir = (game.char.dir + (25 * dt)) % 360

        if xapp.step():
            #xapp.begin_custom_gl()
            #xapp.end_custom_gl()
            game.update(dt)
            xapp.flip()
    game.quit()

if __name__ == '__main__':
    main()

print "exiting..."
