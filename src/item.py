from __future__ import division, absolute_import
import os
from random import choice
from src.lib.aux3d import aux3d as aux
from src.obj import Obj
from src.vector3 import Vector3
from src.event import event
from src.util import Timer, longtext
from src.audio import Audio
from src.rpgsys import rules

def item_factory(typename):
    if typename == 'bottle': return Bottle()
    elif typename == 'alco_bottle': return choice([AlcoBottle, AlcoBottle_var1])()
    elif typename == '9mm_ammo': return _9mm()
    elif typename == '7.62_ammo': return _7_62mm()
    elif typename == 'boots': return Boots()
    elif typename == 'backpack': return Backpack()
    elif typename == 'gloves': return Gloves()
    else:
        print 'unknown typename:', typename
        raise TypeError

class Item(Obj):
    """Abstract item
    """
    def __init__(self, name=None, **kw):
        super(Item, self).__init__(**kw)
        self.name = name
        self.weight = None # kg
        self.bulk = None # approx volume in litres, uncompressed
        self.condition = None #['unknown', 'good', 'bad', 'broken']
        self.durability = 1.0
        self.description = None
        self.long_description = ''
        self.res_loc = None
        self.inv_icon = 'noimage'


        self.container = None # item's holder
        self.slot = None  # tunable slot

        event.sub('ev_obj_click', self.onObjClick)
        event.sub('ev_obj_mouse_over', self.onObjMouseOver)
        event.sub('ev_obj_mouse_out', self.onObjMouseOut)

    @property
    def __props(q):
        return {'name': q.name,
                'weight':  q.weight,
                'description': q.description}

    def spawn(self):
        super(Item, self).spawn()
        event.pub('ev_item_spawn', self)

    def respawn(self):
        super(Item, self).respawn()
        self.spawn()

    def list_props(self):
        print 'item props:'
        for k,v in self.__props.iteritems():
            print ' - %s: %s' % (k,v)

    def onObjClick(self, ob):
        if ob != self: # fixme: messages are broadcast
            return
        print 'hurr'
        if isinstance(ob, Item):
            event.pub('ev_item_click', ob)

    def onObjMouseOver(self, ob):
        if ob != self: #  fixme
            return
        print 'derp'
        if isinstance(ob, Item):
            print 'item.mouse_over'
            event.pub('ev_item_mouse_over', ob)

    def onObjMouseOut(self, ob):
        if ob != self: #  fixme
            return
        if isinstance(ob, Item):
            print 'item.mouse_out'
            event.pub('ev_item_mouse_out', ob)

class AppliableItem(object):
    item_type = 'appliable'
    def __init__(self, **kw):
        super(AppliableItem, self).__init__(**kw)
        self.apply_message = None
        self.stats_modify = {'hp': +1, 'time': 10}
    def apply(self):
        print 'APPLY MESSAGE', self.apply_message

class EquipableItem(rules.Armor):
    """Wear and such
    """
    item_type = 'equip'
    def __init__(self, **kw):
        super(EquipableItem, self).__init__(**kw)
        #self.item_type = 'equip'
        self.wearer = None
        self.insulation = 0


class Weapon(Item, rules.Weapon):
    """Abstract weapon
    """
    item_type = 'weapon'
    def __init__(self, **kw):
        super(Weapon, self).__init__(**kw)
        # item
        #self.slot = 'item'
        
        # weapon
        self.ammotypes = []
        self.type = None
        self.maxrounds = 0
        self.rounds = 0
        self.attackRange = 10 # m
        self.attackInterval = 1 # seconds
        self.reloadTime = 1 # seconds
        self.useCounter = 0

        ## setting obj's default orientation
        self.rot = Vector3(0, -90, 0)
        
        #self.sfx = {'use': None,
        #            'reload': None}
        #self.addSfx('use', self.res_loc + os.sep + 'use.wav')
        #self.addSfx('reload', self.res_loc + os.sep + 'use.wav')

    @property
    def ammo(self):
        return ", ".join([x.caliber_name for x in self.ammotypes])

    def load_res(self):
        self.sfx_use = Audio().add_sound(self.res_loc + os.sep + 'snd_use.wav')
        self.sfx_reload = Audio().add_sound(self.res_loc + os.sep + 'snd_reload.wav')
        try:
            self.sfx_use.linked_obj = self
            self.sfx_reload.linked_obj = self
        except: pass

    @property
    def __props(q):
        return {'ammo': q.ammo,
                'type':  q.type}

    def use(self):
        if not self.maxrounds:
            return True

        super(Weapon, self).use()
        self.rounds -= 1
        if self.rounds == 0:
            print 'need reload'
            return False
        
        self.useCounter += 1
        #sfx[self.weapon.res_loc + '/sfx_use.wav'].play(pos=obj.pos)
        self.sfx_use.play()
        #event.pub('ev_snd_play', {'obj': self, 'name': 'use'})
        event.pub('ev_weapon_use', self)
        return True

    def reload(self):
        print 'reloading', self.name
        self.rounds = self.maxrounds
        self.sfx_reload.play()
        #event.pub('ev_snd_play', dict(obj=self, name='reload'))
        event.pub('ev_weapon_reload', self)

    def fire(self):
        print 'firing', self.name
    
    @property
    def isArmed(self):
        return True
    
    @property
    def needReload(self):
        if self.maxrounds == 0: return False
        return True if self.rounds < 1 else False
        
    def list_props(self):
        Item.list_props(self)
        print 'weapon props:'
        for k,v in self.__props.iteritems():
            print ' - %s: %s' % (k,v)

    @staticmethod
    def factory(typename):
        if typename == 'g17': return G17()
        elif typename == 'hunting_rifle': return HuntingRifle()
        else:
            print 'unknown typename:', typename
            raise TypeError

class G17(Weapon):
    item_typename = 'g17'
    def __init__(self, **kw):
        super(G17, self).__init__(**kw)
        self.__dict__.update({
            'name': 'G17',
            'ammo': '9mm',
            'type': 'gun',                     
            'description': "a modern gun",
            'ammotypes': [_9mm],
            'maxrounds': 10,
            'weight': 0.6,
            'bulk': 0.4,
            'attackRange': 30,
            'attackInterval': 0.4,
            'reloadTime': 1,
            'res_loc': "res/items/gun",
            'v_model': "res/items/gun/model.dae",
            #'gui_icon': "res/items/gun/icon.png",
            'inv_icon': "g17",
            # rules
            'reloadAPCost': 5,
            'useAPCost': 2,
            'modifier': +2,
            'range': 'short',
        })
        self.load_res()

class HuntingRifle(Weapon):
    item_typename = 'hunting_rifle'
    def __init__(self, **kw):
        super(HuntingRifle, self).__init__(**kw)
        q=self
        q.name = 'Hunting Rifle'
        q.type = 'rifle'
        q.description = "an old rifle"
        q.ammotypes = [_7_62mm]
        q.maxrounds = 5
        q.weight = 4.5
        q.bulk = 4
        q.attackRange = 100
        q.attackInterval = 2
        q.reloadTime = 10
        q.res_loc = "res/items/rifle"
        #q.v_model = "res/items/rifle/model.dae"
        #q.gui_icon = "res/items/rifle/icon.png"
        q.inv_icon = 'hunting_rifle'
        q.long_description = longtext("""\
            Despite its beat-up appearance, the hunting rifle can fire a total of about 667 rounds,
            the equivalent of 134 reloads, from full condition before breaking. 

            stats""")
        # rules
        q.reloadAPCost = 5
        q.useAPCost = 2
        q.modifier = +3
        q.range = 'long'
        self.load_res()

class Ammo(Item):
    """Abstract ammo
    """
    caliber_name = None
    item_type = 'ammo'
    def __init__(self, **kw):
        super(Ammo, self).__init__(**kw)

class _7_62mm(Ammo):
    item_typename = '7.62_ammo'
    caliber_name = '7.62'
    def __init__(self, **kw):
        super(_7_62mm, self).__init__(**kw)
        q=self
        # items props
        q.name = '7.62 ammo'
        q.weight = 0.10
        q.bulk = 0.02
        q.res_loc = "res/items/ammo/7.62mm"
        q.inv_icon = '7.62mm'
        q.long_description = longtext("""\
            Single standard 7.62 ammunition.

            stats""")

class _9mm(Ammo):
    item_typename = '9mm_ammo'
    caliber_name = '9mm'
    def __init__(self, **kw):
        super(_9mm, self).__init__(**kw)
        q=self
        # items props
        q.name = '9mm ammo'
        q.weight = 0.08
        q.bulk = 0.01
        q.res_loc = "res/items/9mm"
        q.inv_icon = '9mm'
        q.long_description = longtext("""\
            Standard 9mm ammunition.

            stats""")

"""
class WeaponFactory(object):
    @classmethod
    def get_weapon(self, typename):
        if typename == 'g17':
            w = Weapon(name='g17')
            w.ammo = '9mm'
            w.type = 'gun'
            w.weight = 0.6
        elif typename == 'mosin_nagant':
            w = Weapon(name='Mosin Nagant')
            w.ammo = '7.62mm'
            w.type = 'rifle'
            w.weight = 4.5
        else:
            print 'unknown typename:', typename
            raise TypeError
        return w
"""

class Bottle(Item, AppliableItem):
    item_typename = 'bottle'
    def __init__(self, **kw):
        super(Bottle, self).__init__(**kw)
        q=self
        q.name = 'Bottle'
        q.weight = 0.5
        q.bulk = 1
        q.description = "an empty bottle"
        q.res_loc = 'res/items/bottle'
        q.inv_icon = 'bottle'
        q.apply_message = "you drink the contents of the %s" % self.name

class AlcoBottle(Item, AppliableItem):
    item_typename = 'alco_bottle'
    def __init__(self, **kw):
        super(AlcoBottle, self).__init__(**kw)
        q=self
        q.name = 'Alcoholic drink'
        q.weight = 0.8
        q.bulk = 1
        q.description = "a bottle with alcoholic drink"
        q.res_loc = 'res/items/alco_bottle'
        q.inv_icon = 'noimage' #'alco_bottle'
        q.apply_message = "you drink the contents of the %s" % self.name
        q.long_description = longtext("""\
            A bottle with some strong alcohol.

            HP: -10; AP: +1""")

class AlcoBottle_var1(Item, AppliableItem):
    item_typename = 'alco_bottle'
    def __init__(self, **kw):
        super(AlcoBottle_var1, self).__init__(**kw)
        q=self
        q.name = 'Another alcoholic drink'
        q.weight = 0.6
        q.bulk = 1
        q.description = "a bottle with alcoholic drink"
        q.res_loc = 'res/items/alco_bottle_var1'
        #q.inv_icon = 'alco_bottle'
        q.inv_icon = 'noimage'
        q.long_description = longtext("""\
            A variant of alco_bottle

            HP: -10; AP: +1""")

class Boots(Item, EquipableItem):
    item_typename = 'boots'
    def __init__(self, **kw):
        super(Boots, self).__init__(**kw)
        q=self
        q.name = 'Boots'
        q.weight = 1.0
        q.bulk = 2
        q.durability = 0.8
        q.description = "boots"
        q.res_loc = 'res/items/equip/boots'
        q.inv_icon = 'boots'
        q.long_description = longtext("""\
            Some boots.
            """)
        # equipable
        q.slot = 'foots'
        #q.insulation = .3 * self.get_insulation_area() # .1 (one layer of leather) + .2 (1cm fur)
        #q.insulation = 3 # 0: none, 1: weak, 2: moderate, 3: good, 4: veryGood, 5: excellent
        q.insulation = 3 # suitable for temperature x (approx: -x * 10 degrees)
        # rules
        q.modifier = -1 # light armor
        
class Backpack(Item, EquipableItem):
    item_typename = 'backpack'
    def __init__(self, **kw):
        super(Backpack, self).__init__(**kw)
        q=self
        q.name = 'Backpack'
        q.weight = 2.0
        q.bulk = -50
        q.durability = 0.9
        q.description = "medium backpack"
        q.res_loc = 'res/items/equip/backpack'
        q.inv_icon = 'backpack'
        q.long_description = longtext("""\
            Fine medium backpack.
            """)
        # equipable
        q.slot = 'back'
        q.insulation = 1
        # rules
        q.modifier = 0
        
class Gloves(Item, EquipableItem):
    item_typename = 'gloves'
    def __init__(self, **kw):
        super(Gloves, self).__init__(**kw)
        q=self
        q.name = 'Gloves'
        q.weight = 0.1
        q.bulk = 0.1
        q.durability = 1.0
        q.description = "pair of gloves"
        q.res_loc = 'res/items/equip/gloves'
        q.inv_icon = 'noimage'
        q.long_description = longtext("""\
            A pair of durable gloves.
            Suitable for -20 and up temperatures.
            """)
        # equipable
        q.slot = 'hands'
        q.insulation = 2
        # rules
        q.modifier = 0

if __name__ == '__main__':
    #pistol = G17()
    #rifle = MosinNagant()
    #pistol = WeaponFactory.get_weapon(typename='g17')
    #rifle = WeaponFactory.get_weapon(typename='mosin_nagant')
    pistol = Weapon.factory('g17')
    rifle = Weapon.factory('hunting_rifle')

    pistol.reload()
    rifle.reload()

    pistol.fire()
    rifle.fire()

    for it in (pistol, rifle):
        #print 'it:\n ', \
            #'; '.join([str(x) for x in (it.name, it.ammo, it.type, it.weight)])
        it.list_props()

