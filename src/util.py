#-*- coding: utf-8 -*-
from __future__ import division, absolute_import
from time import time
from math import ceil, floor

_names = {}
def get_autoname(k):
    global _names
    _names[k] = _names[k] + 1 if k in _names else 1
    return k + '#' + str(_names[k])

def longtext(s):
    return "\n".join([x.strip() for x in s.splitlines()])

def format_approx_number(n, postfix):
    print 'n=', n
    s = u""
    b = int(floor(n))
    if n < 0.01:
        return u"—"
    if n < 1:
        s = "<1"
    else:
        s += u"%s" % b
    if n - b > 0.5:
        s += u"½"
    s += unicode(postfix)
    return s

class DotDict(dict):
    def __getattr__(self, attr):
        return self.get(attr, None)
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

class Singleton(type):
    def __init__(cls, name, bases, dict):
        super(Singleton, cls).__init__(name, bases, dict)
        cls.instance = None
    def __call__(cls,*args,**kw):
        if cls.instance is None:
            cls.instance = super(Singleton, cls).__call__(*args, **kw)
        return cls.instance

class Timer(object):
    def __init__(self):
        self.speed = 1.0
        self.time = 0
        self.start_time = 0

    def start(self):
        #assert self.time == 0
        self.start_time = time()
        assert self.start_time > 0
    
    def stop(self):
        raise NotImplementedError

    def reset(self):
        self.time = 0
        self.start()
        

    def update(self, dt):
        self.time = (time() - self.start_time) * self.speed

    @property
    def dt(self):
        return time() - self.start_time
        #return self.time - self.start_time

class TaskSchedule(object):
    def __init__(self, cb, interval, start_delayed=False):
        self.cb = cb
        self.interval = interval
        self.timer = Timer()
        if start_delayed:
            self.start(delayed=True)

    def start(self, delayed=False):
        if not delayed:
            self.execute()
        self.timer.start()

    def reset(self):
        self.timer.reset()

    def execute(self):
        self.cb()

    def update(self, dt):
        if self.timer.dt >= self.interval:
            #print 'bang'
            self.execute()
            self.timer.reset()
