from __future__ import division, absolute_import
from src.vector3 import Vector3

#from src.audio import sfx
from src.event import event

class LocationMgr(object):
    def __init__(self, game):
        self.game = game
        self.loc = None
        
    def setLocation(self, val):
        # cycle levels if val = +1/-1
        if type(val) == int:
            from src.level import getLevelsList
            ll = getLevelsList()
            idx = (ll.index(self.loc.level.name) + val) % len(ll)
            val = ll[idx]
        levname = val
        if self.loc:
            self.loc.free()
            
        if levname in ('mall', 'test_meta', 'empty'): # fixme
            self.loc = Location.factory(levname)
        else:
            print "warning, no location module for levname '%s'" % levname
            print "using DefaultLocation"
            self.loc = Location.factory('defaultlocation')
        self.loc.free()
        self.loc.setLevel(levname)
        self.loc.start()
    
    def update(self, dt):
        self.loc.update(dt)
        

class Location(object):
    """Abstract Location
    """
    def __init__(self, game):
        self.game = game
        self.level = None
        self.spawn_pos = None
        #from src.character import Character
        #self.char = Character()
        self.char = game.char
        self._temperature = None
        event.sub('ev_objscollide', self.onObjsCollide)
        self._reset()

    @property
    def temperature(self):
        if self._temperature is None:
            return self.game.environment.temperature
        else:
            return self._temperature
    @temperature.setter
    def temperature(self, v):
        self._temperature = v

    def _reset(self):
        self.strawberries_collected = 0

    def restart(self):
        self._reset()
        self.level.respawn_strawberries()
        self.char.respawn(self.spawn_pos)

    def setLevel(self, name):
        print 'setLevel:', name
        from src.level import Level
        self.levelName = name
        self.level = Level(self.levelName)
        self.level.spawn()

    def start(self):
        # fixme: metadata must be loaded before level spawned
        try:
            self.spawn_pos = self.level.meta['spawn_pos']['pos']
        except KeyError: pass
        print self.spawn_pos
        spawnpos = self.spawn_pos
        if spawnpos is not None:
            self.char.pos = Vector3(*spawnpos)
        else:
            self.char.pos = Vector3(0, 0, 2)
        """
        if not self.char._spawned:
            self.char.spawn(self.spawn_pos)
            # fixme
        else:
            self.char.respawn(self.spawn_pos)
        """
        #self.char.spawn(self.spawn_pos)
        print "LOCATION START!"
        #sfx['bg'].play()
        #self.game.chv.respawn()
        self.game.char.spawn()
        #print self.game.char.pos

        event.pub('ev_location_start')

    def done(self):
        print "LOCATION DONE!"
        event.pub('ev_stage_done')
        self.restart()

    def free(self):
        if self.level:
            self.level.free()
        self._reset()
        #sfx['bg'].pause()

    def update(self, dt):
        self.level.update(dt)
        
    @staticmethod
    def factory(typename):
        from src.game import Game
        _game = Game()
        if typename == 'mall':
            from src.locations import Mall
            return Mall(_game)
        elif typename == 'test_meta':
            from src.locations import TestMeta
            return TestMeta(_game)
        elif typename == 'defaultlocation':
            from src.locations import DefaultLocation
            return DefaultLocation(_game)
        elif typename == 'empty':
            from src.locations import Empty
            return Empty(_game)
        else:
            print 'error: unknown typename:', typename
            exit()
            
    def onObjsCollide(self, ob1, ob2):
        for ob in (ob1, ob2):
            if not ob or ob.typename != 'strawberry':
                continue

            if ob.isFlower:
                if self.strawberries_collected == self.strawberries_total:
                    self.done()
            else:
                print "COLLECTED STRAWBERRY"
                self.strawberries_collected += 1
                sfx['pick'].play()
