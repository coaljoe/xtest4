from __future__ import division, absolute_import
from math import *
from src.lib.aux3d.contrib.glfw import *
from src.lib.aux3d import aux3d as aux
from src.event import event
from src.vector3 import Vector3
from src.conf import conf

class Scene(object):
    def __init__(self, game):
        self.game = game
        self.reset()
        self.setDefaultScene()

        # events
        event.sub('ev_location_start', self.onLocationStart)

    def reset(self):
        # camera target
        self._pz = 0.0
        self._px = 0.0

    def setDefaultScene(self):
        self.camRad = sqrt(10**2 + 10**2) # diagonal
        self.camRot = 45
        self.camRotSpeed = 60
        self.camRotStep = 15 #360/16
        self.camTilt = 35.264
        self.camTiltMin = 20
        self.camTiltMax = 60
        self.camTiltStep = 10
        self.camZoomSpeed = 1
        self.camZoomStep = 1
        self.camZoomMin = 1
        self.camZoomMax = 4
        self.camDefaultZoom = 3
        self.camVelocity = 10

        # setup main camera
        self.camera = aux.sm.getActiveCamera()
        cam = self.camera
        w, h = conf.vars.resolution
        cam.setupView(10, w / h, -20, 80, ortho=True)

        cam = aux.sm.addCameraNode("shadowCam")
        #cam.setupView(10, self.w.width / self.w.height, 0.1, 100, ortho=True)
        #cam.setupView(30, 1.0, 0.1, 10, ortho=True)
        #cam.setupView(30, 1.0, -10, 10, ortho=True)
        #cam.setupView(10, 1.0, 0.1, 100, ortho=True)
        #cam.addRenderTarget("shadowCamRT", width=512, height=512, depth=True)
        #cam.addRenderTarget("shadowCamRT", width=800, height=600, depth=True)

        #cam.pos = Vector3(10, 5, 5)
        #cam.rot = Vector3(-90, 45, 0)
        #cam.rot = Vector3(-90 + -5, 160, -5)
        #cam.target = Vector3(0, 0, 0)
        #cam.transform(10, 5, 5,  -5, 160, -5)
        #cam.transform(10, 5, 5,  -90 + -5, 160, -5)
        #cam.transform(10, 10, 10,  -35.264, 45, 0)


        size = 512*2
        cam.addRenderTarget("shadowCamRT", width=size, height=size, depth=True)
        # ortho size ~= cam radius * 2
        cam.setupView(20, 1.0, -10, 30, ortho=True)
        #cam.transform(0, 5, 5,  -90 + -5, 160, -5)
        #cam.transform(10, 5, 0,  -90 , 0, 0)
        #cam.transform(0, 0, 0,  -90, 0, 0)
        cam.transform(0, 10, 0,  -90 + -5, 160, -5)

        """
        cam.addRenderTarget("shadowCamRT", width=512, height=512, depth=True)
        #cam.setupView(10, self.w.width / self.w.height, -10, 30, ortho=True)
        cam.setupView(10, 1.0, -10, 20, ortho=True)
        cam.transform(10, 10, 10,  -35.264, 45, 0)
        #light.transform(10, 5, 5,  -90 + -5, 160, -5)
        """

        self.shadowCam = cam
        #self.shadowCam.active = True
        self.camera.active = True
        #self.camera.transform(0, 10, 0,  -90, 0, 0)
        #aux.setActiveCamera(self.shadowCam)
        #aux.setActiveCamera(self.camera)

        light = aux.sm.addLightNode("shadowLight")
        #light.transform(10, 5, 5,  -90 + -5, 160, -5)
        # XXX wrong?
        light.pos = cam.pos
        light.rot = cam.rot
        light.castShadows = True
        light.onlyShadows = True
        light.setCamera(cam)

        # fixme
        aux.renderer.shadowMap = light.camera.rt.tex_id
        self.light = aux.sm.addLightNode("light1")

    def on_key_press(self, key):
        clamp = lambda n, minn, maxn: max(min(maxn, n), minn)
        _range = [self.camTiltMin, self.camTiltMax]

        if key == GLFW_KEY_KP_1:
            self.camRot = (self.camRot + self.camRotStep) % 360
        elif key == GLFW_KEY_KP_3:
            self.camRot = (self.camRot - self.camRotStep) % 360
            self.game.gui.dashboard.hide()
        elif key == GLFW_KEY_KP_7:
            self.camTilt = clamp(self.camTilt + self.camTiltStep, *_range)
        elif key == GLFW_KEY_KP_9:
            self.camTilt = clamp(self.camTilt - self.camTiltStep, *_range)

        mn, mx = self.camZoomMin,  self.camZoomMax
        if key == GLFW_KEY_KP_SUBTRACT:
            self.camera.zoom = clamp(self.camera.zoom + self.camZoomStep, mn, mx)
        elif key == GLFW_KEY_KP_ADD:
            self.camera.zoom = clamp(self.camera.zoom - self.camZoomStep, mn, mx)
        elif key == GLFW_KEY_KP_0:
            self.camera.zoom = self.camDefaultZoom

    def on_key_release(self, key):
        pass

    def onLocationStart(self):
        print 'Scene.onLocationStart'
        self.reset()
        spawnpos = self.game.location.spawn_pos
        if spawnpos:
            self._px += self.game.char.pos.x
            self._pz += self.game.char.pos.z

    def update(self,dt):
        px = cos(radians(self.camRot)) * self.camRad
        pz = sin(radians(self.camRot)) * self.camRad
        self.camera.transform(self._px + px, 10, self._pz + pz, -self.camTilt, 90 - self.camRot, 0)
        self.camera.target = Vector3(self._px, 0, self._pz)
        self.shadowCam.zoom = self.camera.zoom

        #self.shadowCam.rot = self.camera.rot
        #self.shadowCam.rot[1] = self.camera.ry

        ## misc
        #light_pos.x = aux.sm.getActiveCamera().x
        #light_pos.z = aux.sm.getActiveCamera().z

