from __future__ import division, absolute_import

class NPC(object):
    def __init__(self, **kw):
        super(NPC, self).__init__(**kw)
        self.npc = True
        self.ai = None
        self.hostile = False

    @property
    def isHostile(self):
        return self.hostile

    @staticmethod
    def factory(typename):
        if typename == 'kid':
            #XXX fixme
            from src.npcs.kid import KidNPC
            return KidNPC()
        elif typename == 'camilla':
            #XXX fixme
            from src.npcs.camilla import Camilla
            return Camilla()
        else:
            print 'unknown typename:', typename
            raise TypeError

