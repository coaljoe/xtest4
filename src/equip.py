from __future__ import division, absolute_import
from math import sin, cos, radians
from src.vector3 import Vector3
from src.event import event
from src.obj import Obj
from src import geom
from src.inventory import Inventory, ItemContainer
from src.item import item_factory, EquipableItem
from src.util import Timer
from src.rpgsys import rules
from src.rpgsys import specialisms

class CharacterEquip(ItemContainer):
    #slot_names = 'head face neck body legs foots hands belt back'.split()
    slot_names = 'head body legs foots hands back'.split()
    def __init__(self, char):
        super(CharacterEquip, self).__init__(name='equip', cont_name="Characters Equipment")
        self.char = char
        self.head = None
        #self.face = None
        #self.neck = None
        self.body = None
        self.legs = None
        self.foots = None
        #self.larm = None
        #self.rarm = None
        self.hands = None
        #self.belt = None
        self.back = None
        # item slots
        self.item1 = None
        self.item2 = None
    def wear(self, it):
        assert it.container
        if it.wearer:
            raise AlreadyEquippedError        
        self.putItem(it)
        it.wearer = self.char
    def canWear(self, it):
        if isinstance(it, EquipableItem):
            return True
    def takeOff(self, it):
        self.takeItem(it)
        it.wearer = None
    def putItem(self, it, slot_name=None):
        # assign free slot
        if slot_name:
            it.slot = slot_name
        elif it.slot is None:
            it.slot = 'item1' if self.item1 is None else 'item2'
        if getattr(self, it.slot) is not None:
            raise SlotError
        setattr(self, it.slot, it)
        it.container.swapItem(it, self)
    def takeItem(self, it):
        if getattr(self, it.slot) is None:
            print it.slot
            raise SlotError
        setattr(self, it.slot, None)
        if it.slot in ['item1', 'item2']:
            it.slot = None
        it.container.swapItem(it, self.char.inventory)
    def putReplaceItem(self, it, **kw):
        try:
            self.putItem(it, **kw)
        except SlotError:
            # left
            if kw.get('slot_name') == 'item2':
                self.takeItem(getattr(self, 'item2'))
                self.putItem(it, **kw)
            #right
            else:
                self.takeItem(getattr(self, 'item1'))
                self.putItem(it, **kw)
    @property
    def armor(self):
        return [it for it in self.used_slots if it.modifier != 0]
    @property
    def armor_modifier(self):
        return sum([it.modifier for it in self.armor])
    @property
    def total_insulation(self):
        return sum([it.insulation for it in self.used_slots])
    @property
    def slots(self):
        return [getattr(self, name) for name in self.slot_names]
    @property
    def slotsnames(self):
        return [(getattr(self, name), name) for name in self.slot_names]
    @property
    def wear_slotsnames(self):
        # fixme
        return [(it, name) for it, name in self.slotsnames if name not in ['belt', 'back']]
    @property
    def used_slots(self):
        return [slot for slot in self.slots if slot is not None]
    #@property
    #def item(self):
    #    return None
    #@item.setter
    #def item(self, it):
    #    if getattr(self, 'item1') is None:
    #        setattr(self, 'item1', it)
    #    elif getattr(self, 'item2') is None:
    #        setattr(self, 'item2', it)
    #    else:
    #        raise IndexError


class AlreadyEquippedError(Exception):
    def __str__(self):
        return "The equipable is already weared."

class SlotError(Exception):
    pass
