from __future__ import division, absolute_import
from src import pathfind

class CargoRoute(object):
    def __init__(self):
        self.targets = []
        self.nodes = []
        self.path = None
        self.register()

    def addTargets(self, *args):
        self.targets.extend(args)
        t0, t1 = self.targets[0], self.targets[1]
        n0, n1 = ((t0.x + t0.cargo_point[0], t0.y + t0.cargo_point[1]), \
                  (t1.x + t1.cargo_point[0], t1.y + t1.cargo_point[1]))
        self.targets.extend([t0, t1])
        self.nodes.extend([n0, n1])
        self.path = pathfind.path((n0[0], n0[1]), (n1[0], n1[1]))

    def getNextPath(self, from_):
        to = self.getNextNode(from_)
        path = pathfind.path(from_, to)
        print "LOL", from_, to, path
        return path

    def getNextNode(self, from_):
        for i, n in enumerate(self.nodes):
            if n == from_:
               # found
               return self.nodes[(i + 1) % len(self.nodes)]

        # not found
        print "next node not found"
        print from_, self.nodes
        exit()

    def register(self): pass

