from __future__ import division, absolute_import
import numpy as np
from src.const import *

class Field(object):
    def __init__(self):
        print ':: Field created'
        self._field = None
        self.w = int(world_size//cell_size)
        self.h = int(world_size//cell_size)
        self.landmap = np.zeros((self.w, self.h), np.int8)
        self.pointer_x = None # view's
        self.pointer_y = None # view's

    def isCellEmpty(self, x, y):
        return True if self.landmap[x][y] == 0 else False

    def setCellEmpty(self, x, y, val):
        self.landmap[x][y] = val

    def getHeightAtCell(self, x, y):
        #return -30
        #return self._field.getHeightAtCell(x, y)
        pass

    def saveLandmap(self, fn="landmap.png"):
        from PIL import Image
        im = Image.new("1", (self.w, self.h))
        putpixel = im.im.putpixel

        zz = np.flipud(np.rot90(field.landmap))
        for y in xrange(self.h):
            for x in xrange(self.w):
                if zz[x][y]:
                    putpixel((x, y), 1)
                    
        im.save(fn, "PNG")

    def spawn(self):
        pass
        #from cppview import Field
        #self._field = Field()
        #self._field.spawn()

    def onFieldCellActivate(self, px, py, oldx, oldy):
        pass

