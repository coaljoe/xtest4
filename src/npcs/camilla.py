from __future__ import division, absolute_import
from src.character import Character
from src.npc import NPC
from src.rpgsys.rules import d6, d8, d10
from src.util import longtext

class Camilla(Character, NPC):
    """Camilla NPC
    """
    stats_kills_group_name = 'npc'
    def __init__(self, **kw):
        super(Camilla, self).__init__(**kw)
        q=self
        q.res_loc = 'res/npcs/camilla'
        # info
        q.info.age = 21
        q.info.gender = 'F'
        q.info.name = 'Camilla'
        q.info.description = "Camilla (Jacob's daughter)"
        q.info.background = longtext("""\
            Camilla is a hard faced 21 year old who mimics her father's ire.
            Pretty and wise beyond doubt but has been serving in her father's bar since she could remember.
            Fending of leering drunks has forged her into a intolerant and cold person.
            She now all but runs front of house for her father as she comes of age.
            Despite all of Camilla's intolerance to others she is barraged with stories and gossip.
            She is always listening and is more informed than most people about the goings on in the Mall and further afield. 
        """)
        q.info.portrait = 'portraits/camilla'
        # rules.Character
        q.actions = d6
        q.wits = d8
        q.ego = d10
        q.load()
