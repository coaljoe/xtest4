from __future__ import division, absolute_import
from random import randint, choice
from src.character import Character
from src.npc import NPC
from src.rpgsys.rules import d6, d8, d10

class Kid(Character):
    """Random NPC kid
    """
    stats_kills_group_name = 'kids'
    def __init__(self, **kw):
        super(Kid, self).__init__(**kw)
        q=self
        q.res_loc = 'res/npcs/kid'
        # info
        q.info.age = randint(8, 14)
        q.info.gender = choice(['M', 'F'])
        q.info.name = 'Kid'
        q.info.description = 'a kid'
        # rules.Character
        q.actions = d6
        q.wits = d8
        q.ego = d10
        q.load()

class KidNPC(Kid, NPC):
    """Random NPC kid
    """
    def __init__(self, **kw):
        super(KidNPC, self).__init__(**kw)
