from __future__ import division, absolute_import
from src.event import event
from src.util import Singleton, Timer
from src.objmgr import ObjMgr
from src.scene import Scene
from src.environment import Environment
from src.location import LocationMgr
from src.characters import character_factory
from src.itemmgr import ItemMgr
from src.viewmgr import ViewMgr
from src.hud import Hud
from src.gui import Gui
from src.ai import AIMgr
from src.audio import Audio
from src.conf import conf

class Game(object):
    __metaclass__ = Singleton
    def __init__(self):
        self.frame = 0
        self.timer = Timer()
        self.timer.speed = 1.0
        self.timer.start()
        self.event = event
        self.objmgr = ObjMgr(self)
        self.scene = Scene(self)
        self.char = character_factory('default')
        self.locmgr = LocationMgr(self)
        self.environment = Environment(self)
        self.itemmgr = ItemMgr()
        self.aimgr = AIMgr()
        self.viewmgr = ViewMgr(self)
        self.hud = Hud(self)
        self.audio = Audio(self)
        # gui
        self.gui = None
        if conf.vars.enable_gui:
            self.gui =  Gui(self)
        
        self.shed_gen = None
        self._pause = False
        self._app = None
        self._lock = False
        self._gui_halt = False

    def update(self, dt):
        dt = dt * self.timer.speed
        self.timer.update(dt)
        self.event.update(dt)
        
        if not self._gui_halt:
            self.objmgr.update(dt)
            self.scene.update(dt)
            self.locmgr.update(dt)
            self.environment.update(dt)
            self.aimgr.update(dt)
            self.viewmgr.update(dt)
            
        self.hud.update(dt)
        self.audio.update(dt)
        
        if self.gui:
            self.gui.update(dt)

        if self.shed_gen:
            try: self.shed_gen.next()
            except StopIteration: pass

        self.frame += 1

    def playLocation(self, val):
        self._lock = True
        self.locmgr.setLocation(val)
        self._lock = False
        
    @property
    def location(self):
        return self.locmgr.loc

    @property
    def player(self):
        return self.playermgr.player

    @property
    def level(self):
        return self.location.level

    @property
    def time(self):
        return self.timer.time

    @property
    def pause(self):
        return self._pause
    @pause.setter
    def pause(self, val):
        self._pause = val
        self._app.pause(val)
    @property
    def gui_halt(self):
        return self._gui_halt
    @gui_halt.setter
    def gui_halt(self, val):
        self._gui_halt = val

    def quit(self):
        self.audio.free()
