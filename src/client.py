#!/usr/bin/env python
from __future__ import division, absolute_import
from src.game import Game

# remote/master code
remote_game = Game()


# local/client code
game = Game()

# bootstrap:
# set gs from initial client
gs = remote_game.getGameState()
game.setGameState(gs)

assert game.getGameState() == gs

