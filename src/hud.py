from __future__ import division, absolute_import
from src.lib.aux3d.contrib.glfw import *
from src.event import event
from src.character import Character
from src.selbox import SelBox
from src.dialoguemgr import DialogueMgr

class States:
    idle = 0
    selection = 1

class ActionStates:
    default = 0
    attack = 1
    look = 2

class Hud(object):
    def __init__(self, game):
        self.selObj = None
        self.activeObj = None
        self.hoverObj = None
        self.game = game
        self.state = 'idle'
        #self.actionState = ActionStates.default
        self.actionState = 'default'
        self.lock = False

        # fixme
        self.mx = None
        self.my = None
        self.width = None
        self.height = None

        event.sub('ev_item_mouse_over', self.onItemMouseOver)
        event.sub('ev_item_mouse_out', self.onItemMouseOut)
        event.sub('ev_item_click', self.onItemClick)
        event.sub('ev_obj_mouse_over', self.onObjMouseOver)
        event.sub('ev_obj_mouse_out', self.onObjMouseOut)
        event.sub('ev_obj_click', self.onObjClick)

    def onItemMouseOver(self, it):
        print 'hud.item_mouse_over', it
        print 'item:', it.name, ', ', it.description
        self.activeObj = it
        self.game.gui.show_tip(it.description, (self.mxnorm - 0.02, self.mynorm + 0.04))

    def onItemMouseOut(self, it):
        print 'hud.item_mouse_out', it
        self.game.gui.hide_tip()
            
    def onItemClick(self, it):
        self.game.char.pickup_item(it)
            
    def onObjMouseOver(self, obj):
        self.hoverObj = obj
        if self.game.char.canAttack(obj):
            print "aiming at", obj
            
    def onObjMouseOut(self, obj):
        self.hoverObj = None
        
    def onObjClick(self, obj):
        print 'hud.onObjClick', obj
        if self.actionState == 'default':
            if isinstance(obj, Character):
                npc=obj
                if npc.isDead:
                    barter = self.game.gui.barter
                    barter.showFor(self.game.char.inventory, npc.inventory)
                    return
                if npc.isHostile:
                    # attack hostile npcs by default FIXME
                    if self.game.char.canAttack(obj):
                        self.game.char.setTarget(obj)
                if self.game.char.canTalkWith(npc):
                    dialogue = DialogueMgr().getDialogueForNPC(npc)
                    print dialogue
                    if not dialogue:
                        return
                    if dialogue.completed:
                        print "dialog was completed"
                        return
                    dialogueWin = self.game.gui.dialogue
                    dialogueWin.showFor(dialogue)
        elif self.actionState == 'attack':
            if self.game.char.canAttack(obj):
                self.game.char.setTarget(obj)

        if isinstance(obj, SelBox):
            if obj.inventory:
                barter = self.game.gui.barter
                barter.showFor(self.game.char.inventory, obj.inventory)


    """
    def on_mouse_button(self, button, pressed):
        if button == GLFW_MOUSE_BUTTON_LEFT:
            if self.hoverObj:
                if self.game.char.canAttack(obj):
                    self.game.char.setTarget(obj)
    """

    def update(self, dt):
        pass

    @property
    def mxnorm(self): return self.mx / self.width
    @property
    def mynorm(self): return self.my / self.height

