from __future__ import division, absolute_import
from src.building import Building
from src import geom

class Factory(Building):
    typename = 'factory'

    # type's props
    place_size = (2, 2)
    out_point_offset = (-1, -1)
    cargo_point = (-1, -1)

    def __init__(self, *args, **kw):
        Building.__init__(self, *args, **kw)

        self.out_point = geom.vec2add(self.xy, self.out_point_offset)
        self.out_point_dst = geom.vec2add(self.out_point, (-2, -2))

    def produce(self, what):
        what.xy = self.out_point
        what.spawn()
        what.moveTo(*self.out_point_dst)


