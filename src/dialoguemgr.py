from __future__ import division, absolute_import
from src.dialogue import Dialogue
from src.util import Singleton

class DialogueMgr(object):
    __metaclass__ = Singleton
    def __init__(self):
        self.dialogues = []
    def addDialogue(self, d):
        self.dialogues.append(d)
    def getDialogueForNPC(self, npc):
        for d in self.dialogues:
            if d.npc == npc:
                return d

class DialogueNotFoundError(Exception):
    pass

