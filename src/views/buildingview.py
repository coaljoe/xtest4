from __future__ import division, absolute_import
import os.path
from math import sin, cos, radians
from vector3 import Vector3
from src.lib import h3d
from src import conf

class BuildingView(object):
    def __init__(self, m):
        self.m = m
        self.ry = 0
        self.objs = {}
        #self.objs_offsets = {}
  
        class H3DRes: pass
        h3dres = H3DRes()
        self._h3dres = h3dres

        # by default its man
        h3dres.man = h3d.addResource(h3d.ResTypes.SceneGraph, "models/man/man.scene.xml", 0)
        h3d.utils.loadResourcesFromDisk('Content')

        self.model = h3dres.man

    def spawn(self):
        h3dres = self._h3dres
        m=self.m
        # scene geometry
        #from src.field import field
        #field.getHeightAtCell(m.cx, m.cy),
        #m.pos = Vector3(m.x * _cell_size, 0, -m.y * _cell_size)
        m.pos.y = 19.5

        self.node = h3d.addNodes(h3d.RootNode, self.model)
        h3d.setNodeTransform(self.node, m.pos.x, m.pos.y, m.pos.z, 0, 0, 0, 1, 1, 1)


    def update(self, dt):
        pass
  

class FactoryView(BuildingView):
    def __init__(self, *args, **kw):
        super(FactoryView, self).__init__(*args, **kw)
        self._h3dres.factory_mod = h3d.addResource(h3d.ResTypes.SceneGraph, "models/buildings/factory/factory.scene.xml", 0)
        h3d.utils.loadResourcesFromDisk('Content')

        self.model = self._h3dres.factory_mod
