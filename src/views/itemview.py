from __future__ import division, absolute_import
import os
from src.lib.aux3d import aux3d as aux

class ItemView(object):
    def __init__(self, m):
        self.m = m

        # load model
        path = os.path.join(m.res_loc, 'model.dae')
        path_generic = os.path.join(os.path.dirname(m.res_loc), 'generic', 'model.dae')
        if os.path.exists(path):
            _path = path
        elif os.path.exists(path_generic):
            _path = path_generic
        else:
            print 'cant load itemview from patches', (path, path_generic)
            exit(-1)

        self.node = list(aux.scene.loadScene(_path))[0] # fixme
        self.node.linked_obj = self.m

    def spawn(self): pass
    def respawn(self): pass

    def update(self, dt):
        m=self.m
        # fixme
        if m.container: # item inside a container
            self.node.visible = False 
            return
        else:
            self.node.visible = True
        #self.node.transform(m.pos.x, m.pos.y, m.pos.z, 90, 90, 0) # fixme use custom orientation
        self.node.pos = m.pos
        self.node.rot = m.rot
