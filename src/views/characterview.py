from __future__ import division, absolute_import
import os
from src.lib.aux3d import aux3d as aux
from src.event import event

class CharacterView(object):
    def __init__(self, m):
        self.m = m

        self.node = None
        self.node_dead = None

        # by default its man
        if not m.res_loc:
            #self.man = aux.addMesh("models/man.obj", 'materials/blue.json')
            #self.model = aux.resmgr.addMesh("models/man.obj", 'materials/model.json')
            #self.model.material.setTexture('res/models/man.jpg')
            #self.node = aux.scene.addMeshNode(self.model)
            #self.node = list(aux.scene.loadScene('res/models/man.dae'))[0] # fixme
            #self.node.mesh.material.setTexture('res/models/man.jpg')
            self.node = list(aux.scene.loadScene('res/npcs/kid/model.dae'))[0] # fixme
        else:
            self.node = list(aux.scene.loadScene(os.path.join(m.res_loc, 'model.dae')))[0] # fixme
            file = os.path.join(m.res_loc, 'dead.dae')
            if os.path.exists(file):
                self.node_dead = list(aux.scene.loadScene(file))[0] # fixme
                self.node_dead.visible = False

        #self.man = aux.addResource("res/models/man.blend")
        #self.manAnim = aux.addResource("animations/man.anim")

        #self.anim = self.manAnim
        #self.animTime = 0

        # selbuf backlinks
        self.node.linked_obj = self.m

        event.sub('ev_character_dead', self.onCharacterDead)

    def spawn(self): pass
    def respawn(self): pass

    def update(self, dt):
        m=self.m
        #self.node.transform(m.pos.x, m.pos.y, m.pos.z, 0, 90 - m.dir, 0)
        #self.node.transform(m.pos.x, m.pos.y, m.pos.z, m.rot.x, m.rot.y, m.rot.z)
        #self.node.transform(m.pos.x, m.pos.y, m.pos.z, m.rot.x, m.dir, m.rot.z)
        #self.node.transform(m.pos.x, m.pos.y, m.pos.z, m.rot.x, 90 - m.dir, m.rot.z)
        #self.node.rot.y = (90 - m.dir) % 360
        #self.node.rot.z = (270 + m.dir) % 360 # fixme: bug?
        #self.node.transform(m.pos.x, m.pos.y, m.pos.z, 0, 270 + m.dir, 0)

        self.node.pos = m.pos
        self.node.rot = m.rot


    def onCharacterDead(self, obj):
        if obj != self.m:
            return

        if self.node_dead:
            # swap nodes
            self.node.visible = False
            self.node_dead.visible = True
            tmp_linked_obj = self.node.linked_obj
            self.node = self.node_dead
            self.node.linked_obj = tmp_linked_obj

