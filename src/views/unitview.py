from __future__ import division, absolute_import
import os.path
from math import sin, cos, radians
from vector3 import Vector3
from src.lib import h3d
from src import conf

_world_size = 1024
_cell_size = _world_size / 128

class UnitView(object):
  def __init__(self, m):
      self.m = m
      self.ry = 0
      self.objs = {}
      #self.objs_offsets = {}

      class H3DRes: pass
      h3dres = H3DRes()
      self._h3dres = h3dres

      # by default its man
      h3dres.man = h3d.addResource(h3d.ResTypes.SceneGraph, "models/man/man.scene.xml", 0)
      h3d.utils.loadResourcesFromDisk('Content')

      self.model = h3dres.man

  def spawn(self):
      h3dres = self._h3dres
      m=self.m
      # scene geometry
      #from src.field import field
      #field.getHeightAtCell(m.cx, m.cy),
      #m.pos = Vector3(m.x * _cell_size, 0, -m.y * _cell_size)
      m.pos.y = 20.


      self.node = h3d.addNodes(h3d.RootNode, self.model)
      h3d.setNodeTransform(self.node, m.pos.x, m.pos.y, m.pos.z, 0, 0, 0, 1, 1, 1)

      """
      path = os.path.join(conf.data_path, 'units', m.typename)
      dotscene = DotScene(os.path.join(conf.data_path, 'units', m.typename, 'scene.xml'))
      print dotscene.nodes


      body_pos = None
      body_ori = None
      for d in dotscene.nodes:
          if d['name'] == 'body':
              ob = Obj()
              ob.setPos(m.pos + Vector3(*d['position']))
              ob.setOri(Quaternion(*d['orientation']))
              body_pos = Vector3(*d['position'])
              body_ori = Quaternion(*d['orientation'])
              print ">>>", 'units/' + m.typename + '/meshes/' + d['mesh']
              ob.setMesh('units/' + m.typename + '/meshes/' + d['mesh'])
              #ob.phys.setShapeType(1)
              ob.setMaterial("Examples/BumpyMetal")
              ob.spawn()
              self.objs[d['name']] = ob
              #self.objs_offsets[ob] = Vector3(*d['position'])
          else:
              self.objs['body'].addNode(d['name'],
                                        'units/' + m.typename + '/meshes/' + d['mesh'],
                                        Vector3(*d['position']) - body_pos,
                                        Quaternion(*d['orientation']))
        """



  def onSetDir(self):
      m=self.m
      """
      for ob in self.objs.values():
          ar = radians(-90) + m.dir.valueRadians()
          dirVec = Vector3(-cos(ar), 0, sin(ar))
          ob.setDirection(dirVec, 2) # TS_WORLD=2
          #ob.yaw(Radian(self.dir), 2) # TS_WORLD=2
      """
      self.ry = -90 + m.dir

  def update(self, dt):
      m=self.m
      """
      if m.speed:
          for ob in self.objs.values():
              curY = ob.getPos().y
              #offset = self.objs_offsets[ob]
              #ob.setPos(Vector3(m.pos.x + offset.x, curY, -m.pos.z + offset.z))
              ob.setPos(Vector3(m.pos.x, curY, -m.pos.z))
      """

      if m.ev_onSetDir:
          self.onSetDir()
          m.ev_onSetDir = False

      h3d.setNodeTransform(self.node, m.pos.x, m.pos.y, m.pos.z, 0, -90 + (-m.dir), 0, 1, 1, 1)
  
