from __future__ import division, absolute_import
from math import sin, cos, radians
from vector3 import Vector3
from src.lib import h3d
from src.views.unitview import UnitView

# fixme
class States:
    idle = 0
    moving = 1
    loading = 2
    unloading = 3

class DumpTruckView(UnitView):
    def __init__(self, m):
        UnitView.__init__(self, m)

        self._h3dres.truck = h3d.addResource(h3d.ResTypes.SceneGraph, "models/dumptruck/dump-truck.scene.xml", 0)
        h3d.utils.loadResourcesFromDisk('Content')

        self.model = self._h3dres.truck

    def onUnloadCargo(self):
        pass

    def spawn(self):
        super(DumpTruckView, self).spawn()
        if h3d.findNodes(self.node, 'dump_body', h3d.NodeTypes.Mesh):
            self.dump_body = h3d.getNodeFindResult(0)
        else:
            print 'cant find dump_body node'
            exit()

    def update(self, dt):
        UnitView.update(self, dt)
        m=self.m

        if m.state == States.unloading:
            t = h3d.getNodeTransform(self.dump_body)[0]
            h3d.setNodeTransform(self.dump_body, t[0], t[1], t[2], 0, 0, m.dumpBodyAngle, 1, 1, 1)
