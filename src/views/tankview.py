from __future__ import division, absolute_import
from math import sin, cos, radians
from vector3 import Vector3
from src.lib import h3d
from src.views.unitview import UnitView


class TankView(UnitView):
  def __init__(self, m):
      UnitView.__init__(self, m)
      h3dres = self._h3dres

      h3dres.tank = h3d.addResource(h3d.ResTypes.SceneGraph, "models/tank/tank.scene.xml", 0)
      h3d.utils.loadResourcesFromDisk('Content')

      self.model = h3dres.tank

  def spawn(self):
      super(TankView, self).spawn()
      h3dres = self._h3dres
      print 'derp'
      self.turret = h3d.getNodeChild(self.node, 1)

  def update(self, dt):
      super(TankView, self).update(dt)
      a = self.m.turretAngle
      if a:
          # rotate turret
          t = h3d.getNodeTransform(self.turret)[0]
          h3d.setNodeTransform(self.turret, t[0], t[1], t[2], 0, -a, 0, 1, 1, 1)
