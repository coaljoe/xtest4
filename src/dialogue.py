from __future__ import division, absolute_import

class Dialogue(object):
    def __init__(self, data, pcName=None, npcName=None):
        self.pc = None
        self.pcName = pcName
        self.npc = None
        self.npcName = npcName
        self.data = data
        self.proc = DialogueProcessor(self.data)
        self.reset()
    def reset(self):
        self.finished = False
        self.completed = False
        self.proc.reset()
    def iterate(self):
        if self.finished:
            raise DialogueFinishedError
        ret = self.proc.iterate()
        if ret is False:
            self.finished = True
            return False
        sect = self.proc.sect
        print
        print '=-'*12
        #print sect.inputs, sect.replies
        print 
        self.showSayText()
        print 
        self.showReplies()
        print
        #self.replyIdx(0)
        return True
    def getSayText(self):
        txt = "\n".join(self.proc.sect.inputs)
        return txt
    def getReplies(self):
        return self.proc.sect.replies
    def update(self):
        pass
    def showSayText(self):
        print "%s saying:\n" % self.npcName
        print "\t%s" % self.getSayText()
    def replyIdx(self, idx):
        print "-> %s sets reply (idx=%s)" % (self.pcName, idx)
        self.proc.replySectionIdx(idx)
    def showReplies(self):
        print "Replies:\n"
        for reply in self.getReplies():
            print '\t-> ', reply.text


class DialogueProcessor(object):
    def __init__(self, data):
        self.d = data
        self.reset()
    def reset(self):
        self.sectIdx = 0
        # current section
        self.sect = None
    def getSectionIdx(self, idx):
        name = self.d.items()[idx][0]
        return self.getSection(name)
    def getSection(self, name):
        d = self.d[name]
        return Section(d)
    def setSect(self, name):
        self.sect = self.getSection(name)
    def proceedToNextSection(self):
        if self.sect is None:
            self.sect = self.getSectionIdx(0)
            return True
        if not self.sect.isReplied:
            raise SectionNotRepliedError
        nextSectName = self.sect.reply.goto
        if nextSectName == 'exit':
            return False
        #print '->>>', nextSectName
        self.sect = self.getSection(nextSectName)
        return True
        #self.setSection(nextSect)
    def replySection(self, reply):
        self.sect.reply = reply
    def replySectionIdx(self, idx):
        self.sect.reply = self.sect.replies[idx]
    def iterate(self):
        ret = self.proceedToNextSection()
        if not ret:
            return False
        return True
    def update(self):
        pass


class Section(object):
    def __init__(self, data):
        self.d = data
        self.inputs = []
        self.replies = []
        self.reply = None # selected reply
        self.parse()
    def parse(self):
        d = self.d
        input = d['say']
        if type(input) is str:
            self.inputs.append(input)
        elif type(input) is list:
            for el in input:
                # check condition
                cond = el.get('cond')
                if cond and not cond():
                    print 'skip el:', el
                    continue
                # append validated variants
                self.inputs.append(el['say'])
        replies = d.get('replies')
        if replies:
            for el in replies:
                reply = ReplyOption(el)
                self.replies.append(reply)
    @property
    def isReplied(self):
        return True if self.reply is not None else False


class ReplyOption(object):
    def __init__(self, d):
        self.text = d['reply']
        self.goto = d['goto']
    def validate(self):
        pass


class SectionNotRepliedError(Exception):
    def __str__(self):
        return "Section was not replied"

class DialogueFinishedError(Exception):
    def __str__(self):
        return "Dialogue was finished"
