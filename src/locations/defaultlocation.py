from __future__ import division, absolute_import
from random import randint
from src.lib.aux3d import aux3d as aux
from src.vector3 import Vector3
from src.location import Location
from src.item import Item, Weapon


class DefaultLocation(Location):
    def __init__(self, game):
        super(DefaultLocation, self).__init__(game)
        self.w = None
        
    def start(self):
        super(DefaultLocation, self).start()
        
        """
        # item test
        self.w = Weapon.factory('hunting_rifle')
        self.w.pos = self.char.pos + Vector3(1, 0.1, 0) # place near character
        self.w.spawn()

        w2 = Weapon.factory('g17')
        w2.pos = self.char.pos + Vector3(1.5, 0.1, 0) # place near character
        w2.spawn()

        w3 = Weapon.factory('hunting_rifle')
        w3.pos = self.char.pos + Vector3(2.0, 0.1, 0) # place near character
        w3.spawn()
        """
        m = list(aux.scene.loadScene('res/models/armature.dae'))[0]

        """
        # inventory test
        inv = self.char.inventory
        inv.addItem(self.w)
        inv.addItem(self.w)
        inv.listItems()
        inv.removeItem(self.w)
        inv.removeItem(self.w)
        inv.addItem(self.w)
        self.char.drop_item(self.w)
        self.char.pickup_item(self.w)
        self.char.drop_item(self.w)
        self.char.drop_item(self.w)
        """
