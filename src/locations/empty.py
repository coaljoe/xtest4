from __future__ import division, absolute_import
from src.vector3 import Vector3
from src.location import Location


class Empty(Location):
    def __init__(self, game):
        super(Empty, self).__init__(game)
        
    def start(self):
        super(Empty, self).start()
        
