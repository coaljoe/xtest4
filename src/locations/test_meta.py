from __future__ import division, absolute_import
from random import randint
from src.vector3 import Vector3
from src.location import Location
from src.npc import NPC
from src.ai import WanderingAI
from src.item import Weapon, item_factory
from src.inventory import ItemContainer
from src.selbox import SelBox
from src.dialogues.test import test_dialogue

class TestMeta(Location):
    def __init__(self, game):
        super(TestMeta, self).__init__(game)
        self.kids_npcs = []
        
    def start(self):
        super(TestMeta, self).start()
        
        # npc test
        npc = NPC.factory('camilla')
        npc.pos = self.level.meta['npc1_spawnpos']['pos']
        npc.spawn()
        print npc.pos, self.char.pos
        print npc.rot
        print self.char.rot

        # create dialogue to npc
        test_dialogue(npc)

        w = Weapon.factory('g17')
        w.rounds = w.maxrounds
        #w.reload()

        # inventory test
        inv = self.char.inventory
        #inv.addItem(w)
        self.char.pickup_item(w)
        inv.listItems()
        
        # set item
        self.char.equip.putItem(w)
        self.char.item = w
        #self.char.setTarget(npc)

        # dead npc test
        npc1 = NPC.factory('kid')
        npc1.pos = self.game.char.pos + Vector3(-4, 0, 0)
        npc1.spawn()
        npc1.die()
        self.kids_npcs.append(npc1)

        # add items to dead npc
        for name in ['g17', 'hunting_rifle', 'hunting_rifle']:
            w = Weapon.factory(name)
            npc1.pickup_item(w)
        it = item_factory('bottle')
        npc1.pickup_item(it)

        # closet item container test
        it = item_factory('bottle')
        cont = ItemContainer(name='closet', cont_name='Blue Closet')
        cont.addItem(it)
        sb = SelBox() # gameobject (Obj)
        sb.inventory = cont
        # link selbox node to the selbox
        self.level.selbox_nodes_map['closet_selbox'].linked_obj = sb
        #self.levels.selboxes['closet_selbox'].linked_obj = cont
        #self.level.selboxes[0].linked_obj = cont
        #print self.level.selboxes[0]
        #exit()

        # bottle test
        it = item_factory('alco_bottle')
        #it = Weapon.factory('g17')
        it.pos = self.char.pos + Vector3(2.0, 0.1, 0) # place near character
        it.spawn()

        # ammo test
        inv = self.char.inventory
        for i in xrange(30):
            it = item_factory('9mm_ammo')
            inv.addItem(it)

        it = item_factory('7.62_ammo')
        #inv.addItem(w)
        self.char.pickup_item(it)
