from __future__ import division, absolute_import
from random import randint
from src.vector3 import Vector3
from src.location import Location
from src.npc import NPC
from src.ai import WanderingAI
from src.item import Weapon
from src.dialogues.test import test_dialogue

class Mall(Location):
    def __init__(self, game):
        super(Mall, self).__init__(game)
        self.kids_npcs = []
        
    def start(self):
        super(Mall, self).start()
        
        # npc test
        npc = NPC.factory('camilla')
        npc.pos = self.char.pos + Vector3(5, 0, 0)
        #npc.rot = Vector3(0, 0, 0)
        #npc.rot.z = 90.0
        #self.char.rot = Vector3(randint(0, 360), randint(0, 360), randint(0, 360))
        #self.char.rot.x = randint(0, 360)
        #self.char.rot.y = randint(0, 360)
        #self.char.rot.z = randint(0, 360)
        #self.char.rot = Vector3(90, 0, 0)
        #npc.rot = Vector3(90, 0, 0)
        #self.char.rot.x = 90
        #self.char.rot = Vector3(0, 0, 0)
        #npc.ai = WanderingAI(npc)
        npc.spawn()
        print npc.pos, self.char.pos
        print npc.rot
        print self.char.rot

        # dialogue test
        test_dialogue(npc)

        for i in xrange(3):
            npc = NPC.factory('kid')
            npc.pos = self.char.pos + Vector3(randint(0, 4), 0, randint(0, 4))
            npc.ai = WanderingAI(npc)
            npc.hostile = True
            npc.spawn()
            self.kids_npcs.append(npc)

        # add weapons
        w = Weapon.factory('g17')
        w.rounds = w.maxrounds
        #w.reload()

        # inventory
        inv = self.char.inventory
        #inv.addItem(w)
        self.char.pickup_item(w)
        inv.listItems()
        
        # set item
        self.char.item = w
        #self.char.setTarget(npc)
